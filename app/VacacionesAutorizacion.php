<?php

namespace App;

use App\RRHH\VacacionesDeEmpleado;
use Illuminate\Database\Eloquent\Model;

class VacacionesAutorizacion extends Model
{
    public function vacaciones(){
    	return $this->belongsTo(VacacionesDeEmpleado::class, 'id_vacaciones_de_empleado');
    }

    public function director(){
    	return $this->belongsTo(User::class, 'id_director');
    }
}
