<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FupCancellationRequest extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        info($this->request['director']);
        return (new MailMessage)
            ->subject("Solicitud de cancelación de FUP")
            ->markdown('dbpi.mails.fup_cancellation_request', [
                'director' => $this->request['director'],
                'user'     => $this->request['user'],
                'fup'      => $this->request['fup'],
                'reasson'  => $this->request['reasson'],
                'link'     => $this->request['link']
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'link' => $this->request['link'],
            'msg'  => $this->request['msg'],
        ];
    }
}
