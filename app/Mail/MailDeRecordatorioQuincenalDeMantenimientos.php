<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDeRecordatorioQuincenalDeMantenimientos extends Mailable
{
    use Queueable, SerializesModels;

    public $mantenimientos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mantenimientos)
    {
        $this->mantenimientos = $mantenimientos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Servicios de mantenimiento en los próximos 15 días')
            ->markdown('dbpi.mails.mail_de_recordatorio_quincenal_de_mantenimientos')
            ->with([

                'mantenimientos' => $this->mantenimientos

            ]);
    }
}
