<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VacacionesAutorizadas extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Vacaciones autorizadas')
            ->markdown('dbpi.mails.vacaciones-autorizadas')
            ->with([
                'autorizacion' => $this->request['autorizacion'],
                'solicitud' => $this->request['solicitud'],
                'empleado' => $this->request['empleado'],
                'link' => $this->request['link']
            ]);
    }
}
