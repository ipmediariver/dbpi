<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShareFupToUser extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fup        = $this->request['fup'];
        $user       = $this->request['user'];
        $link       = $this->request['link'];
        $fup_number = $fup->number;
        info($link);
        return $this
            ->subject('Solicitud de autorización en FUP #' . $fup_number)
            ->markdown('dbpi.mails.share_fup_to_user')
            ->with([
                'user' => $user,
                'fup'  => $fup,
                'link' => $link,
            ]);
    }
}
