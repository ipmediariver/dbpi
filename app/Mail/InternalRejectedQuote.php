<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InternalRejectedQuote extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $validator = $this->request['validator'];
        $quote     = $this->request['quote'];
        $author    = $this->request['author'];
        $comment  = $this->request['comment'];

        return $this
            ->subject("Cotización rechazada por {$validator->full_name}")
            ->markdown('dbpi.mails.internal_rejected_quote')
            ->with([
                'validator' => $validator,
                'quote'     => $quote,
                'author'    => $author,
                'comment'  => $comment
            ]);
    }
}
