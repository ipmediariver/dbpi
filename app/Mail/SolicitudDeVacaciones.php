<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SolicitudDeVacaciones extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $link = $this->request['link'];

        return $this
            ->subject('Nueva solicitud de vacaciones')
            ->markdown('dbpi.mails.nueva-solicitud-de-vacaciones')
            ->with([
                'solicitud' => $this->request['solicitud'],
                'empleado'  => $this->request['empleado'],
                'link'      => $this->request['link']
            ]);
    }
}
