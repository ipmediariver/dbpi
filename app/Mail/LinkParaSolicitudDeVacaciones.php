<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LinkParaSolicitudDeVacaciones extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Link para solicitud de vacaciones')
            ->markdown('dbpi.mails.link-para-solicitud-de-vacaciones')
            ->with([
                'empleado' => $this->request['empleado'],
                'link' => $this->request['link']
            ]);
    }
}
