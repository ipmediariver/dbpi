<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDeRecordatorioMensualDeMantenimientos extends Mailable
{
    use Queueable, SerializesModels;

    public $mantenimientos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mantenimientos)
    {
        $this->mantenimientos = $mantenimientos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Servicios de mantenimiento en los próximos 30 días')
            ->markdown('dbpi.mails.mail_de_recordatorio_mensual_de_mantenimientos')
            ->with([
                'mantenimientos' => $this->mantenimientos
            ]);
    }
}
