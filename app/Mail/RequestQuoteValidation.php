<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestQuoteValidation extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $quote = $this->request['quote'];
        $user  = $this->request['user'];
        $link  = $this->request['link'];

        return $this
            ->subject("Autorizar cotización #{$quote->number}")
            ->markdown('dbpi.mails.request_quote_validation')
            ->with([
                'quote' => $quote,
                'user'  => $user,
                'link'  => $link,
            ]);
    }
}
