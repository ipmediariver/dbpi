<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDeRecordatorioSemanalDeMantenimientos extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mantenimientos)
    {
        $this->mantenimientos = $mantenimientos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Servicios de mantenimiento dentro de 1 semana')
            ->markdown('dbpi.mails.mail_de_recordatorio_semanal_de_mantenimientos')
            ->with([

                'mantenimientos' => $this->mantenimientos

            ]);
    }
}
