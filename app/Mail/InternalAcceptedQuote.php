<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InternalAcceptedQuote extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {   
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $validator = $this->request['validator'];
        $quote     = $this->request['quote'];
        $author    = $this->request['author'];
        $comment  = $this->request['comment'];

        return $this
            ->subject("Cotización autorizada por {$validator->full_name}")
            ->markdown('dbpi.mails.internal_accepted_quote')
            ->with([
                'validator' => $validator,
                'quote'     => $quote,
                'author'    => $author,
                'comment'  => $comment
            ]);
    }
}
