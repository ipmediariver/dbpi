<?php
namespace App\RRHH;
use Illuminate\Database\Eloquent\Model;
class JobTitle extends Model
{
    protected $guarded = [];
    public function employees(){
    	return $this->hasMany(Employee::class, 'jobtitle_id');
    }
    public function getHighSalaryAttribute(){
    	$employees = $this->employees()->orderBy('salary', 'desc')->get();
    	$salary = $employees->first()->salary;
    	$daily_salary = $employees->first()->salary / 30;
    	return number_format($daily_salary, '2', '.', '');
    }
}