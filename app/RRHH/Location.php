<?php
namespace App\RRHH;
use Illuminate\Database\Eloquent\Model;
class Location extends Model
{
    protected $guarded = [];
    public function employees(){
    	return $this->hasMany(Employee::class);
    }
}