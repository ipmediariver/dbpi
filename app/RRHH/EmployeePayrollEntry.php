<?php
namespace App\RRHH;
use Illuminate\Database\Eloquent\Model;
class EmployeePayrollEntry extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'date'];
    public function category(){
    	return $this->belongsTo(EmployeePayrollEntryCategory::class, 'employee_payroll_entry_category_id')
    		->withDefault(['name' => 'Sin Categoría']);
    }
}