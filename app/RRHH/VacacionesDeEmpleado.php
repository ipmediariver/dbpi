<?php

namespace App\RRHH;

use App\VacacionesAutorizacion;
use Illuminate\Database\Eloquent\Model;

class VacacionesDeEmpleado extends Model
{
    
	protected $guarded = [];

	public function empleado(){
		return $this->belongsTo(Employee::class, 'id_de_empleado');
	}

	public function getFechaEnQueDebePresentarseAttribute(){
		return $this->cuando_debe_presentarse ? $this->cuando_debe_presentarse : 'Sin definir';
	}

	public function autorizaciones(){
		return $this->hasMany(VacacionesAutorizacion::class, 'id_vacaciones_de_empleado');
	}

}
