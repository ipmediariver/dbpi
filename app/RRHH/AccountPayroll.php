<?php
namespace App\RRHH;
use App\CRM\Account;
use Illuminate\Database\Eloquent\Model;
class AccountPayroll extends Model
{
    protected $guarded = [];
    public function account(){
        return $this->belongsTo(Account::class, 'account_id', 'account_id');
    }
    public function employees(){
    	return $this->hasMany(Employee::class, 'account_id', 'account_id');
    }
    public function employee_payrolls(){
    	return $this->hasMany(EmployeePayroll::class, 'account_payroll_id');
    }
    public function date(){
    	return $this->belongsTo(Payroll::class, 'payroll_id');
    }
    public function getFortnightTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('fortnight_salary');
        return number_format($total, 2, '.', '');
    }
    public function getDiscountsTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('discounts_ammount');
        return number_format($total, 2, '.', '');
    }
    public function getSimpleDiscountsTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('simple_discounts');
        return number_format($total, 2, '.', '');
    }
    public function getDiscountsByDaysTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('discounts_by_days');
        return number_format($total, 2, '.', '');
    }
    public function getEntriesTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('entries_ammount');
        return number_format($total, 2, '.', '');
    }
    public function getFonacotTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('fonacot_discount');
        return number_format($total, 2, '.', '');
    }
    public function getInfonavitTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('infonavit_discount');
        return number_format($total, 2, '.', '');
    }
    public function getTotalAttribute(){
        $employees = $this->employee_payrolls;
        $total = $employees->sum('total_ammount');
        return number_format($total, 2, '.', '');
    }
    public function getOpenAttribute(){
        $status = $this->status_id;
        if($status == 0){
            return true;
        }
    }
}