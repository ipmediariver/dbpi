<?php
namespace App\RRHH;

use App\CRM\Account;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'avatar',
        'account_id',
        'location_id',
        'department_id',
        'jobtitle_id',
        'name_1',
        'name_2',
        'last_name_1',
        'last_name_2',
        'email',
        'mobile',
        'address',
        'rfc',
        'curp',
        'birthday',
        'currency_id',
        'salary',
        'status_id',
        'bank_id',
        'bank_account_num',
        'bank_card_num',
        'interbank_clabe',
        'fonacot_discount',
        'infonavit_discount',
        'active_at',
        'inactive_at',
        'nss',
        'imss_active_at',
        'imss_unactive_internal_at',
        'imss_unactive_external_at',
        'assimilated',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'birthday',
        'active_at',
        'inactive_at',
        'imss_active_at',
        'imss_unactive_internal_at',
        'imss_unactive_external_at',
    ];

    protected $appends = [
        'full_name', 
        'numero_de_empleado', 
        'anos_de_servicio', 
        'dias_correspondientes_de_vacaciones',
        'fecha_de_ingreso'
    ];

    public function payrolls()
    {
        return $this->hasMany(EmployeePayroll::class);
    }
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'account_id');
    }
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function attachments()
    {
        return $this->hasMany(EmployeeAttachment::class);
    }
    public function jobtitle()
    {
        return $this->belongsTo(JobTitle::class)->withDefault(['name' => 'Sin puesto laboral']);
    }
    public function status()
    {
        return $this->belongsTo(EmployeeStatus::class);
    }
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    public function location()
    {
        return $this->belongsTo(Location::class);
    }
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
    public function vacaciones(){
        return $this->hasMany(VacacionesDeEmpleado::class, 'id_de_empleado');
    }
    public function getVacacionesPendientesDeAutorizacionAttribute(){
        return $this->vacaciones->where('etapa', 0)->first();
    }
    public function getFonacotAttribute()
    {
        return number_format($this->fonacot_discount, 2, '.', '');
    }
    public function getInfonavitAttribute()
    {
        return number_format($this->infonavit_discount, 2, '.', '');
    }
    public function getFullNameAttribute()
    {
        return $this->name_1 . ' ' . $this->name_2 . ' ' . $this->last_name_1 . ' ' . $this->last_name_2;
    }
    public function getFullNameReverseAttribute()
    {
        return $this->last_name_1 . ' ' . $this->last_name_2 . ' ' . $this->name_1 . ' ' . $this->name_2;
    }
    public function getNamesAttribute()
    {
        return $this->name_1 . ' ' . $this->name_2;
    }
    public function getShortNameAttribute()
    {
        return $this->name_1 . ' ' . $this->last_name_1;
    }
    public function getMonthlySalaryAttribute()
    {
        return number_format($this->salary, 2, '.', '');
    }
    public function getNumeroDeEmpleadoAttribute()
    {
        return str_pad($this->id, 4, '0', STR_PAD_LEFT);
    }
    public function getAnosDeServicioAttribute()
    {
        $fecha_de_ingreso = $this->active_at;
        $hoy = Carbon::now();
        $anos_de_servicio = $fecha_de_ingreso->diffInYears($hoy);
        return $anos_de_servicio;
    }
    public function getFechaDeIngresoAttribute(){
        return $this->active_at->format('Y-m-d');
    }
    public function getDiasCorrespondientesDeVacacionesAttribute(){
        $anos = $this->anos_de_servicio;
        $dias = 0;
        if($anos == 1){
            $dias = 6;
        }elseif($anos == 2){
            $dias = 8;
        }elseif($anos == 3){
            $dias = 10;
        }elseif($anos == 4){
            $dias = 12;
        }elseif($anos >= 5 && $anos <= 9){
            $dias = 14;
        }elseif($anos >= 10 && $anos <= 14){
            $dias = 16;
        }elseif($anos >= 15 && $anos <= 19){
            $dias = 18;
        }elseif($anos >= 20 && $anos <= 24){
            $dias = 20;
        }elseif($anos >= 25 && $anos <= 29){
            $dias = 22;
        }
        return $dias;
    }
}
