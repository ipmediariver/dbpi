<?php
namespace App\RRHH;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EmployeePayroll extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function employee(){
        return $this->belongsTo(Employee::class);
    }
    public function discounts(){
        return $this->hasMany(EmployeePayrollDiscount::class);
    }
    public function entries(){
        return $this->hasMany(EmployeePayrollEntry::class);
    }
    public function bank(){
        return $this->belongsTo(Bank::class);
    }
    public function getFonacotAttribute(){
        return number_format($this->fonacot_discount, 2, '.', '');
    }
    public function getInfonavitAttribute(){
        return number_format($this->infonavit_discount, 2, '.', '');
    }
    public function getFortnightSalaryAttribute(){
        $salary = $this->salary;
        $fortnight = $salary / 2;
        return number_format($fortnight, 2, '.', '');
    }
    public function getDailySalaryAttribute(){
        $salary = $this->salary;
        $daily_salary = $salary / 30;
        return number_format($daily_salary, 2, '.', '');
    }
    public function getDiscountsAmmountAttribute(){
        $discounts = $this->discounts;
        $ammount = $discounts->sum('ammount');
        return number_format($ammount, 2, '.', '');
    }
    public function getSimpleDiscountsAttribute(){
        $discounts = $this->discounts()->whereNull('days')->get();
        $ammount = $discounts->sum('ammount');
        return number_format($ammount, 2, '.', '');   
    }
    public function getDiscountsByDaysAttribute(){
        $discounts = $this->discounts()->whereNotNull('days')->get();
        $ammount = $discounts->sum('ammount');
        return number_format($ammount, 2, '.', '');   
    }
    public function getEntriesAmmountAttribute(){
        $entries = $this->entries;
        $ammount = $entries->sum('ammount');
        return number_format($ammount, 2, '.', '');
    }
    public function getTotalAmmountAttribute(){
        $fortnight = $this->fortnight_salary;
        $discounts = $this->discounts_ammount;
        $entries = $this->entries_ammount;
        $fonacot = $this->fonacot;
        $infonavit = $this->infonavit;
        $ammount = ($fortnight + $entries) - ($discounts + $fonacot + $infonavit);
        return number_format($ammount, 2, '.', '');
    }
}