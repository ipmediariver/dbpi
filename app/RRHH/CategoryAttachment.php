<?php

namespace App\RRHH;

use Illuminate\Database\Eloquent\Model;

class CategoryAttachment extends Model
{
    protected $fillable = [
        'id',
        'description',
    ];
}
