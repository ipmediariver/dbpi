<?php
namespace App\RRHH;
use App\RRHH\UserDepartment;
use Illuminate\Database\Eloquent\Model;
class Department extends Model
{
    protected $guarded = [];
    public function employees(){
    	return $this->hasMany(Employee::class);
    }
    public function users(){
    	return $this->hasMany(UserDepartment::class);
    }
}