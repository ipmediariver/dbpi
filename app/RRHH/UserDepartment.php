<?php

namespace App\RRHH;

use App\RRHH\Department;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserDepartment extends Model
{
    protected $guarded = [];
    public function department(){
    	return $this->belongsTo(Department::class);
    }
    public function user(){
    	return $this->belongsTo(User::class);
    }
    public function getFullNameAttribute(){
        return $this->user->full_name;
    }
    public function getNameAttribute(){
    	return $this->department->name;
    }
    public function getIsSupervisorAttribute(){
    	if($this->supervisor){
    		return "<i class=\"fa fa-check text-success\"></i>";
    	}else{
    		return "<i class=\"fa fa-times text-danger\"></i>";
    	}
    }
}
