<?php
namespace App\RRHH;
use Illuminate\Database\Eloquent\Model;
class Bank extends Model
{
    protected $guarded = [];
    public function employees(){
    	return $this->hasMany(Employee::class);
    }
}