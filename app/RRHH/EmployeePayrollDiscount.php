<?php
namespace App\RRHH;
use Illuminate\Database\Eloquent\Model;
class EmployeePayrollDiscount extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'date'];
    public function category(){
    	return $this->belongsTo(EmployeePayrollDiscountCategory::class, 'employee_payroll_discounts_category_id')
    		->withDefault(['name' => 'Sin Categoría']);
    }
}