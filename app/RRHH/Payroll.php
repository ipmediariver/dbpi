<?php
namespace App\RRHH;
use Illuminate\Database\Eloquent\Model;
class Payroll extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'from', 'to'];
    public function accounts(){
    	return $this->hasMany(AccountPayroll::class);
    }
    public function getStatusBadgeAttribute(){
        switch ($this->status_id) {
            case 0:
                return '<span class="badge badge-pill badge-secondary">Abierta</span>';
                break;
            case 1:
                return '<span class="badge badge-pill badge-success">En proceso</span>';
                break;
            case 2:
                return '<span class="badge badge-pill badge-danger">Cerrada</span>';
                break;
        }
    }
    public function getStatusAttribute(){
    	switch ($this->status_id) {
    		case 0:
    			return 'Abierta';
    			break;
    		case 1:
    			return 'En proceso';
    			break;
    		case 2:
    			return 'Cerrada';
    			break;
    	}
    }
    public function getHasUnclosedAccountsAttribute(){
        $unclosed_account = $this->accounts()->where('status_id', 0)->first();
        return $unclosed_account ? true : false;
    }
}