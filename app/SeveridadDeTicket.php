<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeveridadDeTicket extends Model
{
    protected $connection = 'helpdesk';

    protected $table = 'severities';
}
