<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuoteAttachmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required|string',
            'description'      => 'nullable|string',
            'billing_number'   => 'nullable|string',
            'billing_import'   => 'nullable|string|numeric',
            'billing_currency' => 'nullable|numeric|exists:currencies,id',
        ];
    }

    public function messages()
    {
        return [

            'name.required'         => 'El nombre del archivo es requerido',
            'description.string'    => 'La descripción no es válida',
            'billing_number.string' => 'El número de la factura no es válido',
            'billing_import.numeric'        => 'El importe de la factura no es válido',
            'billing_currency.numeric'      => 'El tipo de moneda de la factura no es válido',
            'billing_currency.exists'      => 'El tipo de moneda de la factura no es válido',

        ];
    }

}
