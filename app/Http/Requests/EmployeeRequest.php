<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'salary' => 'sometimes|required|numeric',
            'fonacot_discount' => 'sometimes|numeric|nullable',
            'infonavit_discount' => 'sometimes|numeric|nullable',
            'bank_account_num' => 'sometimes|numeric|nullable|digits_between:3,20',
            'bank_card_num' => 'sometimes|numeric|nullable|digits_between:3,20',
            'interbank_clabe' => 'sometimes|numeric|nullable|digits_between:3,20',
            'nss' => 'sometimes|numeric|nullable|digits_between:3,15'
         
        ];
    }


    public function messages(){
        return [

            'salary.required' => 'El salario es requerido.',
            'salary.numeric' => 'El salario debe ser un numero sin caracteres especiales',
            'fonacot_discount.numeric' => 'El fonacot debe ser un numero sin caracteres especiales',
            'infonavit_discount.numeric' => 'El infonavit debe ser un numero sin caracteres especiales',
            'bank_account_num.numeric' => 'El numero de cuenta no admite letras o caracteres especiales',
            'bank_account_num.digits_between' => 'El numero de cuenta solo admite un maximo de 20 caracteres',
            'bank_card_num.numeric' => 'El numero de tarjeta no admite letras o caracteres especiales',
            'bank_card_num.digits_between' => 'El numero de tarjeta solo admite un maximo de 20 caracteres',
            'interbank_clabe.numeric' => 'La clave intervancara solo admite numeros sin caracteres especiales',
            'interbank_clabe.digits_between' => 'La clabe interbancaria solo admite un maximo de 20 caracteres',
            'nss.numeric' => 'El numero de seguro no admite letras o caracteres especiales',
            'nss.digits_between' => 'El NSS solo se permite un maximo de 15 caracteres'
            
        ];
    }

}