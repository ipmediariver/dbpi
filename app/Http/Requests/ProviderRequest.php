<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $provider = request('provider');
        return [
            'name' => [
                'required',
                'string',
                Rule::unique('providers')->ignore($provider->id)
            ]
        ];
    }
    public function messages(){
        return [
            'name.required' => 'El nombre del proveedor es requerido',
            'name.string' => 'El nombre del proveedor no es válido',
            'name.unique' => 'El nombre del proveedor ya se encuentra registrado'
        ];
    }
}
