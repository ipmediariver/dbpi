<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class PlantillaDeContratoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id_de_plantilla = request('plantilla') ? request('plantilla')->id : null;

        return [
            'nombre'                 => 'required|string',
            'descripcion'            => 'required|string',
            'codigo'                 => [
                'required',
                Rule::unique('plantilla_de_contratos')->ignore($id_de_plantilla)
            ],
            'id_contrato'            => 'required|exists:contratos,id',
            'id_cobertura'           => 'required|exists:coberturas,id',
            'id_tiempo_de_respuesta' => 'required|exists:tiempo_de_respuestas,id',
        ];
    }
    public function messages()
    {
        return [
            'nombre.required'                 => 'El nombre de la plantilla es requerido',
            'descripcion.required'            => 'La descripcion de la plantilla es requerida',
            'codigo.required'                 => 'El código de la plantilla es requerido',
            'codigo.unique'                   => 'El código proporcionado ya se encuentra en uso',
            'id_contrato.required'            => 'Por favor selecciona un contrato',
            'id_contrato.exists'              => 'El contrato seleccionado no es válido',
            'id_cobertura.required'           => 'Por favor selecciona una cobertura',
            'id_cobertura.exists'             => 'La cobertura seleccionad no es válida',
            'id_tiempo_de_respuesta.required' => 'Por favor selecciona un tiempo de respuesta',
            'id_tiempo_de_respuesta.exists'   => 'El tiempo de respuesta seleccionado no es válido',
        ];
    }
}