<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeAttachmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attachments.*' => 'file|mimes:jpeg,jpg,pdf,png|max:8000',
        ];
    }
    public function messages()
    {
        return [
            'attachments.*.file'  => 'Un archivo adjunto no es válido',
            'attachments.*.mimes' => 'Solo puedes adjuntar archivos de tipo PDF, JPG, JPEG ó PNG',
            'attachments.*.max'   => 'Los archivos deben pesar menos de 8MB',
        ];
    }
}
