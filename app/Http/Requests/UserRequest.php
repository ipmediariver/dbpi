<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = request()->user ? request()->user : null;
        return [
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => [
                'required', 
                'email',
                Rule::unique('users')->ignore($user)
            ],
            'username'   => [
                'required',
                'string',
                'min:5',
                'max:15',
                Rule::unique('users')->ignore($user)
            ],
            'role'       => 'required|in:admin,user',
            'password' => 'nullable|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'first_name.required' => 'El nombre del usuario es requerido',
            'last_name.required'  => 'El apellido del usuario es requerido',
            'email.required'      => 'El correo electrónico del usuario es requerido',
            'email.email'         => 'El correo electrónico proporcionado no es válido',
            'email.unique'        => 'El correo electrónico proporcionado ya se encuentra registrado',
            'username.required'   => 'El nombre de usuario es requerido',
            'username.min'        => 'El nombre de usuario debe contener al menos 5 caracteres',
            'username.max'        => 'El nombre de usuario debe contener máximo 15 caracteres',
            'username.unique'     => 'El nombre de usuario ya se encuentra registrado',
            'role.requried'       => 'El tipo de usuario es requerido',
            'role.in'             => 'El tipo de usuario no es válido',
            'password.confirmed' => 'La confirmación de contraseña no coincide'
        ];
    }
}
