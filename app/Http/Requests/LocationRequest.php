<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:locations',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'El nombre de la ubicación es requerido',
            'name.string'   => 'El nombre de la ubicación no es válido',
            'name.unique'   => 'El nombre de la ubicación ya se encuentra registrado',
        ];
    }
}