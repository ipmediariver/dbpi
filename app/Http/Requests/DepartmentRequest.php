<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:departments',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'El nombre del departamento es requerido',
            'name.string'   => 'El nombre del departamento no es válido',
            'name.unique'   => 'El nombre del departamento ya se encuentra registrado',
        ];
    }
}
