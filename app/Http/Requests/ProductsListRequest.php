<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductsListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|file|mimes:csv,xls,xlsx|max:8000',
        ];
    }

    public function messages()
    {
        return [
            'file.required' => 'El documento es requerido',
            'file.file'     => 'El documento proporcionado no es válido',
            'file.mimes'    => 'El documento proporcionado no es válido',
            'file.max'      => 'El documento debe pesar menos de 8MB',
        ];
    }
}
