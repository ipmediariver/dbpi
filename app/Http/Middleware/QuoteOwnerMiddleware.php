<?php

namespace App\Http\Middleware;

use Closure;

class QuoteOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();

        $quote_id = $request->quote->id;

        $user_quotes_ids = $user->owner_and_shared_quotes->pluck('id')->toArray();

        $is_owner_or_participant = in_array($quote_id, $user_quotes_ids);
        
        if($user->role == 'admin'){
            return $next($request);
        }elseif($is_owner_or_participant){
            return $next($request);
        }else{
            return redirect()->back()->withErrors([
                'No tienes permiso para ingresar al área solicitada'
            ]);
        }
    }
}
