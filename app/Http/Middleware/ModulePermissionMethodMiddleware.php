<?php
namespace App\Http\Middleware;
use App\SYSTEM\Module;
use Closure;
class ModulePermissionMethodMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module_code)
    {
        $user = auth()->user();
        if($user->role == 'admin'){
            return $next($request);
        }else{
            $module = $user->modules->where('code', $module_code)->first();
            $permission = $user->permissions()->where('module_id', $module->id)->first();
            if($request->method() == 'GET'){
                if($permission->read){
                    return $next($request);      
                }else{
                    return redirect()->back()->withErrors([
                        'No tienes permisos para visualizar este módulo.'
                    ]);
                }
            }elseif($request->method() == 'POST' || $request->method() == 'PATCH' || $request->method() == 'PUT'){
                if($permission->edit){
                    return $next($request);      
                }else{
                    return redirect()->back()->withErrors([
                        'No tienes permisos para editar la información.'
                    ]);
                }
            }elseif($request->method() == 'DELETE'){
                if($permission->delete){
                    return $next($request);      
                }else{
                    return redirect()->back()->withErrors([
                        'No tienes permisos para eliminar la información.'
                    ]);
                }
            }
        }

    }
}