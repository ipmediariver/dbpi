<?php

namespace App\Http\Middleware;

use Closure;

class ModifyQuoteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $quote = request('quote');
        $status = $quote->status_id;

        if($status >= 3){
            if(request()->ajax()){
                $response = [
                    'errors' => ["La cotización no puede ser modificada"]
                ];
                return response()->json($response, 401);
            }else{
                return redirect()->back()->withErrors([
                    'La cotización no puede ser modificada'
                ]);
            }
        }else{
            return $next($request);
        }

    }
}
