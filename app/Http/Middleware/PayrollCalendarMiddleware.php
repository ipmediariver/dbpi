<?php
namespace App\Http\Middleware;
use App\RRHH\Payroll;
use Closure;
class PayrollCalendarMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $payrolls = Payroll::whereIn('status_id', [0,1])->get();

        if($payrolls->count()){
            return $next($request);
        }else{
            return redirect()->route('fechas.index')->with('info', 'Por favor crea un calendario de nóminas para continuar.');
        }
    }
}