<?php
namespace App\Http\Middleware;
use Closure;
class ModulePermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$modules)
    {
        $user = auth()->user();
        if($user->role == 'admin'){
            return $next($request);
        }else{
            foreach ($modules as $key => $module) {
                $user_module = $user->modules->where('code', $module)->first();
                if($user_module){
                    return $next($request);
                }else{
                    return redirect()->back()->withErrors([
                        'No tienes permisos para ingresar a esta área.'
                    ]);
                }
            }
        }
    }
}