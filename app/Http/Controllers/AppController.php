<?php
namespace App\Http\Controllers;
use App\SYSTEM\Module;
use Illuminate\Http\Request;
class AppController extends Controller
{
    public function index()
    {
    	$user = auth()->user();
    	$modules = null;
    	if($user->role == 'admin'){
    		$modules = Module::all();
    	}else{
    		$modules = $user->modules;
    	}
        return view('dbpi.apps.index', compact('modules'));
    }


    public function mark_as_read($notification){

        $user = auth()->user();
        $notification = $user->notifications->find($notification);

        $notification->markAsRead();

        $link = $this->format_notification_link($notification->data['link']);

        return redirect()->to($link);

    }

    public function format_notification_link($link){
        return str_replace('godynacom.com', 'dynacom.services', $link);
    }

}