<?php
namespace App\Http\Controllers\RRHH;
use App\Http\Controllers\Controller;
use App\Http\Requests\LocationRequest;
use App\RRHH\Location;
use Illuminate\Http\Request;
class LocationController extends Controller
{
    public function index(){
    	$locations = Location::all();
    	return view('dbpi.rrhh.config.locations.index', compact('locations'));
    }

    public function store(LocationRequest $request){
    	$location = Location::create($request->only('name'));
    	return redirect()->back()->with('success', "Se ha creado la ubicación {$location->name}");
    }

    public function show(Location $location){
        return redirect()->route('ubicaciones.edit', $location);
    }

    public function edit(Location $location){
    	return view('dbpi.rrhh.config.locations.edit', compact('location'));
    }

    public function update(LocationRequest $request, Location $location){
    	$location->update($request->only('name'));
    	return redirect()->back()->with('success', "La ubicación ha sido actualizada");
    }
    public function destroy(Location $location){
    	$location->delete();
    	return redirect()->route('ubicaciones.index')->with('info', "La ubicación ha sido eliminada");	
    }
}