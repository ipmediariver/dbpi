<?php
namespace App\Http\Controllers\RRHH;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeAttachmentRequest;
use App\RRHH\Employee;
use App\RRHH\EmployeeAttachment;
use App\RRHH\CategoryAttachment;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class EmployeeAttachmentController extends Controller
{
    public function index(Employee $employee)
    {
        $categories = CategoryAttachment::orderBy('description', 'asc')->get();
        return view('dbpi.rrhh.employees.forms.attachments', compact('employee','categories'));
    }

    public function store(EmployeeAttachmentRequest $request, Employee $employee)
    {
        $attachments = $request->file('attachments');
        $category_attachment_id = $request->category_attachment_id;

        foreach ($attachments as $attachment) {
            $name      = $attachment->getClientOriginalName();
            $file_name = pathinfo($name,PATHINFO_FILENAME);
            $extension = $attachment->extension();
            $id        = uniqid();
            $pathname  = Str::slug($file_name . ' ' . $id) . '.' . $extension;
            $attachment->storeAs('/attachments', $pathname, 'uploads');
            $employee_attachment = EmployeeAttachment::create([
                'employee_id' => $employee->id,
                'pathname'    => $pathname,
                'name' => $name,
                'ext' => $extension,
                'category_attachment_id' => $category_attachment_id,
            ]);
        }
        return redirect()->back()->with('success', "Se adjuntaron los archivos del empleado exitosamente");
    }
    Public function store_category_attachment(Request $request){
        $date= $request->all();
        $validar  = $this->validar_datos_category_attachment($request);
        CategoryAttachment::create($date);
        $categories = CategoryAttachment::get();
        return redirect()->back()->with('success', 'Categoria creada correctamente.');
    }
    public function edit_category_attachment(Request $request, $id){
        $date= $request->all();
        $validar  = $this->validar_datos_category_attachment($request);
        $category = CategoryAttachment::find($id);
        $category->update($date);

        $categories = CategoryAttachment::get();
        return redirect()->back()->with('success', 'Ha modificado la categoria correctamente.,');
    }
    public function update_category_attachment(Request $request, $id){
        $date= $request->all();
        $employee_attachment = EmployeeAttachment::find($id);
        $employee_attachment->update($date);
        
        return redirect()->back()->with('success', 'La categoria del archivo se ha modificado correctamente.,');
    }
    public function validar_datos_category_attachment($request){
        $reglas_de_validacion = [
            'description' => 'required|string|unique:category_attachments,description'
        ];
        $mensajes_de_validacion = [
            'description.required'  => 'Es necesario introducir el nombre para poder crear la categoria.',
            'description.string'     => 'La categoria proporcionada no es válida.',
            'description.unique'    => 'La categoria proporcionada ya se encuentra registrada.',
        ];
        $validar = Validator::make($request->all(), $reglas_de_validacion, $mensajes_de_validacion)->validate();
    }
    public function destroy(Employee $employee, EmployeeAttachment $attachment){
        $attachment->delete();
        return redirect()->back()->with('success', 'El archivo adjunto ha sido eliminado.');
    }
}
