<?php
namespace App\Http\Controllers\RRHH;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentRequest;
use App\RRHH\Department;
use Illuminate\Http\Request;
class DepartmentController extends Controller
{

    public function index(){
    	$departments = Department::orderBy('name')->get();
        if(request()->ajax()){
            return response()->json($departments);
        }else{
    	   return view('dbpi.rrhh.config.departments.index', compact('departments'));
        }
    }

    public function store(DepartmentRequest $request){
    	$department = Department::create($request->only('name'));
    	return redirect()->back()->with('success', "Se ha creado el departamento {$department->name}");
    }
    public function show(Department $department){
        return redirect()->route('departamentos.edit', $department);
    }
    public function edit(Department $department){
    	return view('dbpi.rrhh.config.departments.edit', compact('department'));
    }
    public function update(DepartmentRequest $request, Department $department){
        $department->update($request->only('name'));
        return redirect()->back()->with('success', 'El departamento ha sido actualizado');
    }
    public function destroy(Department $department){
        $name = $department->name;
        $department->delete();
        return redirect()->route('departamentos.index')->with('info', "Se eliminó el departamento {$name}");
    }
}