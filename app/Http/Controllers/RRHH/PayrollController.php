<?php
namespace App\Http\Controllers\RRHH;

use App\CRM\Account;
use App\Exports\PayrollExport;
use App\Http\Controllers\Controller;
use App\RRHH\AccountPayroll;
use App\RRHH\Employee;
use App\RRHH\EmployeePayroll;
use App\RRHH\EmployeePayrollDiscount;
use App\RRHH\EmployeePayrollDiscountCategory;
use App\RRHH\EmployeePayrollEntry;
use App\RRHH\EmployeePayrollEntryCategory;
use App\RRHH\EmployeeStatus;
use App\RRHH\Payroll;
use App\SYSTEM\MinimunSalary;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PayrollController extends Controller
{
    public function __construct()
    {
        $this->middleware('payroll_calendar')->except('index', 'create_calendar');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $year     = $request->ano ? Carbon::now()->year($request->ano) : Carbon::now();
        $payrolls = Payroll::whereYear('from', $year)->get();
        return view('dbpi.rrhh.payrolls.index', compact('payrolls', 'year'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Payroll $payroll)
    {
        $accounts = $payroll->accounts;
        return view('dbpi.rrhh.payrolls.payroll', compact('accounts', 'payroll'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function create_calendar(Request $request)
    {
        $year                            = Carbon::now();
        $find_unclosed_accounts_payrolls = $this->find_unclosed_accounts_payrolls();
        if ($find_unclosed_accounts_payrolls) {
            return redirect()
                ->back()
                ->withErrors(['Por favor verifica que todas las fechas de nómina de todas las empresas esten cerradas antes de crear un nuevo calendario.']);
        } else {
            $this->create_payrolls_calendar($year);
            $this->activate_next_payroll();
            return redirect()->back()->with('success', "Se ha creado el calendario de nóminas del año {$year->format('Y')}");
        }
    }
    public function next_date()
    {
        $unclosed_account_payroll = AccountPayroll::where('status_id', 0)->first();
        if (!$unclosed_account_payroll) {
            $this->activate_next_payroll();
            return redirect()->route('current-payroll')->with('success', "Se ha activado la siguiente nómina exitosamente.");
        } else {
            return redirect()->back()->withErrors(['Por favor cierra todas las empresas antes de avanzar a la siguiente nómina.']);
        }
    }
    public function activate_next_payroll()
    {
        // Se cierra la fecha actual
        $current_payroll = Payroll::where('status_id', 1)->first();
        if ($current_payroll) {
            $current_payroll->update(['status_id' => 2]);
        }
        // Se activa la fecha siguiente
        $next_payroll = Payroll::orderBy('from')->where('status_id', 0)->first();
        $this->set_accounts_payroll($next_payroll);
        $next_payroll->status_id = 1;
        $next_payroll->save();
    }
    public function set_accounts_payroll($payroll)
    {
        $accouts = Account::internal();
        foreach ($accouts as $key => $account) {
            $account_payroll = AccountPayroll::create([
                'payroll_id' => $payroll->id,
                'account_id' => $account->account_id,
                'name'       => $account->name,
            ]);
            $this->set_account_employees_payroll($account_payroll);
        }
    }
    public function set_account_employees_payroll($account_payroll)
    {
        $employee_statuses = EmployeeStatus::where('code', '<>', 'es4')
            ->where('code', '<>', 'es5')
            ->pluck('id');
        $employees = $account_payroll->employees()
            ->whereIn('status_id', $employee_statuses)
            ->get();
        foreach ($employees as $key => $employee) {
            $employee_payroll = EmployeePayroll::create([
                'employee_id'        => $employee->id,
                'account_payroll_id' => $account_payroll->id,
                'currency_id'        => $employee->currency_id,
                'salary'             => $employee->salary,
                'bank_id'            => $employee->bank_id,
                'bank_account_num'   => $employee->bank_account_num,
                'bank_card_num'      => $employee->bank_card_num,
                'interbank_clabe'    => $employee->interbank_clabe,
                'fonacot_discount'   => $employee->fonacot_discount,
                'infonavit_discount' => $employee->infonavit_discount,
            ]);
        }
    }
    public function find_unclosed_accounts_payrolls()
    {
        $unclosed_payrolls = AccountPayroll::where('status_id', 0)->get();
        return $unclosed_payrolls->count() ? true : false;
    }
    public function create_payrolls_calendar($year)
    {
        for ($i = 1; $i <= 12; $i++) {
            $fortnight_1 = [
                'from' => Carbon::parse($year)->month($i)->day(1),
                'to'   => Carbon::parse($year)->month($i)->day(15),
            ];
            $fortnight_2 = [
                'from' => Carbon::parse($year)->month($i)->day(16),
                'to'   => Carbon::parse($year)->month($i)->endOfMonth(),
            ];
            Payroll::create($fortnight_1);
            Payroll::create($fortnight_2);
        }
    }
    public function get_current_payroll()
    {
        $payroll  = Payroll::where('status_id', 1)->first();
        $accounts = $payroll->accounts;
        return view('dbpi.rrhh.payrolls.current', compact('accounts', 'payroll'));
    }
    public function get_payroll(AccountPayroll $account_payroll)
    {
        $discount_categories = EmployeePayrollDiscountCategory::where('code', '<>', 'd-01')->get();
        $entry_categories    = EmployeePayrollEntryCategory::all();
        return view('dbpi.rrhh.payrolls.show', compact('account_payroll', 'discount_categories', 'entry_categories'));
    }
    public function close_payroll(AccountPayroll $account_payroll)
    {
        $account_payroll->update([
            'status_id' => 1,
        ]);
        return redirect()->back()->with('success', "La nómina ha sido cerrada exitosamente.");
    }
    public function open_payroll(AccountPayroll $account_payroll){
        $account_payroll->update([
            'status_id' => 0,
        ]);
        return redirect()->back()->with('success', "La nómina ha sido desbloqueada exitosamente.");
    }
    public function new_incidence(Request $request)
    {
        $date = Carbon::now()
            ->day($request->date_day)
            ->month($request->date_month)
            ->year($request->date_year);

        $employee_payroll                   = EmployeePayroll::find($request->employee_id);
        $prima                              = $employee_payroll->employee->account->prima;
        $employee_payroll_discount_category = EmployeePayrollDiscountCategory::where('code', 'd-01')->first();
        $ammount                            = $employee_payroll->daily_salary * $prima;
        $discount                           = EmployeePayrollDiscount::create([
            'employee_payroll_id'                    => $employee_payroll->id,
            'employee_payroll_discounts_category_id' => $employee_payroll_discount_category->id,
            'subject'                                => $request->subject,
            'ammount'                                => number_format($ammount, 2, '.', ''),
            'date'                                   => $date,
        ]);
        return redirect()
            ->back()
            ->with("success", "Se ha agregado una falta a {$employee_payroll->employee->short_name}");
    }
    public function new_discount(Request $request)
    {
        $date                               = Carbon::now()->day($request->date_day)->month($request->date_month)->year($request->date_year);
        $employee_payroll                   = EmployeePayroll::find($request->employee_id);
        $employee_payroll_discount_category = EmployeePayrollDiscountCategory::find($request->employee_payroll_discounts_category_id);
        $discount                           = EmployeePayrollDiscount::create([
            'employee_payroll_id'                    => $employee_payroll->id,
            'employee_payroll_discounts_category_id' => $employee_payroll_discount_category->id,
            'subject'                                => $request->subject,
            'ammount'                                => $request->ammount,
            'date'                                   => $date,
        ]);
        return redirect()
            ->back()
            ->with("success", "Se ha agregado un descuento a {$employee_payroll->employee->short_name}");
    }
    public function new_discount_per_days(Request $request)
    {
        // Detectamos que tipo de descuento es si es: Cantidad, Porcentaje o Salario Minimo
        $employee_payroll = EmployeePayroll::find($request->employee_id);
        $type             = $request->discountType;
        $days             = $request->has('days') ? (int) $request->days : 15;
        $date             = Carbon::now();
        $category         = EmployeePayrollDiscountCategory::where('code', 'd-06')->first();
        $subject          = $request->subject;
        if ($type == 'ammount') {
            $ammount_per_day = $request->ammount;
            $ammount_per_day = number_format($ammount_per_day, 2, '.', '');
            $ammount         = $ammount_per_day * $days;
            $ammount         = number_format($ammount, 2, '.', '');

            $discount = EmployeePayrollDiscount::create([
                'employee_payroll_id'                    => $employee_payroll->id,
                'employee_payroll_discounts_category_id' => $category->id,
                'subject'                                => $subject,
                'ammount'                                => $ammount,
                'date'                                   => $date,
                'days'                                   => $days,
                'ammount_per_day'                        => $ammount_per_day,
            ]);
        }elseif($type == 'percent'){
            $percent = (100 - $request->percent) / 100;
            $daily_salary = $employee_payroll->daily_salary;
            $salary_per_days = $employee_payroll->daily_salary * $days;
            $ammount = $salary_per_days * $percent;

            $discount = EmployeePayrollDiscount::create([
                'employee_payroll_id'                    => $employee_payroll->id,
                'employee_payroll_discounts_category_id' => $category->id,
                'subject'                                => $subject,
                'ammount'                                => number_format($ammount, 2, '.', ''),
                'date'                                   => $date,
                'days'                                   => $days,
                'percent_discount'                       => $request->percent,
            ]);
        }elseif($type == 'salary'){
            $minimum_salary = MinimunSalary::first();
            $minimum_salary = $minimum_salary->value;
            $daily_salary = $employee_payroll->daily_salary;
            $discount_minimum_salary_to_daily_salary = $daily_salary - $minimum_salary;
            $ammount = $discount_minimum_salary_to_daily_salary * $days;
            $discount = EmployeePayrollDiscount::create([
                'employee_payroll_id'                    => $employee_payroll->id,
                'employee_payroll_discounts_category_id' => $category->id,
                'subject'                                => $subject,
                'ammount'                                => number_format($ammount, 2, '.', ''),
                'date'                                   => $date,
                'days'                                   => $days
            ]);
        }
        return redirect()
            ->back()
            ->with('success', "Se ha aplicado el descuento a {$employee_payroll->employee->short_name} exitosamente.");
    }
    public function new_entry_per_days(Request $request){
        // Detectamos que tipo de ingreso es si es: Cantidad, Porcentaje o Salario Minimo
        $employee_payroll = EmployeePayroll::find($request->employee_id);
        $type             = $request->entryType;
        $days             = $request->has('days') ? (int) $request->days : 15;
        $date             = Carbon::now();
        $category         = EmployeePayrollEntryCategory::where('code', 'e-07')->first();
        $subject          = $request->subject;
        if ($type == 'ammount') {
            $ammount_per_day = $request->ammount;
            $ammount_per_day = number_format($ammount_per_day, 2, '.', '');
            $ammount         = $ammount_per_day * $days;
            $ammount         = number_format($ammount, 2, '.', '');

            $discount = EmployeePayrollEntry::create([
                'employee_payroll_id'                    => $employee_payroll->id,
                'employee_payroll_entry_category_id' => $category->id,
                'subject'                                => $subject,
                'ammount'                                => $ammount,
                'date'                                   => $date,
                'days'                                   => $days,
                'ammount_per_day'                        => $ammount_per_day,
            ]);
        }elseif($type == 'percent'){
            $percent = $request->percent / 100;
            $daily_salary = $employee_payroll->daily_salary;
            $salary_per_days = $employee_payroll->daily_salary * $days;
            $ammount = $salary_per_days * $percent;

            $discount = EmployeePayrollEntry::create([
                'employee_payroll_id'                    => $employee_payroll->id,
                'employee_payroll_entry_category_id' => $category->id,
                'subject'                                => $subject,
                'ammount'                                => number_format($ammount, 2, '.', ''),
                'date'                                   => $date,
                'days'                                   => $days,
                'percent_entry'                       => $request->percent,
            ]);
        }elseif($type == 'salary'){

            ## ESTE CODIGO ESTA POR VERIFICARSE

            // $minimum_salary = MinimunSalary::first();
            // $minimum_salary = $minimum_salary->value;
            // $daily_salary = $employee_payroll->daily_salary;
            // $discount_minimum_salary_to_daily_salary = $daily_salary - $minimum_salary;
            // $ammount = $discount_minimum_salary_to_daily_salary * $days;
            // $discount = EmployeePayrollDiscount::create([
            //     'employee_payroll_id'                    => $employee_payroll->id,
            //     'employee_payroll_discounts_category_id' => $category->id,
            //     'subject'                                => $subject,
            //     'ammount'                                => number_format($ammount, 2, '.', ''),
            //     'date'                                   => $date,
            //     'days'                                   => $days
            // ]);

            ## ESTE CODIGO ESTA POR VERIFICARSE
        }
        return redirect()
            ->back()
            ->with('success', "Se ha aplicado el ingreso a {$employee_payroll->employee->short_name} exitosamente.");
    }
    public function delete_employee_payroll_discount(EmployeePayrollDiscount $discount)
    {
        $discount->delete();
        return redirect()->back()->with("success", "Se ha eliminado el descuento seleccionado.");
    }
    public function delete_employee_payroll_entry(EmployeePayrollEntry $entry)
    {
        $entry->delete();
        return redirect()->back()->with("success", "Se ha eliminado el ingreso seleccionado.");
    }
    public function new_entry(Request $request)
    {
        $date                            = Carbon::now()->day($request->date_day)->month($request->date_month)->year($request->date_year);
        $employee_payroll                = EmployeePayroll::find($request->employee_id);
        $employee_payroll_entry_category = EmployeePayrollEntryCategory::find($request->employee_payroll_entry_category_id);
        $entry                           = EmployeePayrollEntry::create([
            'employee_payroll_id'                => $employee_payroll->id,
            'employee_payroll_entry_category_id' => $employee_payroll_entry_category->id,
            'subject'                            => $request->subject,
            'ammount'                            => $request->ammount,
            'date'                               => $date,
        ]);
        return redirect()
            ->back()
            ->with("success", "Se ha agregado un ingreso a {$employee_payroll->employee->short_name}");
    }
    public function remove_employee_payroll(Request $request)
    {
        $employee_payroll_id = $request->employee_payroll_id;
        $employee_payroll    = EmployeePayroll::find($employee_payroll_id);
        $name                = $employee_payroll->employee->short_name;
        $employee_payroll->discounts()->delete();
        $employee_payroll->entries()->delete();
        $employee_payroll->delete();
        return redirect()->back()->with("success", "Se ha retirado a {$name} de la nómina actual.");
    }
    public function get_employee_payroll_discounts(AccountPayroll $account_payroll, EmployeePayroll $employee_payroll)
    {
        return view('dbpi.rrhh.payrolls.employee-discounts', compact('account_payroll', 'employee_payroll'));
    }
    public function get_employee_payroll_entries(AccountPayroll $account_payroll, EmployeePayroll $employee_payroll)
    {
        return view('dbpi.rrhh.payrolls.employee-entries', compact('account_payroll', 'employee_payroll'));
    }
    public function download_payroll(AccountPayroll $account_payroll)
    {
        $account  = $account_payroll->name;
        $from     = $account_payroll->date->from->format('d-m-Y');
        $to       = $account_payroll->date->to->format('d-m-Y');
        $filename = Str::slug($account . ' ' . $from . ' ' . $to);
        $filename = strtoupper($filename);
        return Excel::download(new PayrollExport($account_payroll), $filename . '.xlsx');
    }
    public function update_employees_from_account_payroll(AccountPayroll $account_payroll)
    {
        $update          = $this->add_new_employees_to_payroll($account_payroll);
        $down_employees  = $this->remove_down_employees($account_payroll);
        $reset_discounts = $this->reset_employee_discounts($account_payroll);
        if ($update) {
            return redirect()
                ->back()
                ->with('success', "Se ha actualizado la lista de empleados de {$account_payroll->name}");
        } else {
            return redirect()
                ->back()
                ->with('info', "La nómina está actualizada");
        }
    }
    public function add_new_employees_to_payroll($account_payroll)
    {
        $employee_statuses = EmployeeStatus::where('code', '<>', 'es4')
            ->where('code', '<>', 'es5')
            ->pluck('id');
        $employees_list = $account_payroll->employees()
            ->whereIn('status_id', $employee_statuses)
            ->pluck('id')
            ->toArray();
        $employee_payrolls = $account_payroll->employee_payrolls->pluck('employee_id')->toArray();
        $new_employees_id  = array_diff($employees_list, $employee_payrolls);
        $new_employees     = Employee::whereIn('id', $new_employees_id)->get();
        if (count($new_employees)) {
            foreach ($new_employees as $key => $employee) {
                $employee_payroll = EmployeePayroll::create([
                    'employee_id'        => $employee->id,
                    'account_payroll_id' => $account_payroll->id,
                    'currency_id'        => $employee->currency_id,
                    'salary'             => $employee->salary,
                    'bank_id'            => $employee->bank_id,
                    'bank_account_num'   => $employee->bank_account_num,
                    'bank_card_num'      => $employee->bank_card_num,
                    'interbank_clabe'    => $employee->interbank_clabe,
                    'fonacot_discount'   => $employee->fonacot_discount,
                    'infonavit_discount' => $employee->infonavit_discount,
                ]);


                $payroll_from = $account_payroll->date->from->startOfday();

                $employee_active_at = $employee->active_at->startOfday();

                $diff_days = Carbon::parse($payroll_from)->diffInDays($employee_active_at);


                if($diff_days > 0){

                    $discount_category = EmployeePayrollDiscountCategory::where('code', 'd-06')->first();

                    $discount_import = ($employee_payroll->daily_salary * $diff_days);

                    $discount = new EmployeePayrollDiscount();
                    $discount->employee_payroll_id = $employee_payroll->id;
                    $discount->employee_payroll_discounts_category_id = $discount_category->id;
                    $discount->subject = "Descuento de {$diff_days} días por ingreso posterior a la quincena.";
                    $discount->ammount = $discount_import;
                    $discount->date = Carbon::now();
                    $discount->save();

                }

            }
            return true;
        } else {
            return false;
        }
    }
    public function remove_down_employees($account_payroll)
    {
        $employee_payrolls = $account_payroll->employee_payrolls;
        foreach ($employee_payrolls as $employee_payroll) {
            $code = $employee_payroll->employee->status->code;
            if ($code == 'es4' || $code == 'es5') {
                $employee_payroll->discounts()->delete();
                $employee_payroll->entries()->delete();
                $employee_payroll->delete();
            }
        }
    }
    public function reset_employee_discounts($account_payroll)
    {
        $employee_payrolls = $account_payroll->employee_payrolls;
        foreach ($employee_payrolls as $employee_payroll) {
            $employee = $employee_payroll->employee;
            $employee_payroll->update([
                'bank_id' => $employee->bank_id,
                'fonacot_discount'   => $employee->fonacot_discount,
                'infonavit_discount' => $employee->infonavit_discount,
            ]);
        }
    }
}
