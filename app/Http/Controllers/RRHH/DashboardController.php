<?php
namespace App\Http\Controllers\RRHH;
use App\Http\Controllers\Controller;
use App\RRHH\Payroll;
use Illuminate\Http\Request;
class DashboardController extends Controller
{
    public function index(){
    	$payroll  = Payroll::where('status_id', 1)->first();
    	return view('dbpi.rrhh.index', compact('payroll'));
    }
}