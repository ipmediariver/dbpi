<?php
namespace App\Http\Controllers\RRHH;

use App\CRM\Account;
use App\Email;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\RRHH\Bank;
use App\RRHH\Currency;
use App\RRHH\Department;
use App\RRHH\Employee;
use App\RRHH\EmployeePayrollDiscountCategory;
use App\RRHH\EmployeeStatus;
use App\RRHH\JobTitle;
use App\RRHH\Location;
use App\RRHH\Payroll;
use App\RRHH\VacacionesDeEmpleado;
use App\User;
use App\VacacionesAutorizacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::orderBy('name_1')->paginate(10, ['*'], 'pag');
        return view('dbpi.rrhh.employees.index', compact('employees'));
    }
    public function accounts()
    {
        $accounts = Account::internal();
        return view('dbpi.rrhh.accounts.index', compact('accounts'));
    }
    public function account($account)
    {
        $account   = Account::where('account_id', $account)->first();
        $employees = $account->employees()->orderBy('last_name_1')->paginate(10);
        return view('dbpi.rrhh.accounts.show', compact('account', 'employees'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations   = Location::orderBy('name')->get();
        $departments = Department::orderBy('name')->get();
        $accounts    = Account::internal();
        $jobtitles   = JobTitle::orderBy('name')->get();
        $banks       = Bank::all();
        $currencies  = Currency::all();
        return view('dbpi.rrhh.employees.create', compact(
            'locations',
            'departments',
            'accounts',
            'jobtitles',
            'banks',
            'currencies'
        ));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $avatar   = $request->file('avatar');
        $request  = $this->format_timestamp_inputs($request);
        $request  = $this->encrypt_delicated_inputs($request);
        $request  = $this->set_jobtitle($request);
        $request  = $this->set_status_id($request);
        $employee = Employee::create($request->except('avatar'));
        $avatar   = $this->set_employee_avatar($avatar, $employee);
        return redirect()
            ->route('empleados.show', $employee)
            ->with('success', "Se dio de alta a {$employee->short_name} exitosamente.");
    }
    public function set_employee_avatar($avatar, $employee)
    {
        if ($avatar) {
            $id        = uniqid();
            $extension = $avatar->extension();
            $pathname  = $id . '.' . $extension;
            $avatar->storeAs('/avatars', $pathname, 'uploads');
            $employee->avatar = $pathname;
            $employee->save();
        }
    }
    public function set_status_id($request)
    {
        $employee_status      = EmployeeStatus::where('code', 'es1')->first();
        $request['status_id'] = $employee_status->id;
        return $request;
    }
    public function set_jobtitle($request)
    {
        if ($request->new_jobtitle) {
            $jobtitle = JobTitle::firstOrCreate([
                'name' => $request->new_jobtitle,
            ]);
            $request['jobtitle_id'] = $jobtitle->id;
        }
        return $request;
    }
    public function encrypt_delicated_inputs($request)
    {
        $request['bank_account_num'] = encrypt($request->bank_account_num);
        $request['bank_card_num']    = encrypt($request->bank_card_num);
        $request['interbank_clabe']  = encrypt($request->interbank_clabe);
        $request['nss']              = encrypt($request->nss);
        return $request;
    }
    public function format_timestamp_inputs($request)
    {
        
        
        return $request;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return redirect()->route('employee-personal-information', $employee);
    }
    public function personal_information(Employee $employee)
    {
        return view('dbpi.rrhh.employees.forms.personal-information', compact('employee'));
    }
    public function monthly_salary(Employee $employee)
    {
        $currencies = Currency::all();
        return view('dbpi.rrhh.employees.forms.salary-information', compact('employee', 'currencies'));
    }
    public function active_and_unactive_date(Employee $employee)
    {
        return view('dbpi.rrhh.employees.forms.active-at', compact('employee'));
    }
    public function department_and_jobtitle(Employee $employee)
    {
        $departments = Department::all();
        $accounts    = Account::internal();
        $locations   = Location::all();
        $jobtitles   = JobTitle::all();
        $statuses    = EmployeeStatus::all();
        return view('dbpi.rrhh.employees.forms.jobtitle-information',
            compact('employee', 'departments', 'accounts', 'locations', 'jobtitles', 'statuses')
        );
    }
    public function bank_information(Employee $employee)
    {
        $banks = Bank::all();
        return view('dbpi.rrhh.employees.forms.bank-information', compact('employee', 'banks'));
    }
    public function discounts_information(Employee $employee)
    {
        return view('dbpi.rrhh.employees.forms.discounts-information', compact('employee'));
    }
    public function historial_de_vacaciones(Employee $employee)
    {
        $vacaciones = $employee->vacaciones()->orderBy('periodo', 'asc')->get();
        return view('dbpi.rrhh.employees.historial-de-vacaciones', compact('employee', 'vacaciones'));
    }
    public function generar_link_de_solicitud_de_vacaciones(Request $request)
    {
        $empleado = Employee::find($request->id_de_empleado);
        $link     = null;
        if ($empleado) {
            $token = encrypt($empleado->numero_de_empleado);
            $link  = url('/rrhh/solicitud-de-vacaciones?u=' . $token);
        }
        $respuesta = [
            'link' => $link,
        ];
        return $respuesta;
    }
    public function enviar_link_solicitud_de_vacaciones_a_empleado(Request $request)
    {
        $empleado = $this->buscar_empleado_por_numero_encriptado($request);
        if ($empleado) {
            $this->generar_email_con_link_de_solicitud($request, $empleado);
        }
    }
    public function buscar_empleado_por_numero_encriptado($request)
    {
        $empleado = Employee::find($request->id_de_empleado);
        return $empleado;
    }
    public function generar_email_con_link_de_solicitud($request, $empleado)
    {
        $mail_data = [
            'id_empleado' => $empleado->id,
            'link'        => $request->link,
        ];
        $email       = new Email();
        $email->type = 'link-para-solicitud-de-vacaciones';
        $email->to   = $request->email;
        $email->data = json_encode($mail_data);
        $email->save();
    }
    public function plantilla_para_solicitud_de_vacaciones()
    {
        $id_de_empleado = decrypt(request()->u);
        $empleado       = Employee::find($id_de_empleado);
        return view('dbpi.rrhh.plantilla-para-solicitud-de-vacaciones', compact('empleado'));
    }
    public function reglas_de_solicitud_de_vacaciones()
    {
        $reglas = [
            'id_de_empleado'          => 'required|integer|exists:employees,id',
            'dias_a_disfrutar'        => 'required|integer',
            'dias_pendientes'         => 'required|integer',
            'periodo'                 => 'required|integer',
            'terminos'                => 'sometimes|required|accepted',
            'cuando_inicia'           => 'required|date_format:Y-m-d',
            'cuando_termina'          => 'required|date_format:Y-m-d',
            'cuando_debe_presentarse' => 'required|date_format:Y-m-d',
        ];
        return $reglas;
    }
    public function mensajes_de_validacion_en_solicitud_de_vacaciones()
    {
        $mensajes = [
            'id_de_empleado.required'             => 'Ocurrió un error, por favor intenta nuevamente.',
            'id_de_empleado.integer'              => 'Ocurrió un error, por favor intenta nuevamente.',
            'id_de_empleado.exists'               => 'Ocurrió un error, por favor intenta nuevamente.',
            'dias_a_disfrutar.required'           => 'Define los días a disfrutar.',
            'dias_a_disfrutar.integer'            => 'Los días a disfrutar debe ser un número.',
            'dias_pendientes.required'            => 'Define los días pendientes',
            'dias_pendientes.integer'             => 'Los días pendientes debe ser un número',
            'periodo.required'                    => 'El periódo es requerido.',
            'periodo.integer'                     => 'El periodo debe ser un número',
            'terminos.required'                   => 'Es requerido que acceptes tu conformidad de solicitud.',
            'terminos.accepted'                   => 'Por favor confirma tu conformidad de solicitud de vacaciones.',
            'cuando_inicia.required'              => 'Define la fecha de inicio',
            'cuando_inicia.date_format'           => 'El formato de la fecha de inicio no es válido.',
            'cuando_termina.required'             => 'Define la fecha en que terminan las vacaciones',
            'cuando_termina.date_format'          => 'El formato de la fecha en que terminan las vacaciones no es válido.',
            'cuando_debe_presentarse.required'    => 'Define la fecha en que debe presentarse a trabajar',
            'cuando_debe_presentarse.date_format' => 'El formato de la fecha en que debe presentarse a trabajar no es válido.',
        ];
        return $mensajes;
    }
    public function agregar_vacaciones(Request $request){
        $reglas     = $this->reglas_de_solicitud_de_vacaciones();
        $mensajes   = $this->mensajes_de_validacion_en_solicitud_de_vacaciones();
        $validacion = Validator::make($request->all(), $reglas, $mensajes)->validate();
        $solicitud  = $this->generar_solicitud_de_vacaciones($request);
    }

    public function eliminar_vacaciones(Employee $empleado, VacacionesDeEmpleado $vacaciones){

        info($empleado);

        $vacaciones->autorizaciones->each(function($autorizacion){
            $autorizacion->delete();
        });

        $vacaciones->delete();
    }

    public function enviar_plantilla_para_solicitud_de_vacaciones(Request $request)
    {
        $reglas     = $this->reglas_de_solicitud_de_vacaciones();
        $mensajes   = $this->mensajes_de_validacion_en_solicitud_de_vacaciones();
        $validacion = Validator::make($request->all(), $reglas, $mensajes)->validate();
        $solicitud  = $this->generar_solicitud_de_vacaciones($request);
        $this->notificar_a_rrhh_sobre_solicitud($solicitud);
        $this->enviar_solicitud_a_directores($solicitud);
    }
    public function notificar_a_rrhh_sobre_solicitud($solicitud)
    {
        $link            = route('historial-de-vacaciones', $solicitud->id_de_empleado);
        $cuenta_de_email = 'rh@godynacom.com';
        $mail_data       = [
            'link'         => $link,
            'id_solicitud' => $solicitud->id,
        ];
        $email       = new Email();
        $email->type = 'nueva-solicitud-de-vacaciones';
        $email->to   = $cuenta_de_email;
        $email->data = json_encode($mail_data);
        $email->save();
    }
    public function enviar_solicitud_a_directores($solicitud)
    {
        $departamento_de_direccion = Department::where('name', 'direccion')->first();
        $directores                = $departamento_de_direccion->users;
        foreach ($directores as $director) {
            $link      = route('autorizar-vacaciones', $solicitud->id) . '?id=' . encrypt($director->user->id);
            $mail_data = [
                'link'         => $link,
                'id_solicitud' => $solicitud->id,
            ];
            $email       = new Email();
            $email->type = 'autorizar-vacaciones-de-empleado';
            $email->to   = $director->user->email;
            $email->data = json_encode($mail_data);
            $email->save();
        }
    }
    public function generar_solicitud_de_vacaciones($request)
    {
        $solicitud                          = new VacacionesDeEmpleado();
        $solicitud->id_de_empleado          = $request->id_de_empleado;
        $solicitud->dias_correspondientes   = $request->dias_correspondientes;
        $solicitud->dias_a_disfrutar        = $request->dias_a_disfrutar;
        $solicitud->dias_pendientes         = $request->dias_pendientes;
        $solicitud->periodo                 = $request->periodo;
        $solicitud->terminos                = $request->terminos;
        $solicitud->cuando_inicia           = Carbon::parse($request->cuando_inicia);
        $solicitud->cuando_termina          = Carbon::parse($request->cuando_termina);
        $solicitud->cuando_debe_presentarse = Carbon::parse($request->cuando_debe_presentarse);
        $solicitud->save();
        return $solicitud;
    }

    public function autorizar_vacaciones(VacacionesDeEmpleado $solicitud)
    {
        $id       = decrypt(request()->id);
        $director = User::find($id);
        $decision = $solicitud
            ->autorizaciones
            ->where('id_director', $director->id)
            ->first();
        return view('dbpi.rrhh.employees.autorizar-vacaciones', compact('solicitud', 'decision', 'director'));
    }

    public function vacaciones_autorizadas(Request $request, VacacionesDeEmpleado $solicitud)
    {

        $autorizacion                            = new VacacionesAutorizacion();
        $autorizacion->id_vacaciones_de_empleado = $solicitud->id;
        $autorizacion->id_director               = $request->id_director;
        $autorizacion->autorizadas               = true;
        $autorizacion->comentario                = $request->comentario;
        $autorizacion->save();

        $solicitud->etapa = 1;
        $solicitud->save();

        $this->notificar_a_rrhh_sobre_autorizacion_de_vacaciones($autorizacion);

    }

    public function notificar_a_rrhh_sobre_autorizacion_de_vacaciones($autorizacion)
    {

        $link            = route('historial-de-vacaciones', $autorizacion->vacaciones->id_de_empleado);
        $cuenta_de_email = 'rh@godynacom.com';
        $mail_data       = [
            'link'            => $link,
            'id_autorizacion' => $autorizacion->id,
            'id_solicitud'    => $autorizacion->vacaciones->id,
        ];
        $email       = new Email();
        $email->type = 'vacaciones-autorizadas';
        $email->to   = $cuenta_de_email;
        $email->data = json_encode($mail_data);
        $email->save();

    }

    public function notificar_a_rrhh_sobre_rechazo_de_vacaciones($autorizacion){

        $link            = route('historial-de-vacaciones', $autorizacion->vacaciones->id_de_empleado);
        $cuenta_de_email = 'rh@godynacom.com';
        $mail_data       = [
            'link'            => $link,
            'id_autorizacion' => $autorizacion->id,
            'id_solicitud'    => $autorizacion->vacaciones->id,
        ];
        $email       = new Email();
        $email->type = 'vacaciones-rechazadas';
        $email->to   = $cuenta_de_email;
        $email->data = json_encode($mail_data);
        $email->save();

    }

    public function vacaciones_rechazadas(Request $request, VacacionesDeEmpleado $solicitud)
    {
        $autorizacion                            = new VacacionesAutorizacion();
        $autorizacion->id_vacaciones_de_empleado = $solicitud->id;
        $autorizacion->id_director               = $request->id_director;
        $autorizacion->autorizadas               = false;
        $autorizacion->comentario                = $request->comentario;
        $autorizacion->save();

        $solicitud->etapa = 1;
        $solicitud->save();

        $this->notificar_a_rrhh_sobre_rechazo_de_vacaciones($autorizacion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        $msg = null;
        if ($request->has('personal_info')) {
            $msg = $this->update_personal_info($request, $employee);
        } elseif ($request->has('department_info')) {
            $msg = $this->update_department_info($request, $employee);
        } elseif ($request->has('salary_info')) {
            $msg = $this->update_salary_info($request, $employee);
        } elseif ($request->has('discounts_info')) {
            $msg = $this->update_discounts_info($request, $employee);
        } elseif ($request->has('bank_info')) {
            $msg = $this->update_bank_info($request, $employee);
        } elseif ($request->has('active_at_info')) {
            $msg = $this->update_active_at($request, $employee);
        }
        return redirect()->back()->with('success', $msg);
    }
    public function update_active_at($request, $employee)
    {
        if ($request->active_at_day && $request->active_at_month && $request->active_at_year) {
            $request['active_at'] = Carbon::now()
                ->day($request->active_at_day)
                ->month($request->active_at_month)
                ->year($request->active_at_year);
        }
        if ($request->inactive_at_day && $request->inactive_at_month && $request->inactive_at_year) {
            $request['inactive_at'] = Carbon::now()
                ->day($request->inactive_at_day)
                ->month($request->inactive_at_month)
                ->year($request->inactive_at_year);
        }
        $employee->update($request->all());
        $msg = "Se ha actualizado la información de {$employee->short_name}";
        return $msg;
    }
    public function update_bank_info($request, $employee)
    {
        $employee->update([
            'bank_id'          => $request->bank_id,
            'bank_account_num' => $request->bank_account_num ? encrypt($request->bank_account_num) : '',
            'bank_card_num'    => $request->bank_card_num ? encrypt($request->bank_card_num) : '',
            'interbank_clabe'  => $request->interbank_clabe ? encrypt($request->interbank_clabe) : '',
        ]);
        $msg = "Se ha actualizado la información bancaria de {$employee->short_name}";
        return $msg;
    }
    public function update_contact_info($request, $employee)
    {
        $employee->update($request->except('_token', '_method', 'contact_info'));
        $msg = "Se ha actualizado la información de contacto de {$employee->short_name}";
        return $msg;
    }
    public function update_discounts_info($request, $employee)
    {
        
        $employee->fonacot_discount   = $request->fonacot_discount ? $request->fonacot_discount : 0;
        $employee->infonavit_discount = $request->infonavit_discount ? $request->infonavit_discount : 0;
        $employee->imss_active_at = $request->imss_active_at ? $request->imss_active_at : null;
        $employee->imss_unactive_external_at = $request->imss_unactive_external_at ? $request->imss_unactive_external_at : null;
        $employee->nss                = encrypt($request->nss);

        if ($request->imss_unactive_internal_at_day
            && $request->imss_unactive_internal_at_month
            && $request->imss_unactive_internal_at_year) {
            $employee->imss_unactive_internal_at = $this->format_date(
                $request->imss_unactive_internal_at_day,
                $request->imss_unactive_internal_at_month,
                $request->imss_unactive_internal_at_year
            );
        }
        if ($request->imss_unactive_external_at_day
            && $request->imss_unactive_external_at_month
            && $request->imss_unactive_external_at_year) {
            $employee->imss_unactive_external_at = $this->format_date(
                $request->imss_unactive_external_at_day,
                $request->imss_unactive_external_at_month,
                $request->imss_unactive_external_at_year
            );
        }
        $employee->save();
        $msg = "Se han actualizado los descuentos de {$employee->short_name} correctamente.";
        return $msg;
    }
    public function update_salary_info($request, $employee)
    {
        $employee->update($request->except('_token', '_method', 'salary_info'));
        if ($request->update_payroll) {
            $this->update_employee_salary_in_current_payroll($employee);
            $msg = "Se ha actualizado el salario de {$employee->short_name} correctamente y se actualizo en la nómina actual.";
        } else {
            $msg = "Se ha actualizado el salario de {$employee->short_name} correctamente.";
        }
        return $msg;
    }
    public function update_employee_salary_in_current_payroll($employee)
    {
        $current_payroll = Payroll::where('status_id', 1)->first();
        $account_payroll = $current_payroll->accounts()->where('account_id', $employee->account_id)->first();
        if ($account_payroll) {
            $employee_payroll = $account_payroll->employee_payrolls()->where('employee_id', $employee->id)->first();
            if ($employee_payroll) {
                $employee_payroll->salary = $employee->salary;
                $employee_payroll->save();
                // ACTUALIZAR EL MONTO DE LAS FALTAS QUE PUEDA TENER EL EMPLEADO
                $discount_category = EmployeePayrollDiscountCategory::where('code', 'd-01')->first();
                $discounts         = $employee_payroll
                    ->discounts()
                    ->where('employee_payroll_discounts_category_id', $discount_category->id)
                    ->get();
                foreach ($discounts as $key => $discount) {
                    $daily_salary      = $employee_payroll->daily_salary * 1.17;
                    $daily_salary      = number_format($daily_salary, 2, '.', '');
                    $discount->ammount = $daily_salary;
                    $discount->save();
                }
            }
        }
    }
    public function update_department_info($request, $employee)
    {
        $request['assimilated'] = $request->has('assimilated') ? 1 : 0;
        $employee->update($request->except('_token', '_method', 'department_info'));
        $msg = "La información de {$employee->short_name} ha sido actualizada correctamente.";
        return $msg;
    }
    public function update_personal_info($request, $employee)
    {
        
        
        
        $employee->name_1      = $request->name_1;
        $employee->name_2      = $request->name_2;
        $employee->last_name_1 = $request->last_name_1;
        $employee->last_name_2 = $request->last_name_2;
        $employee->curp        = $request->curp;
        $employee->rfc         = $request->rfc;
        $employee->birthday    = $request->birthday;
        $employee->email       = $request->email;
        $employee->mobile      = $request->mobile;
        $employee->address     = $request->address;
        $employee->save();
        $avatar = $request->file('avatar');
        $this->set_employee_avatar($avatar, $employee);
        $msg = "Se ha actualizado la información personal de {$employee->short_name} correctamente.";
        return $msg;
    }
    public function format_date($day, $month, $year)
    {
        $date = Carbon::now()->year($year)->month($month)->day($day);
        return $date;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $name    = $employee->full_name;
        $account = $employee->account;
        $employee->payrolls()->delete();
        $employee->delete();
        return redirect()
            ->route('get-employees-account', $account->account_id)
            ->with('info', "Se ha eliminado a {$name} del sistema");
    }
}
