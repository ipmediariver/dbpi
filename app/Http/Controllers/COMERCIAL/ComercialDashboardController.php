<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Quote;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class ComercialDashboardController extends Controller
{
    public function index(){
    	$internal_auth_subtotal_mxn = cache()->get('quotes.internal_authorization_mxn.subtotal')->toArray();
        $internal_auth_subtotal_usd = cache()->get('quotes.internal_authorization_usd.subtotal')->toArray();
        $client_auth_subtotal_mxn = cache()->get('quotes.authorized_by_cliente_mxn.subtotal')->toArray();
        $client_auth_subtotal_usd = cache()->get('quotes.authorized_by_cliente_usd.subtotal')->toArray();

        $fups_count = cache()->get('fups.count');
        $fups_no_billing_mxn = cache()->get('fups.no_billing_mxn')->toArray();
        $fups_no_billing_usd = cache()->get('fups.no_billing_usd')->toArray();
        
        $response = [
            'internal_auth_count'   => count($internal_auth_subtotal_mxn) + count($internal_auth_subtotal_usd),
            'internal_auth_subtotal_mxn' => number_format(array_sum($internal_auth_subtotal_mxn), 2, '.', ''),
            'internal_auth_subtotal_usd' => number_format(array_sum($internal_auth_subtotal_usd), 2, '.', ''),
            'client_auth_count'   => count($client_auth_subtotal_mxn) + count($client_auth_subtotal_usd),
            'client_auth_subtotal_mxn' => number_format(array_sum($client_auth_subtotal_mxn), 2, '.', ''),
            'client_auth_subtotal_usd' => number_format(array_sum($client_auth_subtotal_usd), 2, '.', ''),
            'fups_count' => $fups_count,
            'fups_no_billing_mxn' => number_format(array_sum($fups_no_billing_mxn), 2, '.', ''),
            'fups_no_billing_usd' => number_format(array_sum($fups_no_billing_usd), 2, '.', ''),
        ];

        return response()->json($response);
    }
}