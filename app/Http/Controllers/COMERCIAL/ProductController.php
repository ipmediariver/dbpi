<?php
namespace App\Http\Controllers\COMERCIAL;

use App\COMERCIAL\Product;
use App\COMERCIAL\ProductBrand;
use App\COMERCIAL\ProductCategory;
use App\COMERCIAL\ProductImage;
use App\COMERCIAL\ProductModel;
use App\COMERCIAL\ProductType;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsListRequest;
use App\Imports\UploadProducts;
use Excel;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('code')->orderBy('description')->paginate(15);
        return view('dbpi.comercial.products.index', compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function upload(ProductsListRequest $request)
    {
        $file      = $request->file('file');
        $extension = $request->file('file')->extension();
        $filename  = hexdec(uniqid()) . '.' . $extension;
        $file->storeAs('/excel', $filename, 'uploads');

        return response()->json($filename, 200);
    }

    public function extract(Request $request)
    {

        // Extraer datos de archivo excel

        Excel::import(new UploadProducts(), 'excel/' . $request->filename, 'uploads');

        return response()->json('Listo', 200);
    }

    public function store_from_quote($request)
    {

        // Primero definimos si el usuario desea agregar un nuevo tipo de producto
        if ($request->has('new_type')) {
            // Si el usuario crea un nuevo tipo de producto por defecto tambien esta creando una nueva marca y un modelo.
            $type = ProductType::firstOrCreate([
                'name' => $request->new_type_name,
            ]);

            // Creamos la nueva marca asociada al tipo de producto
            $brand = ProductBrand::create([
                'product_type_id' => $type->id,
                'name'            => $request->new_brand_name,
            ]);

            // Creamos el nuevo modelo asociado al tipo y marca de producto recientes
            $model = ProductModel::create([
                'product_type_id'  => $type->id,
                'product_brand_id' => $brand->id,
                'name'             => $request->new_model_name,
            ]);
        } else {
            // Si el usuario esta seleccionando un tipo de producto existente hacemos lo siguiente:
            // 1)  Especificamos el id del tipo de producto
            $type = ProductType::find($request->product_type_id);
            // 2)  Definimos si el usuario esta intentando crear una nueva marca.
            if ($request->has('new_brand')) {
                //     Si esta creando una nueva marca tambien por defecto esta creando un nuevo modelo.
                //     Creamos la nueva marca o validamos que ya exista
                $brand = ProductBrand::firstOrCreate([
                    'product_type_id' => $type->id,
                    'name'            => $request->new_brand_name,
                ]);
                //     Creamos el nuevo modelo asociado al tipo y marca recientes
                $model = ProductModel::create([
                    'product_type_id'  => $type->id,
                    'product_brand_id' => $brand->id,
                    'name'             => $request->new_model_name,
                ]);
            } else {

                // Si el usuario esta seleccionando una marca de producto existente hacemos lo siguiente:
                // 1)  Especificamos el id de la marca de producto
                $brand = ProductBrand::find($request->product_brand_id);
                // 2)  Definimos si el usuario esta intentando crear un nuevo modelo de producto
                if ($request->has('new_model')) {
                    //     Creamos el nuevo modelo de producto
                    $model = ProductModel::firstOrCreate([
                        'product_type_id'  => $type->id,
                        'product_brand_id' => $brand->id,
                        'name'             => $request->new_model_name,
                    ]);
                } else {
                    // O bien si el usuario ha seleccionado un modelo existente aqui lo definimos a traves de su id.
                    $model = ProductModel::find($request->product_model_id);
                }
            }
        }

        // Aqui definimos la categoria del producto

        if($request->has('new_category')){
            $product_category = ProductCategory::firstOrCreate([
                'name' => $request->new_category_name
            ]);
        }else{
            $product_category = ProductCategory::find($request->product_category_id);
        }


        // Ya que tenemos definidos nuestro tipo, marca y modelo de producto lo damos de alta
        // Primero buscamos si ya tenemos un producto agregado con el numero de parte o codigo proporcionado

        if ($request->code) {
            $product_with_same_code = Product::where('code', $request->code)->first();
            if ($product_with_same_code) {
                $product = $product_with_same_code;
            } else {
                // De lo contrario creamos un nuevo producto
                $product = Product::create([
                    'product_type_id'     => $type->id,
                    'product_brand_id'    => $brand->id,
                    'product_model_id'    => $model->id,
                    'code'                => $request->code,
                    'product_category_id' => $product_category->id,
                    'description'         => $request->description,
                ]);
            }
        } else {
            $product = Product::create([
                'product_type_id'     => $type->id,
                'product_brand_id'    => $brand->id,
                'product_model_id'    => $model->id,
                'code'                => $request->code,
                'product_category_id' => $request->product_category_id,
                'description'         => $request->description,
            ]);
        }
        // Y ya estando guardando lo enviamos de regreso para continuar con la operacion de quote_product_controller
        return $product;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $categories = ProductCategory::get();
        $product_types = ProductType::orderBy('name')->get();
        $images = $product->images;
        if (request()->ajax()) {
            $response = [
                'product'    => $product,
                'categories' => $categories,
                'types' => $product_types->load('brands.models'),
                'images' => $images,
            ];
            return response()->json($response);
        } else {
            return view('dbpi.comercial.products.show', compact('product', 'categories'));
        }
    }
    public function remove_image(Product $product, ProductImage $img){
        $img->delete();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());
    }

    public function upload_images(Request $request, Product $product){
        
        $files = $request->file('images');

        foreach($files as $file){
            $extension = $file->extension();
            $filename = hexdec(uniqid()) . '.' . $extension;
            $file->storeAs('/products', $filename, 'uploads');

            $product_image = new ProductImage();
            $product_image->product_id = $product->id;
            $product_image->filename = $filename;
            $product_image->save();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
