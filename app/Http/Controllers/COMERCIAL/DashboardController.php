<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteProduct;
use App\COMERCIAL\Fup;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        if ($user->role == 'admin') {
            $indicadores  = $this->obtener_indicadores_por_periodo($request);
            $responsables = $this->obtener_estatus_de_cotizaciones_por_responsable($request);
            return view('dbpi.comercial.index', compact('indicadores', 'responsables'));
        } else {
            return redirect()->route('oportunidades.index');
        }
    }
    public function obtener_estatus_de_cotizaciones_por_responsable($request)
    {
        $responsables = $this->obtenemos_la_lista_de_los_usuarios_que_pueden_cotizar();
        $resumen_de_responsables = $this->generar_reporte_con_rango_de_fecha_por_responsable($request, $responsables);
    }
    public function generar_reporte_con_rango_de_fecha_por_responsable($request, $responsables)
    {
        $resumen_de_reponsables = null;
        if($request->has('periodo')){
        	$periodo = $request->periodo;
        	if($periodo == 'semanal'){
        		$this->generar_resumen_semanal_por_reponsable($responsables);
        	}elseif($periodo  == 'mensual'){
        		$this->generar_resumen_mensual_por_reponsable($responsables);
        	}elseif($periodo  == 'trimestal'){
        		$this->generar_resumen_trimestral_por_reponsable($responsables);
        	}elseif($periodo  == 'anual'){
        		$this->generar_resumen_anual_por_reponsable($responsables);
        	}
        }
        return $resumen_de_reponsables;
    }
    public function generar_resumen_semanal_por_reponsable($responsables){
    	$hoy = Carbon::now();
    	$hace_una_semana = Carbon::now()->subWeek();
    	$resumen = [];
    	foreach($responsables as $responsable){
    		
    		$cotizaciones = $responsable
    			->quotes()
    			->where('created_at', '>=', $hace_una_semana)
    			->where('created_at', '<=', $hoy)
    			->get();

    		$resumen_de_responsable = [
    			
    		];	

    	}
    }
    public function generar_resumen_mensual_por_reponsable($responsables){
    }
    public function generar_resumen_trimestral_por_reponsable($responsables){
    }
    public function generar_resumen_anual_por_reponsable($responsables){
    }
    public function obtenemos_la_lista_de_los_usuarios_que_pueden_cotizar()
    {
        $usuarios_que_pueden_cotizar = [];
        $autores                     = User::where('role', 'user')->orderBy('first_name')->get();
        foreach ($autores as $autor) {
            $tiene_asignado_el_modulo_comercial = $autor->modules->where('code', 'm-02')->count();
            if ($tiene_asignado_el_modulo_comercial) {
                array_push($usuarios_que_pueden_cotizar, $autor);
            }
        }
        return $usuarios_que_pueden_cotizar;
    }
    public function obtener_indicadores_por_periodo($request)
    {
        $indicadores = null;
        if ($request->has('periodo')) {
            $periodo = $request->periodo;
            if ($periodo == 'semanal') {
                $indicadores = $this->generar_indicadores_semanales();
            } elseif ($periodo == 'mensual') {
                $indicadores = $this->generar_indicadores_mensuales();
            } elseif ($periodo == 'trimestral') {
                $indicadores = $this->generar_indicadores_trimestrales();
            } elseif ($periodo == 'anual') {
                $indicadores = $this->generar_indicadores_anuales();
            }
        } else {
            $indicadores = $this->generar_indicadores_semanales();
        }
        return $indicadores;
    }
    public function generar_indicadores_semanales()
    {
        $hoy             = Carbon::now()->startOfDay();
        $hace_una_semana = Carbon::now()->subWeek()->startOfDay();
        $indicadores     = $this->obtener_indicadores_por_rango_de_fechas($hace_una_semana, $hoy);
        return $indicadores;
    }
    public function generar_indicadores_mensuales()
    {
        $hoy         = Carbon::now()->startOfDay();
        $hace_un_mes = Carbon::now()->subMonth()->startOfDay();
        $indicadores = $this->obtener_indicadores_por_rango_de_fechas($hace_un_mes, $hoy);
        return $indicadores;
    }
    public function generar_indicadores_trimestrales()
    {
        $hoy             = Carbon::now()->startOfDay();
        $hace_tres_meses = Carbon::now()->subMonths(3)->startOfDay();
        $indicadores     = $this->obtener_indicadores_por_rango_de_fechas($hace_tres_meses, $hoy);
        return $indicadores;
    }
    public function generar_indicadores_anuales()
    {
        $hoy         = Carbon::now()->startOfDay();
        $hace_un_ano = Carbon::now()->subYear()->startOfDay();
        $indicadores = $this->obtener_indicadores_por_rango_de_fechas($hace_un_ano, $hoy);
        return $indicadores;
    }
    public function obtener_indicadores_por_rango_de_fechas($desde, $hoy)
    {
        $cotizaciones = Quote::where('created_at', '>=', $desde)
            ->where('created_at', '<=', $hoy)
            ->get();
        $indicadores = [
            'cotizaciones_en_proceso'               => $cotizaciones->where('status_id', 0)->count(),
            'cotizaciones_autorizadas_internamente' => $cotizaciones->where('status_id', 2)->count(),
            'cotizaciones_autorizadas_por_cliente'  => $cotizaciones->where('status_id', 6)->count(),
        ];
        return $indicadores;
    }

    // Funciones para generar los indicadores por cliente.

    public function obtener_indicadores_por_periodo_y_cliente(Request $request){
        $cliente_id = $request->a;
        $quotes = null;
        
        if ($request->p == "semanal") {
            $quotes = $this->generar_indicadores_semanales_clientes($cliente_id);
        } elseif ($request->p == 'mensual') {
            $quotes = $this->generar_indicadores_mensuales_clientes($cliente_id);
        } elseif ($request->p == 'trimestral') {
            $quotes = $this->generar_indicadores_trimestrales_clientes($cliente_id);
        } elseif ($request->p == 'anual') {
             $quotes = $this->generar_indicadores_anuales_clientes($cliente_id);
        }


        return $quotes;
    }
    public function generar_indicadores_semanales_clientes($cliente_id)
    {
        $hoy             = Carbon::now()->startOfDay();
        $hace_una_semana = Carbon::now()->subWeek()->startOfDay();
        $quotes     = $this->obtener_cotizaciones_por_cliente($cliente_id,$hace_una_semana, $hoy);
        return $quotes;
    }
    public function generar_indicadores_mensuales_clientes($cliente_id)
    {
        $hoy         = Carbon::now()->startOfDay();
        $hace_un_mes = Carbon::now()->subMonth()->startOfDay();
        $quotes = $this->obtener_cotizaciones_por_cliente($cliente_id,$hace_un_mes, $hoy);
        return $quotes;
    }
    public function generar_indicadores_trimestrales_clientes($cliente_id)
    {
        $hoy             = Carbon::now()->startOfDay();
        $hace_tres_meses = Carbon::now()->subMonths(3)->startOfDay();
        $quotes     = $this->obtener_cotizaciones_por_cliente($cliente_id,$hace_tres_meses, $hoy);
        return $quotes;
    }
    public function generar_indicadores_anuales_clientes($cliente_id)
    {
        $hoy         = Carbon::now()->startOfDay();
        $hace_un_ano = Carbon::now()->subYear()->startOfDay();
        $quotes = $this->obtener_cotizaciones_por_cliente($cliente_id,$hace_un_ano, $hoy);
        return $quotes;
    }
    public function obtener_cotizaciones_por_cliente($cliente_id,$desde,$hoy){

        $cotizaciones = Quote::where('account_id', $cliente_id)
            ->whereDate('created_at', '>=', $desde)
            ->whereDate('created_at', '<=', $hoy)
            ->get();

        $total_de_cotizaciones = $cotizaciones->count();
        $importe_total = $cotizaciones->sum('subtotal');
        $promedio_margen = $cotizaciones->avg('margin_percent');

        $quotes_id = $cotizaciones->pluck('id');

        $fups = Fup::whereIn('quote_id', $quotes_id)
        ->get();

        $total_de_fups = $fups->count();
        $importe_total_facturado_fup = $cotizaciones->sum('billing_total_import');
        $importe_total_por_facturar = $importe_total - $importe_total_facturado_fup;


        $respuesta = [
            'cotizaciones' => [
                'total_de_cotizaciones' => $total_de_cotizaciones,
                'importe_total' => $importe_total,
                'promedio_margen' => number_format($promedio_margen, 2)
            ],
             'fups' => [
                 'total_de_fups'                 => $total_de_fups,
                 'fup_sin_solicitar'             => $fups->where('id_etapa', 1)->count(),
                 'fup_en_proceso'                => $fups->where('id_etapa', 2)->count(),
                 'fup_autorizadas'               => $fups->where('id_etapa', 3)->count(),
                 'importe_total_facturado_fup'   => $importe_total_facturado_fup,
                 'importe_total_por_facturar'    => $importe_total_por_facturar,

             ]
        ];
        return $respuesta;
    }
}