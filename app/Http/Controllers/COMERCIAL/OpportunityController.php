<?php
namespace App\Http\Controllers\COMERCIAL;

use App\COMERCIAL\Contact;
use App\COMERCIAL\Opportunity;
use App\COMERCIAL\Quote;
use App\CRM\Account;
use App\Http\Controllers\Controller;
use App\RRHH\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class OpportunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $opportunities = Opportunity::prospects();
        return view('dbpi.comercial.opportunities.index', compact('opportunities'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function quote(Request $request, $id)
    {
        // TOMAMOS LA OPORTUNIDAD DE CRM
        $opportunity = Opportunity::single($id);
        // VALIDAMOS QUE LA OPORTUNIDAD TENGA LOS CAMPOS REQUERIDOS
        // ['contact_id', 'name', 'deliveryTime', 'iva', 'fob', 'institutionalDiscount']
        $opportunity_validation = $this->validate_if_opportunity_has_required_fields($opportunity);
        if ($opportunity_validation['type'] == 'success') {
            // SI LA OPORTUNIDAD ES VALIDADA EXITOSAMENTE, PROCEDEMOS A CREAR LA COTIZACION
            $quote = $this->build_quote($request, $opportunity);
            return response()->json($quote);
        } elseif ($opportunity_validation['type'] == 'error') {
            // DE LO CONTRARIO ENVIAMOS UNA NOTIFICACION CON LOS ERRORES OBTENIDOS
            return response()->json($opportunity_validation['msg'], 500);
        }
    }
    public function validate_if_opportunity_has_required_fields($opportunity)
    {
        $response = null;
        // VALIDAMOS SI LA OPORTUNIDAD TIENE UNA CUENTA Y UN CONTACTO ASIGNADOS
        $accountId = $opportunity['accountId'];
        $contactId = $opportunity['contactId'];
        
        if ($accountId && $contactId) {
            // VERIFICAMOS QUE LA OPORTUNIDAD TENGA LOS TERMINOS
            $terms = $this->set_quote_terms($opportunity);
            if ($terms) {
                // VERIFICAMOS QUE LA OPORTUNIDAD TENGA UN TIEMPO DE ENTREGA
                $delivery_time = $opportunity['deliveryTime'];
                if ($delivery_time) {
                    $response = ['type' => 'success'];
                } else {
                    // SI NO SE HA ESPECIFICADO EL TIEMPO DE ENTREGA ENVIAMOS LOS ERRORES CORRESPONDIENTES.
                    $response = [
                        'msg'  => 'No se ha especificado el tiempo de entrega en CRM',
                        'type' => 'error',
                    ];
                }
            } else {
                // SI NO SE ESPECIFICARON LOS TERMINOS EN LA OPORTUNIDAD NI EN LA CUENTA, ENVIAMOS EL ERROR CORRESPONDIENTE
                $response = [
                    'msg'  => 'No se han especificado los términos en la oportunidad de CRM',
                    'type' => 'error',
                ];
            }
        } else {
            $response = [
                'msg'  => 'La oportunidad en CRM require de al menos una cuenta y un contacto asignados',
                'type' => 'error',
            ];
        }
        return $response;
    }
    public function build_quote($request, $opportunity)
    {
        // BUSCAMOS EL CONTACT, LA CUENTA Y EL TIPO DE MONEDA A UTILIZAR
        $contact  = Contact::single($opportunity['contactId']);
        $account  = Account::single($opportunity['accountId']);
        $currency = Currency::where('short_name', $opportunity['amountCurrency'])->first();
        $terms    = $this->set_quote_terms($opportunity);
        // SE MODIFICA EL STATUS DE LA OPORTUNIDAD EN CRM
        Opportunity::update_opportunity($opportunity['id'], ['stage' => 'Proposal/Price Quote']);
        // ESPECIFICAMOS EL IVA DE LA OPORTUNIDAD
        $iva = $this->set_quote_iva($opportunity);
        // SE CREA LA COTIZACION
        $request['from_opportunity'] = true;
        $quote                       = app('App\Http\Controllers\COMERCIAL\QuoteController')->create($request);
        $quote->opportunity_id       = $opportunity['id'];
        $quote->account_id           = $opportunity['accountId'];
        $quote->account_logo_id      = $account['logoId'];
        $quote->contact_id           = $contact['id'];
        $quote->contact_email        = $contact['emailAddress'];
        $quote->contact_phone        = $contact['phoneNumber'];
        $quote->contact_name         = $opportunity['contactName'];
        $quote->description          = $opportunity['name'];
        $quote->fup_description      = $opportunity['description'];
        $quote->terms                = $terms;
        $quote->delivery_time        = $opportunity['deliveryTime'];
        $quote->currency_id          = $currency->id;
        $quote->iva                  = $iva;
        $quote->fob                  = $opportunity['fob'];
        $quote->general_discount     = $account['institutionalDiscount'] ? $account['institutionalDiscount'] : 0;
        $quote->etapa_de_facturacion = 1;
        $quote->save();
        return $quote;
    }
    public function set_quote_iva($opportunity)
    {
        $iva = 16;
        if ($opportunity['useGeneralIva']) {
            $account = Account::single($opportunity['accountId']);
            $iva     = $account['iva'] ? preg_replace('/\D/', '', $account['iva']) : 16;
        } else {
            $iva = $opportunity['iva'] ? preg_replace('/\D/', '', $opportunity['iva']) : 16;
        }
        return $iva;
    }
    public function set_quote_terms($opportunity)
    {
        $general_terms = $opportunity['generalTerms']; #VIENE UN ARRAY CON UNA SOLA OPCION QUE DICE "SI"
        $terms         = [];
        if ($general_terms) {
            $account = Account::single($opportunity['accountId']);
            if ($account['termsAdvancePercent']) {
                $text = $account['termsAdvancePercent'] . '% anticipo';
                array_push($terms, $text);
            }
            if ($account['termsDeliveryPercent']) {
                $text = $account['termsDeliveryPercent'] . '% entrega';
                array_push($terms, $text);
            }
            if ($account['termsInstalationPercent']) {
                $text = $account['termsInstalationPercent'] . '% instalación';
                array_push($terms, $text);
            }
        } else {
            if ($opportunity['termsAdvancePercent']) {
                $text = $opportunity['termsAdvancePercent'] . '% anticipo';
                array_push($terms, $text);
            }
            if ($opportunity['termsDeliveryPercent']) {
                $text = $opportunity['termsDeliveryPercent'] . '% entrega';
                array_push($terms, $text);
            }
            if ($opportunity['termsInstalationPercent']) {
                $text = $opportunity['termsInstalationPercent'] . '% instalación';
                array_push($terms, $text);
            }
        }
        $terms = join(', ', $terms);
        return $terms;
    }
    public function sync(Request $request, Quote $quote)
    {
        // TOMAMOS LA OPORTUNIDAD DE CRM
        $opportunity = Opportunity::single($quote->opportunity_id);
        // VALIDAMOS QUE LA OPORTUNIDAD TENGA LOS CAMPOS REQUERIDOS
        // ['contact_id', 'name', 'deliveryTime', 'iva', 'fob', 'institutionalDiscount']
        $opportunity_validation = $this->validate_if_opportunity_has_required_fields($opportunity);
        if ($opportunity_validation['type'] == 'success') {
            // SI LA OPORTUNIDAD ES VALIDADA EXITOSAMENTE, PROCEDEMOS A CREAR LA COTIZACION
            
            $contact  = Contact::single($opportunity['contactId']);
            $account  = Account::single($opportunity['accountId']);
            $currency = Currency::where('short_name', $opportunity['amountCurrency'])->first();
            $terms = $this->set_quote_terms($opportunity);
            $iva = $this->set_quote_iva($opportunity);

            $quote->account_id       = $account['id'];
            $quote->account_logo_id  = $account['logoId'];
            $quote->contact_id       = $contact['id'];
            $quote->contact_email    = $contact['emailAddress'];
            $quote->contact_phone    = $contact['phoneNumber'];
            $quote->contact_name     = $opportunity['contactName'];
            $quote->description      = $opportunity['name'];
            $quote->terms            = $terms;
            $quote->delivery_time    = $opportunity['deliveryTime'];
            $quote->currency_id      = $currency->id;
            $quote->iva              = $iva;
            $quote->fob              = $opportunity['fob'];
            $quote->general_discount = $account['institutionalDiscount'] ? $account['institutionalDiscount'] : 0;
            // GUARDAMOS LAS ACTUALIZACIONES EN LA COTIZACION
            $quote->save();

        } elseif ($opportunity_validation['type'] == 'error') {
            // DE LO CONTRARIO ENVIAMOS UNA NOTIFICACION CON LOS ERRORES OBTENIDOS
            return response()->json($opportunity_validation['msg'], 500);
        }
    }
}
