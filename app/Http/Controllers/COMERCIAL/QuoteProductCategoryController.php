<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class QuoteProductCategoryController extends Controller
{
    public function index(Quote $quote){
    	$product_categories = $quote->product_categories;
    	return response()->json($product_categories);
    }
    public function update(Request $request, Quote $quote, QuoteProductCategory $category){
    	$category->update($request->only('percent'));
    	$respuesta = [
    		'mensaje' => 'Se actualizó el porcentaje de la categoría'
    	];
    	return response()->json($respuesta);
    }
}