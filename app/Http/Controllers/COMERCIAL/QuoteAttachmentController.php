<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteAttachment;
use App\Http\Controllers\Controller;
use App\Http\Requests\QuoteAttachmentRequest;
use Illuminate\Http\Request;
class QuoteAttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuoteAttachmentRequest $request, Quote $quote)
    {


        $file      = $request->file('file');
        $extension = $request->file('file')->extension();
        $filename  = hexdec(uniqid()) . '.' . $extension;
        $file->storeAs('/attachments', $filename, 'uploads');
        $quote_attachment = QuoteAttachment::create([
            'quote_id'         => $quote->id,
            'name'             => $request->name,
            'description'      => $request->description ? $request->description : '',
            'ext'              => $extension,
            'filename'         => $filename,
            'quote_po'         => $request->quote_po ? $request->quote_po : 0,
            'type_id'          => $request->type_id ?? 1, // El numero 1 es un archivo general y el 2 equivale a factura.
            'billing_number'   => $request->billing_number ?? null,
            'billing_import'   => $request->billing_import ?? null,
            'billing_currency' => $request->billing_currency ?? null,
            'paidout' => $request->paidout === 'true' ? 1 : 0
        ]);
        if ($request->type_id == 2) {
            $quote->etapa_de_facturacion = $quote->billing_status;
            $quote->save();
        }
        if (!$request->ajax()) {
            return redirect()->back()->with('success', "Se ha agregado el documento exitosamente.");
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote, QuoteAttachment $attachment)
    {
        $attachment->update($request->except('_method'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quote $quote, QuoteAttachment $attachment)
    {
        $attachment->delete();
        if (!request()->ajax()) {
            return redirect()->back()->with('info', "Se ha eliminado el archivo adjunto exitosamente.");
        }
    }
}