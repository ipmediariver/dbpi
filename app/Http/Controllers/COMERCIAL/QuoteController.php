<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Fup;
use App\COMERCIAL\FupObservation;
use App\COMERCIAL\FupRepository;
use App\COMERCIAL\Opportunity;
use App\COMERCIAL\ProductCategory;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteComment;
use App\COMERCIAL\QuoteGroup;
use App\COMERCIAL\QuoteInternalValidation;
use App\COMERCIAL\QuoteParticipant;
use App\COMERCIAL\QuoteProductCategory;
use App\CRM\Account;
use App\Email;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadProductsToQuoteRequest;
use App\Imports\UploadProductsToQuote;
use App\Mail\InternalAcceptedQuote;
use App\Mail\InternalRejectedQuote;
use App\Notifications\RequestAuthQuote;
use App\RRHH\Currency;
use App\RRHH\JobTitle;
use App\SYSTEM\ExchangeRate;
use App\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PDF;
class QuoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('modify_quote')->only([
            'update',
            'import_products',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (count(request()->except('page'))) {
            $etapas  = json_decode(request()->e, true);
            $autores = json_decode(request()->a, true);
            $cuentas = collect(json_decode(request()->c, true))
                ->pluck('account_id')
                ->toArray();
            // MOSTRAMOS LAS COTIZACIONES FILTRADAS POR LOS PARAMETROS PROPORCIONADOS
            $quotes = Quote::orderBy('created_at', 'desc')
                ->where('current_version', 1)
                ->when(count($etapas), function ($cotizaciones) use ($etapas) {
                    return $cotizaciones->whereIn('status_id', $etapas);
                })
                ->when(count($autores), function ($cotizaciones) use ($autores) {
                    return $cotizaciones->whereIn('user_id', $autores);
                })
                ->when(count($cuentas), function ($cotizaciones) use ($cuentas) {
                    return $cotizaciones->whereIn('account_id', $cuentas);
                })
                ->paginate(15);
        } else {
            // VAMOS A MOSTRAR TODAS LAS COTIZACIONES A TODO EL MODULO COMERCIAL
            $quotes = Quote::orderBy('created_at', 'desc')->where('current_version', 1)->paginate(15);
        }
        return view('dbpi.comercial.quotes.index', compact('quotes'));
    }
    public function search(Request $request)
    {
        $user   = auth()->user();
        $query  = $request->q;
        $quotes = null;
        if ($user->role == 'admin') {
            $quotes = Quote::where('id', 'like', "%{$query}%")->orWhere('description', 'like', "%{$query}%")->paginate(15);
        } else {
            $quotes = Quote::where('id', 'like', '%' . $query . '%')
                ->orWhere('description', 'like', '%' . $query . '%')
                ->get();
            $shared_quotes = $user
                ->shared_quotes
                ->where('id', 'like', '%' . $query . '%');
            $quotes  = $quotes->merge($shared_quotes)->paginate(15);
        }
        return view('dbpi.comercial.quotes.index', compact('quotes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user                 = auth()->user();
        $currency             = Currency::where('short_name', 'USD')->first();
        $exchange_rate        = ExchangeRate::first();
        $quote                = new Quote();
        $quote->user_id       = $user->id;
        $quote->quote_id      = hexdec(uniqid());
        $quote->currency_id   = $currency->id;
        $quote->exchange_rate = $exchange_rate->value;
        $quote->observations  = "NO INCLUYE INGENIERÍA CIVIL, NO INCLUYE MATERIALES ADICIONALES A LOS DESCRITOS EN ESTA COTIZACIÓN, TIEMPO DE ENTREGA PUEDE VARIAR.";
        $quote->save();
        $quote_categories = $this->create_quote_custom_categories($quote);
        $this->create_general_group_in_quote($quote);
        if (request()->has('from_opportunity')) {
            return $quote;
        } else {
            return redirect()
                ->route('cotizaciones.show', $quote);
        }
    }
    public function create_general_group_in_quote($quote)
    {
        $group              = new QuoteGroup();
        $group->quote_id    = $quote->id;
        $group->description = "General";
        $group->save();
    }
    public function create_quote_custom_categories($quote)
    {
        $categories = ProductCategory::all();
        foreach ($categories as $key => $category) {
            $quote_product_category = QuoteProductCategory::create([
                'quote_id'    => $quote->id,
                'category_id' => $category->id,
                'percent'     => $category->percent,
            ]);
        }
    }
    public function actualizar_categorias(Quote $quote){
        $categorias = ProductCategory::all()->pluck('id')->toArray();
        $categorias_en_cotizacion = $quote->product_categories->pluck('category_id')->toArray();
        $ids_categorias_faltantes = array_diff($categorias, $categorias_en_cotizacion);
        foreach($ids_categorias_faltantes as $id_de_categoria){
            $categoria = ProductCategory::find($id_de_categoria);
            QuoteProductCategory::create([
                'quote_id'    => $quote->id,
                'category_id' => $categoria->id,
                'percent'     => $categoria->percent,
            ]);
        }
        $respuesta = [
            'mensaje' => 'Se actualizaron las categorias de productos'
        ];
        return response()->json($respuesta);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Quote $quote)
    {
        if (request()->ajax()) {
            $response = [
                'quote'             => $quote->load('internal_validations', 'attachments', 'comments', 'author', 'participants'),
                'subtotal'          => $quote->subtotal,
                'total'             => $quote->total,
                'subtotal_on_quote' => $quote->subtotal_on_quote,
                'total_on_quote'    => $quote->total_on_quote,
            ];
            return response()->json($response);
        } else {
            return view('dbpi.comercial.quotes.show', compact('quote'));
        }
    }
    public function dependencies(Quote $quote)
    {
        $product_categories = $quote->product_categories;
        $accounts           = Account::external();
        $currencies         = Currency::all();
        $resources          = JobTitle::all();
        $response           = [
            'accounts'           => $accounts,
            'currencies'         => $currencies,
            'resources'          => $resources,
            'product_categories' => $product_categories,
        ];
        return response()->json($response);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote)
    {
        // Fecha de cotizacion
        $date = new Carbon();
        $date->day($request->day);
        $date->month($request->month);
        $date->year($request->year);
        // Agregar fecha de cotizacion a request
        $request['date'] = $date;
        // Actualizamos la cotizacion con los nuevos datos
        $quote->update($request->all());
        // Redactamos un mensaje de confirmacion
        $msg = "La información de la cotizacion ha sido actualizada.";
        // Enviamos la respuesta de confirmación
        return response()->json($msg);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Quote $quote)
    {
        if($request->restaurar_oportunidad){
            $this->restaurar_oportunidad_en_crm($quote);
        }
        // Eliminamos los grupos
        $quote->groups()->delete();
        // Eliminamos los productos
        $quote->products()->delete();
        // Eliminamos los recursos
        $quote->resources()->delete();
        // Eliminamos los contratos
        $quote->contratos()->delete();
        // Eliminamos los adjuntos
        $quote->attachments()->delete();
        // Eliminamos los comentarios
        $quote->comments()->delete();
        // Eliminamos los participantes
        $quote->participants()->delete();
        $id_de_oportunidad = $quote->opportunity_id;
        $quote->delete();
        $respuesta = [
            'id_de_oportunidad' => $id_de_oportunidad
        ];
        return response()->json($respuesta);
    }
    public function restaurar_oportunidad_en_crm($quote){
        Opportunity::update_opportunity($quote->opportunity_id, ['stage' => 'Prospecting']);
    }
    public function internal_validation(Request $request, Quote $quote)
    {
        $quote->internal_validations()->update([
            'recent' => 0,
        ]);
        // Necesitamos encontrar a cada uno de los directores
        $validators_ids = json_decode($request->validators_ids, true);
        $validators     = User::whereIn('id', $validators_ids)->get();
        // Creamos un registro para que los directores validen la cotizacion
        foreach ($validators as $validator) {
            $validation           = new QuoteInternalValidation();
            $validation->user_id  = $validator->id;
            $validation->quote_id = $quote->id;
            $validation->save();
            // Despues le enviamos una notificacion a cada uno con un link hacia la previsualizacion de la cotizacion
            $this->send_internal_validation_email($quote, $validator, $validation);
        }
        // Modificamos el status de la cotizacion
        $quote->update(['status_id' => 1]);
    }
    public function send_internal_validation_email($quote, $validator, $validation)
    {
        $link = route('quote-internal-validation', $quote->id)
        . '?u='
        . encrypt($validator->id)
        . '&v='
        . $validation->id;
        $mail_data = [
            'quote_id' => $quote->id,
            'user_id'  => $validator->id,
            'link'     => $link,
        ];
        $email       = new Email();
        $email->type = 'request-quote-validation';
        $email->to   = $validator->email;
        $email->data = json_encode($mail_data);
        $email->save();
        // SE ENVIA UNA NOTIFICACION EN EL SISTEMA
        $validator->notify(new RequestAuthQuote([
            'link'          => $link,
            'auth_quote_id' => $quote->id,
            'msg'           => "Se solicita tu autorización en la cotización #{$quote->number}",
        ]));
    }
    public function resend_internal_validation_request(Request $request, Quote $quote, QuoteInternalValidation $validation)
    {
        $validator = $validation->user;
        $this->send_internal_validation_email($quote, $validator, $validation);
        return response()
            ->json(['msg' => "Se ha reenviado la solicitud de autorización a {$validator->full_name}"]);
    }
    public function show_internal_validation(Request $request, Quote $quote)
    {
        $user       = User::find(decrypt($request->u));
        $validation = QuoteInternalValidation::find($request->v);
        return view('dbpi.comercial.quotes.internal-validation', compact('quote', 'user', 'validation'));
    }
    public function reject_internal_validation(Request $request, Quote $quote)
    {
        if ($quote->status_id <= 2) {
            $quote_internal_validation = QuoteInternalValidation::find($request->quote_internal_validation_id);
            $author                    = $quote->author;
            $validator                 = $quote_internal_validation->user;
            $quote_internal_validation->update([
                'comment'   => $request->comment ? $request->comment : null,
                'status_id' => 1,
            ]);
            $quote->update([
                'status_id' => 0,
            ]);
            $this->markRelatedNotificationAsRead($quote, $validator);
            // Enviamos una notificacion al autor de la cotizacion con los comentarios.
            $mail_data = [
                'validator' => $validator,
                'quote'     => $quote,
                'author'    => $author,
                'comment'   => $quote_internal_validation->comment,
            ];
            Mail::to($author)->send(new InternalRejectedQuote($mail_data));
        }
    }
    public function accept_internal_validation(Request $request, Quote $quote)
    {
        if ($quote->status_id <= 2) {
            $quote_internal_validation = QuoteInternalValidation::find($request->quote_internal_validation_id);
            $author                    = $quote->author;
            $validator                 = $quote_internal_validation->user;
            $quote_internal_validation->update([
                'comment'   => $request->comment ? $request->comment : null,
                'status_id' => 2,
            ]);
            $this->markRelatedNotificationAsRead($quote, $validator);
            $rejected_validations = $this->validate_if_some_validator_has_rejected_the_quote($quote);
            if (!$rejected_validations) {
                $quote->update([
                    'status_id' => 2,
                ]);
            }
            // Enviamos una notificacion al autor de la cotizacion con los comentarios.
            $mail_data = [
                'validator' => $validator,
                'quote'     => $quote,
                'author'    => $author,
                'comment'   => $quote_internal_validation->comment,
            ];
            Mail::to($author)->send(new InternalAcceptedQuote($mail_data));
        }
    }
    public function markRelatedNotificationAsRead($quote, $validator)
    {
        // Verificamos si existe una notificacion con el id de la cotizacion para marcarla como leida.
        foreach ($validator->unreadNotifications as $notification) {
            if (array_key_exists('auth_quote_id', $notification->data)) {
                if ($notification->data['auth_quote_id'] == $quote->id) {
                    $notification->markAsRead();
                }
            }
        }
    }
    public function validate_if_some_validator_has_rejected_the_quote($quote)
    {
        $rejected_quotes = $quote->internal_validations
            ->where('recent', 1)
            ->where('status_id', 1);
        return $rejected_quotes->count() ? true : false;
    }
    public function modify_status(Request $request, Quote $quote)
    {
        $quote->update([
            'status_id' => $request->status_id,
        ]);
        if ($request->status_id == 4) {
            Opportunity::update_opportunity($quote->opportunity_id, ['stage' => 'Closed Lost']);
        } elseif ($request->status_id == 5) {
            Opportunity::update_opportunity($quote->opportunity_id, ['stage' => 'Negotiation/Review']);
        } elseif ($request->status_id == 6) {
            Opportunity::update_opportunity($quote->opportunity_id, ['stage' => 'Closed Won']);
        }
    }
    public function add_comment(Request $request, Quote $quote)
    {
        $comment = QuoteComment::create([
            'quote_id'  => $quote->id,
            'comment'   => $request->comment,
            'status_id' => $request->status_id,
        ]);
    }
    public function add_participant(Request $request, Quote $quote)
    {
        $user          = User::find($request->participant_id);
        $already_added = $quote->participants()->where('user_id', $user->id)->first();
        if (!$already_added) {
            $participant           = new QuoteParticipant();
            $participant->quote_id = $quote->id;
            $participant->user_id  = $user->id;
            $participant->read     = 1;
            $participant->write    = $request->write ? 1 : 0;
            $participant->delete   = $request->destroy ? 1 : 0;
            $participant->save();
            $response = [
                'msg' => "Ahora {$user->full_name} esta siguiendo esta cotización",
            ];
            return response()->json($response);
        } else {
            $response = [
                'error' => "{$user->full_name} ya esta siguiendo esta cotización",
            ];
            return response()->json($response, 400);
        }
    }
    public function import_products(UploadProductsToQuoteRequest $request, Quote $quote)
    {
        $file      = $request->file('file');
        $extension = $request->file('file')->extension();
        $filename  = hexdec(uniqid()) . '.' . $extension;
        $file->storeAs('/excel', $filename, 'uploads');
        return response()->json($filename, 200);
    }
    public function extract_products(Request $request)
    {
        $import  = new UploadProductsToQuote();
        $extract = Excel::import($import, 'excel/' . $request->filename, 'uploads');
        $status  = $import->getStatus();
        if ($status['status']) {
            return response()->json('Listo', 200);
        } else {
            $response = [
                'errors' => $status['errors'],
            ];
            return response()->json($response, 400);
        }
    }
    function clone (Quote $quote) {
        $clone            = $quote->replicate();
        $clone->status_id = 0;
        $clone->copy_of   = $quote->id;
        $clone->save();
        foreach ($quote->groups as $group) {
            $clone_group           = $group->replicate();
            $clone_group->quote_id = $clone->id;
            $clone_group->save();
            foreach ($group->products as $product) {
                $clone_product                 = $product->replicate();
                $clone_product->quote_id       = $clone->id;
                $clone_product->quote_group_id = $clone_group->id;
                $clone_product->save();
            }
            foreach ($group->resources as $resource) {
                $clone_resource                 = $resource->replicate();
                $clone_resource->quote_id       = $clone->id;
                $clone_resource->quote_group_id = $clone_group->id;
                $clone_resource->save();
            }
        }
        foreach ($quote->product_categories as $product_category) {
            $clone_product_category           = $product_category->replicate();
            $clone_product_category->quote_id = $clone->id;
            $clone_product_category->save();
        }
        $quote->current_version = 0;
        $quote->save();
        $response = [
            'route' => route('cotizaciones.show', $clone),
        ];
        return response()->json($response);
    }
    public function export_to_pdf(Request $request, Quote $quote)
    {
        $data = [
            'quote' => $quote,
        ];
        if($request->has('new_format')){
            $pdf      = PDF::loadView('dbpi.pdf.new-quote-format', $data);
        }else{
            $pdf      = PDF::loadView('dbpi.pdf.quote', $data);
        }
        $pdf_name = \Str::slug($quote->number . ' ' . $quote->description);
        return $pdf->stream($pdf_name . '.pdf');
    }
    public function export_margin_to_pdf(Request $request, Quote $quote)
    {
        $data = ['quote' => $quote];
        $pdf  = PDF::loadView('dbpi.pdf.margin', $data)->setPaper('letter', 'landscape');
        return $pdf->stream("{$quote->number}-Margen.pdf");
    }
    public function generate_fup(Request $request, Quote $quote)
    {
        $repository = null;
        if (!$quote->fup_repository) {
            $fup_repository           = new FupRepository();
            $fup_repository->quote_id = $quote->id;
            $fup_repository->save();
            $repository = $fup_repository;
        } else {
            $repository = $quote->fup_repository;
        }
        ##BUSCAMOS LA CUENTA RELACIONADA CON LA COTIZACION
        $account = Account::single($quote->account_id);
        ##BUSCAMOS LA OPORTUNIDAD RELACIONADA CON LA COTIZACION
        $opportunity = Opportunity::single($quote->opportunity_id);
        if (isset($opportunity['generalTerms'])) {
            $partial_deliveries = isset($account['partialDeliveries']) ? 1 : 0;
            $delivery_monday    = isset($account['deliveryDays']) ? in_array('Lunes', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_tuesday   = isset($account['deliveryDays']) ? in_array('Martes', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_wednesday = isset($account['deliveryDays']) ? in_array('Miercoles', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_thursday  = isset($account['deliveryDays']) ? in_array('Jueves', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_friday    = isset($account['deliveryDays']) ? in_array('Viernes', $account['deliveryDays']) ? 1 : 0 : 0;
        } else {
            $partial_deliveries = isset($opportunity['partialDeliveries']) ? 1 : 0;
            $delivery_monday    = isset($opportunity['deliveryDays']) ? in_array('Lunes', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_tuesday   = isset($opportunity['deliveryDays']) ? in_array('Martes', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_wednesday = isset($opportunity['deliveryDays']) ? in_array('Miercoles', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_thursday  = isset($opportunity['deliveryDays']) ? in_array('Jueves', $account['deliveryDays']) ? 1 : 0 : 0;
            $delivery_friday    = isset($opportunity['deliveryDays']) ? in_array('Viernes', $account['deliveryDays']) ? 1 : 0 : 0;
        }
        foreach($repository->fups as $prev_fup){
            $prev_fup->version_actual = 0;
            $prev_fup->save();
        }
        #CREAMOS LA FUP
        $fup                                         = new Fup();
        $fup->fup_repository_id                      = $repository->id;
        $fup->version                                = $repository->fups()->count() + 1;
        $fup->fup_id                                 = hexdec(uniqid());
        $fup->quote_id                               = $quote->id;
        $fup->fup_description                        = $quote->fup_description;
        $fup->date                                   = Carbon::now();
        $fup->mub                                    = $quote->margin_percent;
        $fup->comercial_name                         = $quote->account_name;
        $fup->business_name_shipment                 = $quote->account_name;
        $fup->business_name_billing                  = $quote->account_name;
        $fup->contact_name                           = $quote->contact_name;
        $fup->contact_email                          = $quote->contact_email;
        $fup->contact_phone                          = $quote->contact_phone;
        $fup->business_name_shipment                 = $account['razonSocial'];
        $fup->shipment_address                       = $account['shippingAddressStreet'];
        $fup->shipment_city_state                    = $account['shippingAddressCity'] . ', ' . $account['shippingAddressState'];
        $fup->shipment_zip_code                      = $account['shippingAddressPostalCode'];
        $fup->business_name_billing                  = $account['razonSocial'];
        $fup->billing_address                        = $account['billingAddressStreet'];
        $fup->billing_city_state                     = $account['billingAddressCity'] . ', ' . $account['billingAddressState'];
        $fup->billing_rfc                            = $account['rfc'];
        $fup->billing_zip_code                       = $account['billingAddressPostalCode'];
        $fup->delivery_time                          = $quote->delivery_time;
        $fup->partial_deliveries                     = $partial_deliveries;
        $fup->delivery_monday                        = $delivery_monday;
        $fup->delivery_tuesday                       = $delivery_tuesday;
        $fup->delivery_wednesday                     = $delivery_wednesday;
        $fup->delivery_thursday                      = $delivery_thursday;
        $fup->delivery_friday                        = $delivery_friday;
        $fup->delivery_schedules                     = isset($account['deliverySchedules']) ? join(', ', $account['deliverySchedules']) : null;
        $fup->payment_conditions_advance_percent     = $opportunity['termsAdvancePercent'];
        $fup->payment_conditions_advance_days        = $opportunity['termsAdvanceDays'];
        $fup->payment_conditions_delivery_percent    = $opportunity['termsDeliveryPercent'];
        $fup->payment_conditions_delivery_days       = $opportunity['termsDeliveryDays'];
        $fup->payment_conditions_instalation_percent = $opportunity['termsInstalationPercent'];
        $fup->payment_conditions_instalation_days    = $opportunity['termsInstalationDays'];
        $fup->final_client_business_name             = $account['razonSocial'];
        $fup->final_client_address                   = $account['shippingAddressStreet'];
        $fup->final_client_city_state                = $account['shippingAddressCity'] . ', ' . $account['shippingAddressState'];
        $fup->final_client_zip_code                  = $account['shippingAddressPostalCode'];
        $fup->final_client_account_executive         = $account['assignedUserName'];
        $fup->payment_method                         = $account['metodoDePago'];
        $fup->payment_form                           = $account['formaDePago'];
        $fup->cfdi                                   = $account['usoDeCFDI'];
        $fup->id_etapa                               = 1;
        $fup->version_actual                         = 1;
        $fup->save();
        #SE CREA UNA OBSERVACION CON LA INFORMACION DE METODO DE PAGO.
        $payment_observation              = new FupObservation();
        $payment_observation->fup_id      = $fup->id;
        $payment_observation->description = "Metodo de pago: {$fup->payment_method}, Forma de pago: {$fup->payment_form}, Uso de CFDI: {$fup->cfdi}";
        $payment_observation->save();
        Opportunity::update_opportunity($quote->opportunity_id, ['fup' => $fup->number]);
        $route = route('fuprep.fups.show', [$repository, $fup]);
        return response()->json($route);
    }
}