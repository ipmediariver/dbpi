<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Fup;
use App\COMERCIAL\FupRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class FupRepositoryController extends Controller
{
    public function __construct()
    {
    }
    public function index(Request $request)
    {
        $fupreps = null;
        if ($request->has('num')) {
            $fupreps = $this->obtener_fup_por_numero($request);
        } elseif (count($request->except(['num', 'page']))) {
            $fupreps = $this->obtener_fups_a_traves_de_parametros($request);
        } else {
            $fupreps = $this->obtener_todas_las_fups();
        }
        return view('dbpi.comercial.fups.index', compact('fupreps'));
    }
    public function obtener_fups_a_traves_de_parametros($request)
    {
        $id_de_cuentas  = collect(json_decode($request->c))->pluck('account_id');
        $autorizaciones = collect(json_decode($request->a));
        $facturacion       = collect(json_decode($request->f));

        $fups = FupRepository::whereHas('quote', function ($cotizaciones) use ($id_de_cuentas, $facturacion) {
            $this->filtrar_repositorios_de_fups_por_cotizaciones($cotizaciones, $id_de_cuentas, $facturacion);
        })->whereHas('fups', function ($fups) use ($autorizaciones) {
            $this->buscar_fups_por_autorizaciones($fups, $autorizaciones);
        })->orderBy('created_at', 'desc')->paginate(15);

        return $fups;
    }

    public function filtrar_repositorios_de_fups_por_cotizaciones($cotizaciones, $id_de_cuentas, $facturacion){

        $cotizaciones
        ->when(count($id_de_cuentas), function($cotizaciones) use ($id_de_cuentas) {
            $cotizaciones->whereIn('account_id', $id_de_cuentas);
        })
        ->when($facturacion['sin_facturar'], function($cotizaciones){
            $cotizaciones->where('etapa_de_facturacion', 1);
        })
        ->when($facturacion['en_proceso'], function($cotizaciones){
            $cotizaciones->where('etapa_de_facturacion', 2);
        })
        ->when($facturacion['facturadas'], function($cotizaciones){
            $cotizaciones->where('etapa_de_facturacion', 3);
        });
    }
    
    public function buscar_fups_por_autorizaciones($fups, $autorizaciones){
        if ($autorizaciones['pendientes'] && !$autorizaciones['autorizadas']) {
            $fups->where('id_etapa', 2)->where('version_actual', '1');
        }elseif($autorizaciones['autorizadas'] && !$autorizaciones['pendientes']){
            $fups->where('id_etapa', 3)->where('version_actual', '1');
        }
    }

    public function obtener_fup_por_numero($request)
    {
        $fup = FupRepository::where('id', $request->num)->paginate(15);
        return $fup;
    }
    public function obtener_todas_las_fups()
    {
        $fups = FupRepository::orderBy('created_at', 'desc')->paginate(15);
        return $fups;
    }
}