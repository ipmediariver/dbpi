<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteGroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class QuoteGroupController extends Controller
{
    public function __construct(){
        $this->middleware('modify_quote')->only([
            'store',
            'update',
            'destroy',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Quote $quote)
    {
        $groups = $quote->groups->load('contratos');
        return response()->json($groups);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Quote $quote)
    {
        $group = new QuoteGroup();
        $group->quote_id = $quote->id;
        $group->description = $request->description;
        $group->hide_name = $request->hide_name;
        $group->hide_in_quote = $request->hide_in_quote;
        $group->hide_items = $request->hide_items;
        $group->exclude_on_margin = $request->exclude_on_margin;
        $group->save();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote, QuoteGroup $group)
    {
        $group->update($request->all());
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Quote $quote, QuoteGroup $group)
    {
        $group->products()->delete();
        $group->resources()->delete();
        $group->delete();
    }
}