<?php
namespace App\Http\Controllers\COMERCIAL;

use App\COMERCIAL\ContratoEnCotizacion;
use App\COMERCIAL\Product;
use App\COMERCIAL\ProductType;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteGroup;
use App\COMERCIAL\QuoteProduct;
use App\COMERCIAL\QuoteProductCategory;
use App\COMPRAS\Provider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class QuoteProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('modify_quote')->only([
            'store',
            'update',
            'destroy',
            'set_custom_margin_percent',
            'set_custom_margin_total',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Quote $quote, QuoteGroup $group)
    {
        $validate = $this->validamos_que_el_producto_seleccionado_sea_valido($request);
        $producto = $this->agregar_producto_a_cotizacion($request, $quote, $group);
        $this->validar_si_el_producto_es_un_contrato($producto);
        return response()->json($producto);
    }
    public function validar_si_el_producto_es_un_contrato($producto)
    {
        $es_un_contrato = $producto->product->id_plantilla_de_contrato;
        if ($es_un_contrato) {
            $contrato_en_cotizacion                            = new ContratoEnCotizacion();
            $contrato_en_cotizacion->id_producto_en_cotizacion = $producto->id;
            $contrato_en_cotizacion->id_cotizacion             = $producto->quote_id;
            $contrato_en_cotizacion->id_grupo_de_cotizacion    = $producto->quote_group_id;
            $contrato_en_cotizacion->id_contrato               = $producto->product->plantilla_de_contrato->id_contrato;
            $contrato_en_cotizacion->id_cobertura              = $producto->product->plantilla_de_contrato->id_cobertura;
            $contrato_en_cotizacion->id_tiempo_de_respuesta    = $producto->product->plantilla_de_contrato->id_tiempo_de_respuesta;
            $contrato_en_cotizacion->mostrar_costo             = 0;
            $contrato_en_cotizacion->save();
        }
    }
    public function validamos_que_el_producto_seleccionado_sea_valido($request)
    {
        Validator::make($request->all(), [
            'product_id' => 'exists:products,id',
        ], [
            'product_id.exists' => 'No se ha encontrado un producto con ese nombre, intenta crear un nuevo producto.',
        ])->validate();
    }
    public function agregar_producto_a_cotizacion($request, $quote, $group)
    {
        $quote_product                               = new QuoteProduct();
        $quote_product->quote_id                     = $quote->id;
        $quote_product->quote_group_id               = $group->id;
        $quote_product->qty                          = $request->qty;
        $quote_product->provider_cost                = $request->provider_cost;
        $quote_product->provider_discount            = $request->provider_discount;
        $quote_product->provider_special_discount    = $request->provider_special_discount;
        $quote_product->customer_discount            = $request->customer_discount;
        $quote_product->hide_in_quote                = $request->hide_in_quote;
        $quote_product->exclude_on_margin            = $request->exclude_on_margin;
        $quote_product->alias                        = $request->alias;
        $quote_product->show_image_on_quote          = $request->show_image_on_quote;
        $quote_product->show_specifications_on_quote = $request->show_specifications_on_quote;
        $quote_product->show_code                    = $request->show_code;
        $quote_product->shipping_cost                = $request->shipping_cost;
        $quote_product->shipping_cost_by_piece       = $request->shipping_cost_by_piece;
        $quote_product->acc_list                     = $request->acc_list;
        $quote_product->provider_id                  = $this->se_define_el_proveedor_del_producto_o_se_crea_uno_nuevo($request);
        $quote_product->product_id                   = $this->buscamos_el_producto_relacionado_o_bien_creamos_uno_nuevo($request, $quote);
        $quote_product->save();
        return $quote_product;
    }
    public function buscamos_el_producto_relacionado_o_bien_creamos_uno_nuevo($request, $quote)
    {
        $id_de_producto = null;
        if ($request->has('new_product')) {
            $product                = app(ProductController::class)->store_from_quote($request);
            $id_de_producto         = $product->id;
            $quote_product_category = QuoteProductCategory::firstOrCreate([
                'quote_id'    => $quote->id,
                'category_id' => $product->category->id,
            ]);
        } else {
            $id_de_producto = $request->product_id;
        }
        return $id_de_producto;
    }
    public function se_define_el_proveedor_del_producto_o_se_crea_uno_nuevo($request)
    {
        $id_de_proveedor = null;
        if ($request->has('new_provider')) {
            $provider = Provider::firstOrCreate([
                'name' => $request->new_provider_name,
            ]);
            $id_de_proveedor = $provider->id;
        } else {
            $id_de_proveedor = $request->provider_id;
        }
        return $id_de_proveedor;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote, QuoteGroup $group, QuoteProduct $quote_product)
    {
        $validate = Validator::make($request->all(), [
            'product_id' => 'exists:products,id',
        ], [
            'product_id.exists' => 'No se ha encontrado un producto con ese nombre, intenta crear un nuevo producto.',
        ])->validate();
        $quote_product->update($request->all());
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quote $quote, QuoteGroup $group, QuoteProduct $quote_product)
    {
        if ($quote_product->contrato) {
            $quote_product->contrato->delete();
        }
        $quote_product->delete();
    }
    public function product_dependencies()
    {
        $products  = Product::all();
        $providers = Provider::all();
        $types     = ProductType::all()->load('brands.models');
        $response  = [
            'products'      => $products,
            'providers'     => $providers,
            'product_types' => $types,
        ];
        return response()->json($response);
    }
    public function set_custom_margin_percent(Request $request, Quote $quote, QuoteGroup $group, QuoteProduct $quote_product)
    {
        $margin                = $request->custom_margin_percent;
        $provider_total_import = $this->add_shipping_cost_to_provider_import($quote_product);
        $new_custom_import     = (($margin / 100) + 1) * $provider_total_import;
        $new_custom_import     = number_format($new_custom_import, 2, '.', '');
        $quote_product->update([
            'custom_import' => $new_custom_import,
        ]);
    }
    public function set_custom_margin_total(Request $request, Quote $quote, QuoteGroup $group, QuoteProduct $quote_product)
    {
        $margin                = $request->custom_margin_total;
        $provider_total_import = $this->add_shipping_cost_to_provider_import($quote_product);
        $new_custom_import     = $provider_total_import + $margin;
        $new_custom_import     = number_format($new_custom_import, 2, '.', '');
        $quote_product->update([
            'custom_import' => $new_custom_import,
        ]);
    }
    public function set_custom_customer_cost(Request $request, Quote $quote, QuoteGroup $group, QuoteProduct $quote_product)
    {
        $qty    = $quote_product->qty;
        $import = $request->custom_customer_cost;
        if ($qty > 0 && $import > 0) {
            $new_custom_import = ($import * $qty);
            $quote_product->update([
                'custom_import' => $new_custom_import,
            ]);
        }
    }
    public function add_shipping_cost_to_provider_import($quote_product)
    {
        $provider_import = $quote_product->provider_total_import;
        // VERIFICAMOS SI HAY COSTO DE ENVIO
        if ($quote_product->shipping_cost) {
            $shipping = $quote_product->shipping_cost;
            $qty      = $quote_product->qty;
            if ($quote_product->shipping_cost_by_piece) {
                $provider_import = $provider_import + ($shipping * $qty);
            } else {
                $provider_import = $provider_import + $shipping;
            }
        }
        return $provider_import;
    }

    public function detalles_adicionales(Request $request, QuoteProduct $producto){
        $producto->detalles = $request->detalles;
        $producto->save();
    }
}
