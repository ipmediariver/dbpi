<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\ContratoEnCotizacion;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteGroup;
use App\Http\Controllers\Controller;
use App\SERVICIO\FechaDeServicio;
use App\SERVICIO\TipoDeServicio;
use App\COMERCIAL\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
class ContratoEnCotizacionController extends Controller
{
    public function guardar(Request $request, Quote $cotizacion, QuoteGroup $grupo)
    {
        $this->validar_datos_de_formulario($request);
        $contrato  = $this->asignamos_el_contrato_a_la_cotizacion($request, $cotizacion, $grupo);
        $respuesta = [
            'mensaje' => 'Se ha agregado el contrato a la cotización exitosamente.',
        ];
        return response()->json($respuesta);
    }
    public function actualizar(Request $request, Quote $cotizacion, QuoteGroup $grupo, ContratoEnCotizacion $contrato)
    {
        $this->validar_datos_de_formulario($request);
        $contrato  = $this->se_agregan_los_datos_obtenidos_al_contrato_y_se_guarda($request, $contrato);
        $respuesta = [
            'mensaje' => 'El contrato ha sido actualizado.',
        ];
        return response()->json($respuesta);
    }
    public function actualizar_costo(Request $request, ContratoEnCotizacion $contrato)
    {
        $costo           = $this->convertir_costo_al_tipo_de_cambio($request, $contrato);
        $contrato->costo = $costo;
        $contrato->save();
    }
    public function convertir_costo_al_tipo_de_cambio($request, $contrato)
    {
        $costo = $request->costo;
        if ($contrato->cotizacion->currency->short_name == 'MXN') {
            $tipo_de_cambio = $contrato->cotizacion->exchange_rate;
            $costo          = ($costo / $tipo_de_cambio);
        }
        return $costo;
    }
    public function eliminar(Request $request, Quote $cotizacion, QuoteGroup $grupo, ContratoEnCotizacion $contrato)
    {
        $contrato->delete();
    }
    public function activar(Request $request, ContratoEnCotizacion $contrato)
    {
        $fecha_de_inicio           = Carbon::parse($request->fecha_de_inicio);
        $contrato->fecha_de_inicio = $fecha_de_inicio;
        $contrato->etapa           = 1;
        $contrato->configuracion   = json_encode($request->except('fecha_de_inicio'));
        $contrato->save();
    }
    public function eliminar_desde_fup(ContratoEnCotizacion $contrato){
        $fechas = $contrato->fechas_de_servicio->each(function($fecha){
            $fecha->delete();
        });
        $contrato->delete();
    }
    public function agendar_servicios_de_mantenimiento(Request $request, ContratoEnCotizacion $contrato)
    {
        $fechas         = $this->obtener_fechas_en_segmentos_de_contrato($contrato);
        $mantenimientos = $this->enlistar_mantenimientos_de_contrato($contrato);
        foreach ($fechas as $key => $fecha) {
            $this->agendar_mantenimiento($mantenimientos[$key], $fecha, $contrato);
        }
        $respuesta = [
            'contrato' => $contrato->load('fechas_de_servicio'),
        ];
        return response()->json($respuesta);
    }
    public function reagendar_servicios_de_mantenimiento(Request $request, ContratoEnCotizacion $contrato)
    {
        $fechas         = $this->obtener_fechas_en_segmentos_de_contrato($contrato);
        $mantenimientos = $contrato->fechas_de_servicio()->orderBy('fecha')->get();
        foreach ($fechas as $key => $fecha) {
            $this->reagendar_mantenimiento($mantenimientos[$key], $fecha);
        }
        $respuesta = [
            'contrato' => $contrato->load('fechas_de_servicio'),
        ];
        return response()->json($respuesta);
    }
    public function enlistar_mantenimientos_de_contrato($contrato)
    {
        $mantenimientos         = [];
        $mantenimientos_menores = $contrato
            ->componentes_de_contrato
            ->where('descripcion', 'Mantenimiento Menor');
        $mantenimientos_mayores = $contrato
            ->componentes_de_contrato
            ->where('descripcion', 'Mantenimiento Mayor');
        foreach ($mantenimientos_menores as $mantenimiento_menor) {
            $cantidad = $mantenimiento_menor->cantidad;
            for ($i = 0; $i < $cantidad; $i++) {
                array_push($mantenimientos, 'Mantenimiento menor');
            }
        }
        foreach ($mantenimientos_mayores as $mantenimiento_mayor) {
            $cantidad = $mantenimiento_mayor->cantidad;
            for ($i = 0; $i < $cantidad; $i++) {
                array_push($mantenimientos, 'Mantenimiento mayor');
            }
        }
        return $mantenimientos;
    }
    public function obtener_fechas_en_segmentos_de_contrato($contrato)
    {
        $total_de_segmentos = $contrato
            ->componentes_de_contrato
            ->whereIn('descripcion', ['Mantenimiento Mayor', 'Mantenimiento Menor'])
            ->sum('cantidad') + 1;
        $duracion_de_segmento = ($contrato->meses_de_contrato / $total_de_segmentos);
        $fechas               = [];
        for ($i = 0; $i < $total_de_segmentos; $i++) {
            if ($i > 0) {
                $meses = ($duracion_de_segmento * $i);
                $fecha = Carbon::parse($contrato->fecha_de_inicio)->addMonths($meses)->next(Carbon::MONDAY);
                array_push($fechas, $fecha);
            }
        }
        return $fechas;
    }
    public function agendar_mantenimiento($mantenimiento, $fecha, $contrato)
    {
        $tipo_de_servicio = TipoDeServicio::firstOrCreate([
            'nombre' => $mantenimiento,
        ]);
        $id_cliente                                                    = $contrato->cotizacion->account_id;
        $fecha_de_servicio_de_mantenimiento                            = new FechaDeServicio();
        $fecha_de_servicio_de_mantenimiento->fecha                     = $fecha;
        $fecha_de_servicio_de_mantenimiento->id_cliente                = $id_cliente;
        $fecha_de_servicio_de_mantenimiento->id_servicio               = $tipo_de_servicio->id;
        $fecha_de_servicio_de_mantenimiento->id_contrato_en_cotizacion = $contrato->id;
        $fecha_de_servicio_de_mantenimiento->save();
    }
    public function reagendar_mantenimiento($fecha_de_servicio_de_mantenimiento, $fecha)
    {
        $fecha_de_servicio_de_mantenimiento->fecha = $fecha;
        $fecha_de_servicio_de_mantenimiento->save();
    }
    public function asignamos_el_contrato_a_la_cotizacion($request, $cotizacion, $grupo)
    {
        $contrato                         = new ContratoEnCotizacion();
        $contrato->id_cotizacion          = $cotizacion->id;
        $contrato->id_grupo_de_cotizacion = $grupo->id;
        $contrato                         = $this->se_agregan_los_datos_obtenidos_al_contrato_y_se_guarda($request, $contrato);
        return $contrato;
    }
    public function se_agregan_los_datos_obtenidos_al_contrato_y_se_guarda($request, $contrato)
    {
        $contrato->id_contrato            = $request->id_contrato;
        $contrato->id_cobertura           = $request->id_cobertura;
        $contrato->id_tiempo_de_respuesta = $request->id_tiempo_de_respuesta;
        $contrato->mostrar_costo          = $request->mostrar_costo;
        $contrato->costo                  = $request->costo;
        $contrato->save();
        return $contrato;
    }
    public function cargar_productos_de_tipo_contrato(){
        $productos_de_tipo_contrato = Product::whereHas('plantilla_de_contrato')->with('plantilla_de_contrato')->get();
        return $productos_de_tipo_contrato;
    }
    public function validar_datos_de_formulario($request)
    {
        $validacion = Validator::make($request->all(), [
            'id_contrato'            => 'required|integer|exists:contratos,id',
            'id_cobertura'           => 'required|integer|exists:coberturas,id',
            'id_tiempo_de_respuesta' => 'required|integer|exists:tiempo_de_respuestas,id',
            'fecha.dia'              => 'nullable|numeric|min:1|max:31',
            'fecha.mes'              => 'nullable|numeric|min:1|max:12',
            'fecha.ano'              => 'nullable|numeric',
            'mostrar_costo'          => 'boolean',
            'costo'                  => 'min:0|numeric',
        ], [
            'id_contrato.required'            => 'Por favor selecciona un contrato',
            'id_contrato.integer'             => 'El contrato no es válido',
            'id_contrato.exists'              => 'El contrato no existe',
            'id_cobertura.required'           => 'Por favor selecciona una cobertura',
            'id_cobertura.integer'            => 'La cobertura no es válida',
            'id_cobertura.exists'             => 'La cobertura no existe',
            'id_tiempo_de_respuesta.required' => 'Por favor selecciona un tiempo de respuesta (SLA)',
            'id_tiempo_de_respuesta.integer'  => 'El tiempo de respuesta no es válido',
            'id_tiempo_de_respuesta.exists'   => 'El tiempo de respuesta no existe',
            'fecha.dia.numeric'               => 'El día no es válido',
            'fecha.dia.min'                   => 'El día no puede ser menor que 1',
            'fecha.dia.max'                   => 'El día no puede ser mayor a 31',
            'fecha.mes.numeric'               => 'El més no es válido',
            'fecha.mes.min'                   => 'El més no puede ser menor que 1',
            'fecha.mes.max'                   => 'El més no puede ser mayor a 12',
            'fecha.ano.numeric'               => 'El año no es válido',
            'mostrar_costo'                   => 'Define si el contrato debe mostrar costo o no',
            'costo.min'                       => 'El costo no puede ser menor a 0',
            'costo.numeric'                   => 'El costo debe ser numerico',
        ])->validate();
    }
    public function actualizar_vigencia(Request $request, ContratoEnCotizacion $contrato)
    {
        $cuando_inicia             = Carbon::parse($request->inicia);
        $contrato->fecha_de_inicio = $cuando_inicia;
        $contrato->save();
        $respuesta = [
            'mensaje'  => 'La fecha de inicio de contrato ha sido modificada.',
            'contrato' => $contrato->load('fechas_de_servicio'),
        ];
        return $respuesta;
    }
}