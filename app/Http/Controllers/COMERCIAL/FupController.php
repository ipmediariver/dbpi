<?php
namespace App\Http\Controllers\COMERCIAL;
use App\COMERCIAL\Fup;
use App\COMERCIAL\FupCancellation;
use App\COMERCIAL\FupComment;
use App\COMERCIAL\FupCommentVersion;
use App\COMERCIAL\FupConfirmation;
use App\COMERCIAL\FupDepartmentAuth;
use App\COMERCIAL\FupObservation;
use App\COMERCIAL\FupRepository;
use App\COMERCIAL\QuoteParticipant;
use App\Email;
use App\Http\Controllers\Controller;
use App\Notifications\FupAuthRequest;
use App\Notifications\FupCancellationAuthorized;
use App\Notifications\FupCancellationRequest;
use App\Notifications\NewFupComment;
use App\RRHH\Department;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;
use Zip;
class FupController extends Controller
{
    public function __construct()
    {
        // $this->middleware('fup_owner')->only('show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FupRepository $fuprep)
    {
        return view('dbpi.comercial.fups.versions', compact('fuprep'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FupRepository $fuprep, Fup $fup)
    {

        $respuesta = null;

        if (request()->has('confirm')) {
            $id   = decrypt(request()->confirm);
            $user = User::find($id);
            $already_confirmed = $fup->confirmations()->where('user_id', $user->id)->first();
            if (!$already_confirmed) {
                $confirmation          = new FupConfirmation();
                $confirmation->fup_id  = $fup->id;
                $confirmation->user_id = $user->id;
                $confirmation->save();
            }
        }
        if (request()->ajax()) {
            $respuesta = response()->json($fup->load('observations', 'comments'));
        } else {
            $respuesta = view('dbpi.comercial.fups.show', compact('fuprep', 'fup'));
        }

        return $respuesta;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FupRepository $fuprep, Fup $fup)
    {
        $user = auth()->user();
        $fup->update($request->except('_method'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function new_observation(Request $request, Fup $fup)
    {
        $observation              = new FupObservation();
        $observation->fup_id      = $fup->id;
        $observation->description = $request->description;
        $observation->save();
    }
    public function remove_observation(Fup $fup, FupObservation $observation)
    {
        $observation->delete();
    }
    public function share(Request $request, Fup $fup)
    {
        $departments = json_decode($request->departments);
        $quote       = $fup->quote;
        $users_ids = collect([]);
        foreach ($departments as $department) {
            $department = Department::find($department->id);
            $department_already_added = $fup->auth_departments->where('department_id', $department->id)->first();
            if (!$department_already_added) {
                $department_auth                = new FupDepartmentAuth();
                $department_auth->fup_id        = $fup->id;
                $department_auth->department_id = $department->id;
                $department_auth->save();
            }
            $department_users = $department->users()->where('supervisor', 1)->get();
            foreach ($department_users as $user) {
                $users_ids->push($user);
            }
        }
        $users_ids = array_unique($users_ids->pluck('user_id')->toArray());
        $users     = User::whereIn('id', $users_ids)->get();
        foreach ($users as $user) {
            // DEBEMOS CREAR UN REGISTRO "FUP_CONFIRMATION" A CADA USUARIO A QUIEN VAMOS A COMPARTIR LA FUP
            // EL STATUS POR DEFECTO DEBE SER 0 -> QUE SIGNIFICA QUE AUN NO HA CONFIRMADO DE RECIBIDO
            $alredy_send_a_confirmation = $fup->confirmations->where('user_id', $user->id)->first();
            if (!$alredy_send_a_confirmation) {
                $fup_confirmation            = new FupConfirmation();
                $fup_confirmation->fup_id    = $fup->id;
                $fup_confirmation->user_id   = $user->id;
                $fup_confirmation->status_id = 0;
                $fup_confirmation->save();
            }
            // DESPUES LO HACEMOS PARTICIPANTE DEL PROYECTO
            $already_added = $quote->participants()->where('user_id', $user->id)->first();
            if (!$already_added) {
                $participant           = new QuoteParticipant();
                $participant->quote_id = $quote->id;
                $participant->user_id  = $user->id;
                $participant->read     = 1;
                $participant->save();
            }
            // LE NOTIFICAMOS AL USUARIO SOBRE LA SOLICITUD DE AUTORIZACION
            $user->notify(new FupAuthRequest([
                'link' => route('fuprep.fups.show', [$fup->repository, $fup]),
                'msg'  => "Se ha solicitado tu autorización en la FUP #{$fup->number}",
            ]));
            // Y FINALMENTE CREAMOS UN CORREO PARA NOTIFICARLE VIA EMAIL
            $data = [
                'fup_id'  => $fup->id,
                'user_id' => $user->id,
                'link'    => route('fuprep.fups.show', [$fup->repository, $fup]),
            ];
            $email       = new Email();
            $email->type = 'share-fup-to-user';
            $email->to   = $user->email;
            $email->data = json_encode($data);
            $email->save();
        }
    }
    public function preview(Fup $fup)
    {
        return view('dbpi.comercial.fups.preview', compact('fup'));
    }
    public function comment(Request $request, Fup $fup)
    {
        $user = auth()->user();
        $comment              = new FupComment();
        $comment->fup_id      = $fup->id;
        $comment->author_id   = $user->id;
        $comment->description = $request->description;
        $comment->save();
        $author            = $fup->author;
        $notification_data = [
            'link' => route('fuprep.fups.show', [$fup->repository, $fup]),
            'msg'  => "{$user->first_name} ha realizado un comentario en la FUP #{$fup->number}: \n \"{$request->description}\"",
        ];
        $author->notify(new NewFupComment($notification_data));
    }
    public function version_comment(Request $request, Fup $fup)
    {
        $comment            = new FupCommentVersion();
        $comment->fup_id    = $fup->id;
        $comment->author_id = auth()->user()->id;
        $comment->body      = $request->body;
        $comment->save();
    }
    public function get_version_comments(Fup $fup)
    {
        $comments = $fup->version_comments;
        return response()->json($comments);
    }
    public function assign_responsable(Request $request, Fup $fup)
    {
        $fup->update([
            'responsable_id' => $request->user_id,
        ]);
    }
    public function confirmed(Request $request, Fup $fup)
    {
        $user         = auth()->user();
        $confirmation = $fup->confirmations->where('user_id', $user->id)->where('statud_id', 0)->first();
        if ($confirmation) {
            $confirmation->signature = $request->signature ? $request->signature : '';
            $confirmation->status_id = 1;
            $confirmation->save();
            $fup->id_etapa = $fup->etapa_id;
            $fup->save();
        }
    }
    public function download(Request $request, Fup $fup)
    {
        $data = [
            'fup'   => $fup,
            'quote' => $fup->quote,
        ];
        $pdf = PDF::loadView('dbpi.pdf.fup', $data);
        return $pdf->stream($fup->number . '.pdf');
    }
    public function request_cancellation(Request $request, Fup $fup)
    {
        $cancellation_already_exists = $fup->cancellation;
        if (!$cancellation_already_exists) {
            // Definimos al usuario en sesión
            $user = auth()->user();
            // Buscamos al director del area comercial
            $department = Department::where('name', 'Comercial')->first();
            $department_supervisors = $department->users->where('supervisor', 1);
            foreach($department_supervisors as $supervisor){
                // Le enviamos una notificacion a traves del sistema
                $data = [
                    'link'     => route('fuprep.fups.show', [$fup->repository, $fup]),
                    'msg'      => "Se ha solicitado tu autorización para cancelar la FUP #{$fup->number}",
                    'director' => $supervisor->user,
                    'user'     => $user,
                    'fup'      => $fup,
                    'reasson'  => $request->reasson,
                ];
                $supervisor->user->notify(new FupCancellationRequest($data));
                // Se crea un registro FupCancellationRequest para la cancelacion de fup
                $cancellation              = new FupCancellation();
                $cancellation->fup_id      = $fup->id;
                $cancellation->user_id     = $user->id;
                $cancellation->director_id = $supervisor->user->id;
                $cancellation->reasson     = $request->reasson;
                $cancellation->save();
            }
        } else {
            $response = [
                'errors' => [
                    ["La FUP ya se encuentra en proceso de cancelación."],
                ],
            ];
            return response()->json($response, 400);
        }
    }
    public function auth_cancellation(Request $request, Fup $fup, FupCancellation $fup_cancellation)
    {
        $quote = $fup->quote;
        $request = new Request([
            'status_id' => 4,
        ]);
        app('App\Http\Controllers\COMERCIAL\QuoteController')->modify_status($request, $quote);
        $fup_cancellation->status_id = 1;
        $fup_cancellation->save();
        $user = $fup_cancellation->user;
        $notification_data = [
            'user' => $user,
            'fup' => $fup,
            'link' => route('fuprep.fups.show', [$fup->repository, $fup]),
            'msg' => "Tu solicitud de cancelación de la FUP #{$fup->number} fue autorizada"
        ];
        $user->notify(new FupCancellationAuthorized($notification_data));
    }
    public function reject_cancellation(Request $request, Fup $fup, FupCancellation $fup_cancellation)
    {
        $fup_cancellation->status_id = 2;
        $fup_cancellation->save();
        $user = $fup_cancellation->user;
        $notification_data = [
            'user' => $user,
            'fup' => $fup,
            'link' => route('fuprep.fups.show', [$fup->repository, $fup]),
            'msg' => "Tu solicitud de cancelación de la FUP #{$fup->number} fue rechazada"
        ];
        $user->notify(new FupCancellationAuthorized($notification_data));
    }
    public function download_fup_and_quote(Request $request, Fup $fup)
    {
        $fup_number   = $fup->number;
        $quote_number = $fup->quote->number;
        // CREAMOS NUESTRA INSTANCIA DE ZIP
        $zip_name = "FUP_{$fup->number}_COTIZACION_{$fup->quote->number}.zip";
        $zip      = Zip::create(public_path('uploads/zip/' . $zip_name));
        // CREAMOS EL PDF DE LA FUP
        $fup_data = ['fup' => $fup, 'quote' => $fup->quote];
        $fup_path = public_path("uploads/zip/FUP_{$fup->number}.pdf");
        $fup_pdf  = PDF::loadView('dbpi.pdf.fup', $fup_data)->save($fup_path);
        // CREAMOS EL PDF DE LA COTIZACION
        $quote_data = ['quote' => $fup->quote];
        $quote_path = public_path("uploads/zip/COTIZACION_{$fup->quote->number}.pdf");
        $quote_pdf  = PDF::loadView('dbpi.pdf.quote', $quote_data)->save($quote_path);
        // CREAMOS EL PDF DEL MARGEN
        $margin_data = ['quote' => $fup->quote];
        $margin_path = public_path("uploads/zip/MARGEN_{$fup->quote->number}.pdf");
        $margin_pdf  = PDF::loadView('dbpi.pdf.margin', $margin_data)->setPaper('letter', 'landscape')->save($margin_path);
        // AGREGAMOS LA FUP Y LA COTIZACION AL ZIP
        $zip->add($fup_path)->add($quote_path)->add($margin_path);
        // AGREGAMOS LOS ADJUNTOS DE LA COTIZACION
        foreach ($fup->quote->attachments as $attachment) {
            $zip->add(public_path('uploads/attachments/' . $attachment->filename));
        }
        // DESCARGAMOS EL ZIP
        $headers = array(
            'Content-Type' => 'application/zip',
        );
        return redirect()->to('uploads/zip/' . $zip_name);
    }
}