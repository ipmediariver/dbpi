<?php
namespace App\Http\Controllers\COMERCIAL;

use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteGroup;
use App\COMERCIAL\QuoteResource;
use App\Http\Controllers\Controller;
use App\RRHH\JobTitle;
use Illuminate\Http\Request;

class QuoteResourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('modify_quote')->only([
            'store',
            'update',
            'destroy',
            'set_custom_margin_percent',
            'set_custom_margin_total',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Quote $quote, QuoteGroup $group)
    {
        $jobtitle       = JobTitle::find($request->resource_id);
        $resource_cost  = $jobtitle->high_salary;
        $quote_resource = QuoteResource::create([
            'quote_id'       => $quote->id,
            'quote_group_id' => $group->id,
            'qty'            => $request->qty,
            'days'           => $request->days,
            'jobtitle_id'    => $request->resource_id,
            'resource_cost'  => number_format($resource_cost, 2, '.', ''),
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote, QuoteGroup $group, QuoteResource $quote_resource)
    {
        $quote_resource->update($request->all());
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Quote $quote, QuoteGroup $group, QuoteResource $quote_resource)
    {
        $quote_resource->delete();
    }
    public function set_custom_margin_percent(Request $request, Quote $quote, QuoteGroup $group, QuoteResource $quote_resource)
    {
        $margin            = $request->custom_margin_percent;
        $total_import      = $quote_resource->total_import;
        $new_custom_import = (($margin / 100) + 1) * $total_import;
        info($new_custom_import);
        $new_custom_import = number_format($new_custom_import, 2, '.', '');
        $quote_resource->update([
            'custom_import' => $new_custom_import,
        ]);
    }
    public function set_custom_margin_total(Request $request, Quote $quote, QuoteGroup $group, QuoteResource $quote_resource)
    {
        $margin            = $request->custom_margin_total;
        $total_import      = $quote_resource->total_import;
        $new_custom_import = $total_import + $margin;
        $new_custom_import = number_format($new_custom_import, 2, '.', '');
        $quote_resource->update([
            'custom_import' => $new_custom_import,
        ]);
    }
}
