<?php
namespace App\Http\Controllers\COMERCIAL;

use App\COMERCIAL\Fup;
use App\COMERCIAL\FupRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TableroController extends Controller
{

    public function index()
    {

        if(!Fup::count()){
            return redirect()->route('comercial')->with("msg", "No se han generado FUP's aún");
        }

        $ano_actual = date('Y');
        $ano        = intval(request('ano') ?? $ano_actual);
        $primer_fecha = intval(Fup::first()->created_at->format('Y'));
        
        $fechas = [];

        for($i = $primer_fecha; $i <= $ano_actual; ++$i){

            $fecha = $i;
            array_push($fechas, $fecha);
        }
        
        $fups = Fup::where('version_actual', 1)
            ->whereYear('created_at', $ano)
            ->orderBy('created_at', 'desc')
            ->paginate(15);
        
        $registros = $this->ordenar_fups_para_tablero($fups);
        

        return view('dbpi.comercial.board.index', compact('fups', 'registros', 'fechas','ano'));
    }
    public function ordenar_fups_para_tablero($fups)
    {
        $registros = [];
        foreach ($fups as $fup) {
            $facturas = $fup->quote
                ->attachments
                ->where('type_id', 2);
            $registro = [
                'fecha'                                     => $fup->created_at->format('d M, Y'),
                'recepcion'                                 => $fup->created_at->format('d M, Y'),
                'id_de_cotizacion'                          => $fup->quote->id,
                'cotizacion'                                => '#' . $fup->quote->number,
                'orden_de_compra'                           => $fup->po_number,
                'id_de_repositorio'                         => $fup->repository->id,
                'id_de_fup'                                 => $fup->id,
                'fup'                                       => '#' . $fup->number,
                'ejecutivo'                                 => ucwords(strtolower($fup->final_client_account_executive)),
                'cliente'                                   => $fup->comercial_name,
                'proyecto'                                  => $fup->quote->description,
                'areas'                                     => $fup->auth_departments,
                'total_del_proyecto_en_pesos'               => '$' . number_format($fup->total_del_proyecto_en_pesos, 2) . ' MXN',
                'pagos_en_pesos'                            => '$' . number_format($fup->pagos_en_pesos, 2) . ' MXN',
                'facturado_pendiente_por_cobrar_en_pesos'   => '$' . number_format($fup->facturado_pendiente_por_cobrar_en_pesos, 2) . ' MXN',
                'pendiente_por_facturar_en_pesos'           => '$' . number_format($fup->pendiente_por_facturar_en_pesos, 2) . ' MXN',
                'total_del_proyecto_en_dolares'             => '$' . number_format($fup->total_del_proyecto_en_dolares, 2) . ' USD',
                'pagos_en_dolares'                          => '$' . number_format($fup->pagos_en_dolares, 2) . ' USD',
                'facturado_pendiente_por_cobrar_en_dolares' => '$' . number_format($fup->facturado_pendiente_por_cobrar_en_dolares, 2) . ' USD',
                'pendiente_por_facturar_en_dolares'         => '$' . number_format($fup->pendiente_por_facturar_en_dolares, 2) . ' USD',
                'facturas'                                  => $facturas ?? null,
                'etapa'                                     => $fup->quote->billing_total_import_badge,
            ];
            array_push($registros, $registro);
        }
        return $registros;
    }

    public function indicadores()
    {
        $moneda                         = request()->moneda;
        $ano                            = request()->ano;
        $fups                           = Fup::where('version_actual', 1)->whereYear('created_at', $ano)->get();
        $total                          = 0;
        $pagos                          = 0;
        $facturado_pendiente_por_cobrar = 0;
        $pendiente_por_facturar         = 0;
        foreach ($fups as $fup) {
            // Totales en moneda nacional (MXN)
            if (request()->moneda == 'MXN') {
                $total += $fup->total_del_proyecto_en_pesos;
                $pagos += $fup->pagos_en_pesos;
                $facturado_pendiente_por_cobrar += $fup->facturado_pendiente_por_cobrar_en_pesos;
                $pendiente_por_facturar += $fup->pendiente_por_facturar_en_pesos;
            } elseif (request()->moneda == 'USD') {
                $total += $fup->total_del_proyecto_en_dolares;
                $pagos += $fup->pagos_en_dolares;
                $facturado_pendiente_por_cobrar += $fup->facturado_pendiente_por_cobrar_en_dolares;
                $pendiente_por_facturar += $fup->pendiente_por_facturar_en_dolares;
            }
            // Totales en dolares (USD)
        }
        $respuesta = [
            'total' => number_format($total, 2, '.', ''),
            'pagos' => number_format($pagos, 2, '.', ''),
            'facturado_pendiente_por_cobrar' => number_format($facturado_pendiente_por_cobrar, 2, '.', ''),
            'pendiente_por_facturar' => number_format($pendiente_por_facturar, 2, '.', '')
        ];
        return $respuesta;
    }
}
