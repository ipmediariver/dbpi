<?php
namespace App\Http\Controllers\SERVICIO;
use App\COMERCIAL\Product;
use App\COMERCIAL\ProductCategory;
use App\COMERCIAL\QuoteProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests\PlantillaDeContratoRequest;
use App\SERVICIO\PlantillaDeContrato;
use Illuminate\Http\Request;
class PlantillaDeContratoController extends Controller
{
    public function index()
    {
        return view('dbpi.servicio.plantillas.index');
    }
    public function lista()
    {
        $plantillas = PlantillaDeContrato::all();
        return response()->json($plantillas);
    }
    public function store(PlantillaDeContratoRequest $request)
    {
        $plantilla = $this->guardar_plantilla_de_contrato($request);
        $producto  = $this->generar_nuevo_producto_relacionado_a_la_plantilla($plantilla);
        $respuesta = [
            'mensaje' => 'Se guardo la plantilla exitosamente',
        ];
        return response()->json($respuesta);
    }
    public function update(PlantillaDeContratoRequest $request, PlantillaDeContrato $plantilla)
    {
        $plantilla->update($request->all());
        $this->actualizar_producto_de_plantilla($plantilla->producto, $plantilla);
        $respuesta = [
            'mensaje' => 'La plantilla ha sido actualizada',
        ];
        return response()->json($respuesta);
    }
    public function guardar_plantilla_de_contrato($request)
    {
        $plantilla                         = new PlantillaDeContrato();
        $plantilla->nombre                 = $request->nombre;
        $plantilla->descripcion            = $request->descripcion;
        $plantilla->codigo                 = $request->codigo;
        $plantilla->id_contrato            = $request->id_contrato;
        $plantilla->id_cobertura           = $request->id_cobertura;
        $plantilla->id_tiempo_de_respuesta = $request->id_tiempo_de_respuesta;
        $plantilla->save();
        return $plantilla;
    }
    public function generar_nuevo_producto_relacionado_a_la_plantilla($plantilla)
    {
        $producto = new Product();
        $this->actualizar_producto_de_plantilla($producto, $plantilla);
        return $producto;
    }
    public function actualizar_producto_de_plantilla($producto, $plantilla)
    {
        $producto->id_plantilla_de_contrato = $plantilla->id;
        $producto->code                     = $plantilla->codigo;
        $producto->product_category_id      = $this->id_de_categoria_de_contratos();
        $producto->description              = $plantilla->nombre;
        $producto->specifications           = $plantilla->descripcion;
        $producto->save();
    }
    public function id_de_categoria_de_contratos()
    {
        $categoria_de_contratos = ProductCategory::firstOrCreate([
            'name' => 'CONTRATOS',
        ]);
        return $categoria_de_contratos->id;
    }
    public function destroy(PlantillaDeContrato $plantilla)
    {
        $respuesta = null;
    	$esta_en_uso = $this->verificamos_si_la_plantilla_esta_siendo_utilizada_por_una_cotizacion($plantilla);
        if($esta_en_uso){
        	$respuesta = [
        		'mensaje' => 'La plantilla no puede ser eliminada porque se encuentra en uso por 1 ó mas cotizaciones.'
        	];
        }else{
        	$plantilla->producto()->delete();
        	$plantilla->delete();
        	$respuesta = [
        	    'mensaje' => 'La plantilla ha sido eliminada',
        	];
        }
        return response()->json($respuesta);
    }
    public function verificamos_si_la_plantilla_esta_siendo_utilizada_por_una_cotizacion($plantilla){
    	$producto_de_contrato = QuoteProduct::where('product_id', $plantilla->producto->id)->first();
    	return $producto_de_contrato;
    }
}