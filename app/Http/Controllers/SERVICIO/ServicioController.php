<?php
namespace App\Http\Controllers\SERVICIO;

use App\CRM\Account;
use App\Http\Controllers\Controller;
use App\SERVICIO\CategoriaDeTicket;
use App\SERVICIO\FechaDeServicio;
use App\SERVICIO\Ingeniero;
use App\SERVICIO\Ticket;
use App\SERVICIO\TipoDeServicio;
use App\SeveridadDeTicket;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ServicioController extends Controller
{
    public function index()
    {
        return view('dbpi.servicio.index');
    }
    public function calendario()
    {
        return view('dbpi.servicio.calendario.index');
    }
    public function obtener_fechas_de_servicio()
    {
        $fechas = FechaDeServicio::orderBy('fecha')->paginate(10);
        return response()->json($fechas);
    }
    public function detalle_de_fecha_de_servicio(FechaDeServicio $fecha_de_servicio)
    {
        return response()->json($fecha_de_servicio);
    }
    public function guardar_servicio(Request $request)
    {

    	$tipo_de_servicio = TipoDeServicio::firstOrCreate(['nombre' => $request->tipo_de_servicio]);

        $cliente                        = Account::where('name', $request->nombre_de_cliente)->first();
        $fecha_de_servicio              = new FechaDeServicio();
        $fecha_de_servicio->fecha       = Carbon::parse($request->dia . ' ' . $request->hora);
        $fecha_de_servicio->id_cliente  = $cliente->account_id;
        $fecha_de_servicio->id_servicio = $tipo_de_servicio->id;
        $fecha_de_servicio->save();

        return response()->json($fecha_de_servicio);
    }
    public function eliminar_servicio(Request $request, FechaDeServicio $fecha_de_servicio){
        $fecha_de_servicio->delete();
        $respuesta = "La fecha de servicio ha sido eliminada.";
        return response()->json($respuesta);
    }
    public function actualizar_fecha(Request $request, FechaDeServicio $fecha_de_servicio){
        $fecha_y_hora = Carbon::parse($request->dia . ' ' . $request->hora);
        $fecha_de_servicio->fecha = $fecha_y_hora;
        $fecha_de_servicio->save();
        return response()->json($fecha_de_servicio);
    }
    public function tipos_de_servicios()
    {
        return response()->json(TipoDeServicio::all());
    }

    public function obtener_cuentas_de_clientes(){

    	$clientes = Account::get();

    	return response()->json($clientes);

    }

    public function obtener_contactos_de_cliente($id_cliente)
    {

        $contactos_en_helpdesk = \DB::connection('helpdesk')
            ->table('accounts')
            ->where('accounts.crm_account_id', $id_cliente)
            ->join('account_contacts', 'accounts.id', '=', 'account_contacts.account_id')
            ->join('contacts', 'account_contacts.contact_id', '=', 'contacts.id')
            ->join('users', 'contacts.user_id', '=', 'users.id')
            ->select('contacts.*', 'users.first_name', 'users.last_name')
            ->get();

        return response()->json($contactos_en_helpdesk);

    }
    public function obtener_ingenieros_de_servicio()
    {
        $ingenieros = Ingeniero::get();
        return response()->json($ingenieros);
    }
    public function obtener_categorias_principales()
    {
        $categorias = CategoriaDeTicket::whereNull('parent_id')->get();
        return response()->json($categorias);
    }
    public function obtener_subcategorias($categoria_padre)
    {
        $subcategorias = CategoriaDeTicket::where('parent_id', $categoria_padre)->get();
        return response()->json($subcategorias);
    }
    public function obtener_severidades_de_tickets()
    {
        $severidades = SeveridadDeTicket::get();
        return response()->json($severidades);
    }
    public function nuevo_ticket_para_fecha_de_servicio(Request $request, FechaDeServicio $fecha_de_servicio)
    {

        $cliente = \DB::connection('helpdesk')
            ->table('accounts')
            ->where('crm_account_id', $request->id_cliente)
            ->first();

        $ingeniero = Ingeniero::find($request->id_ingeniero);

        $siguiente_numero_de_ticket = str_pad((Ticket::count() + 1), 5, '0', STR_PAD_LEFT);

        $id_de_ticket = $cliente->id . $siguiente_numero_de_ticket;

        $ticket                = new Ticket();
        $ticket->ticket_id     = $id_de_ticket;
        $ticket->subject       = $request->asunto;
        $ticket->description   = $request->descripcion;
        $ticket->severity_id   = $request->id_severidad;
        $ticket->account_id    = $cliente->id;
        $ticket->contact_id    = $request->id_contacto;
        $ticket->department_id = $ingeniero->department_id;
        $ticket->agent_id      = $ingeniero->id;
        $ticket->status_id     = 1;
        $ticket->category_id   = $this->id_de_categoria_para_ticket($request);

        $ticket->save();

        $fecha_de_servicio->id_ticket = $ticket->ticket_id;
        $fecha_de_servicio->save();

    }

    public function id_de_categoria_para_ticket($request)
    {

        if ($request->categoria_3) {
            return $request->categoria_3;
        } elseif ($request->categoria_2) {
            return $request->categoria_2;
        } else {
            return $request->categoria_1;
        }

    }


    public function otros_servicios_de_cliente(FechaDeServicio $fecha_de_servicio){

    	$cliente = $fecha_de_servicio->cliente;
    	$fechas_de_servicio = $cliente->fechas_de_servicio()
    		->where('id', '<>', $fecha_de_servicio->id)
    		->orderBy('fecha')
    		->get();

    	return response()->json($fechas_de_servicio);
    }

}
