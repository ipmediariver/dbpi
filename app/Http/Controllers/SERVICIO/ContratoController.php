<?php

namespace App\Http\Controllers\SERVICIO;

use App\Http\Controllers\Controller;
use App\SERVICIO\ComponenteDeContrato;
use App\SERVICIO\Contrato;
use Illuminate\Http\Request;
use Validator;

class ContratoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dbpi.servicio.contratos.index');
    }

    public function obtener_lista_de_contratos()
    {
        $contratos = Contrato::with('componentes')->get();
        return response()->json($contratos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validar_informacion($request);

        $contrato              = new Contrato();
        $contrato->nombre      = $request->nombre;
        $contrato->descripcion = $request->descripcion;
        $contrato->duracion    = $request->duracion;
        $contrato->save();

        $this->guardar_componentes_de_contrato($request, $contrato);

        return response()->json($contrato->load('componentes'));

    }

    public function validar_informacion($request)
    {

        $validar = Validator::make($request->all(), [

            'nombre'                    => 'required|string',
            'descripcion'               => 'required|string',
            'duracion'                  => 'required|numeric',
            'componentes.*.cantidad'    => 'sometimes|required|numeric',
            'componentes.*.descripcion' => 'sometimes|required|string',
            'componentes.*.periodo'     => 'sometimes|required|string|in:Por día,Por semana,Por quincena,Por més,Cada 2 meses,Cada 3 meses,Cada 6 meses,Por año,Por contrato',

        ], [

            'nombre.required'                    => 'El nombre del contrato es requerido',
            'descripcion.required'               => 'Por favor escribe una descripción corta del contrato',
            'duracion.required'                  => 'La duración del contrado es requerida',
            'duracion.numeric'                   => 'La duración del contrato debe definirse en el número de meses',
            'componentes.*.cantidad.required'    => 'Por favor especifica la cantidad de los componentes',
            'componentes.*.cantidad.numeric'     => 'La cantidad de los componentes debe ser un número',
            'componentes.*.descripcion.required' => 'Por favor especifica la descripción de los componentes',
            'componentes.*.periodo.required'     => 'Por favor especifica el periodo de los componentes',
            'componentes.*.periodo.in'           => 'El periodo de uno ó más componentes no es válido',

        ])->validate();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SERVICIO\Contrato  $contrato
     * @return \Illuminate\Http\Response
     */
    public function show(Contrato $contrato)
    {
        return response()->json($contrato->load('componentes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SERVICIO\Contrato  $contrato
     * @return \Illuminate\Http\Response
     */
    public function edit(Contrato $contrato)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SERVICIO\Contrato  $contrato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contrato $contrato)
    {

        $this->validar_informacion($request);

        $contrato->nombre      = $request->nombre;
        $contrato->descripcion = $request->descripcion;
        $contrato->duracion    = $request->duracion;
        $contrato->save();

        $this->guardar_componentes_de_contrato($request, $contrato);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SERVICIO\Contrato  $contrato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contrato $contrato)
    {

        $contrato->delete();

    }
    public function guardar_componentes_de_contrato($request, $contrato)
    {

        $contrato->componentes()->delete();

        $componentes = $request->componentes;

        foreach ($componentes as $componente) {

            $nuevo_componente              = new ComponenteDeContrato();
            $nuevo_componente->id_contrato = $contrato->id;
            $nuevo_componente->cantidad    = $componente['cantidad'];
            $nuevo_componente->descripcion = $componente['descripcion'];
            $nuevo_componente->periodo     = $componente['periodo'];
            $nuevo_componente->save();
        }

    }

    public function eliminar_componente(Request $request, Contrato $contrato, ComponenteDeContrato $componente)
    {

        $componente->delete();

    }
}
