<?php

namespace App\Http\Controllers\SERVICIO;

use App\Http\Controllers\Controller;
use App\SERVICIO\Cobertura;
use App\SERVICIO\HorarioDeCobertura;
use App\SERVICIO\TipoDeCobertura;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class CoberturaController extends Controller
{

    public function index()
    {

        return view('dbpi.servicio.coberturas.index');

    }

    public function obtener_lista_de_coberturas()
    {
        $coberturas = Cobertura::with('horarios')->get();
        $total_de_tipos_de_cobertura = TipoDeCobertura::count();
        $respuesta = [
            'coberturas' => $coberturas,
            'total_de_tipos_de_cobertura' => $total_de_tipos_de_cobertura
        ];
        return response()->json($respuesta);
    }

    public function store(Request $request)
    {

        $this->validar_datos_de_cobertura($request);

        $cobertura = $this->crear_nueva_cobertura($request);

        return response()->json($cobertura);

    }

    public function update(Request $request, Cobertura $cobertura)
    {

        $this->validar_datos_de_cobertura($request);

        $cobertura = $this->actualizar_cobertura($request, $cobertura);

        return response()->json($cobertura);

    }

    public function crear_nueva_cobertura($request)
    {

        $cobertura = new Cobertura();

        $cobertura->nombre = $request->nombre;

        $cobertura->id_tipo_de_cobertura = $request->id_tipo_de_cobertura;

        $cobertura->save();

        $horarios_de_cobertura = $this->agregar_horarios_a_cobertura($request, $cobertura);

        return $cobertura->load('horarios');

    }

    public function actualizar_cobertura($request, $cobertura)
    {

        $cobertura->nombre = $request->nombre;

        $cobertura->id_tipo_de_cobertura = $request->id_tipo_de_cobertura;

        $cobertura->save();

        $horarios_de_cobertura = $this->agregar_horarios_a_cobertura($request, $cobertura);

        return $cobertura->load('horarios');

    }

    public function agregar_horarios_a_cobertura($request, $cobertura)
    {

        $cobertura->horarios()->delete();

        foreach ($request->horarios as $horario) {

            $dias = array_filter($horario['dias'], function ($horario) {
                return $horario;
            });

            $horario_de_cobertura                 = new HorarioDeCobertura();
            $horario_de_cobertura->id_cobertura   = $cobertura->id;
            $horario_de_cobertura->alias          = $horario['alias'];
            $horario_de_cobertura->dias           = $dias;
            $horario_de_cobertura->hora_de_inicio = Carbon::now()
                ->hour(sprintf("%02d", $horario['de']['hora']))
                ->minute(sprintf("%02d", $horario['de']['minuto']));

            $horario_de_cobertura->hora_final = Carbon::now()
                ->hour(sprintf("%02d", $horario['hasta']['hora']))
                ->minute(sprintf("%02d", $horario['hasta']['minuto']));

            $horario_de_cobertura->save();

        }

    }

    public function validar_datos_de_cobertura($request)
    {

        $validar = Validator::make($request->all(), [

            'nombre'                  => 'required|string',
            'id_tipo_de_cobertura'    => 'required|exists:tipo_de_coberturas,id',
            'horarios.*.alias'        => 'required|string',
            'horarios'                => 'required',
            'horarios.*.dias.*'       => 'required|boolean',
            'horarios.*.de.hora'      => 'required|numeric|min:1|max:23',
            'horarios.*.de.minuto'    => 'required|numeric|min:0|max:59',
            'horarios.*.hasta.hora'   => 'required|numeric|min:1|max:23',
            'horarios.*.hasta.minuto' => 'required|numeric|min:0|max:59',

        ], [

            'nombre.required'                  => 'Por favor introduce el nombre de la cobertura',
            'id_tipo_de_cobertura.required'    => 'Por favor selecciona un tipo de cobertura o crea uno en la tabla de abajo',
            'id_tipo_de_cobertura.exists'      => 'El tipo de cobertura seleccionado no es válido',
            'horarios.*.alias.required'        => 'Por favor introduce un alias al horario. Ej: Lun a Vie de 8am a 5pm',
            'horarios.*.alias.string'          => 'El alias no tiene un formato váliado',

            'horarios.required'                => 'Por favor define el horario de la cobertura',

            'horarios.*.dias.*.required'       => 'Por favor revisa que los datos esten correctos o vuelve a recargar la página',
            'horarios.*.dias.*.boolean'        => 'Los datos enviados no tienen un formato válido',

            'horarios.*.de.hora.required'      => 'Por favor define la hora de inicio en los horarios',
            'horarios.*.de.hora.numeric'       => 'La hora de inicio debe ser un número',
            'horarios.*.de.hora.min'           => 'La hora de inicio debe ser mínimo 1',
            'horarios.*.de.hora.max'           => 'La hora de inicio debe ser máximo 23',

            'horarios.*.de.minuto.required'    => 'Por favor define el minuto de inicio en los horarios',
            'horarios.*.de.minuto.numeric'     => 'El minuto de inicio debe ser un número',
            'horarios.*.de.minuto.min'         => 'El minuto de inicio debe ser mínimo 0',
            'horarios.*.de.minuto.max'         => 'El minuto de inicio debe ser máximo 59',

            'horarios.*.hasta.hora.required'   => 'Por favor define la hora final en los horarios',
            'horarios.*.hasta.hora.numeric'    => 'La hora final debe ser un número',
            'horarios.*.hasta.hora.min'        => 'La hora final debe ser mínimo 1',
            'horarios.*.hasta.hora.max'        => 'La hora final debe ser máximo 23',

            'horarios.*.hasta.minuto.required' => 'Por favor define el minuto final en los horarios',
            'horarios.*.hasta.minuto.numeric'  => 'El minuto final debe ser un número',
            'horarios.*.hasta.minuto.min'      => 'El minuto final debe ser mínimo 0',
            'horarios.*.hasta.minuto.max'      => 'El minuto final debe ser máximo 59',

        ])->validate();

    }

    public function destroy(Cobertura $cobertura)
    {
        $cobertura->delete();
    }

    public function obtener_tipos_de_cobertura()
    {
        $tipos_de_cobertura = TipoDeCobertura::get();
        return response()->json($tipos_de_cobertura);
    }

    public function guardar_tipo_de_cobertura(Request $request)
    {

        $this->validar_datos_de_tipo_de_cobertura($request);

        $tipo_de_cobertura                = new TipoDeCobertura();
        $tipo_de_cobertura->nombre        = $request->nombre;
        $tipo_de_cobertura->descripcion   = $request->descripcion;
        $tipo_de_cobertura->configuracion = $request->configuracion;

        $tipo_de_cobertura->save();

        return 'Listo';

    }

    public function eliminar_tipo_de_cobertura(TipoDeCobertura $tipo_de_cobertura)
    {
        $tipo_de_cobertura->delete();
    }

    public function validar_datos_de_tipo_de_cobertura($request)
    {

        $validar = Validator::make($request->all(), [

            'nombre'          => 'required|string',
            'descripcion'     => 'required|string',
            'configuracion' => 'json',

        ], [

            'nombre.required'         => 'Por favor ingresa el nombre del tipo de cobertura',
            'descripcion.required'    => 'Por favor escribe la descripción del tipo de cobertura',
            'configuracion' => 'Por favor revisa que la información este correcta',

        ])->validate();

    }

}
