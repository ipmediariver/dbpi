<?php

namespace App\Http\Controllers\SERVICIO;

use App\Http\Controllers\Controller;
use App\SERVICIO\TiempoDeRespuesta;
use Illuminate\Http\Request;
use Validator;

class TiempoDeRespuestaController extends Controller
{

    public function index()
    {
        return view('dbpi.servicio.tiempos-de-respuesta.index');
    }

    public function lista()
    {
        $slas = TiempoDeRespuesta::get();
        return response()->json($slas);
    }

    public function store(Request $request)
    {

        $this->validar_datos_de_tiempo_de_respuesta($request);

        $tiempo_de_respuesta = new TiempoDeRespuesta();

        $this->aplicar_datos_a_tiempo_de_respuesta($request, $tiempo_de_respuesta);

    }

    public function update(Request $request, TiempoDeRespuesta $tiempo_de_respuesta)
    {
        $this->validar_datos_de_tiempo_de_respuesta($request);

        $this->aplicar_datos_a_tiempo_de_respuesta($request, $tiempo_de_respuesta);
    }

    public function aplicar_datos_a_tiempo_de_respuesta($request, $tiempo_de_respuesta)
    {
        $tiempo_de_respuesta->nombre      = $request->nombre;
        $tiempo_de_respuesta->descripcion = $request->descripcion;
        $tiempo_de_respuesta->urgente     = $request->urgente;
        $tiempo_de_respuesta->alta        = $request->alta;
        $tiempo_de_respuesta->normal      = $request->normal;
        $tiempo_de_respuesta->baja        = $request->baja;

        $tiempo_de_respuesta->save();
    }

    public function validar_datos_de_tiempo_de_respuesta($request)
    {

        $validar = Validator::make($request->all(), [

            'nombre'      => 'required|string',
            'descripcion' => 'required|string',
            'urgente'     => 'required|numeric|min:0',
            'alta'        => 'required|numeric|min:0',
            'normal'      => 'required|numeric|min:0',
            'baja'        => 'required|numeric|min:0',

        ], [

            'nombre.required'      => 'Por favor define el nombre del tiempo de respuesta',
            'descripcion.required' => 'No dejes las descripción en blanco',

            'urgente.required'     => 'Define el tiempo de respuesta con severidad urgente',
            'urgente.numeric'      => 'El tiempo de respuesta debe ser numérico',
            'urgente.min'          => 'El número de horas no debe ser menor a 0',

            'alta.required'        => 'Define el tiempo de respuesta con severidad alta',
            'alta.numeric'         => 'El tiempo de respuesta debe ser numérico',
            'alta.min'             => 'El número de horas no debe ser menor a 0',

            'normal.required'      => 'Define el tiempo de respuesta con severidad normal',
            'normal.numeric'       => 'El tiempo de respuesta debe ser numérico',
            'normal.min'           => 'El número de horas no debe ser menor a 0',

            'baja.required'        => 'Define el tiempo de respuesta con severidad baja',
            'baja.numeric'         => 'El tiempo de respuesta debe ser numérico',
            'baja.min'             => 'El número de horas no debe ser menor a 0',

        ])->validate();

    }

    public function destroy(TiempoDeRespuesta $tiempo_de_respuesta){
    	$tiempo_de_respuesta->delete();
    }

}
