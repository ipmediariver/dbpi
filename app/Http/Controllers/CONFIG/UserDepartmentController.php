<?php

namespace App\Http\Controllers\CONFIG;

use App\Http\Controllers\Controller;
use App\RRHH\Department;
use App\RRHH\UserDepartment;
use App\User;
use Illuminate\Http\Request;

class UserDepartmentController extends Controller
{
	public function assign(Request $request, User $user){
		

		$user_is_already_assigned_to_department = $user->departments->where('department_id', $request->department_id)->first();

		if(!$user_is_already_assigned_to_department){

			$department = Department::find($request->department_id);

			$assign_department = new UserDepartment();
			$assign_department->user_id = $user->id;
			$assign_department->department_id = $department->id;

			if($request->supervisor){
				$assign_department->supervisor = 1;
			}

			$assign_department->save();

			return redirect()->back()->with('success', "Se ha asignado a {$user->full_name} al departamento {$department->name}");

		}else{

			return redirect()->back()->withErrors(['El usuario ya se encuentra asignado al departamento seleccionado.']);

		}


	}

	public function unassign(Request $request, UserDepartment $user_department){
		$username = $user_department->user->full_name;
		$department_name = $user_department->name;
		$user_department->delete();
		return redirect()->back()->with('info', "Se ha desasignado a {$username} del departamento {$department_name} exitosamente.");
	}
}
