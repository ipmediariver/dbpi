<?php
namespace App\Http\Controllers\CONFIG;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class GlobalController extends Controller
{
    public function accounts(){
    	return view('dbpi.config.accounts.sync');
    }
    public function sync_accounts(Request $request){
    	\Artisan::call('accounts:sync');
    	return redirect()->back()->with('success', "Las cuentas han sido sincronizadas correctamente.");
    }
    public function migrations(){
    	return view('dbpi.config.migrations.index');
    }
    public function run_migrations(Request $request){
    	\Artisan::call('migrate');
    	return redirect()->back()->with('success', "Se han ejecutado las migraciones correctamente.");	
    }
    public function actualizaciones(){
        return view('dbpi.config.actualizaciones.index');
    }
    public function actualizar_sistema(){
        \Artisan::call('actualizar-sistema');
        return redirect()->back()->with('success', "El sistema ha sido actualizado correctamente.");
    }
}