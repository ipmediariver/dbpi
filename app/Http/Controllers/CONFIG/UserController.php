<?php

namespace App\Http\Controllers\CONFIG;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\RRHH\Department;
use App\SYSTEM\Module;
use App\SYSTEM\ModulePermission;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('first_name')->get();
        if(request()->ajax()){
            return response()->json($users);
        }else{
            return view('dbpi.config.users.index', compact('users'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dbpi.config.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $password = $request->password;
        $user     = User::create([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'username'   => $request->username,
            'role'       => $request->role,
            'password'   => bcrypt($password),
        ]);
        return redirect()->route('usuarios.edit', $user)->with('success', 'Se ha creado al usuario exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('dbpi.config.users.show', compact('user'));
    }

    public function notifications(){
        $user = auth()->user();
        $notifications = $user->notifications->paginate(15);
        return view('dbpi.user.notifications', compact('notifications', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $departments = Department::orderBy('name')->get();
        $modules = Module::all();
        return view('dbpi.config.users.edit', compact('user', 'modules', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update($request->only('first_name', 'last_name', 'email', 'username', 'role'));
        $this->set_user_password($request, $user);
        $this->set_as_directive($request, $user);
        return redirect()->back()->with('success', 'La información del usuario ha sido actualizada');
    }
    public function set_as_directive($request, $user){
        $user->validator = $request->validator ? 1 : 0;
        $user->save();
    }
    public function set_user_password($request, $user)
    {
        if ($request->password) {
            $password       = $request->password;
            $user->password = bcrypt($password);
            $user->save();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $users_count = User::count();
        if ($users_count == 1) {
            return redirect()->back()->withErrors(['Debe haber al menos un usuario registrado']);
        } else {
            $user->delete();
            return redirect()->route('usuarios.index')->with('info', 'El usuario ha sido eliminado.');
        }
    }
    public function permissions(Request $request, User $user)
    {
        $modules = Module::all();
        $user->permissions()->delete();
        foreach ($modules as $key => $module) {
            $code       = $module->code;
            $can_read   = $code . '_read';
            $can_edit   = $code . '_edit';
            $can_delete = $code . '_delete';
            $permission = new ModulePermission();
            $permission->user_id = $user->id;
            $permission->module_id = $module->id;
            if($request->has($can_read)){
                $permission->read = 1;
            }
            if($request->has($can_edit)){
                $permission->edit = 1;
            }
            if($request->has($can_delete)){
                $permission->delete = 1;
            }
            $permission->save();
        }
        return redirect()->back()->with('success', "Se han actualizado los permisos del usuario");
    }
    public function get_validators(){
       $validators = User::where('validator', 1)->get();
       return response()->json($validators); 
    }
    public function toggle_module_sidebar(Request $request){
    }
}
