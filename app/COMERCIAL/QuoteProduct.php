<?php
namespace App\COMERCIAL;

use App\COMPRAS\Provider;
use Illuminate\Database\Eloquent\Model;

class QuoteProduct extends Model
{
    protected $fillable = [
        'quote_id',
        'quote_group_id',
        'product_id',
        'provider_id',
        'qty',
        'provider_cost',
        'provider_discount',
        'provider_special_discount',
        'customer_discount',
        'custom_import',
        'hide_in_quote',
        'alias',
        'exclude_on_margin',
        'show_image_on_quote',
        'show_specifications_on_quote',
        'show_code',
        'shipping_cost',
        'shipping_cost_by_piece',
        'acc_list',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'quote',
        'provider',
    ];
    protected $appends = [
        'category',
        'product_description',
        'provider_name',
        'provider_special_discount_cost',
        'provider_cost_with_discount',
        'provider_total_import',
        'provider_total_import_without_acc_costs',
        'real_customer_cost',
        'customer_cost',
        'customer_cost_by_unit',
        'customer_cost_by_unit_with_discount',
        'customer_cost_with_custom_import',
        'customer_cost_with_discount',
        'customer_total_import',
        'customer_total_import_on_quote',
        'custom_customer_cost',
        'margin_percent',
        'real_margin_percent',
        'margin_total',
        'real_margin_total',
        'shipping_cost_formated',
        'shipping_cost_per_unit',
        'shipping_cost_total',
        'acc_list_items',
        'acc_list_total',
        'acc_list_items_multiplied_by_qty',
    ];
    public function contrato(){
        return $this->hasOne(ContratoEnCotizacion::class, 'id_producto_en_cotizacion');
    }
    public function quote()
    {
        return $this->belongsTo(Quote::class);
    }
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function getCategoryAttribute()
    {
        $category = [
            'name'  => $this->product->category->name,
            'color' => $this->product->category->color,
        ];
        return $category;
    }
    public function getProductDescriptionAttribute()
    {
        $description = $this->alias ? $this->alias : $this->product->description;
        return $description;
    }
    public function getProviderNameAttribute()
    {
        return $this->provider->name;
    }
    public function getProviderSpecialDiscountCostAttribute()
    {
        $cost     = $this->provider_cost;
        $percent  = $this->provider_special_discount;
        $discount = ($cost * $percent) / 100;
        return $discount;
    }
    public function getProviderCostWithDiscountAttribute()
    {
        $cost     = $this->provider_cost;
        $percent  = $this->provider_discount + $this->provider_special_discount;
        $discount = ($cost * $percent) / 100;
        $total    = $cost - $discount;
        return $total;
    }
    public function getProviderTotalImportAttribute()
    {
        $sub_total = $this->provider_cost_with_discount;
        if (count($this->acc_list_items)) {
            foreach ($this->acc_list_items as $item) {
                $sub_total += $item['cost'];
            }
        }
        $qty   = $this->qty;
        $total = $sub_total * $qty;
        return $total;
    }
    public function getProviderTotalImportWithoutAccCostsAttribute()
    {
        $sub_total = $this->provider_cost_with_discount;
        $qty       = $this->qty;
        $total     = $sub_total * $qty;
        return $total;
    }
    public function getProviderTotalImportWithShippingCostAttribute()
    {
        $provider_total_import = $this->provider_total_import;
        $shipping              = $this->shipping_cost_total;
        $total                 = $provider_total_import + $shipping;
        return $total;
    }
    public function getCustomerCostAttribute()
    {
        $quote_category = $this->quote->product_categories()
            ->where('category_id', $this->product->product_category_id)
            ->first();
        $provider_cost = $this->provider_cost_with_discount;
        $qty           = $this->qty;
        // AQUI VERIFICAMOS SI LLEVA UN COSTO DE ENVIO
        if ($this->shipping_cost) {
            $shipping = $this->shipping_cost;
            if ($this->shipping_cost_by_piece) {
                $provider_cost = $provider_cost + $shipping;
            } else {
                $shipping      = $shipping / $qty;
                $provider_cost = $provider_cost + $shipping;
            }
        }
        if (count($this->acc_list_items)) {
            foreach ($this->acc_list_items as $item) {
                $provider_cost += $item['cost'];
            }
        }
        $category_percent = $quote_category ? $quote_category->percent : 0;
        $percent_total    = ($provider_cost * $category_percent) / 100;
        $total            = $provider_cost + $percent_total;
        return $total;
    }
    public function getCustomCustomerCostAttribute()
    {
        $import = $this->customer_cost;
        if ($this->custom_import) {
            $custom_customer_cost = ($this->custom_import / $this->qty);
            $import               = $custom_customer_cost;
        }
        return $import;
    }
    public function getCustomerCostWithCustomImportAttribute()
    {
        $custom_import = $this->custom_import;
        $qty           = $this->qty;
        if ($custom_import > 0 && $qty > 0) {
            $customer_cost = $custom_import / $qty;
            $customer_cost = $customer_cost;
        } else {
            $customer_cost = 0;
        }
        return $customer_cost;
    }
    public function getCustomerCostWithDiscountAttribute()
    {
        $customer_cost    = $this->customer_cost;
        $discount_percent = $this->customer_discount;
        $discount_cost    = ($customer_cost * $discount_percent) / 100;
        $total            = $customer_cost - $discount_cost;
        return $total;
    }
    public function getCustomerTotalImportAttribute()
    {
        $cost  = $this->customer_cost_with_discount;
        $qty   = $this->qty;
        $total = $cost * $qty;
        return $total;
    }
    public function getMarginPercentAttribute()
    {
        $provider_import = $this->add_shipping_cost_to_provider_import();
        $customer_import = $this->custom_import ? $this->custom_import : $this->customer_total_import;
        if ($customer_import > 0 && $provider_import > 0) {
            $total = (($customer_import / $provider_import) - 1) * 100;
        } else {
            $total = 0;
        }
        return floatval($total);
    }
    public function add_shipping_cost_to_provider_import()
    {
        $provider_import = $this->provider_total_import;
        // VERIFICAMOS SI HAY COSTO DE ENVIO
        if ($this->shipping_cost) {
            $shipping = $this->shipping_cost;
            $qty      = $this->qty;
            if ($this->shipping_cost_by_piece) {
                $provider_import = $provider_import + ($shipping * $qty);
            } else {
                $provider_import = $provider_import + $shipping;
            }
        }
        return $provider_import;
    }
    public function getRealCustomerCostAttribute()
    {
        return $this->customer_cost;
    }
    public function getRealMarginPercentAttribute()
    {
        $provider_import = $this->add_shipping_cost_to_provider_import();
        $customer_import = $this->customer_total_import;
        if ($customer_import > 0 && $provider_import > 0) {
            $total = (($customer_import / $provider_import) - 1) * 100;
        } else {
            $total = 0;
        }
        return floatval($total);
    }
    public function getMarginTotalAttribute()
    {
        $provider_import = $this->add_shipping_cost_to_provider_import();
        $customer_import = $this->custom_import ? $this->custom_import : $this->customer_total_import;
        $total           = $customer_import - $provider_import;
        return $total;
    }
    public function getRealMarginTotalAttribute()
    {
        $provider_import = $this->add_shipping_cost_to_provider_import();
        $customer_import = $this->customer_total_import;
        $total           = $customer_import - $provider_import;
        return $total;
    }
    public function getCustomerCostByUnitAttribute()
    {
        $amount        = $this->custom_import ? $this->customer_cost_with_custom_import : $this->customer_cost;
        $subtotal      = 0;
        $currency      = $this->quote->currency->short_name;
        $exchange_rate = $this->quote->exchange_rate;
        if ($this->quote->display_customer_items_discount) {
            if ($this->custom_import) {
                $percent  = (100 - $this->customer_discount);
                $subtotal = ($amount * 100) / $percent;
            } else {
                $subtotal = $amount;
            }
        } else {
            if ($this->custom_import) {
                $subtotal = $amount;
            } else {
                $percent  = $this->customer_discount;
                $discount = ($amount * $percent) / 100;
                $subtotal = $amount - $discount;
            }
        }
        if ($currency == 'MXN') {
            $subtotal = ($subtotal * $exchange_rate);
        }
        $total = $subtotal;
        return $total;
    }
    public function getCustomerCostByUnitWithDiscountAttribute()
    {
        $amount        = $this->custom_import ? $this->customer_cost_with_custom_import : $this->customer_cost;
        $subtotal      = $amount;
        $exchange_rate = $this->quote->exchange_rate;
        $currency      = $this->quote->currency->short_name;
        if (!$this->custom_import) {
            $percent  = $this->customer_discount;
            $discount = ($amount * $percent) / 100;
            $subtotal = $amount - $discount;
        }
        if ($currency == 'MXN') {
            $subtotal = ($subtotal * $exchange_rate);
        }
        $total = $subtotal;
        return $total;
    }
    public function getCustomerTotalImportOnQuoteAttribute()
    {
        $amount        = $this->custom_import ? $this->custom_import : $this->customer_total_import;
        $subtotal      = $amount;
        $currency      = $this->quote->currency->short_name;
        $exchange_rate = $this->quote->exchange_rate;
        if ($currency == 'MXN') {
            $subtotal = ($amount * $exchange_rate);
        }
        $total = $subtotal;
        return $total;
    }
    public function getShippingCostFormatedAttribute()
    {
        if ($this->shipping_cost) {
            $shipping_cost = $this->shipping_cost;
            $type          = $this->shipping_cost_by_piece ? 'Por pieza' : 'Por grupo';
            return "\${$shipping_cost} {$type}";
        } else {
            return "Sin costo de envío";
        }
    }
    public function getShippingCostPerUnitAttribute()
    {
        $shipping = $this->shipping_cost;
        if ($shipping) {
            $qty = $this->qty;
            if (!$this->shipping_cost_by_piece) {
                $shipping = $shipping / $qty;
            }
        }
        return $shipping;
    }
    public function getShippingCostTotalAttribute()
    {
        $shipping = $this->shipping_cost;
        if ($shipping) {
            $qty = $this->qty;
            if ($this->shipping_cost_by_piece) {
                $shipping = $shipping * $qty;
            }
        }
        return $shipping;
    }
    public function getAccListItemsAttribute()
    {
        $list = $this->acc_list ? json_decode($this->acc_list, true) : [];
        return $list;
    }
    public function getAccListItemsMultipliedByQtyAttribute()
    {
        $list = [];
        $qty  = $this->qty;
        if (count($this->acc_list_items)) {
            foreach ($this->acc_list_items as $item) {
                $cost                   = ($item['cost'] * $qty);
                $item_multiplied_by_qty = [
                    'concept' => $item['concept'],
                    'cost'    => $cost,
                ];
                array_push($list, $item_multiplied_by_qty);
            }
        }
        return $list;
    }
    public function getAccListTotalAttribute()
    {
        $total = 0;
        if (count($this->acc_list_items)) {
            foreach ($this->acc_list_items as $item) {
                $total += $item['cost'];
            }
        }
        return $total;
    }
}
