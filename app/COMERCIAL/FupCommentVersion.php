<?php
namespace App\COMERCIAL;
use App\User;
use Illuminate\Database\Eloquent\Model;
class FupCommentVersion extends Model
{
    protected $guarded = [];
    protected $appends = [
    	'author'
    ];
    protected $casts = [
    	'created_at' => 'datetime:Y-m-d'
    ];
    public function user(){
    	return $this->belongsTo(User::class, 'author_id', 'id');
    }
    public function getAuthorAttribute(){
    	return $this->user->full_name;
    }
}