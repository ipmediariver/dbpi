<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
class Opportunity extends Model
{
    protected $guarded = [];

    public static function prospects(){
    	$url = config('app.crm_uri') . '/Opportunity?maxSize=100&offset=0&orderBy=createdAt&order=desc';

    	$token = config('app.crm_key');
    	$opportunities = Http::withHeaders([
    		'X-Api-Key' => $token
    	])->get($url);
    	
    	return $opportunities->json()['list'];
    }

    public static function single($id){
        $url = config('app.crm_uri') . '/Opportunity/' . $id;
        $token = config('app.crm_key');
        info($url);
        $opportunity = Http::withHeaders([
            'X-Api-Key' => $token
        ])->get($url);
        return $opportunity->json();
    }

    public static function update_opportunity($id, $data){
        $url = config('app.crm_uri') . '/Opportunity/' . $id;
        $token = config('app.crm_key');
        $patch = Http::withHeaders([
            'X-Api-Key' => $token
        ])->put($url, $data);
    }

}