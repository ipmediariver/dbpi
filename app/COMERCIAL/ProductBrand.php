<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
class ProductBrand extends Model
{
    protected $guarded = [];
    public function type(){
    	return $this->belongsTo(ProductType::class, 'product_type_id');
    }
    public function models(){
    	return $this->hasMany(ProductModel::class, 'product_brand_id', 'id');
    }
}