<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
class QuoteGroup extends Model
{
    protected $guarded = [];
    protected $hidden = [
    	'created_at', 
    	'updated_at',
    	'quote_id',
    ];
    protected $appends = [
        'total_items',
        'sub_total',
        'sub_total_on_quote',
        'provider_total_import',
        'margin_percent',
        'margin_cost'
    ];
    public function quote(){
    	return $this->belongsTo(Quote::class);
    }
    public function products(){
    	return $this->hasMany(QuoteProduct::class);
    }
    public function contratos(){
        return $this->hasMany(ContratoEnCotizacion::class, 'id_grupo_de_cotizacion');
    }
    public function resources(){
        return $this->hasMany(QuoteResource::class);
    }
    public function getTotalItemsAttribute(){
        $products = $this->products()->count();
        $resources = $this->resources()->count();
        $contratos = $this->contratos()->count();
        $total = $products + $resources;
        return $total;
    }

    public static function calculate_sub_total($products, $resources, $contratos = null){

        // Sacamos el sub total de todos los contratos
        $subtotal_de_contratos = $contratos ? $contratos->where('mostrar_costo', 1)->sum('costo') : 0;

        // Sacamos el sub total de todos los productos
        $products_subtotal = 0;
        foreach ($products as $product) {
            $products_subtotal += $product->custom_import ? $product->custom_import : $product->customer_total_import;
        }
        // Sacamos el subtotal de todos los recursos
        $resources_subtotal = 0;
        foreach ($resources as $resource) {
            $resources_subtotal += $resource->custom_import > 0 ? $resource->custom_import : $resource->total_import;
        }
        // Sumamos ambos valores para obtener el subtotal
        $subtotal = $products_subtotal + $resources_subtotal;
        return $subtotal;
    }

    public function getSubTotalAttribute(){
        $contratos = $this->contratos;
        $products = $this->products;
        $resources = $this->resources;
        
        $subtotal = QuoteGroup::calculate_sub_total($products, $resources, $contratos);

        return $subtotal;
    }

    public function getSubTotalWithoutProductsAndResourcesHidedOnQuoteAttribute(){
        $contratos = $this->contratos;
        $products = $this->products->where('hide_in_quote', 0);
        $resources = $this->resources;
        
        $subtotal = QuoteGroup::calculate_sub_total($products, $resources, $contratos);

        return $subtotal;
    }

    public function getSubTotalWithoutProductsAndResourcesExcludedOnMarginAttribute(){
        $contratos = $this->contratos;
        $products = $this->products->where('hide_in_quote', 0)->where('exclude_on_margin', 0);
        $resources = $this->resources;
        
        $subtotal = QuoteGroup::calculate_sub_total($products, $resources, $contratos);

        return $subtotal;
    }

    public function getSubTotalOnQuoteAttribute(){

        $amount = $this->custom_import ? $this->custom_import : $this->sub_total_without_products_and_resources_hided_on_quote;
        $subtotal = $amount;
        $currency = $this->quote->currency->short_name;
        $exchange = $this->quote->exchange_rate;

        if($currency == 'MXN'){
            $subtotal = ($amount * $exchange);
        }

        $total = $subtotal;

        return $total;

    }

    public function getProviderTotalImportAttribute(){
        $contratos = $this->contratos;
        $products = $this->products;
        $resources = $this->resources;
        $subtotal = Self::calculate_provider_total_import($products, $resources, $contratos);
        return $subtotal;
    }

    public function getProviderTotalImportWithoutProductsAndResourcesExcludedOnMarginAttribute(){
        $products = $this->products->where('exclude_on_margin', 0);
        $resources = $this->resources;
        $subtotal = Self::calculate_provider_total_import($products, $resources);
        return $subtotal;
    }

    public static function calculate_provider_total_import($products, $resources, $contratos = null){
        // Obtenemos el subtotal de los contratos
        $subtotal_de_contratos = $contratos ? $contratos->where('mostrar_costo', 1)->sum('costo') : null;
        // Sacamos el sub total de todos los productos
        $products_subtotal = $products->sum('provider_total_import_with_shipping_cost');
        
        // Sacamos el subtotal de todos los recursos
        $resources_subtotal = $resources->sum('total_import');
        
        // Sumamos ambos valores para obtener el subtotal
        $subtotal = ($products_subtotal + $resources_subtotal);
        $subtotal = $subtotal;
        return $subtotal;
    }

    public function getMarginPercentAttribute(){

        $margin = 0;

        if($this->custom_import){
            // Definimos el porcentaje de margen personalizado
            $custom_import = $this->custom_import;
            $provider_import = $this->provider_total_import;
            $margin = 0;
            if($custom_import > 0 && $provider_import > 0){
                $margin = (($custom_import / $provider_import) * 100) - 100;
            }
            $margin = $margin;
        }else{

            // Definimos el margen por defecto

            $products = $this->products->where('exclude_on_margin', 0);
            $resources = $this->resources;

            if($products->count() > 0 && $resources->count() > 0){
                info(1);
                $products_avg = $products->avg('margin_percent');
                $resources_avg = $resources->avg('margin_percent');
                $margin = ($products_avg + $resources_avg) / 2;
            }elseif($products->count() > 0 && $resources->count() == 0){
                info(2);
                $margin = $products->avg('margin_percent');
            }elseif($products->count() == 0 && $resources->count() > 0){
                info(3);
                $margin = $resources->avg('margin_percent');
            }

            $margin = $margin;

        }

        return $margin;
    }
    public function getMarginCostAttribute(){
        $custom_import = $this->custom_import;
        $provider_import = $this->provider_total_import;
        $margin = $custom_import - $provider_import;
        $margin = $margin;
        return $margin;
    }
}