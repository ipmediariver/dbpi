<?php
namespace App\COMERCIAL;
use App\RRHH\JobTitle;
use Illuminate\Database\Eloquent\Model;
class QuoteResource extends Model
{
    protected $guarded = [];
    protected $appends = [
        'jobtitle_name', 
        'exchange_cost', 
        'total_import',
        'exchange_cost_with_custom_import',
        'margin_percent',
        'real_margin_percent',
        'margin_total',
        'total_import_on_quote'
    ];
    public function jobtitle(){
    	return $this->belongsTo(JobTitle::class, 'jobtitle_id');
    }
    public function quote(){
        return $this->belongsTo(Quote::class);
    }
    public function getJobtitleNameAttribute(){
    	return $this->jobtitle->name;
    }
    public function getExchangeCostAttribute(){
        $cost = $this->resource_cost;
        $exchange_rate = $this->quote->exchange_rate;
        $total = $cost / $exchange_rate;
        return $total;
    }
    public function getExchangeCostWithCustomImportAttribute(){
        $custom_import = $this->custom_import;
        $qty = $this->qty;
        if($custom_import > 0 && $qty > 0){
            $exchange_cost = $custom_import / $qty;
        }else{
            $exchange_cost = 0;
        }
        $exchange_cost = $exchange_cost;
        return $exchange_cost;
    }
    public function getTotalImportAttribute(){
    	$cost = $this->exchange_cost;
    	$qty = $this->qty;
    	$days = $this->days;
    	$total = ($cost * $days) * $qty;
    	return $total;
    }
    public function getMarginPercentAttribute(){
        $total_import = $this->custom_import ? $this->custom_import : $this->total_import;
        if($total_import > 0 && $this->total_import > 0){
            $margin = (($total_import / $this->total_import) * 100) - 100;
        }else{
            $margin = 0;
        }
        $margin = $margin;
        return $margin;
    }
    public function getRealMarginPercentAttribute(){
        if($this->total_import > 0){
            $margin = (($this->total_import / $this->total_import) * 100) - 100;
        }else{
            $margin = 0;
        }
        $margin = $margin;
        return $margin;   
    }
    public function getMarginTotalAttribute(){
        $total_import = $this->custom_import ? $this->custom_import : $this->total_import;
        $diff = $total_import - $this->total_import;
        $diff = $diff;
        return $diff;
    }
    public function getTotalImportOnQuoteAttribute(){
        $amount = $this->custom_import ? $this->custom_import : $this->total_import;
        $subtotal = $amount;
        $currency = $this->quote->currency->short_name;
        $exchange_rate = $this->quote->exchange_rate;
        if($currency == 'MXN'){
            $subtotal = ($amount * $exchange_rate);
        }
        $total = $subtotal;
        return $total;
    }
}