<?php
namespace App\COMERCIAL;
use App\SERVICIO\Cobertura;
use App\SERVICIO\Contrato;
use App\SERVICIO\FechaDeServicio;
use App\SERVICIO\TiempoDeRespuesta;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
class ContratoEnCotizacion extends Model
{
    protected $hidden = [
        'contrato', 
        'cobertura',
        'tiempo_de_respuesta'
    ];
    protected $appends = [
        'nombre_de_contrato',
        'nombre_de_cobertura',
        'nombre_de_tiempo_de_respuesta',
        'nombre_completo_de_contrato',
        'etapa_actual',
        'tiene_reemplazo_de_partes',
        'tiene_soporte_del_fabricante',
        'config',
        'cuando_inicia',
        'cuando_termina',
        'subtotal'
    ];
    protected $casts = [
        'fecha_de_inicio' => 'datetime:Y/m/d',
    ];
    protected $dates = [
        'cuando_inicia',
        'cuando_termina'
    ];
    public function cotizacion(){
        return $this->belongsTo(Quote::class, 'id_cotizacion');
    }
    public function contrato(){
        return $this->belongsTo(Contrato::class, 'id_contrato');
    }
    public function cobertura(){
        return $this->belongsTo(Cobertura::class, 'id_cobertura');
    }
    public function fechas_de_servicio(){
        return $this->hasMany(FechaDeServicio::class, 'id_contrato_en_cotizacion');
    }
    public function tiempo_de_respuesta(){
        return $this->belongsTo(TiempoDeRespuesta::class, 'id_tiempo_de_respuesta');
    }
    public function getConfigAttribute(){
        return json_decode($this->configuracion, true);
    }
    public function getCuandoIniciaAttribute(){
        $fecha = $this->fecha_de_inicio ? $this->fecha_de_inicio->format('Y-m-d') : 'Sin definir';
        return $fecha;
    }
    public function getSubtotalAttribute(){
        $subtotal = $this->convertir_costo_al_tipo_de_cambio();
        return $subtotal;
    }
    public function convertir_costo_al_tipo_de_cambio(){
        $costo = $this->costo;
        $tipo_de_cambio = $this->cotizacion->exchange_rate;
        if($this->cotizacion->currency->short_name == 'MXN'){
            $costo = ($costo * $tipo_de_cambio);
        }
        return $costo;
    }
    public function getCuandoTerminaAttribute(){
        $vigencia = 'Sin definir';
        if($this->fecha_de_inicio){
            $meses_de_contrato = $this->contrato->duracion;
            $vigencia = Carbon::parse($this->fecha_de_inicio)->addMonths($meses_de_contrato)->format('Y-m-d');
        }
        return $vigencia;
    }
    public function getNombreDeContratoAttribute(){
        return $this->contrato->nombre;
    }
    public function getNombreDeCoberturaAttribute(){
        return optional($this->cobertura)->nombre;
    }
    public function getNombreDeTiempoDeRespuestaAttribute(){
        return optional($this->tiempo_de_respuesta)->nombre;
    }
    public function getNombreCompletoDeContratoAttribute(){
        return "Contrato: {$this->nombre_de_contrato}, Cobertura: {$this->nombre_de_cobertura}, Tiempo de respuesta: {$this->nombre_de_tiempo_de_respuesta}";
    }
    public function getComponentesDeContratoAttribute(){
        return $this->contrato->componentes;
    }
    public function getMesesDeContratoAttribute(){
        return $this->contrato->duracion;
    }
    public function getEtapaActualAttribute(){
        switch ($this->etapa) {
            case 0:
                return "Inactivo";
                break;
            case 1:
                return "Activo";
                break;
            case 2:
                return "Cancelado";
                break;
            case 3:
                return "Expirado";
                break;
        }
    }
    public function getTieneReemplazoDePartesAttribute(){
        return $this->cobertura ? $this->cobertura->tipo->tiene_reemplazo_de_partes : null;
    }
    public function getTieneSoporteDelFabricanteAttribute(){
        return $this->cobertura ? $this->cobertura->tipo->tiene_soporte_del_fabricante : null;
    }
}