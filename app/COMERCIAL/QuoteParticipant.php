<?php
namespace App\COMERCIAL;
use App\User;
use Illuminate\Database\Eloquent\Model;
class QuoteParticipant extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at', 'quote_id', 'read', 'write', 'delete', 'user_id'];
    protected $appends = ['full_name', 'initials'];
    public function quote(){
    	return $this->belongsTo(Quote::class);
    }
    public function user(){
    	return $this->belongsTo(User::class);
    }
    public function getFullNameAttribute(){
    	return $this->user->full_name;
    }
    public function getInitialsAttribute(){
    	return $this->user->initials;
    }
}