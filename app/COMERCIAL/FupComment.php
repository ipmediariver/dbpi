<?php
namespace App\COMERCIAL;
use App\User;
use Illuminate\Database\Eloquent\Model;
class FupComment extends Model
{
    protected $guarded = [];

    protected $casts = [
    	'created_at' => 'datetime:Y-m-d'
    ];	

    protected $appends = ['author_name'];

    public function user(){
    	return $this->belongsTo(User::class, 'author_id');
    }

    public function getAuthorNameAttribute(){
    	return $this->user->full_name;
    }
}