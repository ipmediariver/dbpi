<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
class ProductModel extends Model
{
    protected $guarded = [];
    public function type(){
    	return $this->belongsTo(ProductType::class, 'product_type_id');
    }
    public function brand(){
    	return $this->belongsTo(ProductBrand::class, 'product_brand_id');
    }
    public function products(){
    	return $this->hasMany(Product::class, 'product_model_id');
    }
}