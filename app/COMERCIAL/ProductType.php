<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
class ProductType extends Model
{
    protected $guarded = [];
    public function brands(){
    	return $this->hasMany(ProductBrand::class, 'product_type_id', 'id');
    }
    public function models(){
    	return $this->hasMany(ProductModel::class, 'product_type_id', 'id');
    }
}