<?php

namespace App\COMERCIAL;

use Illuminate\Database\Eloquent\Model;

class QuoteComment extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at'];
    protected $casts = [
    	'created_at' => 'datetime:d M, Y'
    ];
    protected $appends = ['status'];
    public function quote(){
    	return $this->belongsTo(Quote::class);
    }
    public function getStatusAttribute(){
    	switch ($this->status_id) {
    	    case 0:
    	        return 'En proceso';
    	        break;
    	    case 1:
    	        return 'En revisión';
    	        break;
    	    case 2:
    	        return 'Aprobación interna';
    	        break;
    	    case 3:
    	        return 'Esperando aprobación del cliente';
    	        break;
    	    case 4:
    	        return 'Cancelada';
    	        break;
    	    case 5:
    	        return 'Rechazada por cliente';
    	        break;
    	    case 6:
    	        return 'Aprobada por cliente';
    	        break;
    	}
    }
}
