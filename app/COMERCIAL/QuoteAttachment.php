<?php
namespace App\COMERCIAL;
use App\RRHH\Currency;
use Illuminate\Database\Eloquent\Model;
class QuoteAttachment extends Model
{
    protected $guarded = [];

    protected $appends = [

    	'formated_import'

    ];

    protected $casts = [

    	'created_at' => 'datetime:d/M/Y'

    ];

    public function quote(){
    	return $this->belongsTo(Quote::class);
    }

    public function currency(){
    	return $this->belongsTo(Currency::class, 'billing_currency', 'id');
    }

    public function getFormatedImportAttribute(){

    	if($this->billing_import){
    		$billing_currency_short_name = $this->currency->short_name;
    		$import = $this->billing_import;
    		$import = number_format($import, 2, '.', '');
    		$response = '$' . $import . ' ' . $billing_currency_short_name;
    	}else{
    		$response = '';
    	}
    	return $response;
    }

}