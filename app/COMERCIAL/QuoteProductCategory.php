<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
class QuoteProductCategory extends Model
{
    protected $guarded = [];
    protected $hidden = ['category'];
    protected $appends = ['name', 'color'];
    public function category(){
    	return $this->belongsTo(ProductCategory::class);
    }
    public function getNameAttribute(){
    	return $this->category->name;
    }
    public function getColorAttribute(){
    	return $this->category->color;
    }
}