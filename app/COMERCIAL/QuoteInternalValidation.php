<?php
namespace App\COMERCIAL;
use App\User;
use Illuminate\Database\Eloquent\Model;
class QuoteInternalValidation extends Model
{
    protected $guarded = [];
    protected $appends = ['validator_name', 'status'];
    protected $casts = [
    	'created_at' => 'date:d M, Y'
    ];
    public function user(){
    	return $this->belongsTo(User::class);
    }
    public function quote(){
        return $this->belongsTo(Quote::class, 'quote_id');
    }
    // nombre del validador
    public function getValidatorNameAttribute(){
    	return $this->user->full_name;
    }
    // status de la validacion
    public function getStatusAttribute(){
    	switch ($this->status_id) {
    		case 0:
    			return '<span class="badge badge-pill badge-primary">En proceso</span>';
    			break;
    		case 1:
    			return '<span class="badge badge-pill badge-danger">Rechazado</span>';
    			break;
    		case 2:
    			return '<span class="badge badge-pill badge-success">Autorizado</span>';
    			break;
    	}
    }
}