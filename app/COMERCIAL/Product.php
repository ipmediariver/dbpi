<?php
namespace App\COMERCIAL;
use App\SERVICIO\PlantillaDeContrato;
use Illuminate\Database\Eloquent\Model;
class Product extends Model
{
    protected $guarded = [];
    protected $hidden =  ['category', 'created_at', 'updated_at'];
    protected $appends = ['thumb', 'full_name'];

    public function productos_en_cotizacion(){
        return $this->hasMany(QuoteProduct::class, 'product_id');
    }

    public function plantilla_de_contrato(){
        return $this->belongsTo(PlantillaDeContrato::class, 'id_plantilla_de_contrato');
    }

    public function category(){
    	return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }
    public function images(){
        return $this->hasMany(ProductImage::class, 'product_id');
    }
    public function getThumbAttribute(){
        if($this->images()->count()){
            $img = $this->images->first();
            return $img->filename;
        }
    }
    public function type(){
    	return $this->belongsTo(ProductType::class, 'product_type_id');
    }
    public function brand(){
    	return $this->belongsTo(ProductBrand::class, 'product_brand_id');
    }
    public function model(){
    	return $this->belongsTo(ProductModel::class, 'product_model_id');
    }
    public function getFullNameAttribute(){
    	return $this->code ? $this->code .' | '. $this->description : $this->description;
    }
}