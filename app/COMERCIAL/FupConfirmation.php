<?php
namespace App\COMERCIAL;
use App\COMERCIAL\Fup;
use App\User;
use Illuminate\Database\Eloquent\Model;
class FupConfirmation extends Model
{
    protected $guarded = [];
    public function fup(){
        return $this->belongsTo(Fup::class);
    }
    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function getFullNameAttribute(){
        return $this->user->full_name;
    }

    public function getStatusAttribute(){
    	$status = $this->status_id;
    	if($status){
    		return '<span class="badge badge-pill badge-success">Autorizado</span>';
    	}else{
    		return '<span class="badge badge-pill badge-secondary">Pendiente</span>';
    	}
    }
    public function getConfirmedAtAttribute(){
    	if($this->status_id){
    		return $this->updated_at->format('d M, Y h:i a');
    	}else{
    		return "Aún sin confirmar";
    	}
    }
}