<?php
namespace App\COMERCIAL;

use App\CRM\Account;
use App\RRHH\Currency;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'quote_id',
        'account_id',
        'account_logo_id',
        'contact_id',
        'contact_name',
        'date',
        'description',
        'fup_description',
        'hide_description',
        'only_show_description',
        'display_customer_items_discount',
        'terms',
        'delivery_time',
        'status_id',
        'quote_tamplate_id',
        'currency_id',
        'exchange_rate',
        'currency_value',
        'observations',
        'vigency',
        'fob',
        'general_discount',
        'display_iva',
        'iva',
        'final_comment',
        'has_po'
    ];
    protected $dates   = ['created_at', 'updated_at', 'date'];
    protected $appends = [
        'account_name',
        'account_logo',
        'formated_date',
        'number',
        'currency_name',
        'currency_short_name',
        'provider_total_import',
        'margin_percent',
        'margin_cost',
        'iva_ammount',
        'general_discount_ammount',
        'fup_id',
        'has_po',
        'has_fup_repository',
        'billing_total_import',
        'subtotal_on_quote',
    ];
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'account_id')
            ->withDefault([
                'name' => 'Sin asignación',
            ]);
    }
    public function getAccountLogoAttribute(){
        if($this->account_logo_id){
            $url = config('app.crm_root') . '?entryPoint=image&size=large&id=' . $this->account_logo_id;
        }else{
            $url = null;
        }
        return $url;
    }
    public function margin()
    {
        return $this->hasOne(QuoteMargin::class);
    }
    public function groups()
    {
        return $this->hasMany(QuoteGroup::class);
    }
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    public function products()
    {
        return $this->hasMany(QuoteProduct::class);
    }
    public function resources(){
        return $this->hasMany(QuoteResource::class);
    }
    public function contratos(){
        return $this->hasMany(ContratoEnCotizacion::class, 'id_cotizacion');
    }
    public function attachments()
    {
        return $this->hasMany(QuoteAttachment::class);
    }
    public function comments()
    {
        return $this->hasMany(QuoteComment::class);
    }
    public function participants(){
        return $this->hasMany(QuoteParticipant::class);
    }
    public function copy(){
        return $this->belongsTo(Quote::class, 'copy_of');
    }
    public  function fup_repository(){
        return $this->hasOne(FupRepository::class, 'quote_id');
    }
    public function getHasFupRepositoryAttribute(){
        return $this->fup_repository ? true : false;
    }
    public function getFupAttribute(){
        // return $this->fup_repository->current_fup;
        // return $this->hasOne(Fup::class, 'quote_id');
    }
    public function product_categories()
    {
        return $this->hasMany(QuoteProductCategory::class);
    }
    public function internal_validations()
    {
        return $this->hasMany(QuoteInternalValidation::class);
    }
    public function getCurrencyNameAttribute()
    {
        return $this->currency->name;
    }
    public function getCurrencyShortNameAttribute()
    {
        return $this->currency->short_name;
    }
    public function getFormatedDateAttribute()
    {
        return Carbon::parse($this->date)->format('d/m/Y');
    }
    public function getAccountNameAttribute()
    {
        return $this->account->name;
    }
    public function getNumberAttribute()
    {
        $num    = $this->id;
        $number = sprintf("%07d", $num);
        return $number;
    }
    public function getStatusAttribute()
    {
        switch ($this->status_id) {
            case 0:
                return '<span class="badge badge-pill badge-primary">En proceso</span>';
                break;
            case 1:
                return '<span class="badge badge-pill badge-primary">Esperando autorización</span>';
                break;
            case 2:
                return '<span class="badge badge-pill badge-info">Autorizada internamente</span>';
                break;
            case 3:
                return '<span class="badge badge-pill badge-primary">En espera de autorización del cliente</span>';
                break;
            case 4:
                return '<span class="badge badge-pill badge-danger">Cancelada</span>';
                break;
            case 5:
                return '<span class="badge badge-pill badge-primary">Negociación/Revisión</span>';
                break;
            case 6:
                return '<span class="badge badge-pill badge-success">Autorizada por cliente</span>';
                break;
        }
    }
    public function getProviderTotalImportAttribute()
    {
        $groups = $this
            ->groups()
            ->where('hide_in_quote', 0)
            ->get();
        $subtotal = 0;
        foreach ($groups as $group) {
            $subtotal += $group->provider_total_import;
        }
        $subtotal = $subtotal;
        return $subtotal;
    }
    public function getProviderTotalImportWithoutGroupsProductsAndResourcesThatNotAffectMarginAttribute(){
        $groups = $this
            ->groups()
            ->where('exclude_on_margin', 0)
            ->get();
        $subtotal = 0;
        foreach ($groups as $group) {
            $subtotal += $group->provider_total_import_without_products_and_resources_excluded_on_margin;
        }
        $subtotal = $subtotal;
        return $subtotal;
    }
    public function getSubtotalAttribute()
    {
        $groups = $this
            ->groups()
            ->where('hide_in_quote', 0)
            ->get();
        $subtotal = 0;
        foreach ($groups as $group) {
            $subtotal += $group->custom_import ? $group->custom_import : $group->subtotal;
        }
        $subtotal = $subtotal;
        return $subtotal;
    }
    public function getSubtotalExcludingGroupsThatNotAffectMarginAttribute(){
        $groups = $this
            ->groups()
            ->where('hide_in_quote', 0)
            ->get();
        $subtotal = 0;
        foreach ($groups as $group) {
            $subtotal += $group->custom_import ? $group->custom_import : $group->sub_total_without_products_and_resources_hided_on_quote;
        }
        $subtotal = $subtotal;
        return $subtotal;
    }
    public function getSubtotalExcludingGroupsProductsAndResourcesThatNotAffectMarginAttribute(){
        $groups = $this
            ->groups()
            ->where('hide_in_quote', 0)
            ->where('exclude_on_margin', 0)
            ->get();
        $subtotal = 0;
        foreach ($groups as $group) {
            $subtotal += $group->custom_import ? $group->custom_import : $group->sub_total_without_products_and_resources_excluded_on_margin;
        }
        $subtotal = $subtotal;
        return $subtotal;
    }
    public function getSubtotalOnQuoteAttribute(){
        $currency = $this->currency->short_name;
        $subtotal = $this->subtotal_excluding_groups_that_not_affect_margin;
        $exchange_rate = $this->exchange_rate;
        if($currency == 'MXN'){
            $subtotal = ($subtotal * $exchange_rate);
        }
        return $subtotal;
    }
    public function getIvaAmmountAttribute()
    {
        $subtotal = $this->subtotal_on_quote;
        $total    = $subtotal;
        $currency = $this->currency->short_name;
        $exchange_rate = $this->exchange_rate;
        if ($this->general_discount) {
            $discount         = $this->general_discount;
            $discount_ammount = ($total * $discount) / 100;
            $total            = $total - $discount_ammount;
        }

        $iva         = $this->iva;
        $iva_ammount = ($total * $iva) / 100;
        
        // if($currency == 'MXN'){
        //     $total = ($iva_ammount * $exchange_rate);
        // }else{
        //     $total = $iva_ammount;
        // }

        $total = $iva_ammount;

        return $total;
    }
    public function getGeneralDiscountAmmountAttribute()
    {
        $subtotal         = $this->subtotal;
        $discount_ammount = 0;
        $currency = $this->currency->short_name;
        $exchange_rate = $this->exchange_rate;
        if ($this->general_discount) {
            $discount         = $this->general_discount;
            $discount_ammount = ($subtotal * $discount) / 100;
        }
        if($currency == 'MXN'){
            $total = ($discount_ammount * $exchange_rate);
        }else{
            $total = $discount_ammount;
        }
        return $total;
    }
    public static function calculate_total($quote, $subtotal){
        $total = $subtotal;
        if ($quote->general_discount) {
            $discount         = $quote->general_discount;
            $discount_ammount = ($total * $discount) / 100;
            $total            = $total - $discount_ammount;
        }
        if ($quote->display_iva) {
            $iva         = $quote->iva;
            $iva_ammount = ($total * $iva) / 100;
            $total       = $total + $iva_ammount;
        }
        return $total;
    }
    public function getTotalAttribute()
    {
        $total = Quote::calculate_total($this, $this->subtotal);
        return $total;
    }
    public function getTotalOnQuoteAttribute(){
        $currency = $this->currency->short_name;
        $subtotal = Quote::calculate_total($this, $this->subtotal_excluding_groups_that_not_affect_margin);;
        $total = $subtotal;
        $exchange_rate = $this->exchange_rate;
        if($currency == 'MXN'){
            $total = ($subtotal * $exchange_rate);
        }
        return $total;
    }
    public function getContactAttribute()
    {
        if ($this->contact_id) {
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'header' => "x-api-key: " . config('app.crm_key'),
                ),
            ));
            $route   = config('app.crm_uri') . '/Contact/' . $this->contact_id . '?select=name';
            $contact = file_get_contents($route, false, $context);
            $contact = json_decode($contact, false);
            return $contact;
        } else {
            return "Sin contacto asignado";
        }
    }
    public function getMarginPercentAttribute()
    {

        // $margin = $this->groups()
        //     ->where('exclude_on_margin', 0)
        //     ->get()
        //     ->avg('margin_percent');
        // return $margin;

        $percent = 0;
        $provider_import = $this->provider_total_import_without_groups_products_and_resources_that_not_affect_margin;
        $customer_import = $this->subtotal_excluding_groups_products_and_resources_that_not_affect_margin;
        if($provider_import > 0 && $customer_import > 0){
            $percent = (($customer_import / $provider_import) - 1) * 100;
        }
        return $percent;

    }
    public function getMarginCostAttribute()
    {
        $provider_import = $this->provider_total_import_without_groups_products_and_resources_that_not_affect_margin;
        $customer_import   = $this->subtotal_excluding_groups_products_and_resources_that_not_affect_margin;
        $cost = 0;
        if($customer_import){
            $cost = $customer_import - $provider_import;
        }
        return $cost;
    }
    public function getHasPoAttribute(){
        $po = $this->attachments->where('quote_po', 1)->first();
        return $po ? true : false;
    }
    public function getPaymentOrderAttribute(){
        $payment_order = $this->attachments->where('quote_po', 1)->first();
        return $payment_order;
    }
    public function getFupIdAttribute(){
        return $this->fup ? $this->fup->id : false;
    }
    public function getPreviousVersionsAttribute(){
        $opportunity_id = $this->opportunity_id;
        $quotes = Quote::where('opportunity_id', $opportunity_id)->where('current_version', 0)->orderBy('created_at', 'desc')->get();
        return $quotes;
    }
    public function getBillingTotalImportAttribute(){
        
        $billings = $this->attachments->where('type_id',2);
        
        $quote_currency_short_name = $this->currency->short_name;

        $import = 0;

        foreach($billings as $billing){
            
            $billing_currency_short_name = $billing->currency->short_name;

            if($quote_currency_short_name == 'USD' && $billing_currency_short_name == 'MXN'){
                $import += ($billing->billing_import / $this->exchange_rate);
            }elseif($quote_currency_short_name == 'MXN' && $billing_currency_short_name == 'USD'){
                $import += ($billing->billing_import * $this->exchange_rate);
            }else{
                $import += $billing->billing_import;
            }

        }

        return $import;

    }

    public function getBillingStatusAttribute(){

        $status_id = 0;

        $billing_import = $this->billing_total_import;
        $subtotal = $this->subtotal_on_quote;
        $diferencia_en_centavos = number_format(abs($billing_import - $subtotal), 2);

        if($diferencia_en_centavos == 0 || $billing_import > $subtotal){
            $status_id = 3;
        }elseif($billing_import > 0 && $diferencia_en_centavos > 0){
            $status_id = 2;
        }else{
            $status_id = 1;
        }

        return $status_id; 

    }

    public function getBillingTotalImportBadgeAttribute(){

        $billing_import = $this->billing_total_import;
        $subtotal = $this->subtotal_on_quote;
        $currency = $this->currency->short_name;
        $badge_class = '';

        $diferencia_en_centavos = number_format(abs($billing_import - $subtotal), 2);

        if($diferencia_en_centavos == 0 || $billing_import > $subtotal){
            $badge_class = 'badge-success';
        }elseif($billing_import > 0 && $diferencia_en_centavos > 0){
            $badge_class = 'badge-primary';
        }else{
            $badge_class = 'badge-danger';
        }

        $badge = '<span class="badge badge-pill '. $badge_class .'">$' . number_format($billing_import, 2) . ' ' . $currency . '/ $' . number_format($subtotal, 2) . ' ' . $currency . '</span>';

        return $badge;
    }

}
