<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
class Fup extends Model
{
    protected $guarded = [];
    protected $hidden  = [];
    protected $appends = ['number'];
    protected $casts   = [
        'created_at' => 'datetime:Y-m-d',
        'date'       => 'datetime:Y-m-d',
    ];
    public function quote()
    {
        return $this->belongsTo(Quote::class, 'quote_id');
    }
    public function observations()
    {
        return $this->hasMany(FupObservation::class);
    }
    public function comments()
    {
        return $this->hasMany(FupComment::class);
    }
    public function version_comments()
    {
        return $this->hasMany(FupCommentVersion::class, 'fup_id');
    }
    public function confirmations()
    {
        return $this->hasMany(FupConfirmation::class);
    }
    public function auth_departments()
    {
        return $this->hasMany(FupDepartmentAuth::class);
    }
    public function repository()
    {
        return $this->belongsTo(FupRepository::class, 'fup_repository_id', 'id');
    }
    public function cancellation()
    {
        return $this->hasOne(FupCancellation::class)->where('status_id', 0);
    }
    public function getAuthorAttribute()
    {
        return $this->quote->author;
    }
    public function getNumberAttribute()
    {
        $num        = $this->fup_repository_id;
        $number     = sprintf("%07d", $num);
        $fup_number = $number . '-' . $this->version;
        return $fup_number;
    }
    public function getIsCancelledAttribute()
    {
        $quote = $this->quote;
        if ($quote->status_id == 4) {
            return true;
        } else {
            return false;
        }
    }
    public function getEtapaIdAttribute()
    {
        $etapa                             = 1;
        $departamentos_que_deben_autorizar = $this->auth_departments->count();
        $departamentos_que_ya_autorizaron  = $this->auth_departments->where('status', 1)->count();
        if ($departamentos_que_deben_autorizar) {
            if ($departamentos_que_deben_autorizar == $departamentos_que_ya_autorizaron) {
                $etapa = 3;
            } elseif ($departamentos_que_deben_autorizar > $departamentos_que_ya_autorizaron) {
                $etapa = 2;
            }
        }
        return $etapa;
    }
    public function getStatusAttribute()
    {
        $status      = '';
        $departments = $this->auth_departments;
        if (!$departments->count()) {
            return "<span class=\"badge badge-pill badge-secondary\">Sin solicitar</span>";
        } else {
            $total_departments      = $departments->count();
            $authorized_departments = $departments->where('status', 1)->count();
            if ($total_departments != $authorized_departments) {
                return "<span class=\"badge badge-pill badge-primary\">({$authorized_departments}/{$total_departments}) En proceso</span>";
            } else {
                return "<span class=\"badge badge-pill badge-success\">Autorizado</span>";
            }
        }
    }
    public function getTotalDelProyectoEnPesosAttribute()
    {
        $total = 0;
        if ($this->quote->currency->short_name == 'MXN') {
            $total = $this->quote->sub_total_on_quote;
        }
        return $total;
    }
    public function getTotalDelProyectoEnDolaresAttribute()
    {
        $total = 0;
        if ($this->quote->currency->short_name == 'USD') {
            $total = $this->quote->sub_total_on_quote;
        }
        return $total;
    }
    public function getPagosEnPesosAttribute(){
        $pagos_en_pesos = 0;

        if($this->quote->currency->short_name == 'MXN'){

            $pagos_en_pesos = $this->quote
                ->attachments
                ->where('type_id', 2)
                ->where('billing_currency', 1)
                ->where('paidout', 1)
                ->sum('billing_import');

                $pagos_en_dolares = $this->quote
                    ->attachments
                    ->where('type_id', 2)
                    ->where('billing_currency', 2)
                    ->where('paidout', 1)
                    ->sum('billing_import');
                $tipo_de_cambio = $this->quote->exchange_rate;
                $cambio = $pagos_en_dolares * $tipo_de_cambio;
                $pagos_en_pesos += $cambio;
        }

        return $pagos_en_pesos;
    }
    public function getPagosEnDolaresAttribute(){
        $pagos_en_dolares = 0;

        if($this->quote->currency->short_name == 'USD'){

            $pagos_en_dolares = $this->quote
                ->attachments
                ->where('type_id', 2)
                ->where('billing_currency', 2)
                ->where('paidout', 1)
                ->sum('billing_import');
            $pagos_en_pesos = $this->quote
                ->attachments
                ->where('type_id', 2)
                ->where('billing_currency', 1)
                ->where('paidout', 1)
                ->sum('billing_import');
            $tipo_de_cambio = $this->quote->exchange_rate;
            $cambio = $pagos_en_pesos / $tipo_de_cambio;
            $pagos_en_dolares += $cambio;        
        }

        return $pagos_en_dolares;
    }
    public function getFacturadoPendientePorCobrarEnPesosAttribute(){
        
        $facturado_pendiente_por_cobrar_pesos = 0;

        if($this->quote->currency->short_name == 'MXN'){

            $facturado_pendiente_por_cobrar_pesos = $this->quote
                ->attachments
                ->where('type_id', 2)
                ->where('billing_currency', 1)
                ->where('paidout', 0)
                ->sum('billing_import');
            $facturado_pendiente_por_cobrar_dolares = $this->quote
                ->attachments
                ->where('type_id', 2)
                ->where('billing_currency', 2)
                ->where('paidout', 0)
                ->sum('billing_import');
            $tipo_de_cambio = $this->quote->exchange_rate;
            $cambio = $facturado_pendiente_por_cobrar_dolares * $tipo_de_cambio;
            $facturado_pendiente_por_cobrar_pesos += $cambio;
        }

        return $facturado_pendiente_por_cobrar_pesos;
    }
    public function getFacturadoPendientePorCobrarEnDolaresAttribute(){
        
        $facturado_pendiente_por_cobrar_dolares = 0;

        if($this->quote->currency->short_name == 'USD'){

            $facturado_pendiente_por_cobrar_dolares = $this->quote
                ->attachments
                ->where('type_id', 2)
                ->where('billing_currency', 2)
                ->where('paidout', 0)
                ->sum('billing_import');

            $facturado_pendiente_por_cobrar_pesos = $this->quote
            ->attachments
            ->where('type_id', 2)
            ->where('billing_currency', 1)
            ->where('paidout', 0)
            ->sum('billing_import');
            $tipo_de_cambio = $this->quote->exchange_rate;
            $cambio = $facturado_pendiente_por_cobrar_pesos / $tipo_de_cambio;
            $facturado_pendiente_por_cobrar_dolares += $cambio;
        }

        return $facturado_pendiente_por_cobrar_dolares;
    }
    public function getPendientePorFacturarEnPesosAttribute(){

        $pendiente_por_facturar = 0;

        if($this->quote->currency->short_name == 'MXN'){

            $facturado = $this->quote->billing_total_import;
            $total = $this->quote->sub_total_on_quote;

            $pendiente_por_facturar = abs($total - $facturado);

        }

        return $pendiente_por_facturar;

    }
    public function getPendientePorFacturarEnDolaresAttribute(){

        $pendiente_por_facturar = 0;

        if($this->quote->currency->short_name == 'USD'){

            $facturado = $this->quote->billing_total_import;
            $total = $this->quote->sub_total_on_quote;

            $pendiente_por_facturar = abs($total - $facturado);

        }

        return $pendiente_por_facturar;

    }
}