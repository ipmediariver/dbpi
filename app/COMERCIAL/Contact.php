<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
class Contact extends Model
{
    
	public static function single($id){
		$url = config('app.crm_uri') . '/Contact/' . $id;
		$token = config('app.crm_key');

		$contact = Http::withHeaders([
			'X-Api-Key' => $token
		])->get($url)->json();

		return $contact;
	}

}