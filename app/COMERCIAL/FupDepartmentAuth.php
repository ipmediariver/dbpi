<?php

namespace App\COMERCIAL;

use App\COMERCIAL\Fup;
use App\RRHH\Department;
use Illuminate\Database\Eloquent\Model;

class FupDepartmentAuth extends Model
{
    protected $guarded = [];

    protected $casts = [

    	'created_at' => 'datetime:d M, Y h:i a'

    ];

    protected $appends = ['name'];

    public function department(){
    	return $this->belongsTo(Department::class);
    }

    public function fup(){
    	return $this->belongsTo(Fup::class);
    }

    public function getNameAttribute(){
    	return optional($this->department)->name;
    }

    public function getSupervisorsAttribute(){
        $department = $this->department ? $this->department : 'nada';
        $usuarios = $this->department ? $this->department->users->pluck('user_id') : [];
    	$fup_confirmations = $this->fup->confirmations->whereIn('user_id', $usuarios);
    	return $fup_confirmations;
    }

    public function getStatusAttribute(){

    	// verificamos si el departamento es direccion

    	$direction = Department::where('name', 'Dirección')->first();

    	$authorized = false;

    	if($this->department_id == $direction->id){

    		foreach ($this->supervisors as $supervisor) {
    			
    			if($supervisor->status_id == 1){

    				$authorized = true;

    			}

    		}

    	}else{

    		foreach ($this->supervisors as $supervisor) {
    			
    			if($supervisor->status_id == 1){

    				$authorized = true;

    			}

    		}

    	}

    	return $authorized;

    }

    public function getStatusBadgeAttribute(){
    	switch ($this->status) {
    		case true:
    			return "<span class=\"badge badge-pill badge-success\">Autorizado</span>";
    			break;
    		
    		default:
    			return "<span class=\"badge badge-pill badge-secondary\">En proceso</span>";
    			break;
    	}
    }

}
