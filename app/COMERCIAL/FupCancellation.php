<?php

namespace App\COMERCIAL;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FupCancellation extends Model
{
    public function fup(){
    	return $this->belongsTo(Fup::class);
    }

    public function user(){
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function director(){
    	return $this->belongsTo(User::class, 'director_id', 'id');
    }
}
