<?php
namespace App\COMERCIAL;
use Illuminate\Database\Eloquent\Model;
class FupRepository extends Model
{
    protected $guarded = [];
    protected $appends = ['number', 'current_fup'];
    public function quote(){
    	return $this->belongsTo(Quote::class);
    }
    public function fups(){
    	return $this->hasMany(Fup::class, 'fup_repository_id');
    }
    public function getNumberAttribute()
    {
        $num    = $this->id;
        $number = sprintf("%07d", $num);
        return $number;
    }
    public function getCurrentFupAttribute(){
    	$current_fup = $this
            ->fups()
            ->orderBy('created_at', 'desc')
            ->first();
    	return $current_fup;
    }
    public function getPreviousFupsAttribute(){
        $previous_fups = $this
            ->fups()
            ->orderBy('created_at', 'desc')
            ->where('id', '<>', $this->current_fup->id)
            ->get();
        return $previous_fups;
    }
}