<?php

namespace App\SERVICIO;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    
    protected $guarded = [];

    protected $casts = [

    	'created_at' => 'datetime:Y/m/d'

    ];

	public function componentes(){
		return $this->hasMany(ComponenteDeContrato::class, 'id_contrato');
	}

}
