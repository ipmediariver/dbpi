<?php

namespace App\SERVICIO;

use Illuminate\Database\Eloquent\Model;

class Cobertura extends Model
{

	protected $appends = ['tipo_de_cobertura', 'nombre_completo'];

    public function horarios(){
        return $this->hasMany(HorarioDeCobertura::class, 'id_cobertura');
    }

    public function tipo(){
    	return $this->belongsTo(TipoDeCobertura::class, 'id_tipo_de_cobertura');
    }

    public function getTipoDeCoberturaAttribute(){
    	return $this->tipo ? optional($this->tipo)->nombre : '';
    }

    public function getNombreCompletoAttribute(){
        return $this->nombre . ' | Tipo: ' . optional($this->tipo)->nombre;
    }

}
