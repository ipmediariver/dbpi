<?php
namespace App\SERVICIO;

use App\COMERCIAL\ContratoEnCotizacion;
use App\CRM\Account;
use App\SERVICIO\Ticket;
use Illuminate\Database\Eloquent\Model;

class FechaDeServicio extends Model
{
    protected $guarded = [];
    protected $appends = [
        'nombre_de_cliente',
        'tipo_de_servicio',
        'etapa',
        'nombre_de_ingeniero',
        'dia',
        'hora',
        'numero_de_fup',
        'ruta_de_fup'
    ];
    protected $casts = [
        'fecha' => 'datetime:d M Y H:i',
    ];
    public function cliente()
    {
        return $this->belongsTo(Account::class, 'id_cliente', 'account_id');
    }
    public function servicio()
    {
        return $this->belongsTo(TipoDeServicio::class, 'id_servicio');
    }
    public function contrato()
    {
        return $this->belongsTo(ContratoEnCotizacion::class, 'id_contrato_en_cotizacion');
    }
    public function getNumeroDeFupAttribute(){
    	$numero_de_fup = $this->contrato ? $this->contrato->cotizacion->fup_repository->number : 'Sin cotización';
    	return $numero_de_fup;
    }

    public function getRutaDeFupAttribute(){
        if($this->contrato){
        	$fup = $this->contrato->cotizacion->fup_repository;
        	return route('fuprep.fups.show', [$fup, $fup->current_fup]);
        }
    }

    public function getTicketAttribute()
    {
        $ticket = Ticket::where('ticket_id', $this->id_ticket)->first();
        return $ticket;
    }
    public function getIngenieroAttribute()
    {
        $ingeniero = \DB::connection('helpdesk')
            ->table('agents')
            ->where('agents.id', '=', optional($this->ticket)['agent_id'])
            ->join('users', 'agents.user_id', '=', 'users.id')
            ->select('users.first_name', 'users.last_name')
            ->first();
        return json_decode(json_encode($ingeniero), true);
    }
    public function getNombreDeIngenieroAttribute()
    {
        if ($this->ingeniero) {
            return $this->ingeniero['first_name'] . ' ' . $this->ingeniero['last_name'];
        } else {
            return 'Sin asignación';
        }
    }
    public function getEtapaAttribute()
    {
        $badge = '';
        if (!$this->ticket) {
            $badge = '<span class="badge badge-pill badge-secondary">Pendiente</span>';
        } else {
            if ($this->ticket['status_id'] == 6) {
                $badge = '<span class="badge badge-pill badge-success">Terminado</span>';
            } else {
                $badge = '<span class="badge badge-pill badge-primary">En proceso</span>';
            }
        }
        return $badge;
        switch ($this->id_ticket) {
            case null:
                return '<span class="badge badge-pill badge-secondary">Pendiente</span>';
                break;
            case 2:
                return '<span class="badge badge-pill badge-primary">En proceso</span>';
                break;
            case 3:
                return '<span class="badge badge-pill badge-primary">Terminado</span>';
                break;
        }
    }
    public function getNombreDeClienteAttribute()
    {
        return optional($this->cliente)->name;
    }
    public function getTipoDeServicioAttribute()
    {
        return $this->servicio->nombre;
    }
    public function getDiaAttribute()
    {
        return $this->fecha->format('Y-m-d');
    }
    public function getHoraAttribute()
    {
        return $this->fecha->format('H:i');
    }
}
