<?php

namespace App\SERVICIO;

use Illuminate\Database\Eloquent\Model;

class TipoDeServicio extends Model
{
    protected $fillable = ['nombre'];
}
