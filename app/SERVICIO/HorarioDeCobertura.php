<?php

namespace App\SERVICIO;

use Illuminate\Database\Eloquent\Model;

class HorarioDeCobertura extends Model
{

    protected $appends = ['de', 'hasta'];

    protected $dates = ['hora_de_inicio', 'hora_final'];

    protected $casts = [

        'dias' => 'array'

    ];

    public function getDeAttribute(){

        $hora_y_minuto = [

            'hora' => number_format($this->hora_de_inicio->format('H')),
            'minuto' => number_format($this->hora_de_inicio->format('i'))

        ];

        return $hora_y_minuto;

    }


    public function getHastaAttribute(){

        $hora_y_minuto = [

            'hora' => number_format($this->hora_final->format('H')),
            'minuto' => number_format($this->hora_final->format('i'))

        ];

        return $hora_y_minuto;

    }

}
