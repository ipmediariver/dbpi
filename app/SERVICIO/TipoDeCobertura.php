<?php
namespace App\SERVICIO;
use Illuminate\Database\Eloquent\Model;
class TipoDeCobertura extends Model
{
    protected $appends = [
        'definicion_de_reemplazo_de_partes',
        'tiene_reemplazo_de_partes',
        'tiene_soporte_del_fabricante',
    ];
    protected $casts = [
        'configuracion' => 'object',
    ];
    public function getDefinicionDeReemplazoDePartesAttribute()
    {
        $definicion = 'N/A';
        if ($this->tiene_reemplazo_de_partes) {
            if ($this->tiene_soporte_del_fabricante) {
                return 'SI (Con soporte de fabricante)';
            } else {
                return 'SI';
            }
        }
        return $definicion;
    }
    public function getTieneReemplazoDePartesAttribute()
    {
        $tiene_reemplazo_de_partes  = false;
        $elementos_de_configuracion = json_decode($this->configuracion, true);
        foreach ($elementos_de_configuracion as $elemento) {
            if ($elemento['nombre'] == 'Reemplazo de partes') {
                if ($elemento['valor']) {
                    $tiene_reemplazo_de_partes = true;
                }
            }
        }
        return $tiene_reemplazo_de_partes;
    }
    public function getTieneSoporteDelFabricanteAttribute()
    {
        $tiene_soporte_del_fabricante = false;
        $elementos_de_configuracion   = json_decode($this->configuracion, true);
        foreach ($elementos_de_configuracion as $elemento) {
            if ($elemento['nombre'] == 'Soporte de fabricante') {
                if ($elemento['valor']) {
                    $tiene_soporte_del_fabricante = true;
                }
            }
        }
        return $tiene_soporte_del_fabricante;
    }
}