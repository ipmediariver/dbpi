<?php

namespace App\SERVICIO;

use App\SERVICIO\Ingeniero;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    
	protected $connection = 'helpdesk';

	protected $table = 'tickets';

}
