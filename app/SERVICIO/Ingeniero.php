<?php

namespace App\SERVICIO;

use Illuminate\Database\Eloquent\Model;

class Ingeniero extends Model
{
    protected $connection = 'helpdesk';

    protected $table = 'agents';

    protected $appends = ['nombre_completo'];

    public function getNombreCompletoAttribute(){
    	
    	$usuario = \DB::connection('helpdesk')->table('users')->find($this->user_id);

    	$nombre_completo = $usuario->first_name .' '. $usuario->last_name;

    	return $nombre_completo;

    }

    public function getDepartamentoAttribute(){

    	
    }

}
