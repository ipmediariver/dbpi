<?php
namespace App\SERVICIO;
use App\COMERCIAL\Product;
use Illuminate\Database\Eloquent\Model;
class PlantillaDeContrato extends Model
{
    protected $guarded = [];
    protected $appends = [
        'nombre_de_contrato',
        'nombre_de_cobertura',
        'nombre_de_tiempo_de_respuesta',
        'descripcion_completa'
    ];
    public function producto(){
        return $this->hasOne(Product::class, 'id_plantilla_de_contrato');
    }
    public function contrato(){
        return $this->belongsTo(Contrato::class, 'id_contrato');
    }
    public function getNombreDeContratoAttribute(){
        return $this->contrato ? $this->contrato->nombre : 'Sin contrato';
    }
    public function cobertura(){
        return $this->belongsTo(Cobertura::class, 'id_cobertura');
    }
    public function getNombreDeCoberturaAttribute(){
        return $this->cobertura ? $this->cobertura->nombre : 'Sin cobertura';
    }
    public function tiempo_de_respuesta(){
        return $this->belongsTo(TiempoDeRespuesta::class, 'id_tiempo_de_respuesta');
    }
    public function getNombreDeTiempoDeRespuestaAttribute(){
        return $this->tiempo_de_respuesta ? $this->tiempo_de_respuesta->nombre : 'Sin tiempo de respuesta';
    }
    public function getDescripcionCompletaAttribute(){
        return "
            <div class='small'>
                <p class='m-0 text-uppercase'><b>{$this->nombre}</b></p>
                <p class='mb-2 text-muted'>{$this->descripción}</p>
                <p class='mb-2 text-danger small'><b>{$this->codigo}</b></p>
                <p class='mb-2'><strong>Contrato:</strong> <br /> {$this->nombre_de_contrato}</p>
                <p class='mb-2'><strong>Cobertura:</strong> <br /> {$this->nombre_de_cobertura}</p>
                <p class='m-0'><strong>Tiempo de respuesta:</strong> <br /> {$this->nombre_de_tiempo_de_respuesta}</p>
            </div>
        ";
    }
}