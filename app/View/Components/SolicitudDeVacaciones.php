<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SolicitudDeVacaciones extends Component
{
    public $empleado;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($empleado)
    {
        $this->empleado = $empleado;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $solicitud = $this->empleado->vacaciones_pendientes_de_autorizacion;
        return view('components.solicitud-de-vacaciones', compact('solicitud'));
    }
}
