<?php
namespace App\View\Components;
use Illuminate\View\Component;
class IndicadoresDeCotizaciones extends Component
{
    public $indicadores;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($indicadores)
    {
        $this->indicadores = $indicadores;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.indicadores-de-cotizaciones');
    }
}