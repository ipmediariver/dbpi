<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CategoryAttachmentForm extends Component
{

    public $action;
    public $method;
    public $btn;
    public $category;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($action, $method, $btn, $category = null)
    {
        $this->action = $action;
        $this->method = $method;
        $this->btn = $btn;
        $this->category = $category;
        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.category-attachment-form');
    }
}
