<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ConsultarAnosAnteriores extends Component
{

    public $clases;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($clases)
    {
        $this->clases = $clases;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.consultar-anos-anteriores');
    }
}
