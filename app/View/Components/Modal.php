<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Modal extends Component
{

    public $id;
    public $title;
    public $centered;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $id, $centered = false)
    {
        $this->title = $title;
        $this->id = $id;
        $this->centered = $centered;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.modal');
    }
}
