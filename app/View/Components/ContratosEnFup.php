<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ContratosEnFup extends Component
{

    public $fup;
    public $contratos;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($fup)
    {
        $this->fup = $fup;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {

        $this->contratos = $this->fup->quote->contratos->load('fechas_de_servicio');

        return view('components.contratos-en-fup');
    }
}
