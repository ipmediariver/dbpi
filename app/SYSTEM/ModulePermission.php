<?php
namespace App\SYSTEM;
use Illuminate\Database\Eloquent\Model;
class ModulePermission extends Model
{
    protected $guarded = [];
    public function module(){
    	return $this->belongsTo(Module::class);
    }
}