<?php
namespace App;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteParticipant;
use App\RRHH\UserDepartment;
use App\SYSTEM\Module;
use App\SYSTEM\ModulePermission;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'username', 'role', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
        'created_at',
        'updated_at',
        'validator',
    ];
    protected $appends = ['full_name', 'initials'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function permissions()
    {
        return $this->hasMany(ModulePermission::class, 'user_id');
    }
    public function getModulesAttribute()
    {
        $permissions = $this->permissions;
        $modules_ids = [];
        foreach ($permissions as $permission) {
            if ($permission->read == 1 || $permission->edit == 1 || $permission->delete == 1) {
                array_push($modules_ids, $permission->module_id);
            }
        }
        $modules = Module::whereIn('id', $modules_ids)->get();
        return $modules;
    }
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
    public static function can_read_module($user, $module)
    {
        $user_permission = $user->permissions()->where('module_id', $module->id)->first();
        if ($user_permission) {
            if ($user_permission->read) {
                return true;
            }
        }
    }
    public static function can_edit_module($user, $module)
    {
        $user_permission = $user->permissions()->where('module_id', $module->id)->first();
        if ($user_permission) {
            if ($user_permission->edit) {
                return true;
            }
        }
    }
    public static function can_delete_module($user, $module)
    {
        $user_permission = $user->permissions()->where('module_id', $module->id)->first();
        if ($user_permission) {
            if ($user_permission->delete) {
                return true;
            }
        }
    }
    public function departments(){
        return $this->hasMany(UserDepartment::class);
    }
    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }
    public function participating_quotes()
    {
        return $this->hasMany(QuoteParticipant::class);
    }
    public function getSharedQuotesAttribute()
    {
        $quotes = collect([]);
        foreach ($this->participating_quotes as $participating_on) {
            $quote = $participating_on->quote;
            $quotes->push($quote);
        }
        return $quotes;
    }
    public function getOwnerAndSharedQuotesAttribute()
    {
        $quotes        = $this->quotes;
        $shared_quotes = $this->shared_quotes;
        $all_quotes    = $quotes->merge($shared_quotes);
        return $all_quotes;
    }
    public function getInitialsAttribute()
    {
        $first_name    = $this->first_name;
        $first_initial = $first_name[0];
        $last_name     = $this->last_name;
        $last_initial  = $last_name[0];
        $initials      = $first_initial . $last_initial;
        return $initials;
    }
}