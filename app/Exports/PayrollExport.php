<?php

namespace App\Exports;

use App\RRHH\AccountPayroll;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PayrollExport implements FromView, ShouldAutoSize
{
	public $payroll;
	public function __construct($payroll){
		$this->payroll = $payroll;
	}
    public function view(): View {
    	return view('dbpi.excel.payroll', [
    		'payroll' => $this->payroll
    	]);
    }
}
