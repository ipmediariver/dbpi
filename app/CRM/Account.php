<?php

namespace App\CRM;

use App\RRHH\Employee;
use App\SERVICIO\FechaDeServicio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Account extends Model
{
    protected $guarded = [];
    protected $hidden = [];
    public static function get_crm_accounts(){
        $url = $route = config('app.crm_uri') . '/Account?select=name,website,type,deleted,billingAddressCountry&maxSize=1000&offset=0&orderBy=createdAt&order=desc';
        $api_key = config('app.crm_key');
        $external_accounts = Http::withHeaders([
            'x-api-key' => $api_key
        ])->get($route)->json();

        return $external_accounts;
    }
    public static function internal(){
        $accounts = Account::where('type', 'Interno')->get();
        return $accounts;
    }
    public static function external(){
        $accounts = Account::where('type', '<>', 'Interno')->get();
        return $accounts;
    }
    public function employees(){
        return $this->hasMany(Employee::class, 'account_id', 'account_id');
    }
    public static function single($id){
        $url = $route = config('app.crm_uri') . '/Account/' . $id;
        $api_key = config('app.crm_key');
        $account = Http::withHeaders([
            'x-api-key' => $api_key
        ])->get($route)->json();

        return $account;
    }
    public function fechas_de_servicio(){
        return $this->hasMany(FechaDeServicio::class, 'id_cliente', 'account_id');
    }
}
