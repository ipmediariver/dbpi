<?php
namespace App\Console;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->everyMinute();
        $schedule->command('accounts:sync');
        $schedule->command('send:emails')->withoutOverlapping();
        $schedule->command('mantenimientos:diario')->dailyAt('06:00');
        $schedule->command('mantenimientos:semanal')->dailyAt('06:15');
        $schedule->command('mantenimientos:quincenal')->dailyAt('06:30');
        $schedule->command('mantenimientos:mensual')->dailyAt('06:45');
    }
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}