<?php

namespace App\Console\Commands;

use App\COMERCIAL\Fup;
use App\COMERCIAL\Quote;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ComercialCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:comercial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store comercial data into cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        print("Eliminando cache \n");
        \Artisan::call('cache:clear');
        
        print("Subtotal de cotizaciones autorizadas internamente en pesos \n");
        cache()->rememberForever('quotes.internal_authorization_mxn.subtotal', function(){
            return Quote::where('status_id', 2)
                ->where('currency_id', 1)
                ->get()
                ->pluck('subtotal_on_quote');
        });

        print("Subtotal de cotizaciones autorizadas internamente en dolares \n");
        cache()->rememberForever('quotes.internal_authorization_usd.subtotal', function(){
            return Quote::where('status_id', 2)
                ->where('currency_id', 2)
                ->get()
                ->pluck('subtotal_on_quote');
        });

        print("Subtotal de cotizaciones autorizadas por cliente en pesos \n");
        cache()->rememberForever('quotes.authorized_by_cliente_mxn.subtotal', function(){
            return Quote::where('status_id', 6)
                ->where('currency_id', 1)
                ->get()
                ->pluck('subtotal_on_quote');
        });

        print("Subtotal de cotizaciones autorizadas por cliente en dolares \n");
        cache()->rememberForever('quotes.authorized_by_cliente_usd.subtotal', function(){
            return Quote::where('status_id', 6)
                ->where('currency_id', 2)
                ->get()
                ->pluck('subtotal_on_quote');
        });

        print("Total de FUPs \n");
        cache()->rememberForever('fups.count', function(){
            return Fup::count();
        });

        print("Monto sin facturar en pesos \n");
        cache()->rememberForever('fups.no_billing_mxn', function(){
            return Quote::where('status_id', '>', 2)
                ->where('currency_id', 1)
                ->get()
                ->where('billing_status', 1)
                ->pluck('subtotal_on_quote');
        });

        print("Monto sin facturar en dolares \n");
        cache()->rememberForever('fups.no_billing_usd', function(){
            return Quote::where('status_id', '>', 2)
                ->where('currency_id', 2)
                ->get()
                ->where('billing_status', 1)
                ->pluck('subtotal_on_quote');
        });

        print("Datos almacenados en Cache! \n");

    }
}
