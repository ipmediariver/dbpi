<?php
namespace App\Console\Commands;

use App\CRM\Account;
use Illuminate\Console\Command;

class SyncAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounts:sync';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizar cuentas de CRM';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = Account::get_crm_accounts()['list'];
        foreach ($accounts as $key => $account) {
            $find_account = Account::where('account_id', $account['id'])->first();
            if ($find_account) {
                $find_account->update([
                    'name' => $account['name'],
                    'type' => $account['type'],
                ]);
            } else {
                $new_accout = Account::create([
                    'account_id' => $account['id'],
                    'name'       => $account['name'],
                    'type'       => $account['type'],
                ]);
            }
        }
    }
}
