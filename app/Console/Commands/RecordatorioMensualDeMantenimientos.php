<?php

namespace App\Console\Commands;

use App\Mail\MailDeRecordatorioMensualDeMantenimientos;
use App\SERVICIO\FechaDeServicio;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class RecordatorioMensualDeMantenimientos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mantenimientos:mensual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recordatorio de mantenimientos con un més de anticipación.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        print("Se buscan los mantenimientos agendados para el próximo més \n");
        $mantenimientos = $this->obtener_fechas_de_mantenimientos_para_el_proximo_mes();


        print("Se notifica a centro de control sobre los próximos mantenimientos \n");
        $this->notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos);

    }

    public function obtener_fechas_de_mantenimientos_para_el_proximo_mes(){

        $proximo_mes = Carbon::now()->addMonth()->format('Y-m-d');

        $mantenimientos = FechaDeServicio::whereDate('fecha', $proximo_mes)->get();

        return $mantenimientos;

    }

    public function notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos){

        if(count($mantenimientos)){

            Mail::to('support@godynacom.com')
                ->send(new MailDeRecordatorioMensualDeMantenimientos($mantenimientos));

        }else{

            print("No hay mantenimientos agendados en los próximos 30 días. \n");

        }

    }

}
