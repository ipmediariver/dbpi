<?php

namespace App\Console\Commands;

use App\Mail\MailDeRecordatorioSemanalDeMantenimientos;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\SERVICIO\FechaDeServicio;
use Illuminate\Support\Facades\Mail;

class RecordatorioSemanalDeMantenimientos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mantenimientos:semanal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recordatorio de mantenimientos con 1 semana de anticipación.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        print("Se buscan los mantenimientos agendados para la siguiente semana \n");
        $mantenimientos = $this->obtener_fechas_de_mantenimientos_para_la_siguiente_semana();


        print("Se notifica a centro de control sobre los próximos mantenimientos \n");
        $this->notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos);

    }

    public function obtener_fechas_de_mantenimientos_para_la_siguiente_semana(){

        $proxima_semana = Carbon::now()->addWeek()->format('Y-m-d');

        print($proxima_semana . "\n");

        $mantenimientos = FechaDeServicio::whereDate('fecha', $proxima_semana)->get();

        return $mantenimientos;

    }

    public function notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos){

        if(count($mantenimientos)){

            Mail::to('support@godynacom.com')
                ->send(new MailDeRecordatorioSemanalDeMantenimientos($mantenimientos));

        }else{

            print("No hay mantenimientos agendados en la próxima semana. \n");

        }

    }
}
