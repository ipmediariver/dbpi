<?php

namespace App\Console\Commands;

use App\Mail\MailDeRecordatorioQuincenalDeMantenimientos;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\SERVICIO\FechaDeServicio;
use Illuminate\Support\Facades\Mail;

class RecordatorioQuincenalDeMantenimientos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mantenimientos:quincenal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recordatorio de mantenimientos con 15 días de anticipación.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        print("Se buscan los mantenimientos agendados para los próximos 15 días \n");
        $mantenimientos = $this->obtener_fechas_de_mantenimientos_para_los_proximos_15_dias();


        print("Se notifica a centro de control sobre los próximos mantenimientos \n");
        $this->notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos);

    }

    public function obtener_fechas_de_mantenimientos_para_los_proximos_15_dias(){

        $proxima_quincena = Carbon::now()->addDays(15)->format('Y-m-d');

        print($proxima_quincena . "\n");

        $mantenimientos = FechaDeServicio::whereDate('fecha', $proxima_quincena)->get();

        return $mantenimientos;

    }

    public function notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos){

        if(count($mantenimientos)){

            Mail::to('support@godynacom.com')
                ->send(new MailDeRecordatorioQuincenalDeMantenimientos($mantenimientos));

        }else{

            print("No hay mantenimientos agendados en los próximos 15 días. \n");

        }

    }
}
