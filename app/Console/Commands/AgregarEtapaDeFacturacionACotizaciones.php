<?php

namespace App\Console\Commands;

use App\COMERCIAL\Quote;
use Illuminate\Console\Command;

class AgregarEtapaDeFacturacionACotizaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cotizaciones:etapa_de_facturacion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agregar etapa de facturacion en cotizaciones existentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cotizaciones = Quote::all();
        foreach($cotizaciones as $cotizacion){
            $cotizacion->etapa_de_facturacion = $cotizacion->billing_status;
            $cotizacion->save();
        }
    }
}
