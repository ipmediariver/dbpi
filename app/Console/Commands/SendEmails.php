<?php

namespace App\Console\Commands;

use App\COMERCIAL\Fup;
use App\COMERCIAL\Quote;
use App\Email;
use App\Mail\AutorizarVacacionesDeEmpleado;
use App\Mail\LinkParaSolicitudDeVacaciones;
use App\Mail\RequestQuoteValidation;
use App\Mail\ShareFupToUser;
use App\Mail\SolicitudDeVacaciones;
use App\Mail\VacacionesAutorizadas;
use App\Mail\VacacionesRechazadas;
use App\RRHH\Employee;
use App\RRHH\VacacionesDeEmpleado;
use App\User;
use App\VacacionesAutorizacion;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar correos almacenados en base de datos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = Email::orderBy('created_at')->get()->take(3);
        if (count($emails)) {
            foreach ($emails as $email) {
                if ($email->type == 'share-fup-to-user') {
                    $data       = json_decode($email->data, true);
                    $email_data = [
                        'fup'  => Fup::find($data['fup_id']),
                        'user' => User::find($data['user_id']),
                        'link' => $data['link'],
                    ];
                    Mail::to($email->to)->send(new ShareFupToUser($email_data));
                } elseif ($email->type == 'request-quote-validation') {
                    $data       = json_decode($email->data, true);
                    $email_data = [
                        'quote' => Quote::find($data['quote_id']),
                        'user'  => User::find($data['user_id']),
                        'link'  => $data['link'],
                    ];
                    Mail::to($email->to)->send(new RequestQuoteValidation($email_data));
                } elseif ($email->type == 'link-para-solicitud-de-vacaciones') {
                    $data       = json_decode($email->data, true);
                    $email_data = [
                        'empleado' => Employee::find($data['id_empleado']),
                        'link'     => $data['link'],
                    ];
                    Mail::to($email->to)->send(new LinkParaSolicitudDeVacaciones($email_data));
                } elseif ($email->type == 'nueva-solicitud-de-vacaciones') {
                    $data       = json_decode($email->data, true);
                    $solicitud  = VacacionesDeEmpleado::find($data['id_solicitud']);
                    $email_data = [
                        'solicitud' => $solicitud,
                        'empleado'  => $solicitud->empleado,
                        'link'      => $data['link'],
                    ];
                    Mail::to($email->to)->send(new SolicitudDeVacaciones($email_data));
                } elseif ($email->type == 'autorizar-vacaciones-de-empleado') {
                    $data       = json_decode($email->data, true);
                    $solicitud  = VacacionesDeEmpleado::find($data['id_solicitud']);
                    $email_data = [
                        'solicitud' => $solicitud,
                        'empleado'  => $solicitud->empleado,
                        'link'      => $data['link'],
                    ];
                    Mail::to($email->to)->send(new AutorizarVacacionesDeEmpleado($email_data));
                } elseif ($email->type == 'vacaciones-autorizadas') {
                    $data         = json_decode($email->data, true);
                    $solicitud    = VacacionesDeEmpleado::find($data['id_solicitud']);
                    $autorizacion = VacacionesAutorizacion::find($data['id_autorizacion']);
                    $email_data   = [
                        'autorizacion' => $autorizacion,
                        'solicitud'    => $solicitud,
                        'empleado'     => $solicitud->empleado,
                        'link'         => $data['link'],
                    ];
                    Mail::to($email->to)->send(new VacacionesAutorizadas($email_data));
                } elseif ($email->type == 'vacaciones-rechazadas') {
                    $data         = json_decode($email->data, true);
                    $solicitud    = VacacionesDeEmpleado::find($data['id_solicitud']);
                    $autorizacion = VacacionesAutorizacion::find($data['id_autorizacion']);
                    $email_data   = [
                        'autorizacion' => $autorizacion,
                        'solicitud'    => $solicitud,
                        'empleado'     => $solicitud->empleado,
                        'link'         => $data['link'],
                    ];
                    Mail::to($email->to)->send(new VacacionesRechazadas($email_data));
                }
                $email->delete();
            }
        } else {
            print('No hay emails pendientes.');
        }
    }
}
