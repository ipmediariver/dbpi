<?php

namespace App\Console\Commands;

use App\COMERCIAL\FupConfirmation;
use App\COMERCIAL\FupDepartmentAuth;
use Illuminate\Console\Command;

class TurnFupUserAuthIntoDepartmentAuth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fup:auth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convertir autorizaciones de usuarios a autorizaciones de departamentos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fups_confirmations = FupConfirmation::all();
        foreach($fups_confirmations as $confirmation){

            // Definimos la FUP
            $fup = $confirmation->fup;
            // Buscamos al usuario relacionado a la confirmacion
            $user = $confirmation->user;
            // Definimos el o los departamentos del usuario
            $user_departments = $user->departments->where('supervisor', 1);


            if($user_departments->count()){
                
                print("{$user->full_name} supervisa {$user_departments->count()} departamentos. \n");

                // Verificamos si la FUP ya tiene una solicitud de autorizacion a cada departamento del usuario

                foreach($user_departments as $user_department){

                    $find_fup_auth_dep = $fup->auth_departments->where('department_id', $user_department->department_id)->first();

                    if(!$find_fup_auth_dep){
                        print("Agregando departamento \"{$user_department->name}\" a FUP #{$fup->number} \n");

                        $fup_auth_dep = new FupDepartmentAuth();
                        $fup_auth_dep->fup_id = $fup->id;
                        $fup_auth_dep->department_id = $user_department->department_id;
                        $fup_auth_dep->save();
                    }

                }
            }

        }
    }
}
