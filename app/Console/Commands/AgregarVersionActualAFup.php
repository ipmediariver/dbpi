<?php
namespace App\Console\Commands;
use App\COMERCIAL\FupRepository;
use Illuminate\Console\Command;
class AgregarVersionActualAFup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fup:version_actual';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agregar version actual en FUPs existentes';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repositorios_de_fups = FupRepository::all();
        foreach($repositorios_de_fups as $repositorio){
            $fup_actual = $repositorio->current_fup;
            $fup_actual->version_actual = 1;
            $fup_actual->save();
            $fups_anteriores = $repositorio->previous_fups;
            $fups_anteriores->each(function($fup){
                $fup->version_actual = 0;
                $fup->save();
            });
        }
    }
}