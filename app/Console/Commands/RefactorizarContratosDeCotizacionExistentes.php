<?php
namespace App\Console\Commands;
use App\COMERCIAL\ContratoEnCotizacion;
use App\COMERCIAL\QuoteProduct;
use App\SERVICIO\FechaDeServicio;
use App\SERVICIO\PlantillaDeContrato;
use Illuminate\Console\Command;
class RefactorizarContratosDeCotizacionExistentes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contratos:refactorizar';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refactorizar contratos de cotización existentes.';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contratos = ContratoEnCotizacion::all();
        foreach ($contratos as $contrato) {
            $this->asignar_plantilla_a_contrato_existente($contrato);
        }
    }
    public function asignar_plantilla_a_contrato_existente($contrato)
    {
        $la_plantilla_ya_existe = PlantillaDeContrato::where('id_contrato', $contrato->id_contrato)
            ->where('id_cobertura', $contrato->id_cobertura)
            ->where('id_tiempo_de_respuesta', $contrato->id_tiempo_de_respuesta)
            ->first();
        $plantilla = $la_plantilla_ya_existe ? $la_plantilla_ya_existe : $this->creamos_una_nueva_plantilla($contrato);
        $this->asignamos_la_plantilla_al_contrato_en_cotizacion($contrato, $plantilla);
        $this->se_agrega_el_producto_de_la_plantilla_a_la_cotizacion($contrato, $plantilla);
        $this->se_relacionan_las_fechas_de_servicio_al_contrato($contrato, $plantilla);
    }
    public function se_agrega_el_producto_de_la_plantilla_a_la_cotizacion($contrato, $plantilla)
    {
        $cotizacion = $contrato->cotizacion;
        $producto   = $plantilla->producto;
        $producto_en_cotizacion                 = new QuoteProduct();
        $producto_en_cotizacion->quote_id       = $cotizacion->id;
        $producto_en_cotizacion->product_id     = $producto->id;
        $producto_en_cotizacion->quote_group_id = $contrato->id_grupo_de_cotizacion;
        $producto_en_cotizacion->provider_cost  = $contrato->costo;
        $producto_en_cotizacion->qty            = 1;
        $producto_en_cotizacion->provider_id    = 5;
        $producto_en_cotizacion->save();
    }
    public function se_relacionan_las_fechas_de_servicio_al_contrato($contrato, $plantilla)
    {
        $id_cliente         = $contrato->cotizacion->account_id;
        $fechas_de_servicio = FechaDeServicio::where('id_cliente', $id_cliente)->get();
        foreach($fechas_de_servicio as $fecha){
            info($fecha->fecha);
            $fecha->update([
                'id_contrato_en_cotizacion' => $contrato->id
            ]);
        }
    }
    public function asignamos_la_plantilla_al_contrato_en_cotizacion($contrato, $plantilla)
    {
        $contrato->id_producto_en_cotizacion = $plantilla->producto->id;
        $contrato->save();
    }
    public function creamos_una_nueva_plantilla($contrato)
    {
        $plantilla = new PlantillaDeContrato([
            'nombre'                 => $contrato->nombre_completo_de_contrato,
            'codigo'                 => 'CONTRATO-' . hexdec(uniqid()),
            'id_contrato'            => $contrato->id_contrato,
            'id_cobertura'           => $contrato->id_cobertura,
            'id_tiempo_de_respuesta' => $contrato->id_tiempo_de_respuesta,
        ]);
        $plantilla->save();
        app('App\Http\Controllers\SERVICIO\PlantillaDeContratoController')
            ->generar_nuevo_producto_relacionado_a_la_plantilla($plantilla);
        return $plantilla;
    }
}