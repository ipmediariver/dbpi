<?php

namespace App\Console\Commands;

use App\COMERCIAL\Fup;
use Illuminate\Console\Command;

class agregar_id_de_etapa_en_fups_existentes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fup:id_de_etapa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agregar id de etapa a fups existentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fups = Fup::all();
        foreach($fups as $fup){
            $fup->id_etapa = $fup->etapa_id;
            $fup->save();
        }
    }
}
