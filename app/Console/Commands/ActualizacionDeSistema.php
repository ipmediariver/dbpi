<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
class ActualizacionDeSistema extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actualizar-sistema';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualizacion de sistema.';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Actualizacion de sistema para el dia 22 de Abril 2021
        info("Se migra la base de datos.");
        Artisan::call('migrate');

        info("Se agrega el id de etapa a las fups exsitentes.");
        Artisan::call('fup:id_de_etapa');

        info("Se agrega la version actual a las fups exsitentes.");
        Artisan::call('fup:version_actual');

        info("Se agrega la etapa de facturacion a cotizaciones existentes.");
        Artisan::call('cotizaciones:etapa_de_facturacion');
    }
}