<?php

namespace App\Console\Commands;

use App\Mail\MailDeRecordatorioDeMantenimientos;
use App\SERVICIO\FechaDeServicio;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class RecordatorioDeMantenimientos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mantenimientos:diario';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recordatorio de mantenimientos del día.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        print("Se buscan los mantenimientos agendados para el día actual \n");
        $mantenimientos = $this->obtener_fechas_de_mantenimientos_para_el_dia_actual();


        print("Se notifica a centro de control sobre los próximos mantenimientos \n");
        $this->notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos);

    }

    public function obtener_fechas_de_mantenimientos_para_el_dia_actual(){

        $hoy = Carbon::now()->format('Y-m-d');

        $mantenimientos = FechaDeServicio::whereDate('fecha', $hoy)->get();

        return $mantenimientos;

    }

    public function notificar_a_centro_de_control_sobre_proximos_mantenimientos($mantenimientos){

        if(count($mantenimientos)){

            Mail::to('support@godynacom.com')
                ->send(new MailDeRecordatorioDeMantenimientos($mantenimientos));

        }else{

            print("No hay mantenimientos agendados para el día de hoy. \n");

        }

    }

}
