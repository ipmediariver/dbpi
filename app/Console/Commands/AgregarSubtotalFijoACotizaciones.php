<?php

namespace App\Console\Commands;

use App\COMERCIAL\Quote;
use Illuminate\Console\Command;

class AgregarSubtotalFijoACotizaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subtotal-fijo-a-cotizaciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se agrega el subtotal fijo a cotizaciones existentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        print("Agregando valores fijos a cotizaciones existentes... \n");

        $cotizaciones = Quote::all()->each(function ($cotizacion) {

            print("Modificando cotizacion #{$cotizacion->quote_id} \n");

            $cotizacion->subtotal_fijo          = $cotizacion->subtotal;
            $cotizacion->porcentaje_margen_fijo = $cotizacion->margin_percent;
            $cotizacion->total_margen_fijo      = $cotizacion->margin_cost;
            $cotizacion->total_fijo             = $cotizacion->total;

            $cotizacion->save();

        });

    }
}
