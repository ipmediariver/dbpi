<?php

namespace App\Console\Commands;

use App\COMERCIAL\Quote;
use Illuminate\Console\Command;

class SetCurrentVersionToExistingQuotes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quote:set-current-version';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Definir la version actual de cotizaciones existentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $quotes = Quote::all();

        foreach($quotes as $quote){

            $opportunity_id = $quote->opportunity_id;

            $quotes_from_same_oppotunity = Quote::where('opportunity_id', $opportunity_id)->orderBy('created_at', 'desc')->get();

            foreach($quotes_from_same_oppotunity as $key => $quote_from_opportunity){
                print("Definiendo la cotizacion actual de la oportunidad {$opportunity_id} \n");
                if($key == 0){
                    $quote_from_opportunity->current_version = 1;
                    $quote_from_opportunity->save();
                }else{
                    $quote_from_opportunity->current_version = 0;
                    $quote_from_opportunity->save();
                }
            }

        }
    }
}
