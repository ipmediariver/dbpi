<?php
namespace App\Imports;
use App\COMERCIAL\Product;
use App\COMERCIAL\ProductBrand;
use App\COMERCIAL\ProductCategory;
use App\COMERCIAL\ProductModel;
use App\COMERCIAL\ProductType;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class UploadProducts implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $valid_format = $this->validate_excel_format($collection);
        if($valid_format){
            foreach ($collection as $row) {
                if($row['tipo']){
                    $type = ProductType::firstOrCreate([
                        'name' => strtoupper($row['tipo']),
                    ]);
                }else{
                    $type = ProductType::firstOrCreate([
                        'name' => 'SIN TIPO'
                    ]);
                }
                if($row['marca']){
                    $brand = ProductBrand::firstOrCreate([
                        'product_type_id' => $type->id,
                        'name' => strtoupper($row['marca']),
                    ]);
                }else{
                    $brand = ProductBrand::firstOrCreate([
                        'product_type_id' => $type->id,
                        'name' => 'SIN MARCA',
                    ]);
                }
                if($row['modelo']){
                    $model = ProductModel::firstOrCreate([
                        'product_type_id' => $type->id,
                        'product_brand_id' => $brand->id,
                        'name' => strtoupper($row['modelo']),
                    ]);
                }else{
                    $model = ProductModel::firstOrCreate([
                        'product_type_id' => $type->id,
                        'product_brand_id' => $brand->id,
                        'name' => 'SIN MODELO',
                    ]);
                }
                if($row['categoria']){
                    $category = ProductCategory::firstOrCreate([
                        'name'    => strtoupper($row['categoria'])
                    ]);
                }else{
                    $category = ProductCategory::firstOrCreate([
                        'name'    => 'SIN CATEGORIA',
                    ]);
                }
                $product = Product::firstOrCreate([
                    'product_type_id'     => $type->id,
                    'product_brand_id'    => $brand->id,
                    'product_model_id'    => $model->id,
                    'code'                => $row['codigo'],
                    'sat_code'            => $row['sat'],
                    'product_category_id' => $category->id,
                    'description'         => strtoupper($row['nombre']),
                ]);
            }
        }else{
            abort();
        }
    }

    public function validate_excel_format($collection){
        $row = $collection[0];
        $valid = false;
        if( isset($row['codigo']) &&
            isset($row['nombre']) &&
            isset($row['sat']) &&
            isset($row['categoria']) &&
            isset($row['tipo']) &&
            isset($row['marca']) &&
            isset($row['modelo'])){

            $valid = true;

        }

        return $valid;
    }
}