<?php
namespace App\Imports;

use App\COMERCIAL\Product;
use App\COMERCIAL\ProductBrand;
use App\COMERCIAL\ProductCategory;
use App\COMERCIAL\ProductModel;
use App\COMERCIAL\ProductType;
use App\COMERCIAL\Quote;
use App\COMERCIAL\QuoteProduct;
use App\COMERCIAL\QuoteProductCategory;
use App\COMPRAS\Provider;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UploadProductsToQuote implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */

    private $data_validation = [
        'status' => false,
        'errors' => []
    ];

    public function collection(Collection $collection)
    {
        $valid_format = $this->validate_excel_format($collection);

        if ($valid_format) {

            $this->validate_data($collection);

            if ($this->data_validation['status']) {

                $quote = Quote::find(request('quote'));
                $group = $quote->groups()->find(request('group'));

                foreach ($collection as $row) {
                    if ($row['cantidad'] > 0) {
                        $product = $this->store_product($row);
                        $this->add_product_to_quote($quote, $group, $product, $row);
                    }
                }

            }else{
                info("La información no esta bien estructurada.");
            }

        }
    }
    public function validate_excel_format($collection)
    {
        $row   = $collection[0];
        $valid = false;
        if (isset($row['cantidad']) &&
            isset($row['no_de_parte']) &&
            isset($row['codigo_sat']) &&
            isset($row['nombre']) &&
            isset($row['tipo']) &&
            isset($row['marca']) &&
            isset($row['modelo']) &&
            isset($row['categoria']) &&
            isset($row['proveedor']) &&
            isset($row['precio_de_lista']) &&
            isset($row['desc']) &&
            isset($row['desc_especial']) &&
            isset($row['desc_cliente']) &&
            isset($row['importe_de_venta'])) {

            $valid = true;

        }else{
            $this->data_validation['status'] = false;
            array_push($this->data_validation['errors'], "El formato no es válido"); 
        }

        return $valid;
    }
    public function validate_data($collection)
    {

        foreach ($collection as $key => $row) {
            $row_number = $key + 2;
            if (!$row['cantidad'] > 0) {
                $error = "La cantidad debe ser mayor a 0 (renglón #{$row_number})";
                array_push($this->data_validation['errors'], $error);
            }

            if (!$row['no_de_parte']) {
                $error = "El número de parte es requerido en (renglón #{$row_number})";
                array_push($this->data_validation['errors'], $error);
            }

            if (!$row['nombre']) {
                $error = "El nombre del producto es requerido en (renglón #{$row_number})";
                array_push($this->data_validation['errors'], $error);
            }

            if (is_null($row['precio_de_lista'])) {
                $error = "El precio de lista no puede estar vacío, agrega un 0 si no hay valor en (renglón #{$row_number})";
                array_push($this->data_validation['errors'], $error);
            }
        }

        $this->data_validation['status'] = count($this->data_validation['errors']) ? false : true;

    }
    public function store_product($row)
    {

        $product = null;

        if ($row['no_de_parte']) {

            $product = Product::where('code', $row['no_de_parte'])->first();

        } else {

            $product = Product::where('description', strtoupper($row['nombre']))->first();

        }

        if (!$product) {
            if ($row['tipo']) {
                $type = ProductType::firstOrCreate([
                    'name' => strtoupper($row['tipo']),
                ]);
            } else {
                $type = ProductType::firstOrCreate([
                    'name' => 'SIN TIPO',
                ]);
            }
            if ($row['marca']) {
                $brand = ProductBrand::firstOrCreate([
                    'product_type_id' => $type->id,
                    'name'            => strtoupper($row['marca']),
                ]);
            } else {
                $brand = ProductBrand::firstOrCreate([
                    'product_type_id' => $type->id,
                    'name'            => 'SIN MARCA',
                ]);
            }
            if ($row['modelo']) {
                $model = ProductModel::firstOrCreate([
                    'product_type_id'  => $type->id,
                    'product_brand_id' => $brand->id,
                    'name'             => strtoupper($row['modelo']),
                ]);
            } else {
                $model = ProductModel::firstOrCreate([
                    'product_type_id'  => $type->id,
                    'product_brand_id' => $brand->id,
                    'name'             => 'SIN MODELO',
                ]);
            }
            if ($row['categoria']) {
                $category = ProductCategory::firstOrCreate([
                    'name' => strtoupper($row['categoria']),
                ]);
            } else {
                $category = ProductCategory::firstOrCreate([
                    'name' => 'SIN CATEGORIA',
                ]);
            }
            $product = Product::firstOrCreate([
                'product_type_id'     => $type->id,
                'product_brand_id'    => $brand->id,
                'product_model_id'    => $model->id,
                'code'                => $row['no_de_parte'],
                'sat_code'            => $row['codigo_sat'],
                'product_category_id' => $category->id,
                'description'         => strtoupper($row['nombre']),
            ]);
        }

        return $product;
    }
    public function add_product_to_quote($quote, $group, $product, $row)
    {

        // PROVEEDOR

        if ($row['proveedor']) {
            $provider = Provider::firstOrCreate([
                'name' => strtoupper($row['proveedor']),
            ]);
        } else {
            $provider = Provider::firstOrCreate([
                'name' => 'SIN PROVEEDOR',
            ]);
        }

        // CATEGORIA

        QuoteProductCategory::firstOrCreate([
            'quote_id'    => $quote->id,
            'category_id' => $product->category->id,
        ]);

        // PRODUCTO

        $quote_product = QuoteProduct::create([
            'quote_id'                  => $quote->id,
            'quote_group_id'            => $group->id,
            'product_id'                => $product->id,
            'provider_id'               => $provider->id,
            'qty'                       => $row['cantidad'] ?? 1,
            'provider_cost'             => $row['precio_de_lista'] ?? 0,
            'provider_discount'         => $row['desc'] ?? 0,
            'provider_special_discount' => $row['desc_especial'] ?? 0,
            'customer_discount'         => $row['desc_cliente'] ?? 0,
            'custom_import'             => $row['importe_de_venta'] ?? 0,
        ]);
    }

    public function getStatus(){
        return $this->data_validation;
    }
}
