<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => 'auth'], function () {
    // RECURSOS HUMANOS
    Route::group([
        'prefix' => 'rrhh', 
        'namespace' => 'RRHH', 
        'middleware' => [
            'module_permission:m-01', 'module_permission_method:m-01'
        ]
    ], function () {
        Route::get('/', 'DashboardController@index')
            ->name('rrhh');
        Route::redirect('/', 'rrhh/cuentas');
        Route::get('cuentas', 'EmployeeController@accounts')
            ->name('get-employees-accounts');
        Route::get('cuentas/{account}', 'EmployeeController@account')
            ->name('get-employees-account');
        Route::resource('empleados', 'EmployeeController')
            ->parameters(['empleados' => 'employee']);
        Route::resource('empleados.archivos-adjuntos', 'EmployeeAttachmentController')
            ->parameters(['empleados' => 'employee', 'archivos-adjuntos' => 'attachment']);

        Route::post('/categoriesattachment', 'EmployeeAttachmentController@store_category_attachment')
            ->name('storecategoriesattachment');

        Route::put('/categoriesattachment/{id}', 'EmployeeAttachmentController@edit_category_attachment')
            ->name('editcategoriesattachment');
        
            Route::put('/archivos-adjuntos/{id}', 'EmployeeAttachmentController@update_category_attachment')
            ->name('archivos-adjuntos.update');

            Route::get('empleados/{employee}/informacion-personal', 'EmployeeController@personal_information')
            ->name('employee-personal-information');
        Route::get('empleados/{employee}/salario-mensual', 'EmployeeController@monthly_salary')
            ->name('employee-monthly-salary');
        Route::get('empleados/{employee}/fecha-contratacion', 'EmployeeController@active_and_unactive_date')
            ->name('employee-active-and-unactive-date');
        Route::get('empleados/{employee}/departamento', 'EmployeeController@department_and_jobtitle')
            ->name('employee-department');
        Route::get('empleados/{employee}/informacion-bancaria', 'EmployeeController@bank_information')
            ->name('employee-bank-information');
        Route::get('empleados/{employee}/descuentos', 'EmployeeController@discounts_information')
            ->name('employee-discounts-information');
        Route::get('empleados/{employee}/historial-de-vacaciones', 'EmployeeController@historial_de_vacaciones')
            ->name('historial-de-vacaciones');
        Route::get('empleados/{employee}/link-de-solicitud-de-vacaciones', 'EmployeeController@generar_link_de_solicitud_de_vacaciones')
            ->name('generar-link-de-solicitud-de-vacaciones');
        Route::post('enviar-link-de-solicitud-de-vacaciones-a-empleado', 'EmployeeController@enviar_link_solicitud_de_vacaciones_a_empleado');
        Route::post('empleados/{empleado}/vacaciones', 'EmployeeController@agregar_vacaciones');
        Route::delete('empleados/{empleado}/vacaciones/{vacaciones}', 'EmployeeController@eliminar_vacaciones');

        Route::resource('fechas', 'PayrollController')
            ->parameters(['fechas' => 'payroll']);
        Route::post('crear-calendario-de-nominas', 'PayrollController@create_calendar')
            ->name('create-payroll-calendar');
        Route::redirect('nomina', 'nomina-actual');
        Route::get('nomina/{account_payroll}', 'PayrollController@get_payroll')
            ->name('payroll');
        Route::post('nomina/{account_payroll}/update-employees', 'PayrollController@update_employees_from_account_payroll')
            ->name('update-employees-from-account-payroll');
        Route::get('nomina/{account_payroll}/empleado/{employee_payroll}/descuentos', 'PayrollController@get_employee_payroll_discounts')
            ->name('employee_discounts');
        Route::delete('eliminar-descuento/{discount}', 'PayrollController@delete_employee_payroll_discount')
            ->name('delete-discount');
        Route::get('nomina/{account_payroll}/empleado/{employee_payroll}/ingresos', 'PayrollController@get_employee_payroll_entries')
            ->name('employee_entries');
        Route::delete('eliminar-ingreso/{entry}', 'PayrollController@delete_employee_payroll_entry')
            ->name('delete-entry');
        Route::post('siguiente-fecha', 'PayrollController@next_date')
            ->name('next-date');
        Route::post('cerrar-nomina/{account_payroll}', 'PayrollController@close_payroll')
            ->name('close-payroll');
        Route::post('abrir-nomina/{account_payroll}', 'PayrollController@open_payroll')
            ->name('open-payroll');
        Route::get('nomina-actual', 'PayrollController@get_current_payroll')
            ->name('current-payroll');
        Route::post('nueva-falta', 'PayrollController@new_incidence')
            ->name('new-incidence');
        Route::post('nuevo-descuento', 'PayrollController@new_discount')
            ->name('new-discount');
        Route::post('nuevo-descuento-de-dias', 'PayrollController@new_discount_per_days')
            ->name('new-discount-per-days');
        Route::post('nuevo-ingreso-de-dias', 'PayrollController@new_entry_per_days')
            ->name('new-entry-per-days');
        Route::post('nuevo-ingreso', 'PayrollController@new_entry')
            ->name('new-entry');
        Route::post('eliminar-empleado-de-nomina', 'PayrollController@remove_employee_payroll')
            ->name('remove-employee-payroll');
        Route::group(['prefix' => 'config'], function () {
            Route::redirect('/', 'config/departamentos');
            Route::resource('departamentos', 'DepartmentController')
                ->parameters(['departamentos' => 'department']);
            Route::resource('ubicaciones', 'LocationController')
                ->parameters(['ubicaciones' => 'location']);
        });
        Route::post('descargar-nomina/{account_payroll}', 'PayrollController@download_payroll')
            ->name('download-payroll');
    });
});


## DEPARTAMENTOS
Route::get('lista-de-departamentos', 'RRHH\DepartmentController@index');
Route::get('rrhh/solicitud-de-vacaciones', 'RRHH\EmployeeController@plantilla_para_solicitud_de_vacaciones');
Route::post('rrhh/solicitud-de-vacaciones', 'RRHH\EmployeeController@enviar_plantilla_para_solicitud_de_vacaciones');
Route::get('rrhh/autorizar-vacaciones/{solicitud}', 'RRHH\EmployeeController@autorizar_vacaciones')->name('autorizar-vacaciones');
Route::post('rrhh/autorizar-vacaciones/{solicitud}', 'RRHH\EmployeeController@vacaciones_autorizadas')->name('vacaciones-autorizadas');
Route::post('rrhh/rechazar-vacaciones/{solicitud}', 'RRHH\EmployeeController@vacaciones_rechazadas')->name('rechazar-autorizadas');