<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => 'auth'], function () {
    // SERVICIO
    Route::group([
        'prefix'     => 'help-desk',
        'namespace'  => 'SERVICIO',
        'middleware' => [
            'module_permission:m-07', 'module_permission_method:m-07',
        ],
    ], function () {
        Route::redirect('/', '/help-desk/calendario')->name('servicio');
        Route::group(['prefix' => 'calendario'], function () {
            Route::get('/', 'ServicioController@calendario')->name('calendario');
        });
        Route::get('fechas-de-servicio', 'ServicioController@obtener_fechas_de_servicio');
        Route::post('guardar-servicio', 'ServicioController@guardar_servicio');
        Route::delete('eliminar-fecha-de-servicio/{fecha_de_servicio}', 'ServicioController@eliminar_servicio');
        Route::put('actualizar-fecha-de-servicio/{fecha_de_servicio}', 'ServicioController@actualizar_fecha');
        Route::get('lista-de-tipos-de-servicios', 'ServicioController@tipos_de_servicios');
        Route::get('detalles-de-fecha-de-servicio/{fecha_de_servicio}', 'ServicioController@detalle_de_fecha_de_servicio');
        Route::get('obtener-cuentas-de-clientes', 'ServicioController@obtener_cuentas_de_clientes');
        Route::get('obtener-ingenieros-de-servicio', 'ServicioController@obtener_ingenieros_de_servicio');
        Route::post('crear-ticket-para-fecha-de-servicio/{fecha_de_servicio}', 'ServicioController@nuevo_ticket_para_fecha_de_servicio');
        Route::get('obtener-categorias-principales-de-tickets', 'ServicioController@obtener_categorias_principales');
        Route::get('obtener-subcategorias/{categoria_padre}', 'ServicioController@obtener_subcategorias');
        Route::get('obtener-severidades-de-tickets', 'ServicioController@obtener_severidades_de_tickets');
        Route::get('obtener-contactos-de-cliente/{id_cliente}', 'ServicioController@obtener_contactos_de_cliente');
        Route::get('obtener-los-otros-servicios-de-cliente/{fecha_de_servicio}', 'ServicioController@otros_servicios_de_cliente');
        Route::group(['prefix' => 'contratos'], function () {
            Route::get('/', 'ContratoController@index')->name('contratos');
            Route::post('/', 'ContratoController@store');
            Route::get('/lista', 'ContratoController@obtener_lista_de_contratos');
            Route::get('/{contrato}', 'ContratoController@show');
            Route::put('/{contrato}', 'ContratoController@update');
            Route::delete('/{contrato}', 'ContratoController@destroy');
        });
        Route::group(['prefix' => 'coberturas'], function () {
            Route::get('/', 'CoberturaController@index')->name('coberturas');
            Route::get('/lista', 'CoberturaController@obtener_lista_de_coberturas');
            Route::post('/', 'CoberturaController@store');
            Route::put('/{cobertura}', 'CoberturaController@update');
            Route::delete('/{cobertura}', 'CoberturaController@destroy');
        });
        Route::group(['prefix' => 'tipos-de-cobertura'], function () {
            Route::get('/', 'CoberturaController@obtener_tipos_de_cobertura');
            Route::post('/', 'CoberturaController@guardar_tipo_de_cobertura');
            Route::delete('/{tipo_de_cobertura}', 'CoberturaController@eliminar_tipo_de_cobertura');
        });
        Route::group(['prefix' => 'tiempos-de-respuesta'], function () {
            Route::get('/', 'TiempoDeRespuestaController@index')->name('tiempos-de-respuesta');
            Route::get('/lista', 'TiempoDeRespuestaController@lista');
            Route::post('/', 'TiempoDeRespuestaController@store');
            Route::put('/{tiempo_de_respuesta}', 'TiempoDeRespuestaController@update');
            Route::delete('/{tiempo_de_respuesta}', 'TiempoDeRespuestaController@destroy');
        });
        Route::group(['prefix' => 'plantillas'], function () {
            Route::get('/', 'PlantillaDeContratoController@index')->name('plantillas-de-contratos');
            Route::post('/', 'PlantillaDeContratoController@store');
            Route::put('/{plantilla}', 'PlantillaDeContratoController@update');
            Route::delete('/{plantilla}', 'PlantillaDeContratoController@destroy');
            Route::get('/lista', 'PlantillaDeContratoController@lista');
        });
    });
});
