<?php
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => 'auth'], function () {
    // COMERCIAL
    Route::group([
        'prefix'     => 'comercial',
        'namespace'  => 'COMERCIAL',
        'middleware' => [
            'module_permission:m-02', 'module_permission_method:m-02',
        ],
    ], function () {
        Route::get('/', 'DashboardController@index')->name('comercial');
        ##  DASHBOARD
        Route::group(['prefix' => 'dashboard-data'], function(){
            Route::get('/', 'ComercialDashboardController@index');
        });
        ##  OPORTUNIDADES
        Route::resource('oportunidades', 'OpportunityController');
        Route::post('cotizar-oportunidad/{id}', 'OpportunityController@quote')->name('quote-opportunity');
        Route::post('sinc-crm-info/{quote}', 'OpportunityController@sync')->name('sync-data');
        ##  COTIZACIONES
        Route::resource('cotizaciones', 'QuoteController')->parameters([
            'cotizaciones' => 'quote',
        ]);
        Route::post('cotizaciones/resturar-oportunidad-en-crm', 'QuoteController@restaurar_oportunidad_en_crm')
            ->name('restaurar_oportunidad_en_crm');
        Route::post('cotizaciones/{quote}/validacion-interna', 'QuoteController@internal_validation');
        Route::post('cotizaciones/{quote}/reenviar-validacion-interna/{validation}', 'QuoteController@resend_internal_validation_request');
        Route::post('cotizaciones/{quote}/modificar-status', 'QuoteController@modify_status');
        // Grupos en cotizaciones
        Route::resource('cotizaciones.grupos', 'QuoteGroupController')->parameters([
            'cotizaciones' => 'quote',
            'grupos'       => 'group',
        ]);
        // Comentarios en contizaciones
        Route::post('cotizaciones/{quote}/comentario', 'QuoteController@add_comment');
        // Productos en cotizaciones
        Route::resource('cotizaciones.grupos.productos', 'QuoteProductController')->parameters([
            'cotizaciones' => 'quote',
            'grupos'       => 'group',
            'productos'    => 'quote_product',
        ]);
        Route::post('agregar-detalles-a-producto/{producto}', 'QuoteProductController@detalles_adicionales');
        // Recursos en cotizaciones
        Route::post('cotizaciones/{quote}/grupos/{group}/productos/{quote_product}/porcentaje-de-margen', 'QuoteProductController@set_custom_margin_percent');
        Route::post('cotizaciones/{quote}/grupos/{group}/productos/{quote_product}/total-de-margen', 'QuoteProductController@set_custom_margin_total');
        Route::post('cotizaciones/{quote}/grupos/{group}/productos/{quote_product}/precio-de-venta', 'QuoteProductController@set_custom_customer_cost');
        Route::resource('cotizaciones.grupos.recursos', 'QuoteResourceController')->parameters([
            'cotizaciones' => 'quote',
            'grupos'       => 'group',
            'recursos'     => 'quote_resource',
        ]);
        // Contratos en cotizaciones
        Route::post('cotizaciones/{cotizacion}/grupos/{grupo}/contratos', 'ContratoEnCotizacionController@guardar')
            ->name('guardar_contrato');
        Route::put('cotizaciones/{cotizacion}/grupos/{grupo}/contratos/{contrato}', 'ContratoEnCotizacionController@actualizar')
            ->name('editar_contrato');
        Route::delete('cotizaciones/{cotizacion}/grupos/{grupo}/contratos/{contrato}', 'ContratoEnCotizacionController@eliminar')
            ->name('eliminar_contrato');
        Route::post('activar-contrato/{contrato}', 'ContratoEnCotizacionController@activar');
        Route::delete('eliminar-contrato-desde-fup/{contrato}', 'ContratoEnCotizacionController@eliminar_desde_fup');
        Route::post('agendar-servicios/{contrato}', 'ContratoEnCotizacionController@agendar_servicios_de_mantenimiento');
        Route::post('reagendar-servicios/{contrato}', 'ContratoEnCotizacionController@reagendar_servicios_de_mantenimiento');
        Route::post('/actualizar-costo-de-contrato/{contrato}', 'ContratoEnCotizacionController@actualizar_costo');
        Route::put('/actualizar-vigencia-de-contrato/{contrato}', 'ContratoEnCotizacionController@actualizar_vigencia');
        Route::get('/cargar-productos-de-tipo-contrato', 'ContratoEnCotizacionController@cargar_productos_de_tipo_contrato');
        // Terminan contratos en cotizaciones
        Route::post('cotizaciones/{quote}/grupos/{group}/recursos/{quote_resource}/porcentaje-de-margen', 'QuoteResourceController@set_custom_margin_percent');
        Route::post('cotizaciones/{quote}/grupos/{group}/recursos/{quote_resource}/total-de-margen', 'QuoteResourceController@set_custom_margin_total');
        Route::resource('cotizaciones.categorias', 'QuoteProductCategoryController')->parameters([
            'cotizaciones' => 'quote',
            'categorias'   => 'category',
        ]);
        Route::post('/cotizaciones/{quote}/actualizar-categorias', 'QuoteController@actualizar_categorias');
        Route::get('dependencias-para-productos', 'QuoteProductController@product_dependencies');
        Route::get('dependencias-de-cotizacion/{quote}', 'QuoteController@dependencies');
        Route::resource('cotizaciones.adjuntos', 'QuoteAttachmentController')
            ->parameters([
                'cotizaciones' => 'quote',
                'adjuntos' => 'attachment'
            ]);
        Route::post('cotizaciones/{quote}/clonar', 'QuoteController@clone');
        Route::post('cotizaciones/{quote}/generar-fup', 'QuoteController@generate_fup');
        Route::post('cotizaciones/{quote}/importar-productos', 'QuoteController@import_products');
        Route::post('cotizaciones/{quote}/extraer-productos/grupo/{group}', 'QuoteController@extract_products');
        Route::post('cotizaciones/{quote}/exportar-pdf', 'QuoteController@export_to_pdf')->name('export-quote-to-pdf');
        Route::post('cotizaciones/{quote}/exportar-margin-pdf', 'QuoteController@export_margin_to_pdf')->name('export-margin-to-pdf');
        Route::post('cotizaciones/{quote}/participante', 'QuoteController@add_participant');
        Route::post('cotizaciones/buscar', 'QuoteController@search')->name('search-quote');
        ##  FUPS
        Route::resource('fuprep', 'FupRepositoryController');
        Route::resource('fuprep.fups', 'FupController');
        Route::post('/fups/{fup}/nueva-observacion', 'FupController@new_observation');
        Route::delete('/fups/{fup}/eliminar-observacion/{observation}', 'FupController@remove_observation');
        Route::post('/fups/{fup}/compartir', 'FupController@share');
        Route::get('/fups/{fup}/previsualizacion', 'FupController@preview')->name('fup-preview');
        Route::post('/fups/{fup}/comentario', 'FupController@comment');
        Route::post('/fups/{fup}/comentar-version', 'FupController@version_comment');
        Route::get('/fups/{fup}/comentarios-de-version', 'FupController@get_version_comments');
        Route::post('/fups/{fup}/asignar-responsable', 'FupController@assign_responsable');
        Route::post('/fups/{fup}/confirmar-de-recibido', 'FupController@confirmed');
        Route::post('/fups/{fup}/descargar', 'FupController@download')->name('download-fup');
        Route::post('/fups/{fup}/descargar-fup-y-cotizacion', 'FupController@download_fup_and_quote')->name('download-fup-and-quote');
        Route::post('/fups/{fup}/solicitar-cancelacion', 'FupController@request_cancellation')->name('fup-cancellation-request');
        Route::post('/fups/{fup}/cancelacion/{fup_cancellation}/autorizar', 'FupController@auth_cancellation');
        Route::post('/fups/{fup}/cancelacion/{fup_cancellation}/rechazar', 'FupController@reject_cancellation');
        ## PRODUCTOS
        Route::resource('productos', 'ProductController')->parameters([
            'productos' => 'product'
        ]);
        Route::post('/productos/importar', 'ProductController@upload');
        Route::post('/productos/extraer', 'ProductController@extract');
        Route::post('productos/{product}/agregar-imagenes', 'ProductController@upload_images');
        Route::post('productos/{product}/eliminar-imagen/{img}', 'ProductController@remove_image');
        ## PROVEEDORES
        Route::resource('proveedores', 'ProviderController')->parameters([
            'proveedores' => 'provider'
        ]);
        // TABLERO
        Route::get('tablero', 'TableroController@index')->name('tablero');
        Route::get('tablero/indicadores', 'TableroController@indicadores')->name('indicadores-tablero');
        Route::get('tablero/solicitar','TableroController@solicitar_indicadores_de_tablero_por_ano');
        // RUTAS ESPECIALES
        Route::get('/obtener-listado-de-cuentas', function(){
            $cuentas = App\CRM\Account::orderBy('name')->select('name', 'account_id')->get();
            return $cuentas;
        });
        Route::get('/obtener-etapas-de-cotizacion', function(){
            $etapas = [
                [
                    'id' => 0,
                    'nombre' => 'En proceso'
                ],
                [
                    'id' => 1,
                    'nombre' => 'Esperando autorización'
                ],
                [
                    'id' => 2,
                    'nombre' => 'Autorizada internamente'
                ],
                [
                    'id' => 3,
                    'nombre' => 'En espera de autorización del cliente'
                ],
                [
                    'id' => 4,
                    'nombre' => 'Cancelada'
                ],
                [
                    'id' => 5,
                    'nombre' => 'Negociación/Revisión'
                ],
                [
                    'id' => 6,
                    'nombre' => 'Autorizada por cliente'
                ],
            ];
            return $etapas;
        });
        Route::get('/obtener-usuarios-que-pueden-cotizar', 'DashboardController@obtenemos_la_lista_de_los_usuarios_que_pueden_cotizar');
    });
});
Route::group(['prefix' => 'comercial', 'namespace'  => 'COMERCIAL',], function(){
    Route::get('cotizaciones/{quote}/validar', 'QuoteController@show_internal_validation')
        ->name('quote-internal-validation');
    Route::post('cotizaciones/{quote}/reject', 'QuoteController@reject_internal_validation');
    Route::post('cotizaciones/{quote}/accept', 'QuoteController@accept_internal_validation');
    //Rutas para peticiones de indicadores.
    Route::get('indicadores','DashboardController@obtener_indicadores_por_periodo_y_cliente');
});