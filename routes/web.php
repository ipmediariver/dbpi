<?php
use App\COMERCIAL\FupConfirmation;
use App\RRHH\Location;
use Illuminate\Support\Facades\Route;
Auth::routes(['register' => false]);
Route::group(['middleware' => 'auth'], function () {
    // Route::redirect('register', 'login');
    Route::get('/', 'AppController@index')->name('apps');
    // NOTIFICACIONES

    Route::get('notificacion/{notification}', 'AppController@mark_as_read')->name('mark_notification_as_read');
    Route::get('notificaciones', 'CONFIG\UserController@notifications')->name('notifications');
    // GLOBAL CONFIG
    Route::group(['prefix' => 'config', 'namespace' => 'CONFIG'], function () {
        Route::redirect('/', 'config/usuarios');
        Route::resource('usuarios', 'UserController')
        	->parameters(['usuarios' => 'user']);
        Route::post('asignar-departamento-a-usuario/{user}', 'UserDepartmentController@assign')
            ->name('asignar-departamento');
        Route::delete('desasignar-departamento-a-usuario/{user_department}', 'UserDepartmentController@unassign')
            ->name('desasignar-departamento');
        Route::post('desplegar-menu-de-modulos', 'UserController@toggle_module_sidebar');
        Route::get('usuarios-validadores', 'UserController@get_validators');
        Route::post('otorgar-permisos/{user}', 'UserController@permissions')
        	->name('otorgar-permisos');
        Route::get('cuentas', 'GlobalController@accounts')
        	->name('cuentas');
        Route::post('sincronizar-cuentas', 'GlobalController@sync_accounts')
        	->name('sincronizar-cuentas');
        Route::get('migraciones', 'GlobalController@migrations')
            ->name('migraciones');
        Route::post('correr-migraciones', 'GlobalController@run_migrations')
            ->name('correr-migraciones');
        Route::get('actualizaciones', 'GlobalController@actualizaciones')
            ->name('actualizaciones');
        Route::post('actualizar-sistema', 'GlobalController@actualizar_sistema')
            ->name('actualizar-sistema');
    });

});
