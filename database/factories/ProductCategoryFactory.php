<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\COMERCIAL\ProductCategory;
use Faker\Generator as Faker;
$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'percent' => 30,
        'color' => $faker->hexcolor
    ];
});