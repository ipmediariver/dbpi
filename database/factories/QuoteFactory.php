<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\COMERCIAL\Quote;
use Carbon\Carbon;
use Faker\Generator as Faker;
$factory->define(Quote::class, function (Faker $faker) {
    return [
        'quote_id' => 1,
        'account_id' => 1,
        'date' => Carbon::now(),
    ];
});