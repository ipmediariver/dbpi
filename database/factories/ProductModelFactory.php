<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\COMERCIAL\ProductModel;
use Faker\Generator as Faker;

$factory->define(ProductModel::class, function (Faker $faker) {
    return [
        'product_type_id' => 1,
        'product_brand_id' => 1,
        'name' => $faker->word
    ];
});
