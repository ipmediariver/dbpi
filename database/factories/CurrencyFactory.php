<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\RRHH\Currency;
use Faker\Generator as Faker;
$factory->define(Currency::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'short_name' => $faker->word,
    ];
});