<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\COMERCIAL\Product;
use App\COMERCIAL\ProductBrand;
use App\COMERCIAL\ProductModel;
use App\COMERCIAL\ProductType;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
    	'product_type_id' => 1,
    	'product_brand_id' => 1,
    	'product_model_id' => 1,
        'code'                => hexdec(uniqid()),
        'product_category_id' => rand(1,5),
        'description'         => $faker->sentence,
    ];
});
