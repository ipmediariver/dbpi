<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SERVICIO\Contrato;
use Faker\Generator as Faker;

$factory->define(Contrato::class, function (Faker $faker) {
    return [
        'nombre'      => 'Contrato anual',
        'descripcion' => 'Descripcion de contrato anual',
        'duracion'    => 12,
    ];
});
