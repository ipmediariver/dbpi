<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SERVICIO\Cobertura;
use Faker\Generator as Faker;

$factory->define(Cobertura::class, function (Faker $faker) {
    return [
        'nombre'               => 'Estandard',
        'id_tipo_de_cobertura' => TipoDeCobertura::factory()
    ];
});
