<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\RRHH\EmployeeStatus;
use Faker\Generator as Faker;
$factory->define(EmployeeStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'code' => $faker->word
    ];
});