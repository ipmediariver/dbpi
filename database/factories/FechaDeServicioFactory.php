<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CRM\Account;
use App\SERVICIO\FechaDeServicio;
use App\SERVICIO\TipoDeServicio;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(FechaDeServicio::class, function (Faker $faker) {
    return [
        'id_ticket'          => hexdec(uniqid()),
        'fecha'              => Carbon::now()->addDays(rand(1, 5))->hour(rand(1,12))->minute(rand(1,59)),
        'id_cliente'         => Account::inRandomOrder()->first()->account_id,
        'id_servicio'        => factory(TipoDeServicio::class)
    ];
});