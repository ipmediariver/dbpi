<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RRHH\Bank;
use Faker\Generator as Faker;

$factory->define(Bank::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'color' => $faker->hexcolor
    ];
});
