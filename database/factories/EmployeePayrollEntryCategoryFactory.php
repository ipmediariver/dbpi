<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\RRHH\EmployeePayrollEntryCategory;
use Faker\Generator as Faker;
$factory->define(EmployeePayrollEntryCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'code' => $faker->word
    ];
});