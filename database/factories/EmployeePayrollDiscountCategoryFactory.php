<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RRHH\EmployeePayrollDiscountCategory;
use Faker\Generator as Faker;

$factory->define(EmployeePayrollDiscountCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'code' => $faker->word
    ];
});
