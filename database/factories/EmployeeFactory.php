<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\CRM\Account;
use App\RRHH\Employee;
use App\RRHH\JobTitle;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Employee::class, function (Faker $faker) {
    $salary    = $faker->randomFloat($nbMaxDecimals = 2, $min = 5000, $max = 20000);
    $fonacot   = $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 400);
    $infonavit = $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 400);
    return [
        'avatar'             => 'https://api.adorable.io/avatars/120/' . rand(1, 10) . '.png',
        'account_id'         => Account::internal()->random(1)->first()->account_id,
        'location_id'        => rand(1,3),
        'department_id'      => rand(1,8),
        'jobtitle_id'        => factory(JobTitle::class),
        'name_1'             => $faker->firstName,
        'name_2'             => $faker->firstName,
        'last_name_1'        => $faker->lastName,
        'last_name_2'        => $faker->lastName,
        'email'              => $faker->email,
        'mobile'             => $faker->tollFreePhoneNumber,
        'address'            => $faker->streetAddress,
        'rfc'                => Str::random(12),
        'curp'               => Str::random(18),
        'birthday'           => Carbon::now(),
        'currency_id'        => 1,
        'salary'             => $salary,
        'status_id'          => rand(1,6),
        'bank_id'            => rand(1,6),
        'bank_account_num'   => $faker->bankAccountNumber,
        'bank_card_num'      => $faker->creditCardNumber,
        'interbank_clabe'    => $faker->bankAccountNumber,
        'fonacot_discount'   => $fonacot,
        'infonavit_discount' => $infonavit,
        'nss'                => Str::random(15),
        'active_at'          => Carbon::now(),
    ];
});
