<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\RRHH\Location;
use Faker\Generator as Faker;
$factory->define(Location::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});