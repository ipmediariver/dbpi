<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SERVICIO\TipoDeServicio;
use Faker\Generator as Faker;

$factory->define(TipoDeServicio::class, function (Faker $faker) {
    return [
        'nombre' => $faker->word
    ];
});
