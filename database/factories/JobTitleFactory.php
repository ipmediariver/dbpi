<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\RRHH\JobTitle;
use Faker\Generator as Faker;
$factory->define(JobTitle::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle
    ];
});