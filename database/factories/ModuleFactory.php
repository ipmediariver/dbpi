<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\SYSTEM\Module;
use Faker\Generator as Faker;

$factory->define(Module::class, function (Faker $faker) {
    return [
        'name'        => $faker->word,
        'code'        => $faker->word,
        'description' => $faker->paragraph,
        'icon'        => 'http://placehold.it/70x70',
        'slug'        => $faker->word,
    ];
});
