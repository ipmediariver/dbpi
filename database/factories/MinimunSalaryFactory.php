<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SYSTEM\MinimunSalary;
use Faker\Generator as Faker;

$factory->define(MinimunSalary::class, function (Faker $faker) {
    return [
        'value' => 185.56
    ];
});
