<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SYSTEM\ExchangeRate;
use Faker\Generator as Faker;

$factory->define(ExchangeRate::class, function (Faker $faker) {
    return [
        'value' => '22.5'
    ];
});
