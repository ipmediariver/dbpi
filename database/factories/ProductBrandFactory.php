<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\COMERCIAL\ProductBrand;
use Faker\Generator as Faker;

$factory->define(ProductBrand::class, function (Faker $faker) {
    return [
        'product_type_id' => 1,
        'name' => $faker->word
    ];
});
