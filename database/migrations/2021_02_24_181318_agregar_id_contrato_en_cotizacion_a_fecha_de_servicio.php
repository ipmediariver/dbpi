<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarIdContratoEnCotizacionAFechaDeServicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fecha_de_servicios', function (Blueprint $table) {
            $table->integer('id_contrato_en_cotizacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fecha_de_servicios', function (Blueprint $table) {
            $table->dropColumn('id_contrato_en_cotizacion');
        });
    }
}
