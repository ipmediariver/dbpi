<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeIdToQuoteAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quote_attachments', function (Blueprint $table) {
            $table->integer('type_id')->default(0);
            $table->double('billing_import')->nullable();
            $table->integer('billing_currency')->nullable();
            $table->string('billing_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quote_attachments', function (Blueprint $table) {
            $table->dropColumn('type_id');
            $table->dropColumn('billing_import');
            $table->dropColumn('billing_currency');
            $table->dropColumn('billing_number');
        });
    }
}
