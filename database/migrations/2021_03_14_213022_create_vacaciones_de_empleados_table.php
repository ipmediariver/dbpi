<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacacionesDeEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacaciones_de_empleados', function (Blueprint $table) {
            $table->id();
            $table->integer('id_de_empleado');
            $table->integer('dias_correspondientes');
            $table->integer('dias_a_disfrutar');
            $table->integer('dias_pendientes');
            $table->integer('periodo');
            $table->boolean('terminos')->nullable()->default(0);
            $table->date('cuando_inicia');
            $table->date('cuando_termina');
            $table->date('cuando_debe_presentarse')->nullable();
            $table->integer('etapa')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacaciones_de_empleados');
    }
}
