<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFupCancellationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fup_cancellations', function (Blueprint $table) {
            $table->id();
            $table->integer('fup_id');
            $table->integer('user_id');
            $table->text('reasson')->nullable();
            $table->integer('director_id');
            $table->integer('status_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fup_cancellations');
    }
}
