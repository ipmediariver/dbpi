<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateQuoteResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_resources', function (Blueprint $table) {
            $table->id();
            $table->integer('quote_id');
            $table->integer('quote_group_id');
            $table->double('qty');
            $table->double('days');
            $table->integer('jobtitle_id');
            $table->double('resource_cost');
            $table->double('custom_import')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_resources');
    }
}