<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fups', function (Blueprint $table) {
            $table->id();
            $table->string('responsable_id')->nullable();
            $table->integer('fup_repository_id');
            $table->integer('version')->default(1);
            $table->string('fup_id');
            $table->integer('quote_id');
            $table->string('order_number')->nullable();
            $table->string('po_number')->nullable();
            $table->timestamp('date');
            $table->double('mub')->nullable();
            $table->string('comercial_name')->nullable();
            $table->string('business_name_shipment')->nullable();
            $table->text('shipment_address')->nullable();
            $table->text('shipment_city_state')->nullable();
            $table->text('shipment_zip_code')->nullable();
            $table->string('business_name_billing')->nullable();
            $table->text('billing_address')->nullable();
            $table->text('billing_city_state')->nullable();
            $table->text('billing_rfc')->nullable();
            $table->text('billing_zip_code')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->integer('partial_deliveries')->default(0);
            $table->integer('delivery_monday')->default(0);
            $table->integer('delivery_tuesday')->default(0);
            $table->integer('delivery_wednesday')->default(0);
            $table->integer('delivery_thursday')->default(0);
            $table->integer('delivery_friday')->default(0);
            $table->integer('delivery_saturday')->default(0);
            $table->integer('delivery_sunday')->default(0);
            $table->string('delivery_schedules')->nullable();
            $table->text('references')->nullable();
            $table->string('payment_conditions_advance_percent')->nullable();
            $table->string('payment_conditions_advance_days')->nullable();
            $table->string('payment_conditions_delivery_percent')->nullable();
            $table->string('payment_conditions_delivery_days')->nullable();
            $table->string('payment_conditions_instalation_percent')->nullable();
            $table->string('payment_conditions_instalation_days')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('delivery_commitment')->nullable();
            $table->string('instalation_date')->nullable();
            $table->string('project_date')->nullable();
            $table->string('final_client_business_name')->nullable();
            $table->string('final_client_address')->nullable();
            $table->string('final_client_city_state')->nullable();
            $table->string('final_client_zip_code')->nullable();
            $table->string('final_client_account_executive')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_form')->nullable();
            $table->string('cfdi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fups');
    }
}
