<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_payrolls', function (Blueprint $table) {
            $table->id();
            $table->integer('employee_id');
            $table->integer('account_payroll_id');
            $table->integer('currency_id')->nullable();
            $table->double('salary')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('bank_account_num')->nullable();
            $table->string('bank_card_num')->nullable();
            $table->string('interbank_clabe')->nullable();
            $table->double('fonacot_discount', 8, 2)->nullable();
            $table->double('infonavit_discount', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_payrolls');
    }
}
