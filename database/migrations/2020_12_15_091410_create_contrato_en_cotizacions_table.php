<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratoEnCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrato_en_cotizacions', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cotizacion')->nullable();
            $table->integer('id_grupo_de_cotizacion')->nullable();
            $table->integer('id_contrato')->nullable();
            $table->integer('id_cobertura')->nullable();
            $table->integer('id_tiempo_de_respuesta')->nullable();
            $table->timestamp('fecha_de_inicio')->nullable();
            $table->integer('etapa')->default(0); // 0 = Inactivo, 1 = Activo, 2 = Cancelado, 3 = Caducado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('contrato_en_cotizacions');
    }
}
