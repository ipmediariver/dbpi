<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiempoDeRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiempo_de_respuestas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('descripcion', 255);
            $table->double('urgente');
            $table->double('alta');
            $table->double('normal');
            $table->double('baja');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiempo_de_respuestas');
    }
}
