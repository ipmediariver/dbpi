<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateQuoteProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_products', function (Blueprint $table) {
            $table->id();
            $table->integer('quote_id');
            $table->integer('quote_group_id');
            $table->integer('product_id');
            $table->integer('provider_id');
            $table->double('qty')->default(1);
            $table->double('provider_cost')->default(0);
            $table->double('provider_discount')->default(0);
            $table->double('provider_special_discount')->default(0);
            $table->double('customer_discount')->default(0);
            $table->double('custom_import')->nullable();
            $table->integer('show_image_on_quote')->default(0);
            $table->integer('show_specifications_on_quote')->default(0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_products');
    }
}