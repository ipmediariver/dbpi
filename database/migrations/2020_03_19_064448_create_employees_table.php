<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('avatar')->nullable();
            $table->string('account_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('jobtitle_id')->nullable();
            $table->string('name_1');
            $table->string('name_2')->nullable();
            $table->string('last_name_1');
            $table->string('last_name_2')->nullable();
            $table->string('email');
            $table->string('mobile')->nullable();
            $table->string('address');
            $table->string('rfc')->nullable();
            $table->string('curp')->nullable();
            $table->dateTime('birthday', 0);
            $table->integer('currency_id')->nullable();
            $table->double('salary')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('bank_account_num')->nullable();
            $table->string('bank_card_num')->nullable();
            $table->string('interbank_clabe')->nullable();
            $table->double('fonacot_discount', 8, 2)->nullable()->default(0);
            $table->double('infonavit_discount', 8, 2)->nullable()->default(0);
            $table->timestamp('active_at')->nullable();
            $table->timestamp('inactive_at')->nullable();
            $table->string('nss')->nullable();
            $table->timestamp('imss_active_at')->nullable();
            $table->timestamp('imss_unactive_internal_at')->nullable();
            $table->timestamp('imss_unactive_external_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
