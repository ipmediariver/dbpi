<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class AddDaysToEmployeePayrollDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_payroll_discounts', function (Blueprint $table) {
            $table->double('days')->nullable();
            $table->double('ammount_per_day')->nullable();
            $table->double('percent_discount')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_payroll_discounts', function (Blueprint $table) {
            $table->dropColumn('days');
            $table->dropColumn('ammount_per_day');
            $table->dropColumn('percent_discount');
        });
    }
}