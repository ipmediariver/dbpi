<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarCostoAContratoEnCotizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_en_cotizacions', function (Blueprint $table) {
            $table->boolean('mostrar_costo')->defualt(0);
            $table->double('costo')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_en_cotizacions', function (Blueprint $table) {
            //
        });
    }
}
