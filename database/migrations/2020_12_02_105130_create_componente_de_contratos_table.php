<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComponenteDeContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('componente_de_contratos', function (Blueprint $table) {
            $table->id();
            $table->integer('id_contrato');
            $table->double('cantidad');
            $table->string('descripcion'); // Mantenimiento Mayor, Mantenimiento Menor, Monitoreo
            $table->string('periodo'); //Ej. Cada 2 semanas
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('componente_de_contratos');
    }
}
