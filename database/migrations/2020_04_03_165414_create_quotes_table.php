<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('quote_id');
            $table->string('opportunity_id')->nullable();
            $table->integer('copy_of')->nullable();
            $table->string('account_id')->nullable();
            $table->string('account_logo_id')->nullable();
            $table->string('contact_id')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->timestamp('date')->nullable();
            $table->text('description')->nullable();
            $table->boolean('only_show_description')->default(0);
            $table->string('terms')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('vigency')->nullable()->default(15);
            $table->string('status_id')->default(0);
            $table->integer('currency_id')->nullable();
            $table->double('exchange_rate')->nullable();
            $table->boolean('display_iva')->default(0);
            $table->double('iva')->default(8)->nullable();
            $table->text('observations')->nullable();
            $table->string('fob')->nullable();
            $table->integer('quote_tamplate_id')->default(0);
            $table->double('custom_import')->nullable();
            $table->double('general_discount')->default(0)->nullable();
            $table->integer('display_customer_items_discount')->default(0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}