<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorarioDeCoberturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario_de_coberturas', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cobertura');
            $table->string('alias')->nullable();
            $table->string('dias');
            $table->timestamp('hora_de_inicio')->nullable();
            $table->timestamp('hora_final')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario_de_coberturas');
    }
}
