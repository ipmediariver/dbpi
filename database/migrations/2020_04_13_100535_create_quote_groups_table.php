<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateQuoteGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_groups', function (Blueprint $table) {
            $table->id();
            $table->integer('quote_id');
            $table->string('description')->nullable();
            $table->double('custom_import')->default(0);
            $table->boolean('hide_in_quote')->default(0);
            $table->boolean('hide_items')->default(0);
            $table->boolean('exclude_on_margin')->default(0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_groups');
    }
}