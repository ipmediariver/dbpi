<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFechaDeServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fecha_de_servicios', function (Blueprint $table) {
            $table->id();
            $table->string('id_ticket')->nullable();
            $table->timestamp('fecha');
            $table->string('id_cliente');
            $table->integer('id_servicio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fecha_de_servicios');
    }
}
