<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('product_type_id')->nullable();
            $table->integer('product_brand_id')->nullable();
            $table->integer('product_model_id')->nullable();
            $table->string('code')->nullable();
            $table->integer('product_category_id')->nullable();
            $table->string('description');
            $table->string('sat_code')->nullable();
            $table->text('specifications')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}