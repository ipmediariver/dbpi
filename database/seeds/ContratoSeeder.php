<?php

use App\SERVICIO\Contrato;
use Illuminate\Database\Seeder;

class ContratoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contrato::factory()->create([
            'nombre'      => 'Contrato Anual',
            'descripcion' => 'Contrato anual (Básico)',
            'duracion'    => 12,
        ]);

        Contrato::factory()->create([
            'nombre'      => 'Contrato Semestral',
            'descripcion' => 'Contrato semestral (Básico)',
            'duracion'    => 6,
        ]);
    }
}
