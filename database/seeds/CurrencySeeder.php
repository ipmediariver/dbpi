<?php
use App\RRHH\Currency;
use Illuminate\Database\Seeder;
class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Currency::class, 1)->create([
        	'name' => 'Pesos Mexicanos',
        	'short_name' => 'MXN'
        ]);
        factory(Currency::class, 1)->create([
        	'name' => 'Dolares',
        	'short_name' => 'USD'
        ]);
    }
}