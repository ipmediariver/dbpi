<?php
use App\COMERCIAL\Product;
use App\COMERCIAL\ProductBrand;
use App\COMERCIAL\ProductModel;
use App\COMERCIAL\ProductType;
use Illuminate\Database\Seeder;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	print("Creando tipos, marcas y modelos. \n");
    	factory(ProductType::class, 5)->create()->each(function($product_type){
    		factory(ProductBrand::class, 5)->create([
    			'product_type_id' => $product_type->id
    		])->each(function($product_brand) use($product_type) {
    			factory(ProductModel::class, 5)->create([
    				'product_type_id' => $product_type->id,
    				'product_brand_id' => $product_brand->id,
    			]);
    		});
    	});
    	print("Creando listado de productos. \n");
        factory(Product::class, 30)->create()->each(function($product){
        	$type = ProductType::inRandomOrder()->first();
        	$brand = $type->brands()->inRandomOrder()->first();
        	$model = $brand->models()->inRandomOrder()->first();
        	$product->update([
        		'product_type_id' => $type->id,
        		'product_brand_id' => $brand->id,
        		'product_model_id' => $model->id,
        	]);
        });
    }
}