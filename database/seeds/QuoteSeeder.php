<?php
use Illuminate\Database\Seeder;
class QuoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Quote::class, 15)->create();
    }
}