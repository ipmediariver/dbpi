<?php

use Illuminate\Database\Seeder;
use App\SERVICIO\FechaDeServicio;

class FechaDeServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FechaDeServicio::class, 20)->create();
    }
}
