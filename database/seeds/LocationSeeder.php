<?php
use App\RRHH\Location;
use Illuminate\Database\Seeder;
class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Location::class, 1)->create([
        	'name' => 'Tijuana'
        ]);
        factory(Location::class, 1)->create([
        	'name' => 'Mexicali'
        ]);
        factory(Location::class, 1)->create([
        	'name' => 'Nogales'
        ]);
        factory(Location::class, 1)->create([
            'name' => 'Hermosillo'
        ]);
        factory(Location::class, 1)->create([
            'name' => 'La Paz'
        ]);
    }
}