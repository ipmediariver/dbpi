<?php

use Illuminate\Database\Seeder;

class CoberturaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cobertura::factory()->create([
        	'Básica'
        ]);
    }
}
