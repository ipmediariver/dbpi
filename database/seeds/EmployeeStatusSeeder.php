<?php
use App\RRHH\EmployeeStatus;
use Illuminate\Database\Seeder;
class EmployeeStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EmployeeStatus::class, 1)->create([
            'name' => 'Activo',
            'code' => 'es1'
        ]);
        factory(EmployeeStatus::class, 1)->create([
            'name' => 'De vacaciones',
            'code' => 'es2'
        ]);
        factory(EmployeeStatus::class, 1)->create([
            'name' => 'Incapacitado',
            'code' => 'es3'
        ]);
        factory(EmployeeStatus::class, 1)->create([
            'name' => 'Prospecto',
            'code' => 'es4'
        ]);
        factory(EmployeeStatus::class, 1)->create([
            'name' => 'De baja',
            'code' => 'es5'
        ]);
        factory(EmployeeStatus::class, 1)->create([
            'name' => 'Suspendido',
            'code' => 'es6'
        ]);
    }
}