<?php
use App\COMPRAS\Provider;
use Illuminate\Database\Seeder;
class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Provider::class, 1)->create([
        	'name' => 'LUGUER'
        ]);
        factory(Provider::class, 1)->create([
        	'name' => 'SENSA'
        ]);
        factory(Provider::class, 1)->create([
        	'name' => 'SYSCOM'
        ]);
        factory(Provider::class, 1)->create([
        	'name' => 'ASHPI'
        ]);
    }
}