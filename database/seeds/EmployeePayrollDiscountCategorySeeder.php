<?php
use App\RRHH\EmployeePayrollDiscountCategory;
use Illuminate\Database\Seeder;
class EmployeePayrollDiscountCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EmployeePayrollDiscountCategory::class, 1)->create([
        	'name' => 'Falta',
        	'code' => 'd-01'
        ]);
        factory(EmployeePayrollDiscountCategory::class, 1)->create([
        	'name' => 'Retardo',
        	'code' => 'd-02'
        ]);
        factory(EmployeePayrollDiscountCategory::class, 1)->create([
        	'name' => 'Suspensión',
        	'code' => 'd-03'
        ]);
        factory(EmployeePayrollDiscountCategory::class, 1)->create([
        	'name' => 'Prestamo',
        	'code' => 'd-04'
        ]);
        factory(EmployeePayrollDiscountCategory::class, 1)->create([
        	'name' => 'Devolución',
        	'code' => 'd-05'
        ]);
        factory(EmployeePayrollDiscountCategory::class, 1)->create([
        	'name' => 'Otro Descuento',
        	'code' => 'd-06'
        ]);
    }
}