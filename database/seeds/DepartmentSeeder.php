<?php
use App\RRHH\Department;
use Illuminate\Database\Seeder;
class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Department::class, 1)->create([
        	'name' => 'ITS'
        ]);
        factory(Department::class, 1)->create([
        	'name' => 'Comercial'
        ]);
        factory(Department::class, 1)->create([
        	'name' => 'Servicio'
        ]);
        factory(Department::class, 1)->create([
            'name' => 'Diseño'
        ]);
        factory(Department::class, 1)->create([
        	'name' => 'Finanzas'
        ]);
        factory(Department::class, 1)->create([
        	'name' => 'Suministro'
        ]);
        factory(Department::class, 1)->create([
        	'name' => 'Sistemas'
        ]);
        factory(Department::class, 1)->create([
        	'name' => 'RRHH'
        ]);
    }
}