<?php
use App\SYSTEM\Module;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Module::class, 1)->create([
            'name'        => 'Recursos Humanos',
            'description' => 'Empleados, Nóminas, Incidencias',
            'code'        => 'm-01',
            'icon'        => 'RH',
            'slug'		  => Str::slug('RRHH')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'Comercial',
            'description' => 'Ventas, Margen, Cotización, FUP',
            'code'        => 'm-02',
            'icon'        => 'Ventas',
            'slug'		  => Str::slug('Comercial')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'Operaciones',
            'description' => 'Levantamientos, Mantenimientos',
            'code'        => 'm-03',
            'icon'        => 'Operaciones',
            'slug'		  => Str::slug('Operaciones')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'Compras',
            'description' => 'Compras, Inventario',
            'code'        => 'm-04',
            'icon'        => 'Compras',
            'slug'		  => Str::slug('Compras')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'Inventario',
            'description' => 'Margen, Materiales',
            'code'        => 'm-05',
            'icon'        => 'Inventario',
            'slug'		  => Str::slug('Inventario')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'Finanzas',
            'description' => 'Margen, Materiales',
            'code'        => 'm-06',
            'icon'        => 'Finanzas',
            'slug'		  => Str::slug('Finanzas')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'Help Desk',
            'description' => 'Requerimientos',
            'code'        => 'm-07',
            'icon'        => 'HelpDesk',
            'slug'		  => Str::slug('Help Desk')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'CRM',
            'description' => 'Clientes, Prospectos, Funel',
            'code'        => 'm-08',
            'icon'        => 'CRM',
            'slug'		  => Str::slug('CRM')
        ]);
        factory(Module::class, 1)->create([
            'name'        => 'Business Intelligence',
            'description' => 'Reportes / Indicadores',
            'code'        => 'm-09',
            'icon'        => 'BI',
            'slug'		  => Str::slug('Business Intelligence')
        ]);
    }
}
