<?php
use App\RRHH\Employee;
use Illuminate\Database\Seeder;
class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Employee::class, 60)->create();
    }
}