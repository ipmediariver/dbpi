<?php
use App\RRHH\Bank;
use Illuminate\Database\Seeder;
class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Bank::class, 1)->create(['name' => 'Santander', 'color' => '#ec0000']);
        factory(Bank::class, 1)->create(['name' => 'Banamex', 'color' => '#056dae']);
        factory(Bank::class, 1)->create(['name' => 'Banorte', 'color' => '#eb0029']);
        factory(Bank::class, 1)->create(['name' => 'Bancomer', 'color' => '#072146']);
        factory(Bank::class, 1)->create(['name' => 'HSBC', 'color' => '#db0011']);
        factory(Bank::class, 1)->create(['name' => 'Banco Azteca', 'color' => '#17a54d']);
    }
}