<?php
use App\COMERCIAL\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ProductCategory::class, 1)->create([
            'name'    => 'CABLEADO',
            'percent' => 35,
            'color' => '#fcfee2'
        ]);
        factory(ProductCategory::class, 1)->create([
            'name'    => 'TELECOM',
            'percent' => 30,
            'color' => '#efe6fe',
        ]);
        factory(ProductCategory::class, 1)->create([
            'name'    => 'SOPORTERIA',
            'percent' => 30,
            'color' => '#ffe6e1'
        ]);
        factory(ProductCategory::class, 1)->create([
            'name'    => 'TUBERIA',
            'percent' => 30,
            'color' => '#e1f8ff'
        ]);
        factory(ProductCategory::class, 1)->create([
            'name'    => 'ELEVACION',
            'percent' => 20,
            'color' => '#e1ffeb'
        ]);
    }
}
