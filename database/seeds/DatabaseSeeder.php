<?php
use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(EmployeeSeeder::class);
        $this->call(ExchangeRateSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(BankSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(EmployeeStatusSeeder::class);
        $this->call(EmployeePayrollDiscountCategorySeeder::class);
        $this->call(EmployeePayrollEntryCategorySeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(ProviderSeeder::class);
        // $this->call(ProductSeeder::class);
        $this->call(ProductCategorySeeder::class);
    }
}