<?php
use App\SYSTEM\MinimunSalary;
use Illuminate\Database\Seeder;
class MinimunSalarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(MinimunSalary::class, 1)->create();
    }
}