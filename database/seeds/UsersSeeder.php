<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create([
            'first_name' => 'DBPI',
            'last_name'  => 'Admin',
            'email'      => 'admin@dbpi.com',
            'username'   => 'admin',
            'role'       => 'admin',
        ]);

        factory(User::class, 1)->create([
            'first_name' => 'Jaime',
            'last_name'  => 'Mejía',
            'email'      => 'jmejia@godynacom.com',
            'username'   => 'jmejia',
            'role'       => 'admin',
            'validator'  => 1,
        ]);

        factory(User::class, 1)->create([
            'first_name' => 'Agustín',
            'last_name'  => 'Abaroa',
            'email'      => 'ceo@godynacom.com',
            'username'   => 'aabaroajr',
            'role'       => 'admin',
            'validator'  => 1,
        ]);

        factory(User::class, 1)->create([
            'first_name' => 'Eduardo',
            'last_name'  => 'Magadan',
            'email'      => 'emagadan@godynacom.com',
            'username'   => 'emagadan',
            'role'       => 'admin',
            'validator'  => 1,
        ]);
    }
}
