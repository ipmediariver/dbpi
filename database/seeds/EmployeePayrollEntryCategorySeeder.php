<?php

use App\RRHH\EmployeePayrollEntryCategory;
use Illuminate\Database\Seeder;

class EmployeePayrollEntryCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EmployeePayrollEntryCategory::class, 1)->create([
        	'name' => 'Día extra',
        	'code' => 'e-01'
        ]);
        factory(EmployeePayrollEntryCategory::class, 1)->create([
        	'name' => 'Tiempo extra',
        	'code' => 'e-02'
        ]);
        factory(EmployeePayrollEntryCategory::class, 1)->create([
        	'name' => 'Bono',
        	'code' => 'e-03'
        ]);
        factory(EmployeePayrollEntryCategory::class, 1)->create([
        	'name' => 'Retroactivo',
        	'code' => 'e-04'
        ]);
        factory(EmployeePayrollEntryCategory::class, 1)->create([
        	'name' => 'Premio',
        	'code' => 'e-05'
        ]);
        factory(EmployeePayrollEntryCategory::class, 1)->create([
        	'name' => 'Comisión',
        	'code' => 'e-06'
        ]);
        factory(EmployeePayrollEntryCategory::class, 1)->create([
        	'name' => 'Otro ingreso',
        	'code' => 'e-07'
        ]);
    }
}
