- Se ingresa a la FUP
	Usuario
		- Click en boton
		 	- Envia solicitud con un comentario requerido (una sola vez)
	Director
		- Recibe una notificacion en el sistema
		- Recibe un email de notificacion
			- Ingresa a la fup
				- Se le muestra una alerta con la solicitud y el comentario del usuario
					Tiene dos opciones:
						- Autorizar la cancelacion
							- Se cancela la FUP
							- Se cancela la cotizacion
							- Se cancela la oportunidad
							- Se notifica al usuario sobre la cancelacion
						Rechazar solicitud de cancelacion
							- Se rechaza la solicitud
							- Se notifica al usuario sobre el rechazo de solicitud
								- Se muestra un boton para que el director redacte un comentario adicional al usuario
									- Se agrega el comentario y se notifica al usuario
								- O bien se puede omitir el cometario adicional