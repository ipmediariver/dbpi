require('./bootstrap');
import Vue from 'vue';
import store from './store/index';
import VueCurrencyFilter from 'vue-currency-filter';
import VueTheMask from 'vue-the-mask';
import VueSignaturePad from 'vue-signature-pad';
import Notifications from 'vue-notification';
import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use(VueTheMask);
Vue.use(VueSignaturePad);
Vue.use(Notifications);
Vue.use(CKEditor);
Vue.use(VueCurrencyFilter,
{
  symbol : '$',
  thousandsSeparator: ',',
  fractionCount: 2,
  fractionSeparator: '.',
  symbolPosition: 'front',
  symbolSpacing: false
});

Vue.filter('uppercase', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.toUpperCase()
});

Vue.filter('round', function(value){
  let number = Number(Math.round(value * 10) / 10);
  return parseFloat(number).toFixed(2); 
});

// Componentes de modulo RRHH
import SetEmployeeDiscountType from './components/rrhh/SetEmployeeDiscountType.vue';
import AddEntryByDays from './components/rrhh/AddEntryByDays.vue';
import EnviarLinkDeSolicitudDeVacaciones from './components/rrhh/EnviarLinkDeSolicitudDeVacaciones.vue';
import FormularioParaSolicitarVacaciones from './components/rrhh/FormularioParaSolicitarVacaciones.vue';
import BotonesParaAutorizarVacaciones from './components/rrhh/BotonesParaAutorizarVacaciones.vue';
import BtnAgregarVacaciones from './components/rrhh/BtnAgregarVacaciones.vue';
import EliminarVacaciones from './components/rrhh/EliminarVacaciones.vue';

// Componentes de modulo Comercial
import ComercialDashboard from './components/comercial/ComercialDashboard';
import Quote from './components/comercial/Quote.vue';
import RequestInternalValidation from './components/comercial/RequestInternalValidation.vue';
import InternalValidationBtns from './components/comercial/InternalValidationBtns.vue';
import QuoteStatusSelect from './components/comercial/QuoteStatusSelect.vue';
import DeleteQuoteBtn from './components/comercial/DeleteQuoteBtn.vue';
import CloneQuoteBtn from './components/comercial/CloneQuoteBtn.vue';
import GenerateFup from './components/comercial/GenerateFup.vue';
import Fup from './components/comercial/Fup.vue';
import RequestFupAuth from './components/comercial/RequestFupAuth.vue';
import CommentAboutFup from './components/comercial/CommentAboutFup.vue';
import AuthFupBtn from './components/comercial/AuthFupBtn.vue';
import AssignResponsableToFup from './components/comercial/AssignResponsableToFup.vue';
import FupComments from './components/comercial/FupComments.vue';
import OpportunitiesList from './components/comercial/OpportunitiesList.vue';
import NewFupVersion from  './components/comercial/NewFupVersion.vue';
import UploadProductsList from './components/comercial/UploadProductsList.vue';
import CommentFupVersion from './components/comercial/CommentFupVersion.vue';
import ProductForm from './components/comercial/ProductForm.vue';
import RefreshQuoteCrmData from './components/comercial/RefreshQuoteCrmData.vue';
import QuoteParticipants from './components/comercial/QuoteParticipants.vue';
import OpportunityForm from './components/comercial/OpportunityForm.vue';
import UploadQuoteAttachments from './components/comercial/UploadQuoteAttachments';
import FupBillingImport from './components/comercial/FupBillingImport';
import CancelFup from './components/comercial/CancelFup';
import FupCancellationAlert from './components/comercial/FupCancellationAlert';
import ContratoEnFup from './components/comercial/ContratoEnFup';
import BtnDeActivacionDeContratoEnFup from './components/comercial/BtnDeActivacionDeContratoEnFup';
import FiltradorDeCotizaciones from './components/comercial/FiltradorDeCotizaciones';
import CategoriasDeProductos from './components/comercial/CategoriasDeProductos';
import FiltradorDeFups from './components/comercial/FiltradorDeFups';
import IndicadoresPorClientes from './components/comercial/IndicadoresPorClientes';


import TableroDynacom from './components/comercial/TableroDynacom';
import IndicadoresTableroDynacom from './components/comercial/IndicadoresTableroDynacom';
import RegistroEnTablero from './components/comercial/RegistroEnTablero';
// Componentes de modulo Servicio
import CalendarioServicio from './components/servicio/calendario/CalendarioServicio';
import Contratos from './components/servicio/contratos/Contratos';
import Coberturas from './components/servicio/coberturas/Coberturas';
import TiemposDeRespuesta from './components/servicio/tiempos-de-respuesta/TiemposDeRespuesta';
import PlantillasDeContratos from './components/servicio/plantillas/PlantillasDeContratos';

const app = new Vue({
    el: '#app',
    components: {
      ComercialDashboard,
      Quote,
      SetEmployeeDiscountType,
      EnviarLinkDeSolicitudDeVacaciones,
      FormularioParaSolicitarVacaciones,
      BotonesParaAutorizarVacaciones,
      BtnAgregarVacaciones,
      EliminarVacaciones,
      RequestInternalValidation,
      InternalValidationBtns,
      AddEntryByDays,
      QuoteStatusSelect,
      DeleteQuoteBtn,
      CloneQuoteBtn,
      GenerateFup,
      Fup,
      RequestFupAuth,
      CommentAboutFup,
      AuthFupBtn,
      AssignResponsableToFup,
      FupComments,
      OpportunitiesList,
      NewFupVersion,
      UploadProductsList,
      CommentFupVersion,
      ProductForm,
      RefreshQuoteCrmData,
      QuoteParticipants,
      OpportunityForm,
      UploadQuoteAttachments,
      FupBillingImport,
      CancelFup,
      FupCancellationAlert,
      CalendarioServicio,
      Contratos,
      Coberturas,
      TiemposDeRespuesta,
      ContratoEnFup,
      BtnDeActivacionDeContratoEnFup,
      FiltradorDeCotizaciones,
      FiltradorDeFups,
      IndicadoresTableroDynacom,
      TableroDynacom,
      RegistroEnTablero,
      PlantillasDeContratos,
      CategoriasDeProductos,
      IndicadoresPorClientes,
    },
    data: {
      module_sidebar: (localStorage.getItem('dbpi_sidebar') == 'true'),
    	root: document.head.querySelector('[name="document-root"]').content,
    	token: document.head.querySelector('[name="csrf-token"]').content,
      crm: document.head.querySelector('[name="crm-uri"]') ? document.head.querySelector('[name="crm-uri"]').content : '',
      helpdesk_root: document.head.querySelector('[name="helpdesk-root"]') ? document.head.querySelector('[name="helpdesk-root"]').content : '',
      crm_token: document.head.querySelector('[name="crm-token"]') ? document.head.querySelector('[name="crm-token"]').content : ''
    },
    created(){
      let t = this;
      store.commit('set_root', t.root);
      store.commit('set_crm_root', t.crm);
      store.commit('set_crm_token', t.crm_token);
    },
    methods: {
      toggle_module_sidebar(){
        let t = this;
        let route = '/config/desplegar-menu-de-modulos/';
        if(t.module_sidebar){
          localStorage.setItem('dbpi_sidebar', false);
          t.module_sidebar = false;
        }else{
          localStorage.setItem('dbpi_sidebar', true);
          t.module_sidebar = true;
        }
      } 		   	
    },
    store
});
