import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
const store = new Vuex.Store({
	state: {
		root: null,
		crm_root: null,
		crm_token: null,
		counter: 0,
		quote: null,
		quote_subtotal: null,
		quote_total: null,
		quote_subtotal_on_quote: null,
		quote_total_on_quote: null,
		quote_dependencies: null,
		account_contacts: null,
		quote_date: {
			day: null,
			month: null,
			year: null,
		},
		quote_groups: null,
		quote_group: null,
		quote_product: null,
		quote_resource: null,
		new_product_dependencies: null,
		fup: null,
		opportunities: new Array,
		acc_list: [],
		servicio: {
			fechas: {
				lista: null,
				pagina_actual: null,
				total_de_paginas: null,
			},
			contratos: null,
			coberturas: null,
			tipos_de_cobertura: null,
			total_de_tipos_de_cobertura: 0,
			tiempos_de_respuesta: null,
			plantillas_de_contrato: null,
		},
		productos_de_tipo_contrato: null,
	},
	mutations: {
		set_root(state, data){
			state.root = data;
		},
		set_crm_root(state, data){
			state.crm_root = data;
		},
		set_crm_token(state, data){
			state.crm_token = data;
		},
		set_quote(state, data){
			state.quote = data;
		},
		set_quote_groups(state, groups){
			state.quote_groups = groups;
		},
		set_quote_subtotal(state, data){
			state.quote_subtotal = data;
		},
		set_quote_subtotal_on_quote(state, data){
			state.quote_subtotal_on_quote = data;
		},
		set_quote_total(state, data){
			state.quote_total = data;
		},
		set_quote_total_on_quote(state, data){
			state.quote_total_on_quote = data;
		},
		set_quote_account_id(state, account_id){
			state.quote.account_id = account_id;
		},
		set_quote_contact_id(state, contact_id){
			state.quote.contact_id = contact_id;
		},
		set_quote_contact_name(state, contact_name){
			state.quote.contact_name = contact_name;
		},
		set_quote_terms(state, terms){
			state.quote.terms = terms;
		},
		set_quote_delivery_time(state, delivery_time){
			state.quote.delivery_time = delivery_time;
		},
		set_quote_description(state, description){
			state.quote.description = description;
		},
		set_quote_hide_description(state, value){
			state.quote.hide_description = value;
		},
		set_quote_only_show_description(state, value){
			state.quote.only_show_description = value;
		},
		set_quote_display_customer_items_discount(state, value){
			state.quote.display_customer_items_discount = value;
		},
		set_quote_observations(state, observations){
			state.quote.observations = observations;
		},
		set_quote_currency_id(state, currency_id){
			state.quote.currency_id = currency_id;
		},
		set_quote_exchange_rate(state, exchange_rate){
			state.quote.exchange_rate = exchange_rate;
		},
		set_quote_vigency(state, vigency){
			state.quote.vigency = vigency;
		},
		set_quote_fob(state, fob){
			state.quote.fob = fob;
		},
		set_quote_general_discount(state, general_discount){
			state.quote.general_discount = general_discount
		},
		set_quote_iva(state, iva){
			state.quote.iva = iva;
		},
		set_quote_display_iva(state, value){
			state.quote.display_iva = value;
		},
		set_quote_day(state, day){
			state.quote_date.day = day;
		},
		set_quote_month(state, month){
			state.quote_date.month = month;
		},
		set_quote_year(state, year){
			state.quote_date.year = year;
		},
		set_quote_dependencies(state, data){
			state.quote_dependencies = data;
		},
		set_account_contacts(state, data){
			state.account_contacts = data;
		},
		set_quote_group(state, data){
			state.quote_group = data;
		},
		set_quote_group_hide_name(state, value){
			state.quote_group.hide_name = value;
		},
		set_quote_group_hide_in_quote(state, value){
			state.quote_group.hide_in_quote = value;
		},
		set_quote_group_hide_items(state, value){
			state.quote_group.hide_items = value;
		},
		set_exclude_on_margin(state, value){
			state.quote_group.exclude_on_margin = value;
		},
		set_new_product_dependencies(state, data){
			state.new_product_dependencies = data;
		},
		set_quote_product(state, data){
			state.quote_product = data;
		},
		set_quote_product_qty(state, qty){
			state.quote_product.qty = qty;
		},
		set_quote_product_id(state, product_id){
			state.quote_product.product_id = product_id;
		},
		set_quote_product_provider_id(state, provider_id){
			state.quote_product.provider_id = provider_id;
		},
		set_quote_product_provider_cost(state, provider_cost){
			state.quote_product.provider_cost = provider_cost;
		},
		set_quote_product_provider_discount(state, provider_discount){
			state.quote_product.provider_discount = provider_discount;
		},
		set_quote_product_provider_special_discount(state, provider_special_discount){
			state.quote_product.provider_special_discount = provider_special_discount;	
		},
		set_quote_product_customer_discount(state, customer_discount){
			state.quote_product.customer_discount = customer_discount;
		},
		set_quote_product_show_image_on_quote(state, show_image_on_quote){
			state.quote_product.show_image_on_quote = show_image_on_quote;
		},
		set_quote_product_show_specifications_on_quote(state, show_specifications_on_quote){
			state.quote_product.show_specifications_on_quote = show_specifications_on_quote;	
		},
		set_quote_resource(state, data){
			state.quote_resource = data;
		},
		set_fup(state, fup){
			state.fup = fup;
		},
		set_fup_responsable(state, user_id){
			state.fup.responsable_id = user_id;
		},
		set_fup_description(state, value){
			state.fup.fup_description = value;
		},
		set_po_number(state, value){
			state.fup.po_number = value;
		},
		set_comercial_name(state, value){
			state.fup.comercial_name = value;
		},
		set_business_name_shipment(state, value){
			state.fup.business_name_shipment = value;
		},
		set_shipment_address(state, value){
			state.fup.shipment_address = value;
		},
		set_shipment_city_state(state, value){
			state.fup.shipment_city_state = value;
		},
		set_shipment_zip_code(state, value){
			state.fup.shipment_zip_code = value;
		},
		set_business_name_billing(state, value){
			state.fup.business_name_billing = value;
		},
		set_billing_address(state, value){
			state.fup.billing_address = value;
		},
		set_billing_city_state(state, value){
			state.fup.billing_city_state = value;
		},
		set_billing_rfc(state, value){
			state.fup.billing_rfc = value;
		},
		set_billing_zip_code(state, value){
			state.fup.billing_zip_code = value;
		},
		set_contact_name(state, value){
			state.fup.contact_name = value;
		},
		set_contact_email(state, value){
			state.fup.contact_email = value;
		},
		set_contact_phone(state, value){
			state.fup.contact_phone = value;
		},
		set_partial_deliveries(state, value){
			state.fup.partial_deliveries = value;
		},
		set_delivery_monday(state, value){
			state.fup.delivery_monday = value;
		},
		set_delivery_tuesday(state, value){
			state.fup.delivery_tuesday = value;
		},
		set_delivery_wednesday(state, value){
			state.fup.delivery_wednesday = value;
		},
		set_delivery_thursday(state, value){
			state.fup.delivery_thursday = value;
		},
		set_delivery_friday(state, value){
			state.fup.delivery_friday = value;
		},
		set_delivery_schedules(state, value){
			state.fup.delivery_schedules = value;
		},
		set_references(state, value){
			state.fup.references = value;
		},
		set_payment_conditions_advance_percent(state, value){
			state.fup.payment_conditions_advance_percent = value;
		},
		set_payment_conditions_advance_days(state, value){
			state.fup.payment_conditions_advance_days = value;
		},
		set_payment_conditions_delivery_percent(state, value){
			state.fup.payment_conditions_delivery_percent = value;
		},
		set_payment_conditions_delivery_days(state, value){
			state.fup.payment_conditions_delivery_days = value;
		},
		set_payment_conditions_instalation_percent(state, value){
			state.fup.payment_conditions_instalation_percent = value;
		},
		set_payment_conditions_instalation_days(state, value){
			state.fup.payment_conditions_instalation_days = value;
		},
		set_delivery_time(state, value){
			state.fup.delivery_time = value;
		},
		set_delivery_commitment(state, value){
			state.fup.delivery_commitment = value;
		},
		set_instalation_date(state, value){
			state.fup.instalation_date = value;
		},
		set_project_date(state, value){
			state.fup.project_date = value;
		},
		set_final_client_business_name(state, value){
			state.fup.final_client_business_name = value;
		},
		set_final_client_address(state, value){
			state.fup.final_client_address = value;
		},
		set_final_client_city_state(state, value){
			state.fup.final_client_city_state = value;
		},
		set_final_client_zip_code(state, value){
			state.fup.final_client_zip_code = value;
		},
		set_final_client_account_executive(state, value){
			state.fup.final_client_account_executive = value;
		},
		set_opportunities(state, opportunities){
			for(let index in opportunities){
				let opportunity = opportunities[index];
				state.opportunities.push(opportunity);
			}
		},
		add_item_in_acc_list(state, item){
			state.acc_list.push(item);
		},
		remove_item_in_acc_list(state, index){
			state.acc_list.splice(index, 1);
		},
		reset_acc_list(state){
			state.acc_list = [];
		},
		set_quote_product_acc_list(state, quote_product){
			state.acc_list = quote_product.acc_list ? JSON.parse(quote_product.acc_list) : [];
		},
		establecer_contratos_de_servicio(state, contratos){
			state.servicio.contratos = contratos;
		},
		establecer_coberturas_de_servicio(state, coberturas){
			state.servicio.coberturas = coberturas;
		},
		establecer_tipos_de_cobertura_de_servicio(state, tipos_de_cobertura){
			state.servicio.tipos_de_cobertura = tipos_de_cobertura;
		},
		establecer_total_de_tipos_de_coberturas_de_servicio(state, total){
			state.servicio.total_de_tipos_de_cobertura = total
		},
		establecer_tiempos_de_respuesta_de_servicio(state, tiempos_de_respuesta){
			state.servicio.tiempos_de_respuesta = tiempos_de_respuesta;
		},
		establecer_fechas_de_servicio(state, data){
			state.servicio.fechas.lista = data.lista;
			state.servicio.fechas.pagina_actual = data.pagina_actual;
			state.servicio.fechas.total_de_paginas = data.total_de_paginas;
		},
		establecer_plantillas_de_contratos(state, data){
			state.servicio.plantillas_de_contrato = data;
		},
		establecer_productos_de_tipo_contrato(state, data){
			state.productos_de_tipo_contrato = data;
		}
	},
	getters: {
		get_root(state){
			return state.root;
		},
		get_crm_root(state){
			return state.crm_root;
		},
		get_crm_token(state){
			return state.crm_token;
		},
		get_quote(state){
			return state.quote;
		},
		get_quote_groups(state){
			return state.quote_groups;
		},
		get_quote_subtotal(state){
			return state.quote_subtotal;
		},
		get_quote_subtotal_on_quote(state){
			return state.quote_subtotal_on_quote;
		},
		get_quote_total(state){
			return state.quote_total;
		},
		get_quote_total_on_quote(state){
			return state.quote_total_on_quote;
		},
		get_quote_dependencies(state){
			return state.quote_dependencies;
		},
		get_account_contacts(state){
			return state.account_contacts;
		},
		get_quote_day(state){
			return state.quote_date.day;
		},
		get_quote_month(state){
			return state.quote_date.month;
		},
		get_quote_year(state){
			return state.quote_date.year;
		},
		get_quote_group(state){
			return state.quote_group;
		},
		get_new_product_dependencies(state){
			return state.new_product_dependencies;
		},
		get_quote_product(state){
			return state.quote_product;
		},
		get_quote_resource(state){
			return state.quote_resource;
		},
		get_fup(state){
			return state.fup;
		},
		get_opportunities(state){
			return state.opportunities;
		},
		get_acc_list(state){
			return state.acc_list;
		},
		obtener_componentes_de_servicio(state){
			return state.servicio;
		},
		obtener_fechas_de_servicio(state){
			return state.servicio.fechas;
		},
		obtener_plantillas_de_contratos(state){
			return state.servicio.plantillas_de_contrato;
		},
		obtener_productos_de_tipo_contrato(state){
			return state.productos_de_tipo_contrato;
		},
	},
	actions: {
		get_quote(context, quote_id){
			let t = context;
			let route = t.getters.get_root + '/comercial/cotizaciones/' + quote_id;
			axios.get(route).then(({ data }) => {
				t.commit('set_quote', data.quote);
				t.commit('set_quote_subtotal', data.subtotal);
				t.commit('set_quote_total', data.total);
				t.commit('set_quote_subtotal_on_quote', data.subtotal_on_quote);
				t.commit('set_quote_total_on_quote', data.total_on_quote);
				setTimeout(function(){
					t.dispatch('get_quote_dependencies', data.quote.id);
				},200);
			}).catch((error) => {
				console.log(error.response);
			});
		},
		get_quote_dependencies(context, quote_id){
			let t = context;
			let route = t.getters.get_root + '/comercial/dependencias-de-cotizacion/' + quote_id;
			axios.get(route).then(({data}) => {
				t.commit('set_quote_dependencies', data);
			});
		},
		get_account_contacts(context, account_id){
			let t = context;
			let route = t.getters.get_crm_root + '/Account/' + account_id + '/contacts?select=id,name';
			axios.get(route, {
				headers: {
					'X-Api-Key': t.getters.get_crm_token,
				},
			}).then(({data}) => {
				let contacts = data.list.length ? data.list : null;
				t.commit('set_account_contacts', contacts);
			}).catch((error) => {
				console.log(error.response);
			});
		},
		get_new_quote_product_dependencies(context){
			let t = context;
			let route = t.getters.get_root + '/comercial/dependencias-para-productos';
			axios.get(route).then(({data}) => {
				t.commit('set_new_product_dependencies', data);
			});
		},
		get_fup(context, data){
			let t = context;
			let fup_rep_id = data.rep;
			let fup_id = data.id;
			let route = '/comercial/fuprep/' + fup_rep_id + '/fups/' + fup_id;

			axios.get(route).then(({data}) => {
				t.commit('set_fup', data);
				t.dispatch('get_quote', data.quote_id);
			})
			.catch((error) => {
				console.log(error.response);
			});
		},
		get_opportunities(context){
			let t = context;
			let offset = t.getters.get_opportunities.length; 
			let query = "/Opportunity?select=name%2CaccountId%2CaccountName%2Cstage%2CcreatedAt%2CamountCurrency%2Camount%2CassignedUserId%2CassignedUserName%2CusuariosAsignados&maxSize=20&offset="+offset+"&orderBy=createdAt&order=desc&where%5B0%5D%5Btype%5D=in&where%5B0%5D%5Battribute%5D=stage&where%5B0%5D%5Bvalue%5D%5B%5D=Prospecting&where%5B0%5D%5Bvalue%5D%5B%5D=Needs+Analysis&where%5B0%5D%5Bvalue%5D%5B%5D=Qualification&where%5B0%5D%5Bvalue%5D%5B%5D=Value+Proposition&where%5B0%5D%5Bvalue%5D%5B%5D=Id.+Decision+Makers&where%5B0%5D%5Bvalue%5D%5B%5D=Perception+Analysis"
			let route = t.getters.get_crm_root + query;
			axios.get(route, {
				headers: {
					'X-Api-Key': t.getters.get_crm_token
				}
			}).then(({data}) => {
				t.commit('set_opportunities', data.list);
			});	
		},
		get_quote_groups(context, quote_id){
			let t = context;
			let route = '/comercial/cotizaciones/' + quote_id + '/grupos';
			axios.get(route).then(({data}) => {
				t.commit('set_quote_groups', data);
				t.dispatch('get_quote', quote_id);
			});
		},
		obtener_coberturas(context){
			let t = context;
			let ruta = '/help-desk/coberturas/lista';
			axios.get(ruta)
			.then(({data}) => {
				t.commit('establecer_coberturas_de_servicio', data.coberturas);
				t.commit('establecer_total_de_tipos_de_coberturas_de_servicio', data.total_de_tipos_de_cobertura);
			})
			.catch((error) => {
				console.log(error);
			});
		},
		obtener_tipos_de_cobertura(context){
			let t = context;
			let ruta = '/help-desk/tipos-de-cobertura';
			axios.get(ruta)
			.then(({data}) => {
			  t.commit('establecer_tipos_de_cobertura_de_servicio', data);
			})
			.catch((error) => {
			  console.log(error.response);
			});
		},
		obtener_tiempos_de_respuesta(context){
			let t = context;
			let ruta = '/help-desk/tiempos-de-respuesta/lista';
			axios.get(ruta).then(({data}) => {
				t.commit('establecer_tiempos_de_respuesta_de_servicio', data);
			});
		},
		obtener_fechas_de_servicio(context, pagina){
			let t = context;
			let route = '/help-desk/fechas-de-servicio?page=' + pagina;
			axios.get(route).then(({data}) => {
				let fechas = {
					lista: data.data,
					pagina_actual: data.current_page,
					total_de_paginas: data.last_page
				}
				t.commit('establecer_fechas_de_servicio', fechas);
			});
		},
		errors(context, errors){
			let t = context;
			for(let msg in errors){
				let error_msg = errors[msg];
				this._vm.$notify({
					group: 'general',
					title: 'Upss!',
					type: 'error',
					text: error_msg
				});
			}
		},
		obtener_contratos(context){
			const t = context;
			const ruta = '/help-desk/contratos/lista';
			axios.get(ruta).then(({data}) => {
				t.commit('establecer_contratos_de_servicio', data);
			});	
		},
		obtener_plantillas_de_contratos(context){
			const t = context;
			const ruta = '/help-desk/plantillas/lista';
			axios.get(ruta).then(({data}) => {
				t.commit('establecer_plantillas_de_contratos', data);
			});
		},
		cargar_productos_de_tipo_contrato(context){
			let t = context;
			let ruta = '/comercial/cargar-productos-de-tipo-contrato';
			axios.get(ruta).then(({data}) => {
				console.log(data);
				t.commit('establecer_productos_de_tipo_contrato', data);
			});
		}
	}
});
export default store;