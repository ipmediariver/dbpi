<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu contraseña ha sido restaurada!',
    'sent' => 'Te hemos enviado a tu correo un link de restauración de contraseña!',
    'throttled' => 'Por favor espera unos minutos antes de intentar nuevamente.',
    'token' => 'El link ha expirado.',
    'user' => "No pudimos encontrar un usuario con el correo proporcionado.",

];
