<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token & Document Root & CRM root -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="document-root" content="{{ url('/') }}">
    @auth
    <meta name="module-sidebar" content="{{ auth()->user()->module_sidebar }}">
    <meta name="crm-uri" content="{{ config('app.crm_uri') }}">
    <meta name="crm-token" content="{{ config('app.crm_key') }}">
    <meta name="helpdesk-root" content="{{ config('app.helpdesk_root') }}">
    @endauth
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/133291f590.js" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">