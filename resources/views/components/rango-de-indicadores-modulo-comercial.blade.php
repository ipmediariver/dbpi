<div>
    <form action="{{ route('comercial') }}" method="get" class="mb-3 form-row">
    	<div class="col-sm-9">
    		<select name="periodo" class="custom-select">
    			<option value="semanal" @if(request()->periodo == 'semana') selected @endif>
    				Actividad semanal
    			</option>
    			<option value="mensual" @if(request()->periodo == 'mensual') selected @endif>
    				Actividad mensual
    			</option>
    			<option value="trimestral" @if(request()->periodo == 'trimestral') selected @endif>
    				Actividad trimestral
    			</option>
    			<option value="anual" @if(request()->periodo == 'anual') selected @endif>
    				Actividad anual
    			</option>
    		</select>
    	</div>
    	<div class="col-sm-3">
    		<button type="submit" href="#" class="btn btn-success btn-block">
    			Generar reporte
    		</button>
    	</div>
    </form>
</div>