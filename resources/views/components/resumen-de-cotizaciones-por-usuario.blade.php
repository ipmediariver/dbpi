<div>
    <table class="table table-sm table-bordered">
    	<thead>
    		<tr>
    			<th>Responsable:</th>
    			<th>Total de cotizaciones:</th>
    			<th>En proceso</th>
    			<th>Autorizadas internamente</th>
    			<th>Autorizadas por cliente</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($responsables as $responsable)
	    		<tr>
	    			<th>{{ $responsable->nombre_completo }}</th>
	    			<th>
	    				<p class="mb-2">
	    					Total de cotizaciones: 
	    					<b>{{ $responsable->cotizaciones_en_proceso }}</b>
	    				</p>
	    				<p class="mb-2">
	    					Monto cotizado: 
	    					<b>{{ $responsable->monto_cotizado_en_proceso }}</b>
	    				</p>
	    			</th>
	    			<th>
	    				<p class="mb-2">
	    					Total de cotizaciones: 
	    					<b>{{ $responsable->cotizaciones_autorizadas_internamente }}</b>
	    				</p>
	    				<p class="mb-2">
	    					Monto cotizado: 
	    					<b>{{ $responsable->monto_cotizado_y_autorizado_internamente }}</b>
	    				</p>
	    			</th>
	    			<th>
	    				<p class="mb-2">
	    					Total de cotizaciones: 
	    					<b>{{ $responsable->cotizaciones_autorizadas_por_cliente }}</b>
	    				</p>
	    				<p class="mb-2">
	    					Monto cotizado: 
	    					<b>{{ $responsable->monto_cotizado_y_autorizado_por_cliente }}</b>
	    				</p>
	    			</th>
	    		</tr>
    		@endforeach
    	</tbody>
    </table>
</div>