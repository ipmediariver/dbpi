<form action="{{$action}}" method="post" onsubmit="submitCategoryBtn.disabled = true; return true;">
    @csrf
    @if($method == 'put')
        @method('PUT')
    @endif
    <div class="d-flex align-items-center">
        <div class="form-group m-0 w-100">
            <label for="description" class="sr-only">Nombre</label>
            <input type="input" 
                class="form-control form-control-lg" 
                name="description" 
                id="description" 
                placeholder="Nombre" 
                required
                autofocus
                autocomplete="off"
                @if($category) value="{{ $category->description }}" @endif>
        </div>
        <div class="form-group m-0 ml-2">                
            <button type="submit" 
                class="btn btn-primary btn-lg"
                name="submitCategoryBtn">
                <i class="bi bi-check2"></i>
                {{ $btn }}
            </button>
        </div>
    </div>
</form>