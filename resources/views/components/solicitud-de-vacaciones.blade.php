<div>
    <p><b>Solicitud pendiente</b></p>
    <table class="table table-bordered table-sm m-0">
    	<tbody>
    		<tr>
    			<th>Periodo a disfrutar:</th>
    			<td width="50%">Periodo {{ $solicitud->periodo }}</td>
    		</tr>

    		<tr>
    			<th>Desde:</th>
    			<td>{{ $solicitud->cuando_inicia }}</td>
    		</tr>

    		<tr>
    			<th>Hasta:</th>
    			<td>{{ $solicitud->cuando_termina }}</td>
    		</tr>

    		<tr>
    			<th>Días correspondientes:</th>
    			<td>({{ $solicitud->dias_correspondientes }}) días</td>
    		</tr>

    		<tr>
    			<th>Días a disfrutar:</th>
    			<td>({{ $solicitud->dias_a_disfrutar }}) días</td>
    		</tr>

    		<tr>
    			<th>Días pendientes:</th>
    			<td>({{$solicitud->dias_pendientes}}) días</td>
    		</tr>

    		<tr>
    			<th>Fecha en que deberá presentarse a trabajar:</th>
    			<td>{{ $solicitud->fecha_en_que_debe_presentarse }}</td>
    		</tr>
    	</tbody>
    </table>
    <div class="d-flex align-items-center">
    	<a href="#" class="btn btn-success">
    		<i class="fa fa-check fa-sm mr-2"></i>
    		Autorizar vacaciones
    	</a>
    	<a href="#" class="btn btn-outline-danger ml-3">
    		<i class="fa fa-times fa-sm mr-2"></i>
    		Rechazar solicitud
    	</a>
    </div>
</div>