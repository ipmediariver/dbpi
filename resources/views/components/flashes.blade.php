<div class="mb-3">
	@if (session('success'))
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	        {{ session('success') }}
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	    </div>
	@endif
	@if (session('info'))
	    <div class="alert alert-info alert-dismissible fade show" role="alert">
	        {{ session('info') }}
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	    </div>
	@endif
	@if (session('status'))
	    <div class="alert alert-info alert-dismissible fade show" role="alert">
	        {{ session('status') }}
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	    </div>
	@endif
	@if ($errors->any())
	    <div class="alert alert-danger alert-dismissible fade show" role="alert">
	        @foreach ($errors->all() as $error)
	            <p class="m-0">{{ $error }}</p>
	        @endforeach
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	    </div>
	@endif
</div>