<div class="{{ $clases }}">
	<form action="{{ route('fechas.index') }}" method="get">
		<div class="form-group">
			<label for="">Consultar años anteriores:</label>
			<div class="d-flex align-items-center">
				<select class="custom-select" name="ano" id="">
					@for($i = 0; $i < 5; $i++)
					@php
						$ano = date('Y') - $i;
					@endphp
						<option @if(request()->ano == $ano) selected @endif value="{{ $ano }}">{{ $ano }}</option>
					@endfor
				</select>
				<button class="btn btn-success ml-2">
				Ir
				</button>
			</div>
		</div>
	</form>
</div>