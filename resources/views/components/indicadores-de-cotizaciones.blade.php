<div class="form-row">
	<div class="col-sm-4">
		<div class="card">
			<div class="card-body bg-primary text-white">
				<h2>{{ $indicadores['cotizaciones_en_proceso'] }}</h2>
				<p class="lead m-0">Cotizaciones en proceso</p>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card">
			<div class="card-body bg-info text-white">
				<h2>{{ $indicadores['cotizaciones_autorizadas_internamente'] }}</h2>
				<p class="lead m-0">Autorizadas internamente</p>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card">
			<div class="card-body bg-success text-white">
				<h2>{{ $indicadores['cotizaciones_autorizadas_por_cliente'] }}</h2>
				<p class="lead m-0">Autorizadas por cliente</p>
			</div>
		</div>
	</div>
</div>