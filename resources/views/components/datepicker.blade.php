<div class="input-group">
	<select name="{{ $prefix }}_day" 
		class="custom-select"
		@if($required) required @endif>
		<option disabled 
			selected 
			value="">
			Día
		</option>
		<option @if($day == '01') selected @endif value="01">01</option>
		<option @if($day == '02') selected @endif value="02">02</option>
		<option @if($day == '03') selected @endif value="03">03</option>
		<option @if($day == '04') selected @endif value="04">04</option>
		<option @if($day == '05') selected @endif value="05">05</option>
		<option @if($day == '06') selected @endif value="06">06</option>
		<option @if($day == '07') selected @endif value="07">07</option>
		<option @if($day == '08') selected @endif value="08">08</option>
		<option @if($day == '09') selected @endif value="09">09</option>
		<option @if($day == '10') selected @endif value="10">10</option>
		<option @if($day == '11') selected @endif value="11">11</option>
		<option @if($day == '12') selected @endif value="12">12</option>
		<option @if($day == '13') selected @endif value="13">13</option>
		<option @if($day == '14') selected @endif value="14">14</option>
		<option @if($day == '15') selected @endif value="15">15</option>
		<option @if($day == '16') selected @endif value="16">16</option>
		<option @if($day == '17') selected @endif value="17">17</option>
		<option @if($day == '18') selected @endif value="18">18</option>
		<option @if($day == '19') selected @endif value="19">19</option>
		<option @if($day == '20') selected @endif value="20">20</option>
		<option @if($day == '21') selected @endif value="21">21</option>
		<option @if($day == '22') selected @endif value="22">22</option>
		<option @if($day == '23') selected @endif value="23">23</option>
		<option @if($day == '24') selected @endif value="24">24</option>
		<option @if($day == '25') selected @endif value="25">25</option>
		<option @if($day == '26') selected @endif value="26">26</option>
		<option @if($day == '27') selected @endif value="27">27</option>
		<option @if($day == '28') selected @endif value="28">28</option>
		<option @if($day == '29') selected @endif value="29">29</option>
		<option @if($day == '30') selected @endif value="30">30</option>
		<option @if($day == '31') selected @endif value="31">31</option>
	</select>
	<select name="{{ $prefix }}_month" 
		class="custom-select"
		@if($required) required @endif>
		<option disabled 
			selected 
			value="">
			Més
		</option>
		<option @if($month == '01') selected @endif value="01">Ene</option>
		<option @if($month == '02') selected @endif value="02">Feb</option>
		<option @if($month == '03') selected @endif value="03">Mar</option>
		<option @if($month == '04') selected @endif value="04">Abr</option>
		<option @if($month == '05') selected @endif value="05">May</option>
		<option @if($month == '06') selected @endif value="06">Jun</option>
		<option @if($month == '07') selected @endif value="07">Jul</option>
		<option @if($month == '08') selected @endif value="08">Ago</option>
		<option @if($month == '09') selected @endif value="09">Sep</option>
		<option @if($month == '10') selected @endif value="10">Oct</option>
		<option @if($month == '11') selected @endif value="11">Nov</option>
		<option @if($month == '12') selected @endif value="12">Dic</option>
	</select>
	@php
		$now = Carbon\Carbon::now()->format('Y');
	@endphp
	<select name="{{ $prefix }}_year" 
		class="custom-select"
		@if($required) required @endif>
		<option disabled selected value="">
			Año
		</option>
		@for($i = 1; $i < 2; $i++)
		<option @if($year == ($now + $i)) selected @endif value="{{ $now + $i }}">{{ $now + $i }}</option>
		@endfor

		@for($i = 0; $i < 40; $i++)
		<option @if($year == ($now - $i)) selected @endif value="{{ $now - $i }}">{{ $now - $i }}</option>
		@endfor
	</select>
</div>