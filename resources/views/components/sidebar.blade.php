<aside class="sidebar_master d-flex flex-column">
	<div class="text-center d-flex flex-column">
		<a href="{{ route('apps') }}" class="mb-4">
			<img src="{{ asset('img/apps_icons/DBPI.svg') }}" class="img-fluid" alt="">
		</a>
		<a href="#" class="text-white">
			<i class="fa fa-search fa-lg fa-fw"></i>
		</a>
	</div>
	<div class="mt-auto text-center w-100 d-flex flex-column">
		@role('admin')
		<a href="{{ route('usuarios.index') }}" class="text-white mt-4">
			<i class="fa fa-cog fa-lg fa-fw"></i>
		</a>
		@endrole
		<a href="{{ route('logout') }}" 
			class="text-white mt-4" 
			onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
			<i class="fa fa-power-off fa-lg fa-fw"></i>
		</a>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		    @csrf
		</form>
	</div>
</aside>