<ol class="breadcrumb small text-uppercase m-0">
	<li class="breadcrumb-item">
		<a href="{{ route('apps') }}">
			Inicio
		</a>
	</li>
	@foreach(request()->segments() as $key => $segment)
		@if($loop->last)
			<li class="breadcrumb-item active">
				{{ $segment }}
			</li>
		@else
			@php
				$segments = array_slice(request()->segments(), 0, ($key + 1));
				$link = implode('/', $segments);
			@endphp
			<li class="breadcrumb-item">
				<a href="{{ url($link) }}">{{ $segment }}</a>
			</li>
		@endif
	@endforeach
</ol>