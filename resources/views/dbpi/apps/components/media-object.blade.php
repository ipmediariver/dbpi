<div class="card shadow-sm">
	<div class="card-body p-3">
		<div class="d-flex align-items-center">
			<div class="thumb rounded mr-3" 
				style="background-image: url('{{ $icon }}')">
				</div>
			<div>
				<p class="lead mb-1 text-dark">
					{{ $title }}
				</p>
				<p class="m-0 text-muted small">
					{{ $subtitle }}
				</p>
			</div>
		</div>
	</div>
</div>