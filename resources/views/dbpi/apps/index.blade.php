@extends('layouts.full')
@section('full_content')
<h1 class="h4">Hola, {{ auth()->user()->full_name }}</h1>
<hr>
<p>Tus aplicaciones</p>

@if(count($modules) > 0)
<div class="row">
	@foreach($modules as $module)
	<div class="col-sm-4 mb-3">
		<a href="{{ url('/'. $module->slug) }}">
			@component('dbpi.apps.components.media-object', [
				'icon' => asset('img/apps_icons/'. $module->icon .'.svg'),
				'title' => $module->name,
				'subtitle' => $module->description
			])
			@endcomponent
		</a>
	</div>
	@endforeach
</div>
@else
<span class="text-muted">No se te han asignado aplicaciones.</span>
@endif
@stop