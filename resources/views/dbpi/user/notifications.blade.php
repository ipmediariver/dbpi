@extends('layouts.full')
@section('full_content')
	<div class="row">
		<div class="col-sm-7 mx-auto">
			<a href="{{ route('apps') }}"><i class="fa fa-angle-left fa-sm mr-2"></i> Volver al inicio</a>
			<div class="card card-body shadow-sm mt-3">
				<p><b>Lista de notificaciones</b></p>
				@if($notifications->count())
					<table class="table m-0">
						<tr>
							<th width="60%">Notificación</th>
							<th class="text-right">Fecha</th>
						</tr>
						@foreach($notifications as $notification)
						<tr>
							<td class="@if(!$notification->read_at) cell-primary font-weight-bold @endif">
								<a href="{{route('mark_notification_as_read', $notification->id)}}">
									{{ \Str::limit($notification->data['msg'], 150) }}
								</a>
							</td>
							<td class="text-right @if(!$notification->read_at) cell-primary font-weight-bold @endif">
								{{ $notification->created_at->format('d M, Y h:i') }}
							</td>
						</tr>
						@endforeach
					</table>
					
					@if($notifications->total() > 15)
		
						{{ $notifications->links() }}
				
					@endif

				@else
				No tienes notificaciones
				@endif
			</div>
		</div>
	</div>
@stop