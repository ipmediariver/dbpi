@extends('layouts.module')
@section('module_sidebar')

	<ul class="list-group">
		<a href="{{ route('servicio') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('help-desk/calendario')) ? 'active' : '' }}
			">
			<i class="fa fa-calendar-alt fa-fw fa-sm mr-2"></i>
			Calendario
		</a>


		<a href="{{ route('contratos') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('help-desk/contratos')) ? 'active' : '' }}
			">
			<i class="fa fa-file-signature fa-fw fa-sm mr-2"></i>
			Contratos
		</a>

		<a href="{{ route('coberturas') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('help-desk/coberturas')) ? 'active' : '' }}
			">
			<i class="fa fa-hands-helping fa-fw fa-sm mr-2"></i>
			Coberturas
		</a>

		<a href="{{ route('tiempos-de-respuesta') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('help-desk/tiempos-de-respuesta')) ? 'active' : '' }}
			">
			<i class="fa fa-stopwatch fa-fw fa-sm mr-2"></i>
			Tiempos de respuesta
		</a>

		<a href="{{ route('plantillas-de-contratos') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('help-desk/plantillas')) ? 'active' : '' }}
			">
			<i class="fa fa-layer-group fa-fw fa-sm mr-2"></i>
			Plantillas
		</a>

		<a href="{{ route('servicio') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('servicio')) ? 'active' : '' }}
			">
			<i class="fa fa-boxes fa-fw fa-sm mr-2"></i>
			Inventarios
		</a>

		<a href="{{ route('servicio') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('servicio')) ? 'active' : '' }}
			">
			<i class="fa fa-chart-bar fa-fw fa-sm mr-2"></i>
			Reportes
		</a>
	</ul>
@stop