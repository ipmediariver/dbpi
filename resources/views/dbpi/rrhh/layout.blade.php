@extends('layouts.module')
@section('module_sidebar')
	<ul class="list-group">
		{{--
		<a href="{{ route('rrhh') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('rrhh')) ? 'active' : '' }}
			">
			<i class="fa fa-desktop fa-fw fa-sm mr-2"></i>
			Escritorio
		</a>
		--}}
		<a href="{{ route('get-employees-accounts') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('rrhh/cuentas')) ? 'active' : '' }}
				{{ (request()->is('rrhh/cuentas/*')) ? 'active' : '' }}
			">
			<i class="fa fa-users fa-fw fa-sm mr-2"></i>
			Empleados
		</a>
		<a href="{{ route('current-payroll') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('rrhh/nomina-actual')) ? 'active' : '' }}
				{{ (request()->is('rrhh/nomina/*')) ? 'active' : '' }}
			">
			<i class="fa fa-file-invoice fa-fw fa-sm mr-2"></i>
			Nómina
		</a>
		<a href="{{ route('fechas.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('rrhh/fechas')) ? 'active' : '' }}
				{{ (request()->is('rrhh/fechas/*')) ? 'active' : '' }}
			">
			<i class="fa fa-calendar-alt fa-fw fa-sm mr-2"></i>
			Calendario
		</a>
		<a href="{{ route('departamentos.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('rrhh/config')) ? 'active' : '' }}
				{{ (request()->is('rrhh/config/*')) ? 'active' : '' }}
			">
			<i class="fa fa-cog fa-fw fa-sm mr-2"></i>
			Configuración
		</a>
	</ul>
@stop