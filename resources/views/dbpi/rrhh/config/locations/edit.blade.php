@extends('dbpi.rrhh.config.layout')
@section('config_content')
	<div class="card card-body shadow-sm">
		<form action="{{ route('ubicaciones.update', $location) }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
			@csrf
			@method('PATCH')
			<p><b>Editar ubicación</b></p>
			<div class="form-group">
				<label for="">Nombre:</label>
				<input type="text" name="name" value="{{ $location->name }}" class="form-control">
			</div>
			<div class="d-flex align-items-center mt-4">
				<button type="submit" class="btn btn-success" name="submit_btn"><i class="fa fa-check fa-sm mr-2"></i> Actualizar</button>
				<button type="submit" 
					form="deleteLocationForm" 
					class="btn btn-link text-danger ml-2" 
					name="delete_submit_btn" 
					onclick="return confirm('Se eliminará la ubicación, ¿Deseas continuar?')">
					<i class="fa fa-trash-alt fa-sm mr-2"></i>
					Eliminar
				</button>
			</div>
		</form>

		<form action="{{ route('ubicaciones.destroy', $location) }}" 
			id="deleteLocationForm" 
			method="post" 
			onsubmit="delete_submit_btn.disabled = true; return true;">
			@csrf
			@method('DELETE')
		</form>
	</div>
@stop