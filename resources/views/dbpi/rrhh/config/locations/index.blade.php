@extends('dbpi.rrhh.config.layout')
@section('config_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>Ubicaciones</b></p>
		<a href="#" data-toggle="collapse" data-target="#newLocationCollapse" class="btn btn-primary ml-auto">
			Nueva Ubicación
		</a>
	</div>
	<div class="collapse" id="newLocationCollapse">
		<div class="card card-body shadow-sm mb-4">
			<p><b>Agregar nueva ubicación</b></p>
			<form action="{{ route('ubicaciones.store') }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
				@csrf
				<div class="form-group">
					<label for="">Nombre:</label>
					<input type="text" name="name" class="form-control">
				</div>
				<button type="submit" name="submit_btn" class="btn btn-success mt-4">
					<i class="fa fa-check fa-sm mr-2"></i>
					Guardar
				</button>
			</form>
		</div>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Nombre</th>
				<th class="text-center">Empleados asignados</th>
				<th class="text-right">Editar</th>
			</tr>
		</thead>
		<tbody>
			@foreach($locations as $location)
			<tr>
				<td>{{ $location->name }}</td>
				<td class="text-center">({{ $location->employees->count() }}) Empleados</td>
				<td class="text-right">
					<a href="{{ route('ubicaciones.edit', $location) }}">Editar</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop