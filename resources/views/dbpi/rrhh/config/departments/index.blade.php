@extends('dbpi.rrhh.config.layout')
@section('config_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>Departamentos</b></p>
		<a href="#" data-toggle="collapse" data-target="#newDepartmentCollapse" class="btn btn-primary ml-auto">
			Nuevo Departamento
		</a>
	</div>
	<div class="collapse" id="newDepartmentCollapse">
		<div class="card card-body shadow-sm mb-4">
			<p><b>Agregar nuevo departamento</b></p>
			<form action="{{ route('departamentos.store') }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
				@csrf
				<div class="form-group">
					<label for="">Nombre:</label>
					<input type="text" name="name" class="form-control">
				</div>

				<button type="submit" name="submit_btn" class="btn btn-success mt-4">
					<i class="fa fa-check fa-sm mr-2"></i>
					Guardar
				</button>
			</form>
		</div>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Nombre</th>
				<th class="text-center">Empleados asignados</th>
				<th class="text-right">Editar</th>
			</tr>
		</thead>
		<tbody>
			@foreach($departments as $department)
			<tr>
				<td>{{ $department->name }}</td>
				<td class="text-center">({{ $department->employees->count() }}) Empleados</td>
				<td class="text-right">
					<a href="{{ route('departamentos.edit', $department) }}">Editar</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop