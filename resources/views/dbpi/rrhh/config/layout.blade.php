@extends('dbpi.rrhh.layout')
@section('module_title', 'Configuración')
@section('module_content')
<div class="row">
	<div class="col-sm-3">
		<ul class="list-group shadow-sm">
			<a href="{{ route('departamentos.index') }}" 
				class="list-group-item 
					list-group-item-action 
					{{ (request()->is('rrhh/config/departamentos')) ? 'active' : '' }}
					{{ (request()->is('rrhh/config/departamentos/*')) ? 'active' : '' }}
				">
				Departamentos
			</a>
			<a href="{{ route('ubicaciones.index') }}" 
				class="list-group-item 
					list-group-item-action 
					{{ (request()->is('rrhh/config/ubicaciones')) ? 'active' : '' }}
					{{ (request()->is('rrhh/config/ubicaciones/*')) ? 'active' : '' }}
				">
				Ubicaciones
			</a>
		</ul>
	</div>
	<div class="col-sm-9">
		@yield('config_content')
	</div>
</div>
@stop