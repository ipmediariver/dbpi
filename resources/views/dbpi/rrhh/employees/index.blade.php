@extends('dbpi.rrhh.layout')
@section('module_title', 'Empleados')
@section('module_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>Listado de empleados</b></p>
		<a href="{{ route('empleados.create') }}" class="btn btn-primary ml-auto">Nuevo Empleado</a>
	</div>
	@if($employees->count())
		<table class="table m-0">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Puesto</th>
					<th>Compañía</th>
					<th>E-mail</th>
					<th>Estatus</th>
					<th>Asimilado</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($employees as $employee)
				<tr>
					<td>
						<a href="{{ route('empleados.show', $employee) }}" 
							class="d-flex align-items-center">
							<span class="avatar sm mr-3" style="background-image: url('{{ asset('uploads/avatars/' . $employee->avatar) }}')"></span>
							{{ $employee->full_name_reverse }}
						</a>
					</td>
					<td>{{ optional($employee->jobtitle)->name }}</td>
					<td>{{ $employee->account->name }}</td>
					<td>{{ $employee->email }}</td>
					<td>{{ $employee->status->name }}</td>
					<td>
						@if($employee->assimilated)
							<i class="fa fa-check fa-sm text-success"></i>
						@else
							<i class="fa fa-times fa-sm text-danger"></i>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@if($employees->total() > $employees->perPage())
			<div class="mt-3">
				{{ $employees->links() }}
			</div>
		@endif
	@else
		<p class="text-muted m-0">
			No se han agregado empleados
		</p>
	@endif
@stop