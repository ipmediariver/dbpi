@extends('dbpi.rrhh.employees.layout')

@section('employee_content')
	<div class="card shadow-sm mb-3">
		<div class="card-body">
			<p><b>Información bancaria</b></p>
			<form action="{{ route('empleados.update', $employee) }}" 
				method="post"
				onsubmit="bank_info_btn.disabled = true; return true;">
				@csrf
				@method('PATCH')
				<input type="hidden" name="bank_info">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Banco:</label>
							<select name="bank_id" class="custom-select" id="" required>
								<option disabled selected value="">Elige una opción</option>
								@foreach($banks as $bank)  
								<option @if($employee->bank_id == $bank->id) selected @endif value="{{ $bank->id }}">{{ $bank->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Número de cuenta:</label>
							
							<input type="number" step="any" name="bank_account_num" value="{{ $employee->bank_account_num ? decrypt($employee->bank_account_num) :  old('bank_account_num')  }}" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Número de tarjeta:</label>
							<input type="number" step= "any" name="bank_card_num" value="{{ $employee->bank_card_num ? decrypt($employee->bank_card_num) : old('bank_card_num')}}" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Clabe interbancaria:</label>
							<input type="number" step="any" name="interbank_clabe" value="{{ $employee->interbank_clabe ? decrypt($employee->interbank_clabe) : old('interbank_clabe') }}" class="form-control">
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-primary mt-4" name="bank_info_btn">Actualizar</button>
			</form>
		</div>
	</div>
@stop