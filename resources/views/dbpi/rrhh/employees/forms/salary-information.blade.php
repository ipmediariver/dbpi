@extends('dbpi.rrhh.employees.layout')
@section('employee_content')
	<div class="card shadow-sm mb-3">
		<div class="card-body">
			<p><b>Salario mensual</b></p>
			<form action="{{ route('empleados.update', $employee) }}" method="post" onsubmit="salary_info_btn.disabled = true; return true;">
				@csrf
				@method('PATCH')
				<input type="hidden" name="salary_info">
				<div class="form-row">
					<div class="col-sm-6">
						<label for="" required>Cantidad</label>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text">
									<i class="fa fa-dollar-sign fa-sm"></i>
								</span>
							</div>
							<input  type="number" 
							step=".01"
							name="salary" 
							class="form-control" 
							value="{{ $employee->salary }}"
							required>
						</div>
					</div>
					<div class="col-sm-6">
						<label for="" required>Moneda:</label>
						<select name="currency_id" id="" class="custom-select" required>
							<option disabled selected value="">Elige una opción</option>
							@foreach($currencies as $currency)
							<option value="{{ $currency->id }}"
								@if($employee->currency_id == $currency->id) selected @endif>
								{{ $currency->short_name }}
							</option>
							@endforeach
						</select>
					</div>
				</div>
				<hr>
				<p class="text-muted">
					Al seleccionar la siguiente casilla se actualizará el sueldo en la nómina actual <br> de lo contrario se verá reflejado hasta la próxima nómina.
				</p>
				<div class="custom-control custom-checkbox">
					<input type="checkbox" name="update_payroll" class="custom-control-input" id="updatePayrollCheckbox">
					<label class="custom-control-label" for="updatePayrollCheckbox">Actualizar sueldo en nómina actual</label>
				</div>
				<hr>
				<button type="submit" class="btn btn-primary mt-4" name="salary_info_btn">Actualizar</button>
			</form>
		</div>
	</div>
@stop