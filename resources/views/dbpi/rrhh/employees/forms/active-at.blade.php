@extends('dbpi.rrhh.employees.layout')
@section('employee_content')
<div class="card shadow-sm mb-3">
	<div class="card-body">
		<p><b>Fechas contratación / baja</b></p>
		<form action="{{ route('empleados.update', $employee) }}" 
			method="post"
			onsubmit="active_at_btn.disabled = true; return true;">
			@csrf
			@method('PATCH')
			<input type="hidden" name="active_at_info">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="">Fecha de contratación</label>
						<input type="date" name="active_at" class="form-control" value="{{ $employee->active_at->format('Y-m-d') }}" >
					</div>
				</div>
				<div class="col-sm-6">
					<label for="">Fecha de baja:</label>
					<input type="date" name="inactive_at" value="{{ optional($employee->inactive_at)->format('Y-m-d') }}" class="form-control" >	
				</div>
			</div>
			<button type="submit" class="btn btn-primary mt-4" name="active_at_btn">Actualizar</button>
		</form>
	</div>
</div>
@stop