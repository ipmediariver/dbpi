@extends('dbpi.rrhh.employees.layout')
@section('employee_content')
	<div class="card shadow-sm mb-3">
		<div class="card-body">
			<form action="{{ route('empleados.update', $employee) }}"
				method="post"
				enctype="multipart/form-data"
				onsubmit="personal_info_btn.disabled = true; return true;">
				@csrf
				@method('PATCH')
				<p><b>Foto de perfil</b></p>
				<input type="file" name="avatar" accept="image/*">
				<p class="mt-4"><b>Información personal</b></p>
				<input type="hidden" name="personal_info">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Primer nombre:</label>
							<input type="text"
							name="name_1"
							class="form-control"
							value="{{ old('name_1') ??  $employee->name_1 }}"
							required>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Segundo nombre:</label>
							<input type="text"
							name="name_2"
							class="form-control"
							value="{{ old('name_2') ?? $employee->name_2  }}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Apellido paterno:</label>
							<input type="text"
							name="last_name_1"
							class="form-control"
							value="{{ old('last_name_1') ?? $employee->last_name_1  }}"
							required>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Apellido materno:</label>
							<input type="text"
							name="last_name_2"
							class="form-control"
							value="{{ old('last_name_2') ?? $employee->last_name_2 }}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">RFC:</label>
							<input type="text" 
								name="rfc" 
								value="{{ old('rfc') ?? $employee->rfc }}" 
								class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">CURP:</label>
							<input type="text" 
								name="curp" 
								value="{{ old('curp') ?? $employee->curp }}" 
								class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Fecha de nacimiento:</label>
							<input type="date" name="birthday" value="{{ $employee->birthday->format('Y-m-d') }}" class="form-control">
						</div>
					</div>
				</div>
				<p class="mt-4"><b>Información de contacto</b></p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>E-mail:</label>
							<input type="email"
							name="email"
							class="form-control"
							value="{{ $employee->email }}"
							required>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Celular:</label>
							<input type="text" 
								name="mobile" 
								value="{{ $employee->mobile }}" 
								class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" required>Dirección:</label>
					<input type="text" 
						name="address" 
						value="{{ $employee->address }}" 
						class="form-control">
				</div>
				<button type="submit" 
					class="btn btn-primary mt-4"
					name="personal_info_btn">
				Actualizar
				</button>
			</form>
		</div>
	</div>
@stop