@extends('dbpi.rrhh.employees.layout')
@section('employee_content')
	<div class="card shadow-sm mb-3">
		<div class="card-body">
			<p><b>Puesto laboral</b></p>
			<form action="{{ route('empleados.update', $employee) }}" method="post" onsubmit="jobtitle_btn.disabled = true; return true;">
				@csrf
				@method('PATCH')
				<input type="hidden" name="department_info">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Empresa:</label>
							<select name="account_id" class="custom-select" required>
								<option disabled selected value="">Elige una opción</option>
								@foreach($accounts as $account)
								<option value="{{ $account->account_id }}"
									@if($employee->account_id == $account->account_id) selected @endif>
									{{ $account->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Ubicación:</label>
							<select name="location_id" class="custom-select" required>
								<option disabled selected value="">Elige una opción</option>
								@foreach($locations as $location)
								<option 
									@if($employee->location_id == $location->id) 
										selected 
									@endif 
									value="{{ $location->id }}">
									{{ $location->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Departmento:</label>
							<select name="department_id" class="custom-select" required>
								<option disabled selected value="">Elige una opción</option>
								@foreach($departments as $department)
								<option value="{{ $department->id }}"
									@if($employee->department_id == $department->id)
										selected
									@endif>
									{{ $department->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Puesto laboral:</label>
							<select name="jobtitle_id" class="custom-select" required>
								<option disabled selected value="">Elige una opción</option>
								@foreach($jobtitles as $jobtitle)
								<option value="{{ $jobtitle->id }}"
								@if($employee->jobtitle_id == $jobtitle->id) selected @endif>
									{{ $jobtitle->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row d-flex align-items-center">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Estatus:</label>
							<select name="status_id" class="custom-select">
								<option disabled selected value="">Elige una opción</option>
								@foreach($statuses as $status)
								<option value="{{ $status->id }}"
									@if($employee->status_id == $status->id) selected @endif>
									{{ $status->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group mt-2">
							<div class="custom-control custom-checkbox">
							  <input type="checkbox" 
							  	name="assimilated" 
							  	class="custom-control-input" 
							  	id="assimilatedCheckbox" 
							  	@if($employee->assimilated) checked @endif>
							  <label class="custom-control-label" for="assimilatedCheckbox">Marcar empleado como asimilado</label>
							</div>
						</div>
					</div>
				</div>
				<button type="submit" 
					class="btn btn-primary mt-4"
					name="jobtitle_btn">
					Actualizar
				</button>
			</form>
		</div>
	</div>
@stop