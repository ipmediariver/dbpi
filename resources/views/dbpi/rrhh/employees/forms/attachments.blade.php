@extends('dbpi.rrhh.employees.layout')
@section('employee_content')
<!-- ARCHIVOS ADJUNTOS -->
<div class="card card-body shadow-sm mb-3">
	<!-- FORMULARIO ADJUNTAR NUEVO ARCHIVO -->
	<p><b>Archivos Adjuntos</b></p>
	<form action="{{ route('empleados.archivos-adjuntos.store', $employee) }}"
		method="post"
		enctype="multipart/form-data"
		onsubmit="submit_btn.disabled = true; return true;"
		class="mb-4">
		@csrf
		<div class="form-group m-0">
			<div>
				<input name="attachments[]" type="file" accept="application/pdf,image/*" multiple required>
				<p class="m-0 text-muted mt-3 small">Agrega archivos de tipo PDF, JPG, PNG (Max. 8MB por archivo)</p>
			</div>
			<div class="form-group mt-2 w-50">
				<label>Selecciona la categoria a la que pertenecen los arhivos.</label>
				<div class="d-flex align-items-center">
					<select class="custom-select" name="category_attachment_id" id="category_attachment_id" required>
						<option value="" disabled selected> Selecciona una categoria.</option>
						@foreach($categories as $category)
						<option value="{{$category->id}}">{{$category->description}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<p class="m-0">
				¿Necesitas agregar una nueva categoría? 
				<a href="#" class="btn-link p-0 nowrap" 
						data-toggle="modal" 
						data-target="#Crear_categoria">
						[Haz click aquí]
				</a>
			</p>
		</div>
		<button type="submit" class="btn btn-primary mt-4" name="submit_btn">Adjuntar</button>
	</form>
	<!-- TERMINA FORMULARIO -->
	<!-- ACORDION DE CATEGORIAS -->
	@if($employee->attachments()->count())
		<div class="accordion" id="employeeAttachmentsCollapse">
			<!-- ARCHIVOS CATEGORIZADOS -->
			@foreach($categories as $category)
				@php
					$attachments = $employee->attachments->where('category_attachment_id', $category->id);
				@endphp
				<div class="card">
					<div class="card-header d-flex align-items-center" id="encabezado_{{ $category->id }}">
						<h2 class="mb-0">
							<button class="btn btn-link btn-block text-left p-0" 
								type="button" 
								data-toggle="collapse" 
								data-target="#collapse{{$category->id}}" 
								aria-expanded="true" 
								aria-controls="collapseOne">
								<span class="mr-2">({{ count($attachments) }})</span>
								{{$category->description}}
							</button>
						</h2>
						<a href="#" 
							data-toggle="modal" 
							data-target="#editar{{$category->id}}"
							class="ml-auto d-inline-block">
							<i class="fa fa-xs fa-pencil-alt mr-1"></i> 
							Editar
						</a>
						<x-modal title="Editar Categoria" :id="'editar' . $category->id">
							<x-slot name="body">
								<x-category-attachment-form 
									method="put" 
									:action="route('editcategoriesattachment', $category->id)" 
									btn="Actualizar"
									:category="$category">
								</x-category-attachment-form>
							</x-slot>
						</x-modal>
					</div>
					<div id="collapse{{$category->id}}" 
						class="collapse" 
						aria-labelledby="encabezado_{{ $category->id }}" 
						data-parent="#employeeAttachmentsCollapse">
						@if(count($attachments) != 0)
							<ul class="list-group list-group-flush">
								@foreach($attachments as $attachment)
									<li class="list-group-item d-flex align-items-center">
										<a target="_blank" href="{{ asset('uploads/attachments/' . $attachment->pathname) }}">
											{{ \Str::limit($attachment->name, 50) }}
										</a>
										<form action="{{ route('empleados.archivos-adjuntos.destroy', [$employee, $attachment]) }}"
											class="ml-auto"
											method="post"
											onsubmit="submit_btn.disabled = true; return true;">
											@csrf
											@method('delete')
											<button type="submit"
												name="submit_btn"
												href="#"
												class="btn btn-link p-0 text-danger"
												onclick="return confirm('Se eliminará el archivo adjunto, ¿Deseas continuar?');">
												[Eliminar]
											</button>
										</form>
										<a data-toggle="modal" 
											class="text-info ml-2" 
											type="button"  
											data-target="#updatecategories{{$attachment->id}}">
											[Mover]
										</a>
										<x-modal title="Mover archivo de categoria" 
											:id="'updatecategories' . $attachment->id">
											<x-slot name="body">
												<form action="{{ route('archivos-adjuntos.update', $attachment->id) }}" method="post">
													@csrf
													@method('PUT')
													<div class="d-flex align-items-center">
														<select class="form-control form-control-lg" name="category_attachment_id" id="category_attachment_id" required>
															<option value="" disabled selected> Selecciona una categoria.</option>
															@foreach($categories as $category)
															<option value="{{$category->id}}">{{$category->description}}</option>
															@endforeach
														</select>
														<button type="submit" class="btn btn-primary btn-lg ml-2">
															<i class="fa fa-check"></i>
														</button>
													</div>
												</form>
											</x-slot>
										</x-modal>
									</li>
								@endforeach
							</ul>
						@else
							<div class="card-body">
								<span class="ml-auto">No se han adjuntado archivos para esta categoria.</span>
							</div>
						@endif
					</div>
				</div>
			@endforeach
			<!-- TERMINAN ARCHIVOS CATEGORIZADOS -->

			<!-- ARCHIVOS SIN CATEGORIA -->
			@php
				$uncategorized_attachments = $employee->attachments
					->where('category_attachment_id', Null);
			@endphp
			<div class="card">
				<div class="card-header d-flex align-items-center border-0">
					<h2 class="mb-0">
						<button class="btn btn-link btn-block text-left p-0"
							id="headingOne" 
							type="button" 
							data-toggle="collapse" 
							data-target="#collapseSinCategorias" 
							aria-expanded="true" 
							aria-controls="collapseOne">
							<span class="mr-2">({{ count($uncategorized_attachments) }})</span> 
							Sin Categoria
						</button>
					</h2>
				</div>
				<div id="collapseSinCategorias" 
					class="collapse" 
					aria-labelledby="headingOne" 
					data-parent="#employeeAttachmentsCollapse">
					@if(count($uncategorized_attachments) != 0)
						<ul class="list-group list-group-flush">
							@foreach($uncategorized_attachments as $attachment)
								<li class="list-group-item d-flex align-items-center">
									<a target="_blank" href="{{ asset('uploads/attachments/' . $attachment->pathname) }}">
										{{ \Str::limit($attachment->name, 50) }}
									</a>
									<form action="{{ route('empleados.archivos-adjuntos.destroy', [$employee, $attachment]) }}"
										class="ml-auto"
										method="post"
										onsubmit="submit_btn.disabled = true; return true;">
										@csrf
										@method('delete')
										<button type="submit"
										name="submit_btn"
										href="#"
										class="btn btn-link p-0 text-danger"
										onclick="return confirm('Se eliminará el archivo adjunto, ¿Deseas continuar?');">
											[Eliminar]
										</button>
									</form>
									<a data-toggle="modal" class="text-info ml-2" type="button"  data-target="#updatecategory{{$attachment->id}}">
										[Mover]
									</a>
									<x-modal title="Mover archivo de categoria" 
										:id="'updatecategory' . $attachment->id">
										<x-slot name="body">
											<form action="{{ route('archivos-adjuntos.update', $attachment->id) }}" 
												method="post">
												@csrf
												@method('PUT')
												<div class="d-flex align-items-center">
													<select class="form-control" name="category_attachment_id" id="category_attachment_id" required>
														<option value="" disabled selected> Selecciona una categoria.</option>
														@foreach($categories as $category)
														<option value="{{$category->id}}">{{$category->description}}</option>
														@endforeach
													</select>
													<div class="form-group m-2">
														<button type="submit" class="btn btn-primary btn-lg">
															<i class="fa fa-check"></i>
														</button>
													</div>
												</div>
											</form>
										</x-slot>
									</x-modal>
								</li>
							@endforeach
						</ul>
					@else
						<div class="card-body">
							<span class="ml-auto">No se han adjuntado archivos para esta categoria.</span>
						</div>
					@endif
				</div>
			</div>
			<!-- TERMINAN ARCHIVOS SIN CATEGORIA -->
		</div>
	@else
		<p class="text-muted">
			No se han adjuntado archivos.
		</p>
	@endif
	<!-- TERMINA ACORDION DE CATEGORIAS -->
</div>
<!-- NUEVA CATEGORIA -->
<x-modal title="Crear nueva categoria" id="Crear_categoria" centered="true">
	<x-slot name="body">
		<x-category-attachment-form 
			method="post" 
			:action="route('storecategoriesattachment')" 
			btn="Crear">
		</x-category-attachment-form>
	</x-slot>
</x-modal>
<!-- TERMINA MODAL DE NUEVA CATEGOGIA -->
@stop