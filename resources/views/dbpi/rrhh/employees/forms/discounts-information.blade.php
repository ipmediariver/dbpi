@extends('dbpi.rrhh.employees.layout')
@section('employee_content')
	<div class="card shadow-sm" id="imss">
		<div class="card-body">
			<p><b>Descuentos</b></p>
			<form action="{{ route('empleados.update', $employee) }}" method="post" onsubmit="discounts_btn.disabled = true; return true;">
				@csrf
				@method('PATCH')
				<input type="hidden" name="discounts_info">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Fonacot:</label>
							<div class="input-group mb-3">
							  <div class="input-group-prepend">
							    <span class="input-group-text">
							    	<i class="fa fa-dollar-sign fa-sm"></i>
							    </span>
							  </div>
							  <input type="number" 
							  	step = "any"
							  	name="fonacot_discount" 
							  	class="form-control" 
								value="{{ old('fonacot_discount') ?? $employee->fonacot_discount ?? 'default' }}" >
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Infonavit:</label>
							<div class="input-group mb-3">
							  <div class="input-group-prepend">
							    <span class="input-group-text">
							    	<i class="fa fa-dollar-sign fa-sm"></i>
							    </span>
							  </div>
							  <input type="number" 
							  	step = "any"
							  	name="infonavit_discount" 
							  	class="form-control"
							  	value="{{ old('infonavit_discount') ?? $employee->infonavit_discount }}">
							</div>
						</div>
					</div>
				</div>
				<p><b>IMSS</b></p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">(NSS) Número de seguro social</label>
							<input type="number" step="any" name="nss" value="{{  decrypt($employee->nss) ? decrypt($employee->nss) : old('nss') }}" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Fecha de alta en IMSS:</label>
							<input type="date" name="imss_active_at" class="form-control" value="{{ optional($employee->imss_active_at)->format('Y-m-d') }}" >
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Fecha de baja en IMSS:</label>
							<input type="date" name="imss_unactive_external_at" value="{{ optional($employee->imss_unactive_external_at)->format('Y-m-d') }}" class="form-control" >	
						</div>
					</div>
				</div>
				<button class="btn btn-primary mt-4" name="discounts_btn">Actualizar</button>
			</form>
		</div>
	</div>
@stop