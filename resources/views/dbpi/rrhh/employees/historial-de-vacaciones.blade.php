@extends('dbpi.rrhh.employees.layout')
@section('employee_content')
	@if($employee->anos_de_servicio)
	<div class="card shadow-sm mb-3">
		<div class="card-body">
			<enviar-link-de-solicitud-de-vacaciones :empleado="{{ $employee }}" />
		</div>
	</div>

	<div class="card shadow-sm mb-3">
		<div class="card-body">
			<div class="d-flex align-items-center mb-3">
				<p class="m-0"><b>Historial de vacaciones</b></p>
				<btn-agregar-vacaciones clases="ml-auto" :empleado="{{ $employee }}" :cuenta="{{ $employee->account }}"></btn-agregar-vacaciones>
			</div>

			@if(count($vacaciones))
				@foreach($vacaciones as $vacaciones)

					<table class="table table-sm table-bordered table-sm @if(!$loop->last) mb-3 @endif">
						<tbody>
							<tr>
								<th>Periodo a disfrutar:</th>
								<th width="50%" class="cell-primary">Periodo {{ $vacaciones->periodo }}</th>
							</tr>

							<tr>
								<th>Desde:</th>
								<td>{{ $vacaciones->cuando_inicia }}</td>
							</tr>

							<tr>
								<th>Hasta:</th>
								<td>{{ $vacaciones->cuando_termina }}</td>
							</tr>

							<tr>
								<th>Días correspondientes:</th>
								<td>({{ $vacaciones->dias_correspondientes }}) días</td>
							</tr>

							<tr>
								<th>Días a disfrutar:</th>
								<td>({{ $vacaciones->dias_a_disfrutar }}) días</td>
							</tr>

							<tr>
								<th>Días pendientes:</th>
								<td>({{$vacaciones->dias_pendientes}}) días</td>
							</tr>

							<tr>
								<th>Fecha en que deberá presentarse a trabajar:</th>
								<td>{{ $vacaciones->fecha_en_que_debe_presentarse }}</td>
							</tr>

							@if(count($vacaciones->autorizaciones))
							<tr>
								<th>Autorizaciones:</th>
								<td>
									@foreach($vacaciones->autorizaciones as $autorizacion)
										<div class="@if($autorizacion->autorizadas) cell-success @else cell-danger @endif p-2 @if(!$loop->last) mb-2 @endif">
											<p class="m-0"><b>{{ $autorizacion->director->full_name }}</b></p>
											@if($autorizacion->autorizadas)
												<p class="m-0 text-success"><b>Autorizadas</b></p>
											@else
												<p class="m-0 text-danger"><b>Rechazadas</b></p>
											@endif
											<p class="m-0"><small>17 Mar 2020</small></p>
											@if($autorizacion->comentario)
											<p class="m-0 small">
												<b class="text-muted">Comentario adicional:</b> <br> {{ $autorizacion->comentario }}
											</p>
											@endif
										</div>
									@endforeach
								</td>
							</tr>
							@endif

							<tr>
								<th>Eliminar:</th>
								<td>
									<eliminar-vacaciones :vacaciones="{{ $vacaciones }}"></eliminar-vacaciones>
								</td>
							</tr>								
						</tbody>
					</table>

				@endforeach
			@else
				No se han registrado vacaciones.
			@endif

		</div>
	</div> 
	@else
	<p class="text-uppercase text-danger">
		<b>{{ $employee->name_1 }} aun no tiene antigüedad para solicitar vacaciones.</b>
	</p>
	@endif
@stop