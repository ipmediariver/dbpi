@extends('dbpi.rrhh.layout')
@section('module_title', 'Nuevo Empleado') @section('module_content')
	<div class="card shadow-sm">
		<div class="card-body">
			<form action="{{ route('empleados.store') }}" 
				method="post" 
				enctype="multipart/form-data"
				onsubmit="submit_btn.disabled = true; return true;">
				@csrf
				@component('components.form-header', [
					'description' => 'Por favor llena los siguientes campos para dar de alta a un nuevo empleado.'
				])
				@endcomponent
				<p class="lead text-muted">Foto de perfil</p>
				<input type="file" name="avatar" accept="image/*">
				<p class="lead text-muted mt-4">Información personal</p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Primer nombre:</label>
							<input name="name_1" type="text" value="{{ old('name_1') }}" class="form-control" required>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Segundo nombre:</label>
							<input name="name_2" type="text" value="{{ old('name_2') }}" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Apellido paterno:</label>
							<input name="last_name_1" type="text" value="{{ old('last_name_1') }}" class="form-control" required>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Apellido materno:</label>
							<input name="last_name_2" type="text" value="{{ old('last_name_2') }}" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Fecha de nacimiento:</label>
							<input type="date" required name="birthday" class="form-control" value="{{ old('birthday') }}" >
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">RFC:</label>
							<input type="text" 
								name="rfc" 
								value="{{ old('rfc') }}"
								class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">CURP:</label>
							<input type="text" 
								name="curp" 
								value="{{ old('curp') }}"
								class="form-control">
						</div>
					</div>
				</div>
				<p class="lead text-muted mt-4">Información de contacto</p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>E-mail:</label>
							<input type="email" name="email" value="{{ old('email') }}" class="form-control" required>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Celular:</label>
							<input type="text" name="mobile" value="{{ old('mobile') }}" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="" required>Dirección</label>
							<input type="text" name="address" value="{{ old('address') }}" class="form-control" required>
						</div>
					</div>
				</div>
				<p class="lead text-muted mt-4">Departamento y puesto</p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Empresa:</label>
							<select name="account_id" value="{{ old('account_id') }}" class="custom-select" required>
								<option disabled selected value="">Elige una opción</option>
								@foreach($accounts as $account)
								<option value="{{ $account->account_id }}" 
									@if(request()->account_id) {{ request()->account_id == $account->account_id ? 'selected' : '' }} @endif >
									{{ $account->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Ubicación:</label>
							<select name="location_id" value="{{ old('location_id') }}" class="custom-select" required>
								<option disabled selected value="">Elige una opción</option>
								@foreach($locations as $location)
								<option value="{{ $location->id }}">
									{{ $location->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" required>Departmento:</label>
					<select name="department_id" value="{{ old('department_id') }}" class="custom-select" required>
						<option disabled selected value="">Elige una opción</option>
						@foreach($departments as $department)
						<option value="{{ $department->id }}">
							{{ $department->name }}
						</option>
						@endforeach
					</select>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Elige un puesto laboral:</label>
							<select name="jobtitle_id" value="{{ old('jobtitle_id') }}" class="custom-select">
								<option disabled selected value="">Elige una opción</option>
								@foreach($jobtitles as $jobtitle)
								<option value="{{ $jobtitle->id }}">
									{{ $jobtitle->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Ó bien crea uno nuevo</label>
							<input type="text" name="new_jobtitle" placeholder="Nuevo puesto laboral" value="{{ old('new_jobtitle') }}" class="form-control">
						</div>
					</div>
				</div>
				<p class="lead text-muted mt-4">Fecha de contratación / Sueldo</p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="" required>Fecha de contratación</label>
							<input type="date" required name="active_at" class="form-control" value="{{ old('active_at') }}" >
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="" required>Salario mensual</label>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text">
												<i class="fa fa-dollar-sign fa-sm"></i>
											</span>
										</div>
										<input type="number" step=".01" name="salary" value="{{ old('salary') }}" class="form-control" required>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="" required>Moneda:</label>
									<select name="currency_id" id="" value="{{ old('currecy_id') }}" class="custom-select" required>
										<option disabled selected value="">Elige una opción</option>
										@foreach($currencies as $currency)
										<option value="{{ $currency->id }}">
											{{ $currency->short_name }}
										</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p class="lead text-muted mt-4">Información bancaria</p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Banco:</label>
							<select name="bank_id" value="{{ old('bank_id') }}" class="custom-select" id="">
								<option disabled selected value="">Elige una opción</option>
								@foreach($banks as $bank)
								<option value="{{ $bank->id }}">{{ $bank->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Número de cuenta:</label>
							<input type="number" 
								step="any"
								name="bank_account_num"
								value="{{ old('bank_account_num') }}" 
								class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Número de tarjeta:</label>
							<input type="number" 
								step="any"
								name="bank_card_num"
								value="{{ old('bank_card_num') }}"  
								class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Clabe interbancaria:</label>
							<input type="number" 
								step="any"
								name="interbank_clabe" 
								value="{{ old('interbank_clabe') }}"
								class="form-control">
						</div>
					</div>
				</div>
				<p class="lead text-muted mt-4">Descuentos / IMSS</p>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Fonacot:</label>
							<input type="number" 
								step="any"
								name="fonacot_discount" 
								value="{{ old('fonacot_discount') }}"
								class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Infonavit:</label>
							<input type="number" 
								step="any"
								name="infonavit_discount" 
								value="{{ old('infonavit_discount') }}"
								class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">(NSS) Número de seguro social</label>	
							<input type="text" name="nss" value="{{ old('nss') }}" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Fecha de alta en IMSS:</label>
							<input type="date" name="imss_active_at" class="form-control" value="{{ old('imss_active_at') }}" >
						</div>
					</div>
				</div>
				<button type="submit" 
					class="btn btn-success mt-4"
					name="submit_btn">
					<i class="fa fa-check fa-sm mr-2"></i>
					Guardar Empleado
				</button>
			</form>
		</div>
	</div>
@stop