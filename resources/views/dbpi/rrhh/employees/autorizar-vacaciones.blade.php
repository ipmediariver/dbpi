@extends('layouts.guest')

@section('content')
	
	<div class="card shadow-sm">
		<h5 class="card-header">Solicitud de vacaciones</h5>
		<div class="card-body">
			
			<p class="text-uppercase">{{ $solicitud->empleado->full_name }} SOLICITA SUS VACACIONES.</p>

			<table class="table table-bordered table-sm">
				<tbody>
					<tr>
						<th>Periodo a disfrutar:</th>
						<td width="50%">Periodo {{ $solicitud->periodo }}</td>
					</tr>

					<tr>
						<th>Desde:</th>
						<td>{{ $solicitud->cuando_inicia }}</td>
					</tr>

					<tr>
						<th>Hasta:</th>
						<td>{{ $solicitud->cuando_termina }}</td>
					</tr>

					<tr>
						<th>Días correspondientes:</th>
						<td>({{ $solicitud->dias_correspondientes }}) días</td>
					</tr>

					<tr>
						<th>Días a disfrutar:</th>
						<td>({{ $solicitud->dias_a_disfrutar }}) días</td>
					</tr>

					<tr>
						<th>Días pendientes:</th>
						<td>({{$solicitud->dias_pendientes}}) días</td>
					</tr>

					<tr>
						<th>Fecha en que deberá presentarse a trabajar:</th>
						<td>{{ $solicitud->fecha_en_que_debe_presentarse }}</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>

@stop

@section('sidebar')

	dd($decision)

	<botones-para-autorizar-vacaciones 
		:solicitud="{{ $solicitud }}"
		:decision="{{ $decision ? 'true' : 'false' }}"
		:director="{{ $director }}">
	</botones-para-autorizar-vacaciones>

@stop