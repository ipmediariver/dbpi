<div class="d-flex flex-column">
	<div class="avatar" style="background-image: url('{{ asset('uploads/avatars/' . $employee->avatar) }}')"></div>
</div>
<h1 class="h3 my-3">
{{ $employee->short_name }}
</h1>
<p class="mb-1 small">
	<i class="fa fa-user text-muted fa-fw"></i>
	{{ $employee->jobtitle->name }}
</p>
<p class="mb-1 small">
	<i class="fa fa-envelope text-muted fa-fw"></i>
	{{ $employee->email }}
</p>
<p class="mb-1 small">
	<i class="fa fa-calendar-alt text-muted fa-fw"></i>
	Desde: {{ $employee->active_at->format('d-m-Y') }}
</p>

<hr>

<ul class="list-group shadow-sm">
	<a href="{{ route('employee-personal-information', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/informacion-personal')) ? 'active' : '' }}
		">
		Información Personal
	</a>
	<a href="{{ route('employee-monthly-salary', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/salario-mensual')) ? 'active' : '' }}
		">
		Salario Mensual
	</a>
	<a href="{{ route('employee-active-and-unactive-date', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/fecha-contratacion')) ? 'active' : '' }}
		">
		Fecha Contratación / Baja
	</a>
	<a href="{{ route('employee-department', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/departamento')) ? 'active' : '' }}
		">
		Puesto Laboral
	</a>
	<a href="{{ route('employee-bank-information', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/informacion-bancaria')) ? 'active' : '' }}
		">
		Información Bancaria
	</a>
	<a href="{{ route('employee-discounts-information', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/descuentos')) ? 'active' : '' }}
		">
		Descuentos / IMSS
	</a>
	<a href="{{ route('historial-de-vacaciones', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/historial-de-vacaciones')) ? 'active' : '' }}
		">
		Vacaciones
	</a>
	<a href="{{ route('empleados.archivos-adjuntos.index', $employee) }}" 
		class="
			list-group-item 
			list-group-item-action
			{{ (request()->is('rrhh/empleados/*/archivos-adjuntos')) ? 'active' : '' }}
		">
		({{ $employee->attachments()->count() }}) Archivos Adjuntos
	</a>
</ul>

<form action="{{ route('empleados.destroy', $employee) }}"
	method="post" 
	class="mt-4 border-top pt-3"
	onsubmit="submit_btn.disabled = true; return true;">
	@csrf
	@method('DELETE')
	<button type="submit" 
		class="btn btn-link btn-block text-danger" 
		name="submit_btn"
		onclick="return confirm('Se eliminará al empleado del sistema ¿Desease Continuar?')">
		<i class="fa fa-trash fa-sm mr-2"></i>
		Eliminar empleado
	</button>
</form>