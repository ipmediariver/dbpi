@extends('dbpi.rrhh.layout')
@section('module_title', 'Información del empleado')
@section('module_actions')
	<div class="d-flex align-items-center">
		<a href="#" class="btn btn-outline-primary ml-3">
			<i class="fa fa-download fa-sm mr-2"></i>
			Descargar expediente
		</a>
	</div>
@stop
@section('module_content')
<div class="row">
	<div class="col-sm-3">
		@include('dbpi.rrhh.employees.components.employee-sidebar')
	</div>
	<div class="col-sm-9 border-left">
		@yield('employee_content')
	</div>
</div>
@stop