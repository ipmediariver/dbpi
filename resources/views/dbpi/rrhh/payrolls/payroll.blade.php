@extends('dbpi.rrhh.layout')
@section('module_title', 'Detalles de nómina')
@section('module_content')
	<p class="mb-4">
		<b>
			<i class="fa fa-calendar-alt fa-sm text-muted mr-2"></i> 
			{{ $payroll->from->format('d-m-Y') .' al '. $payroll->to->format('d-m-Y') }}
		</b>
	</p>
	@foreach($accounts as $account)
		<div class="d-flex align-items-center">
			<a href="{{ route('payroll', $account) }}">
				<i class="fa {{ $account->open ? 'fa-lock-open text-success' : 'fa-lock text-danger' }} fa-sm mr-2"></i>
				{{ $account->name }}
			</a>
		</div>
		@if(!$loop->last)
		<hr>
		@endif
	@endforeach
@stop