@extends('dbpi.rrhh.layout')
@section('module_title')
	<span class="avatar sm mr-3" style="background-image: url('{{ $employee_payroll->employee->avatar }}')"></span>
	<span>{{ $employee_payroll->employee->full_name }} | {{ $account_payroll->date->from->format('d-m-Y') }} a {{ $account_payroll->date->to->format('d-m-Y') }}</span>
@stop
@section('module_content')
<p>
	<b>
		Otros Ingresos
	</b>
</p>
@if($employee_payroll->entries->count())
<table class="table">
	<thead>
		<tr>
			<th width="20%">Fecha</th>
			<th width="40%">Motivo</th>
			<th width="20%">Categoría</th>
			<th width="10%" class="text-right">Total</th>
			@if($account_payroll->open)
			<th width="10%" class="text-right">Eliminar</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach($employee_payroll->entries as $entry)
		<tr>
			<td>{{ $entry->date->format('d-m-Y') }}</td>
			<td>
				{{ $entry->subject }}
				@if($entry->ammount_per_day)
					<br>
					<span class="text-info">
						Ingreso de (${{ number_format($entry->ammount_per_day, '2') }}) pesos diarios por ({{ $entry->days }}) días.
					</span>
				@elseif($entry->percent_entry)
					<br>
					<span class="text-info">
						Ingreso del ({{ $entry->percent_entry }}%) por ({{ $entry->days }}) días.
					</span>
				@elseif($entry->days)
					<br>
					<span class="text-info">
						Ingreso del salario mínimo por ({{ $entry->days }}) días.
					</span>
				@endif
			</td>
			<td>{{ $entry->category->name }}</td>
			<td class="text-right cell-success">${{ number_format($entry->ammount, 2, '.', '') }}</td>
			@if($account_payroll->open)
			<td class="text-right">
				<form action="{{ route('delete-entry', $entry) }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					@method('DELETE')
					<button type="submit" name="submit_btn" class="btn btn-link p-0 text-danger" onclick="return confirm('Se va a eliminar este ingreso, ¿Deseas continuar?')">
						Eliminar
					</button>
				</form>
			</td>
			@endif
		</tr>
		@endforeach
	</tbody>
	<tbody>
		<tr>
			<th colspan="4" class="text-right text-success">${{ $employee_payroll->entries_ammount }}</th>
			@if($account_payroll->open)
			<th></th>
			@endif
		</tr>
	</tbody>
</table>
@else
<span class="text-muted d-block">No se han registrado ingresos.</span>
@endif
@stop