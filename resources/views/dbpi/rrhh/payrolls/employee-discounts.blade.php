@extends('dbpi.rrhh.layout')
@section('module_title')
	<span class="avatar sm mr-3" style="background-image: url('{{ $employee_payroll->employee->avatar }}')"></span>
	<span>{{ $employee_payroll->employee->full_name }} | {{ $account_payroll->date->from->format('d-m-Y') }} a {{ $account_payroll->date->to->format('d-m-Y') }}</span>
@stop
@section('module_content')
<p>
	<b>
		Otros Descuentos
	</b>
</p>
@if($employee_payroll->discounts->count())
<table class="table">
	<thead>
		<tr>
			<th width="20%">Fecha</th>
			<th width="40%">Motivo</th>
			<th width="20%">Categoría</th>
			<th width="10%" class="text-right">Total</th>
			@if($account_payroll->open)
			<th width="10%" class="text-right">Eliminar</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach($employee_payroll->discounts as $discount)
		<tr>
			<td>{{ $discount->date->format('d-m-Y') }}</td>
			<td>
				{{ $discount->subject }}
				@if($discount->ammount_per_day)
					<br>
					<span class="text-info">
						Descuento de (${{ number_format($discount->ammount_per_day, '2') }}) pesos diarios por ({{ $discount->days }}) días.
					</span>
				@elseif($discount->percent_discount)
					<br>
					<span class="text-info">
						Descuento del ({{ $discount->percent_discount }}%) por ({{ $discount->days }}) días.
					</span>
				@elseif($discount->days)
					<br>
					<span class="text-info">
						Descuento del salario mínimo por ({{ $discount->days }}) días.
					</span>
				@endif
			</td>
			<td>{{ $discount->category->name }}</td>
			<td class="text-right cell-danger">${{ number_format($discount->ammount, 2, '.', '') }}</td>
			@if($account_payroll->open)
			<td class="text-right">
				<form action="{{ route('delete-discount', $discount) }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					@method('DELETE')
					<button type="submit" name="submit_btn" class="btn btn-link p-0 text-danger" onclick="return confirm('Se va a eliminar este descuento, ¿Deseas continuar?')">
						Eliminar
					</button>
				</form>
			</td>
			@endif
		</tr>
		@endforeach
	</tbody>
	<tbody>
		<tr>
			<th colspan="4" class="text-right text-danger">${{ $employee_payroll->discounts_ammount }}</th>
			@if($account_payroll->open)
			<th></th>
			@endif
		</tr>
	</tbody>
</table>
@else
<span class="text-muted d-block">No se han registrado descuentos.</span>
@endif
@stop