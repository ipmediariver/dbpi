@extends('dbpi.rrhh.layout')
@section('module_title', $account_payroll->name)
@section('module_actions')
	@if($account_payroll->open)
		<div class="d-flex align-items-center">
			<a class="btn btn-outline-danger" data-toggle="collapse" href="#discountByPercentCollapse" title="Agregar descuento por días">
				<i class="fa fa-calendar-times fa-fw fa-sm"></i>
			</a>
			<a class="btn btn-outline-danger ml-2" data-toggle="collapse" href="#incidentCollapse" title="Agregar una falta">
				<i class="fa fa-user-slash fa-fw fa-sm"></i>
			</a>
			<a class="btn btn-outline-danger ml-2" data-toggle="collapse" href="#discountCollapse" title="Agregar un descuento">
				<i class="fa fa-user-minus fa-fw fa-sm"></i>
			</a>
			<a class="btn btn-outline-success ml-2" data-toggle="collapse" href="#entryByDays" title="Agregar ingreso por días.">
				<i class="fa fa-calendar-check fa-fw fa-sm"></i>
			</a>
			<a class="btn btn-outline-success ml-2" data-toggle="collapse" href="#entryCollapse" title="Agregar un ingreso">
				<i class="fa fa-user-plus fa-fw fa-sm"></i>
			</a>
			<a class="btn btn-outline-secondary ml-2" data-toggle="collapse" href="#removeEmployeeCollapse" title="Eliminar un empleado">
				<i class="fa fa-user-times fa-fw fa-sm"></i>
			</a>
			<div class="ml-3 border-left pl-3 d-flex align-items-center">
				<form action="{{ route('update-employees-from-account-payroll', $account_payroll) }}" 
					method="post"
					onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					<button class="btn btn-outline-secondary"
						name="submit_btn" 
						title="Actualizar Lista de Empleados" 
						onclick="return confirm('Se actualizará la lista de empleados de la nómina. Nuevos Empleados, Aumentos, etc. se agregarán a la nómina actual, ¿Deseas continuar?')">
						<i class="fa fa-sync fa-fw fa-sm"></i>
					</button>
				</form>
				<form action="{{ route('download-payroll', $account_payroll) }}" class="ml-2" method="post">
					@csrf
					<button type="submit" 
						class="btn btn-outline-secondary" 
						name="download_btn"
						title="Descargar Nómina">
						<i class="fa fa-file-excel fa-fw fa-sm"></i>
					</button>
				</form>
				<form action="{{ route('close-payroll', $account_payroll) }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					<button type="submit"
						class="btn btn-outline-danger ml-2" 
						name="submit_btn"
						onclick="return confirm('Estas a un paso de cerrar el ejercicio de esta nómina. ¿Deseas continuar?');"
						title="Cerrar Nómina">
						<i class="fa fa-lock fa-fw fa-sm"></i>
					</button>
				</form>
			</div>
		</div>
	@else
		<div class="d-flex align-items-center">
			<span>
				<i class="fa fa-lock text-danger fa-sm mr-2"></i> Nómina Cerrada.
			</span>
			@role('admin')
			<form action="{{route('open-payroll', $account_payroll)}}" 
				class="ml-3"
				method="post" 
				onsubmit="open_payroll_btn.disabled = true; return true;">
				@csrf
				<button type="submit" name="open_payroll_btn" class="btn btn-link" onclick="return confirm('La nómina será desbloqueada, ¿Desease continuar?')">
					<i class="fa fa-lock-open text-success mr-2"></i>
					Desbloquear
				</button>
			</form>
			@endrole
			<form action="{{ route('download-payroll', $account_payroll) }}" method="post" class="ml-3">
				@csrf
				<button type="submit" class="btn btn-outline-secondary" name="download_btn">
					<i class="fa fa-file-excel fa-sm mr-2"></i>
					Descargar
				</a>
			</form>
		</div>
	@endif

@stop
@section('module_content')
	<div class="accordion" id="actionsAccordion">
		@include('dbpi.rrhh.payrolls.components.discount-percent')
		@include('dbpi.rrhh.payrolls.components.incidence-form')
		@include('dbpi.rrhh.payrolls.components.discount-form')
		@include('dbpi.rrhh.payrolls.components.entry-by-days')
		@include('dbpi.rrhh.payrolls.components.entry-form')
		@include('dbpi.rrhh.payrolls.components.remove-employee-form')
	</div>

	<p>
		<b>
			<i class="fa fa-calendar-alt text-muted fa-sm mr-2"></i>
			{{ $account_payroll->date->from->format('d-m-Y') .' a '. $account_payroll->date->to->format('d-m-Y') }}
		</b>
	</p>
	@if($account_payroll->employee_payrolls->count())
	<table class="table m-0">
		<thead>
			<tr>
				<th>Nombre de empleado</th>
				<th class="text-right">Importe Bruto</th>
				<th class="text-right">Otros Descuentos</th>
				<th class="text-right">Otros Ingresos</th>
				<th class="text-right">Fonacot</th>
				<th class="text-right">Infonavit</th>
				<th class="text-right">Total a depositar</th>
			</tr>
		</thead>
		<tbody>
			@foreach($account_payroll->employee_payrolls->sortBy('employee.last_name_1') as $payroll)
				<tr>
					<td class="@if($payroll->employee->assimilated) cell-primary @endif">
						<div class="d-flex align-items-center">
							<span class="avatar sm mr-3" style="background-image: url('{{ asset('uploads/avatars/' . $payroll->employee->avatar) }}')"></span>
							<div>
								<a href="{{ route('empleados.show', $payroll->employee) }}">
									{{ $payroll->employee->full_name_reverse }}
								</a>
								@if($payroll->employee->assimilated)
								<div class="small">(Asimilado)</div>
								@endif
							</div>
						</div>
					</td>
					<td class="text-right cell-warning">${{ number_format($payroll->fortnight_salary, 2) }}</td>
					<td class="text-right cell-danger">
						<a href="{{ route('employee_discounts', [$account_payroll, $payroll]) }}">${{ number_format($payroll->discounts_ammount, 2) }}</a>
					</td>
					<td class="text-right cell-success">
						<a href="{{ route('employee_entries', [$account_payroll, $payroll]) }}">
							${{ number_format($payroll->entries_ammount, 2) }}
						</a>
					</td>
					<td class="text-right cell-primary">${{ number_format($payroll->fonacot, 2) }}</td>
					<td class="text-right cell-primary">${{ number_format($payroll->infonavit, 2) }}</td>
					<td class="text-right cell-warning">${{ number_format($payroll->total_ammount, 2) }}</td>
				</tr>
			@endforeach
		</tbody>
		<tbody>
			<tr>
				<td></td>
				<th class="text-right cell-warning">${{ number_format($account_payroll->fortnight_total, 2) }}</th>
				<th class="text-right cell-danger">${{ number_format($account_payroll->discounts_total, 2) }}</th>
				<th class="text-right cell-success">${{ number_format($account_payroll->entries_total, 2) }}</th>
				<th class="text-right cell-primary">${{ number_format($account_payroll->fonacot_total, 2) }}</th>
				<th class="text-right cell-primary">${{ number_format($account_payroll->infonavit_total, 2) }}</th>
				<th class="text-right cell-warning">${{ number_format($account_payroll->total, 2) }}</th>
			</tr>
		</tbody>
	</table>
	@else
		No se han agregado empleados a esta empresa
	@endif
@stop