<div class="collapse" id="discountByPercentCollapse" data-parent="#actionsAccordion">
	<div class="card mb-4 card-body shadow-sm">
		<div class="row">
			<div class="col-sm-5 mx-auto">
				<p class="lead">Nuevo Descuento Por Días</p>
				@component('components.form-header', [
					'description' => 'Llena los siguientes campos para agregar un descuento'
				])
				@endcomponent
				<set-employee-discount-type>
					<template slot-scope="props">
						<form action="{{ route('new-discount-per-days') }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
							@csrf
							<div class="form-group">
								<label for="" required>Elige un empleado:</label>
								<select name="employee_id" class="custom-select" id="" required>
									<option disabled selected value="">Elige una opción</option>
									@foreach($account_payroll->employee_payrolls->sortBy('employee.last_name_1') as $payroll)
									<option value="{{ $payroll->id }}">{{ $payroll->employee->full_name_reverse }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Motivo de descuento:</label>
								<textarea name="subject" 
									id="" 
									class="form-control" 
									style="resize: none" 
									placeholder="Escribe aquí..." required></textarea>
							</div>
							<div class="form-group">
								<label for="">Tipo de descuento:</label>
								<ul class="list-group">
									<li class="list-group-item">
										<div class="custom-control custom-radio">
											<input type="radio"
												id="discountType1" 
												name="discountType" 
												class="custom-control-input"
												@change.prevent="props.set_discount_type('ammount')"
												:checked="props.type == 'ammount'"
												value="ammount">
											<label class="custom-control-label" for="discountType1">Monto Fijo</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="custom-control custom-radio">
											<input type="radio" 
												id="discountType2" 
												name="discountType" 
												class="custom-control-input"
												@change.prevent="props.set_discount_type('percent')"
												:checked="props.type == 'percent'"
												value="percent">
											<label class="custom-control-label" for="discountType2">Porcentaje</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="custom-control custom-radio">
											<input type="radio" 
												id="discountType3" 
												name="discountType" 
												class="custom-control-input"
												@change.prevent="props.set_discount_type('salary')"
												:checked="props.type == 'salary'"
												value="salary">
											<label class="custom-control-label" for="discountType3">Salario Mínimo</label>
										</div>
									</li>
								</ul>
							</div>
							<div class="form-group" v-if="props.type == 'ammount'">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="fa fa-dollar-sign fa-sm"></i>
										</span>
									</div>
									<input type="text" 
										name="ammount" 
										class="form-control" 
										aria-label="Ammount"
										required>
								</div>
							</div>
							<div class="form-group"  v-if="props.type == 'percent'">
								<div class="input-group">
									<input type="text" 
										class="form-control" 
										aria-label="Porcentaje"
										name="percent"
										required>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="fa fa-percent fa-sm"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" 
										class="custom-control-input" 
										id="applyToAllDaysCheckbox"
										:checked="props.all_days"
										@change="props.set_all_days"
										name="all_days">
									<label class="custom-control-label" 
										for="applyToAllDaysCheckbox">
										Aplicar a todos los días de la quincena
									</label>
								</div>
								<p class="small text-muted mt-2 m-0">(Deselecciona esta casilla para especificar el número de días a aplicar el descuento.)</p>
							</div>
							<div class="form-group" v-if="!props.all_days">
								<label for="">Días:</label>
								<input type="text" name="days" class="form-control" required>
							</div>
							<button type="submit" class="btn btn-danger mt-3" name="submit_btn">
								Aplicar descuento
							</button>
						</form>
					</template>
				</set-employee-discount-type>
			</div>
		</div>
	</div>
</div>