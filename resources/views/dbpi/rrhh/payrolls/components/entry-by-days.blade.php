<div class="collapse" id="entryByDays" data-parent="#actionsAccordion">
	<div class="card mb-4 card-body shadow-sm">
		<div class="row">
			<div class="col-sm-5 mx-auto">
				<p class="lead">Nuevo Ingreso Por Días</p>
				@component('components.form-header', [
					'description' => 'Llena los siguientes campos para agregar un ingreso'
				])
				@endcomponent

				<add-entry-by-days>
					<template slot-scope="props">
						<form action="{{ route('new-entry-per-days') }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
							@csrf
							<div class="form-group">
								<label for="" required>Elige un empleado:</label>
								<select name="employee_id" class="custom-select" id="" required>
									<option disabled selected value="">Elige una opción</option>
									@foreach($account_payroll->employee_payrolls->sortBy('employee.last_name_1') as $payroll)
									<option value="{{ $payroll->id }}">{{ $payroll->employee->full_name_reverse }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Motivo de ingreso:</label>
								<textarea name="subject" 
									id="" 
									class="form-control" 
									style="resize: none" 
									placeholder="Escribe aquí..." required></textarea>
							</div>
							<div class="form-group">
								<label for="">Tipo de ingreso:</label>
								<ul class="list-group">
									<li class="list-group-item">
										<div class="custom-control custom-radio">
											<input type="radio"
												id="entryType1" 
												name="entryType" 
												class="custom-control-input"
												@change.prevent="props.set_entry_type('ammount')"
												:checked="props.type == 'ammount'"
												value="ammount">
											<label class="custom-control-label" for="entryType1">Monto Fijo</label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="custom-control custom-radio">
											<input type="radio" 
												id="entryType2" 
												name="entryType" 
												class="custom-control-input"
												@change.prevent="props.set_entry_type('percent')"
												:checked="props.type == 'percent'"
												value="percent">
											<label class="custom-control-label" for="entryType2">Porcentaje</label>
										</div>
									</li>
								</ul>
							</div>
							<div class="form-group" v-if="props.type == 'ammount'">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="fa fa-dollar-sign fa-sm"></i>
										</span>
									</div>
									<input type="text" 
										name="ammount" 
										class="form-control" 
										aria-label="Ammount"
										required>
								</div>
							</div>
							<div class="form-group"  v-if="props.type == 'percent'">
								<div class="input-group">
									<input type="text" 
										class="form-control" 
										aria-label="Porcentaje"
										name="percent"
										required>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="fa fa-percent fa-sm"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" 
										class="custom-control-input" 
										id="applyEntryToAllDays"
										:checked="props.all_days"
										@change="props.set_all_days"
										name="all_days">
									<label class="custom-control-label" 
										for="applyEntryToAllDays">
										Aplicar a todos los días de la quincena
									</label>
								</div>
								<p class="small text-muted mt-2 m-0">(Deselecciona esta casilla para especificar el número de días a aplicar el ingreso.)</p>
							</div>
							<div class="form-group" v-if="!props.all_days">
								<label for="">Días:</label>
								<input type="text" name="days" class="form-control" required>
							</div>
							<button type="submit" class="btn btn-success mt-3" name="submit_btn">
								Aplicar ingreso
							</button>
						</form>
					</template>
				</add-entry-by-days>
			</div>
		</div>
	</div>
</div>