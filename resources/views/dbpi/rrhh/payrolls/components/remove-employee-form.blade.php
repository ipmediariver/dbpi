<div class="collapse" id="removeEmployeeCollapse" data-parent="#actionsAccordion">
	<div class="card mb-4 card-body shadow-sm">
		<div class="row">
			<div class="col-sm-5 mx-auto">
				<p class="lead">Eliminar a un empleado de la nómina</p>
				@component('components.form-header', [
					'description' => 'Selecciona el empleado que deseas retirar de la nómina'
				])
				@endcomponent
				<form action="{{ route('remove-employee-payroll') }}" 
					method="post"
					onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					<div class="form-group">
						<label for="" required>Empleado:</label>
						<select name="employee_payroll_id" class="custom-select">
							<option disabled selected value="">Elige un empleado</option>
							@foreach($account_payroll->employee_payrolls->sortBy('employee.name_1') as $payroll)
							<option value="{{ $payroll->id }}">{{ $payroll->employee->full_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="d-flex align-items-center mt-4">
						<button type="submit" 
							class="btn btn-danger" 
							name="submit_btn"
							onclick="return confirm('Se eliminará al empleado seleccionado de la nómina actual, ¿Deseas Continuar?')">
							<i class="fa fa-times fa-sm mr-2"></i>
							Eliminar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>