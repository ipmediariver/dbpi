<div class="collapse" id="discountCollapse" data-parent="#actionsAccordion">
	<div class="card mb-4 card-body shadow-sm">
		<div class="row">
			<div class="col-sm-5 mx-auto">
				<p class="lead">Nuevo Descuento</p>
				@component('components.form-header', [
					'description' => 'Llena los siguientes campos para agregar un descuento'
				])
				@endcomponent
				<form action="{{ route('new-discount') }}" 
					method="post"
					onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					<div class="form-group">
						<label for="" required>Fecha:</label>
						@component('components.datepicker', [
							'prefix' => 'date',
							'required' => true,
							'day' => Carbon\Carbon::now()->format('d'),
							'month' => Carbon\Carbon::now()->format('m'),
							'year' => Carbon\Carbon::now()->format('Y')
						])
						@endcomponent
					</div>
					<div class="form-group">
						<label for="" required>Elige un empleado:</label>
						<select name="employee_id" class="custom-select" id="" required>
							<option disabled selected value="">Elige una opción</option>
							@foreach($account_payroll->employee_payrolls->sortBy('employee.name_1') as $payroll)
							<option value="{{ $payroll->id }}">{{ $payroll->employee->full_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="" required>Tipo de decuento:</label>
						<select name="employee_payroll_discounts_category_id" 
							class="custom-select"
							required>
							<option disabled selected value="">Elige una opción</option>
							@foreach($discount_categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label for="" required>Motivo del descuento:</label>
						<textarea name="subject" class="form-control" required></textarea>
					</div>

					<div class="form-group">
						<label for="" required>Monto a descontar:</label>
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">$</span>
						  </div>
						  <input type="text" class="form-control" name="ammount" required>
						</div>
					</div>

					<div class="d-flex align-items-center mt-4">
						<button type="submit" class="btn btn-danger" name="submit_btn">
							<i class="fa fa-times fa-sm mr-2"></i>
							Descontar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>