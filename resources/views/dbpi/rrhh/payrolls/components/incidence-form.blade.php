<div class="collapse" id="incidentCollapse" data-parent="#actionsAccordion">
	<div class="card mb-4 card-body shadow-sm">
		<div class="row">
			<div class="col-sm-5 mx-auto">
				<p class="lead">Nueva Falta</p>
				@component('components.form-header', [
					'description' => 'Llena los siguientes campos para asignar una falta'
				])
				@endcomponent
				<form action="{{ route('new-incidence') }}" 
					method="post"
					onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					<div class="form-group">
						<label for="" required>Fecha:</label>
						@component('components.datepicker', [
							'prefix' => 'date',
							'required' => true,
							'day' => Carbon\Carbon::now()->format('d'),
							'month' => Carbon\Carbon::now()->format('m'),
							'year' => Carbon\Carbon::now()->format('Y')
						])
						@endcomponent
					</div>
					<div class="form-group">
						<label for="" required>Elige un empleado:</label>
						<select name="employee_id" class="custom-select" id="" required>
							<option disabled selected value="">Elige una opción</option>
							@foreach($account_payroll->employee_payrolls->sortBy('employee.name_1') as $payroll)
							<option value="{{ $payroll->id }}">{{ $payroll->employee->full_name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label for="" required>Motivo de falta:</label>
						<textarea name="subject" class="form-control" required></textarea>
					</div>

					<div class="d-flex align-items-center mt-4">
						<button type="submit" 
							class="btn btn-danger"
							name="submit_btn">
							<i class="fa fa-times fa-sm mr-2"></i>
							Descontar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>