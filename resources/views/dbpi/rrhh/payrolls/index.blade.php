@extends('dbpi.rrhh.layout')
@section('module_title', 'Fechas de nómina')
@section('module_content')
	<div class="d-flex align-items-center">
		<p>Nómina {{ $year->format('Y') }}</p>
		<x-consultar-anos-anteriores clases="ml-auto" />
	</div>

	@if($payrolls->count())
		<table class="table m-0">
			<thead>
				<tr>
					<th>De:</th>
					<th>Hasta:</th>
					<th>Estatus:</th>
					<th class="text-right">Ver</th>
				</tr>
			</thead>
			<tbody>
				@foreach($payrolls as $payroll)
				<tr>
					<td>{{ $payroll->from->format('Y-m-d') }}</td>
					<td>{{ $payroll->to->format('Y-m-d') }}</td>
					<td>{!! $payroll->status_badge !!}</td>
					<td class="text-right">
						<a href="{{ route('fechas.show', $payroll) }}">Ver detalles</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@else

		@php
			$ano = date('Y');
			$ano_de_busqueda = request()->ano;
		@endphp

		@if(!$ano_de_busqueda)
		<p class="text-muted">No se ha creado el calendario anual.</p>
		<form action="{{ route('create-payroll-calendar') }}" 
			method="post"
			onsubmit="submit_btn.disabled = true; return true">
			@csrf
			<button type="submit" class="btn btn-outline-primary" name="submit_btn">
				<i class="fa fa-calendar-alt fa-sm mr-2"></i>
				Crear calendario
			</button>
		</form>
		@endif
	@endif
@stop