@extends('dbpi.rrhh.layout')
@section('module_title', 'Escritorio')
@section('module_content')
	@if($payroll)
	<p><b>Nómina actual: {{ $payroll->from->format('d-m-Y') }} al {{ $payroll->to->format('d-m-Y') }}</b></p>
	<div class="card-group">
		<div class="card card-body shadow-sm">
			<p><b>Total de empleados:</b></p>
			<h1 class="h2 m-0">50</h1>
		</div>
		<div class="card card-body shadow-sm">
			<p><b>Número de incidencias:</b></p>
			<h1 class="h2 m-0">50</h1>
		</div>
		<div class="card card-body shadow-sm">
			<p><b>Monto de incidencias:</b></p>
			<h1 class="h2 m-0">50</h1>
		</div>
	</div>
	@endif
@stop