@extends('dbpi.rrhh.layout')
@section('module_title', 'Cuentas') 
@section('module_content')
<p><b>Lista de empresas</b></p>
<table class="table m-0">
	<thead>
		<tr>
			<th>Cuenta</th>
		</tr>
	</thead>
	<tbody>
		@foreach($accounts as $account)
		<tr>
			<td>
				<a href="{{ route('get-employees-account', $account->account_id) }}">{{ $account->name }}</a>
				<div class="d-block small text-muted">
					({{ $account->employees()->count() }}) Empleados
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@stop