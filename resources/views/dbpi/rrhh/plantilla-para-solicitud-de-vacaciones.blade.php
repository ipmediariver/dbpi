@extends('layouts.blank')
@section('content')
	
	<div class="container py-5">
		<div class="row">
			@if(!$empleado->vacaciones_pendientes_de_autorizacion)
			<div class="col-sm-8 mx-auto">
				<formulario-para-solicitar-vacaciones 
					:empleado="{{ $empleado }}"
					:cuenta="{{ $empleado->account }}">
				</formulario-para-solicitar-vacaciones>
			</div>
			@else
			<div class="col-sm-8 mx-auto text-center">
				<h5 class="text-uppercase"><b>Hola {{ $empleado->full_name }}</b></h5>
				<p class="lead">
					Ya tienes una solicitud de vacaciones pendiente de autorizacion.
				</p>
			</div>
			@endif
		</div>
	</div>

@stop