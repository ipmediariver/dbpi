@extends('layouts.module', [
	
	'remove_notifications' => true
	
])
@section('module_sidebar')
	<ul class="list-group">
		<a href="{{ route('usuarios.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('config/usuarios')) ? 'active' : '' }}
				{{ (request()->is('config/usuarios/*')) ? 'active' : '' }}
			">
			<i class="fa fa-users fa-fw fa-sm mr-2"></i>
			Usuarios
		</a>

		<a href="{{ route('cuentas') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('config/cuentas')) ? 'active' : '' }}
			">
			<i class="fa fa-sync fa-fw fa-sm mr-2"></i>
			Cuentas
		</a>

		<a href="{{ route('migraciones') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('config/migraciones')) ? 'active' : '' }}
			">
			<i class="fa fa-database fa-fw fa-sm mr-2"></i>
			Migraciones
		</a>

		<a href="{{ route('actualizaciones') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('config/actualizaciones')) ? 'active' : '' }}
			">
			<i class="fa fa-code-branch fa-fw fa-sm mr-2"></i>
			Actualizaciones
		</a>
	</ul>
@stop