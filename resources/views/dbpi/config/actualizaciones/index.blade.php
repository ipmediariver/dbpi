@extends('dbpi.config.layout')
@section('module_title', 'Actualizaciones')
@section('module_content')
	<div class="align-items-center mb-4">
		<p class="m-0"><b>Actualizar sistema</b></p>
		<p class="m-0 text-danger small">(Esto podría tardar unos minutos)</p>
	</div>

	<form action="{{ route('actualizar-sistema') }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
		@csrf
		<button type="submit" class="btn btn-primary" name="submit_btn">
			<i class="fa fa-database fa-sm mr-2"></i>
			Click para actualizar sistema
		</button>
	</form>

@stop