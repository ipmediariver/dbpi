@component('components.form-header', [
	'description' => 'Llena los siguientes campos para crear o modificar un usuario'
])
@endcomponent
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="" required>Nombre(s):</label>
			<input type="text" 
				value="{{ isset($user) ? $user->first_name : old('first_name') }}"
				name="first_name" 
				class="form-control" 
				required>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="" required>Apellido(s):</label>
			<input type="text" 
				name="last_name" 
				class="form-control" 
				value="{{ isset($user) ? $user->last_name : old('last_name') }}"
				required>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="" required>Correo electrónico:</label>
			<input type="email" 
				name="email" 
				class="form-control" 
				value="{{ isset($user) ? $user->email : old('email') }}"
				required>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="" required>Usuario:</label>
			<input type="text" 
				name="username" 
				class="form-control" 
				value="{{ isset($user) ? $user->username : old('username') }}"
				required>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="" required>Tipo:</label>
	<select name="role" id="" class="custom-select" required>
		<option disabled selected value="">Elige una opción</option>
		<option {{ isset($user) ? $user->role == 'admin' ? 'selected' : '' : '' }} value="admin">Administrador</option>
		<option {{ isset($user) ? $user->role == 'user' ? 'selected' : '' : '' }} value="user">Usuario</option>
	</select>
</div>

@isset($user)
	<div class="form-group">
		<div class="custom-control custom-checkbox">
		  <input type="checkbox" @if($user->validator) checked @endif name="validator" class="custom-control-input" id="userAsDirectiveCheckbox">
		  <label class="custom-control-label" for="userAsDirectiveCheckbox">Marcar como Directivo</label>
		</div>
	</div>
	<hr>
	<p><i class="fa fa-key fa-sm mr-2 text-muted"></i> Restaurar contraseña</p>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="">Nueva contraseña</label>
				<input type="password" name="password" class="form-control">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="">Confirmar contraseña</label>
				<input type="password" name="password_confirmation" class="form-control">
			</div>
		</div>
	</div>
@else
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="" required>Contraseña</label>
				<input type="password" name="password" class="form-control" required>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="" required>Confirmar contraseña</label>
				<input type="password" name="password_confirmation" class="form-control" required>
			</div>
		</div>
	</div>
@endisset