@extends('dbpi.config.layout')
@section('module_title', 'Usuarios del sistema')
@section('module_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>Lista de usuarios</b></p>
		<a href="{{ route('usuarios.create') }}" class="btn btn-primary ml-auto">Nuevo Usuario</a>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Email</th>
				<th>Usuario</th>
				<th>Tipo</th>
				<th>Departamentos</th>
				<th class="text-center">Directivo</th>
				<th class="text-right">Editar</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<td>{{ $user->full_name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->username }}</td>
				<td>{{ $user->role }}</td>
				<td class="text-uppercase">
					@foreach($user->departments as $department)
						{{ $department->name }} 
						@if($department->supervisor) 
							<i class="fa fa-star text-warning"></i> 
						@endif 
						<br>
					@endforeach
				</td>
				<td class="text-center">
					@if($user->validator)
						<i class="fa fa-check fa-sm text-success"></i>
					@else
						<i class="fa fa-times fa-sm text-danger"></i>
					@endif
				</td>
				<td class="text-right">
					<a href="{{ route('usuarios.edit', $user) }}">Editar</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop