@extends('dbpi.config.layout')
@section('module_title', 'Editar Usuario')
@section('module_content')
	<div class="row">
		<div class="col-sm-7">
			<div class="card card-body shadow-sm mb-4">
				<p><b>Información general</b></p>
				<form action="{{ route('usuarios.update', $user) }}" 
					method="post"
					onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					@method('PATCH')
					@include('dbpi.config.users.forms.user')
					<div class="d-flex align-items-center mt-4">
						<button type="submit" class="btn btn-success" name="submit_btn">
							<i class="fa fa-check fa-sm mr-2"></i>
							Actualizar
						</button>
						<button type="submit" 
							class="btn btn-outline-danger ml-2" 
							form="deleteUserForm"
							name="delete_btn" 
							onclick="return confirm('El usuario se eliminará, ¿Deseas continuar?')">
							<i class="fa fa-trash-alt fa-sm mr-2"></i> Eliminar
						</button>
					</div>
				</form>
				<form action="{{ route('usuarios.destroy', $user) }}" 
					method="post" 
					id="deleteUserForm"
					onsubmit="delete_btn.disabled = true; return true;">
					@csrf
					@method('DELETE')
				</form>
			</div>
			<div class="card card-body shadow-sm">
				<p><b>Asignar uno ó más departamentos al usuario</b></p>
				<form action="{{ route('asignar-departamento', [$user]) }}" method="post" class="mb-4">
					@csrf
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="">Departamento:</label>
								<select name="department_id" id="" class="custom-select" required>
									<option disabled selected value="">Elige una opción</option>
									@foreach($departments as $department)
									<option value="{{ $department->id }}">{{ strtoupper($department->name) }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox">
						  <input type="checkbox" name="supervisor" class="custom-control-input" id="checkAsDepartmentSupervisor">
						  <label class="custom-control-label" for="checkAsDepartmentSupervisor">Marcar como responsable de departamento</label>
						</div>
					</div>
					<button type="submit" class="btn btn-success mt-4">
						<i class="fa fa-check fa-sm mr-2"></i>
						Asignar departamento
					</button>
				</form>

				@if($user->departments->count())
					<table class="table">
						<tr>
							<th>Departamento</th>
							<th class="text-center">Encargado</th>
							<th></th>
						</tr>
						@foreach($user->departments as $department)
						<tr>
							<th>{{ $department->name }}</th>
							<td class="text-center">{!! $department->is_supervisor !!}</td>
							<td class="text-right">
								<form action="{{ route('desasignar-departamento', $department) }}" method="post">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-link p-0 text-danger" onclick="return confirm('Se desasignara a {{ $user->full_name }} del departamento {{ $department->name }}, ¿Deseas continuar?')">
										Desasignar
									</a>
								</form>
							</td>
						</tr>
						@endforeach
					</table>
				@else
					No se ha asignado un departamento a {{$user->full_name}}
				@endif
			</div>
		</div>
		<div class="col-sm-5">
			<div class="card card-body shadow-sm">
				<form action="{{ route('otorgar-permisos', $user) }}" 
					method="post"
					onsubmit="submit_permissions_btn.disabled = true; return true;">
					@csrf
					<p class="m-0"><b>Permisos en módulos</b></p>
					<hr class="my-3">
					@foreach($modules as $module)
					<p class="mb-2"><b>{{ $module->name }}</b></p>
						<div class="custom-control custom-checkbox">
							<input name="{{ $module->code }}_read" 
								type="checkbox" 
								class="custom-control-input" 
								id="{{ $module->code }}_read"
								@if($user->can_read_module($user, $module)) checked @endif>
							<label class="custom-control-label" for="{{ $module->code }}_read">Leer</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input name="{{ $module->code }}_edit" 
								type="checkbox" 
								class="custom-control-input" 
								id="{{ $module->code }}_edit"
								@if($user->can_edit_module($user, $module)) checked @endif>
							<label class="custom-control-label" for="{{ $module->code }}_edit">Editar</label>
						</div>
						<div class="custom-control custom-checkbox mb-3">
							<input name="{{ $module->code }}_delete" 
								type="checkbox" 
								class="custom-control-input" 
								id="{{ $module->code }}_delete"
								@if($user->can_delete_module($user, $module)) checked @endif>
							<label class="custom-control-label" for="{{ $module->code }}_delete">Eliminar</label>
						</div>
					@endforeach
					<button class="btn btn-success mt-4" name="submit_permissions_btn">
						<i class="fa fa-check fa-sm mr-2"></i>
						Guardar
					</button>
				</form>
			</div>
		</div>
	</div>
@stop