@extends('dbpi.config.layout')
@section('module_title', 'Crear Usuario')
@section('module_content')
	<div class="row">
		<div class="col-sm-7">
			<div class="card card-body shadow-sm">
				<p><b>Información general</b></p>
				<form action="{{ route('usuarios.store') }}" 
					method="post"
					onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					@include('dbpi.config.users.forms.user')
					<button type="submit" class="btn btn-success mt-4" name="submit_btn">
						<i class="fa fa-check fa-sm mr-2"></i>
						Guardar
					</button>
				</form>
			</div>
		</div>
	</div>
@stop