@extends('dbpi.config.layout')
@section('module_title', 'Migraciones')
@section('module_content')
	<div class="align-items-center mb-4">
		<p class="m-0"><b>Migrar base de datos</b></p>
		<p class="m-0 text-danger small">(Esto podría tardar unos minutos)</p>
	</div>
	<form action="{{ route('correr-migraciones') }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
		@csrf
		<button type="submit" class="btn btn-primary" name="submit_btn">
			<i class="fa fa-database fa-sm mr-2"></i>
			Migrar base de datos
		</button>
	</form>
@stop