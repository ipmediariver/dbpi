@extends('dbpi.config.layout')
@section('module_title', 'Sincronización de cuentas')
@section('module_content')
	<div class="align-items-center mb-4">
		<p class="m-0"><b>Sincronizar cuentas de CRM</b></p>
		<p class="m-0 text-danger small">(Esto podría tardar unos minutos)</p>
	</div>
	<form action="{{ route('sincronizar-cuentas') }}" method="post" onsubmit="submit_btn.disabled = true; return true;">
		@csrf
		<button type="submit" class="btn btn-primary" name="submit_btn">
			<i class="fa fa-sync fa-sm mr-2"></i>
			Sincronizar ahora
		</button>
	</form>
@stop