<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cotización #{{ $quote->number }}</title>
	<style>
		*{
			text-transform: uppercase !important;
		}
		body{
			font-size: 6px;
			padding: 90px 10px 100px 10px;
			margin: 0;
			font-family: DejaVu Sans, sans-serif;
			border: 1px solid #BA2025;
		}
		#header {
			position: fixed;
			top: 15px;
			left: 0px;
			right: 0px;
			padding: 15px;
		}
		#footer{
			position: fixed;
			bottom: 100px;
			left: 0px;
			right: 0px;
			padding: 15px;
		}
		#page{
			position: fixed;
			bottom: 35px;
			right: 0px;
			text-align: right;
			padding: 0 15px 0 0;
		}
		.lead{
			font-size: 12px;
		}
		.cell-gray{
			background-color: #f2f2f2
		}
		.cell-warning{
			background-color: #fffce3;
		}
		.text-uppercase{
			text-transform: uppercase;
		}
		table{
			width: 100%;
			max-width: 100%;
			border-collapse: collapse;
		}
		table tr th, table tr td{
			text-align: left;
			border: 1px solid #ccc;
			vertical-align: top;
			height: 10px;
			padding: 0px;
		}
		.text-center{
			text-align: center;
		}
		.text-left{
			text-align: left;
		}
		.text-right{
			text-align: right;
		}
		.text-danger {
			color: #BA2025;
		}
		.account_logo{
			margin: 10px 0;
		}
		.mb-3{
			margin-bottom: 15px;
		}
		.mb-0{
			margin-bottom: 0 !important;
		}
		.small{
			font-size: 85% !important;
		}
		.no_wrap{
		 !important;
		}
		.page-number:before {
		  content: "Página " counter(page);
		  font-size: 11px;
		  font-weight: bold;
		}
		.quote_product_thumb{
			width: 130px;
			height: auto;
		}
		#quoteNumber{
			position: fixed;
			top: 30px;
			right: 15px;
			text-align: right;
			padding: 0;
			font-size: 12px;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<!-- Logo y direccion -->
	<div id="header">
		<img src="{{ public_path('img/logo.png') }}" width="300px">
	</div>

	<!-- Footer -->
	<div id="footer">
		<hr>
		<p class="small">DYNACOM- FPF-02 REV 2.3 NOV 1999 AAJR.</p>
		<p class="text-danger small m-0">
			<b>CONFIDENTIAL DYNAMIC COMMUNICATIONS PROPRIETARY</b>
		</p>
	</div>

	<div id="page" class="page-number"></div>

	<div id="quoteNumber">
		MARGEN DE COTIZACIÓN<br>
		<span class="text-danger">#{{ $quote->number }}</span>
	</div>

	<!-- Margen -->

	<table cellpadding="0" cellspacing="0" style="margin: 0">
		<thead>
			<tr>
				<th width="5%" rowspan="2" class="text-center" style="vertical-align: middle;">Cantidad:</th>
				<th width="5%" rowspan="2" class="text-center" style="vertical-align: middle;">No. parte</th>
				<th width="5%" rowspan="2" class="text-center" style="vertical-align: middle;">Producto</th>
				<th width="35%" colspan="7" class="text-center cell-primary">Proveedor</th>
				<th width="15%" colspan="3" class="text-center cell-warning">Cliente</th>
				<th width="10%" colspan="2" class="text-center cell-success">Margen</th>
			</tr>
			</tr>
				<!-- Proveedor -->
				<th width="5%">Nombre</th>
				<th width="5%">Reseller price</th>
				<th width="5%">% Desc</th>
				<th width="5%">% Desc especial</th>
				<th width="5%">$ Desc especial</th>
				<th width="5%">Precio extendido</th>
				<th width="5%">Importe</th>
				<!-- Cliente -->
				<th width="5%">Precio de venta</th>
				<th width="5%">% Desc</th>
				<th width="5%">Importe</th>
				<!-- Margen -->
				<th width="5%" class="text-center">%</th>
				<th width="5%" class="text-center">$</th>
			</tr>
		</thead>
	</table>

	<table cellpadding="0" cellspacing="0" border="0" style="margin: 0">
		<tbody>
			@foreach($quote->groups as $group)
				<tr>
					<td width="10%" colspan="2" class="cell-gray"></td>
					<th width="5%" class="cell-gray font-weight-bold text-uppercase">{{ $group->description }}</th>
					<th width="45%" colspan="9" class="cell-gray text-center">
						@if($group->custom_import)
							<span class="text-danger">Importe total modificado en cotización.</span>
						@endif
					</th>
					<td width="5%" class="cell-gray text-right">
						@if($group->custom_import)
							${{ number_format($group->custom_import, 2) }}
						@endif
					</td>
					<td width="5%" class="cell-gray text-center">
						@if($group->custom_import)
							{{ number_format($group->margin_percent, 2) }}%
						@endif
					</td>
					<td width="5%" class="cell-gray text-right">
						@if($group->custom_import)
							${{ number_format($group->margin_cost, 2) }}
						@endif
					</td>
				</tr>

				<!-- Productos en grupo de cotización -->
				@foreach($group->products as $quote_product)
					<tr>
						<td width="5%" class="text-center">{{ $quote_product->qty }}</td>
						<td width="5%" style="background-color: {{ $quote_product->category['color']  }}">
							{{ optional($quote_product->product)->code }}
						</td>
						<td width="5%" style="background-color: {{ $quote_product->category['color'] }}">
							{{ $quote_product->product->description }}
						</td>
						<td width="5%" class="cell-primary">
							{{ $quote_product->provider_name }}
						</td>
						<td width="5%" class="text-right cell-primary">${{ number_format($quote_product->provider_cost, 2) }}</td>
						<td width="5%" class="text-right cell-primary">{{ number_format($quote_product->provider_discount, 2) }}%</td>
						<td width="5%" class="text-right cell-primary">{{ number_format($quote_product->provider_special_discount, 2) }}%</td>
						<td width="5%" class="text-right cell-primary">${{ number_format($quote_product->provider_special_discount_cost, 2) }}</td>
						<td width="5%" class="text-right cell-primary">
							${{ number_format($quote_product->provider_cost_with_discount, 2) }}
							@if(count($quote_product->acc_list_items) || $quote_product->shipping_cost)
								<p class="small mb-0" style="margin-top: 7px; margin-bottom: 0;"><b>Costos Adicionales</b></p>
								@if($quote_product->shipping_cost)
									<p class="small mb-0" style="margin: 0;">
										<span>Envío:</span>
										<b>${{ number_format($quote_product->shipping_cost, 2) }}</b>
									</p>
								@endif
								@foreach($quote_product->acc_list_items as $item)
									<p class="small mb-0" style="margin: 0;">
										<span>{{ $item['concept'] }}:</span>
										<b>${{ number_format($item['cost'], 2) }}</b>
									</p>
								@endforeach
							@endif
						</td>
						<td width="5%" class="text-right cell-primary">
							${{ number_format($quote_product->provider_total_import_without_acc_costs, 2) }}
							@if(count($quote_product->acc_list_items) || $quote_product->shipping_cost)
								<p class="small mb-0" style="margin-top: 7px; margin-bottom: 0;"><b>Costos Adicionales</b></p>
								@if($quote_product->shipping_cost)
									<p class="small mb-0" style="margin: 0;">
										<span>Envío:</span>
										<b>${{ number_format($quote_product->shipping_cost_total, 2) }}</b>
									</p>
								@endif
								@foreach($quote_product->acc_list_items_multiplied_by_qty as $item)
									<p class="small mb-0" style="margin: 0;">
										<span>{{ $item['concept'] }}:</span>
										<b>${{ number_format($item['cost'], 2) }}</b>
									</p>
								@endforeach
							@endif
						</td>
						<td width="5%" class="text-right cell-warning">
							@if($group->custom_import)
								<s>${{ number_format($quote_product->customer_cost, 2) }}</s>
							@else
								@if($quote_product->custom_import)
									<s>${{ number_format($quote_product->customer_cost, 2) }}</s> <br>
									<span>${{ number_format($quote_product->custom_customer_cost, 2) }}</span>
								@else
									<span>${{ number_format($quote_product->customer_cost, 2) }}</span>
								@endif
							@endif
						</td>
						<td width="5%" class="text-right cell-warning">{{ number_format($quote_product->customer_discount, 2) }}%</td>
						<td width="5%" class="text-right cell-warning">
							@if($group->custom_import)
								<s>${{ number_format($quote_product->customer_total_import, 2) }}</s>
							@else
								@if($quote_product->custom_import)
									<s>${{ number_format($quote_product->customer_total_import, 2) }}</s> <br>
									<span>${{ number_format($quote_product->custom_import, 2) }}</span>
								@else
									<span>${{ number_format($quote_product->customer_total_import, 2) }}</span>
								@endif
							@endif
						</td>
						<td width="5%" class="text-right cell-success">
							@if($group->custom_import)
								<s>{{ number_format($quote_product->margin_percent, 2) }}%</s>
							@else
								{{ number_format($quote_product->margin_percent, 2) }}%
							@endif
						</td>
						<td width="5%" class="text-right cell-success">
							@if($group->custom_import)
								<s>${{ number_format($quote_product->margin_total, 2) }}</s>
							@else
								${{ number_format($quote_product->margin_total, 2) }}
							@endif
						</td>
					</tr>
				@endforeach

				<!-- Recursos en grupo de cotización -->
				@foreach($group->resources as $quote_resource)
					<tr>
						<td width="5%" class="text-center">{{ $quote_resource->qty }}</td>
						<td width="5%"></td>
						<td width="5%">{{ $quote_resource->jobtitle_name }}</td>
						<td width="30%" colspan="6" class="cell-primary">
							({{ $quote_resource->days }}) Días
						</td>
						<td width="5%" class="text-right cell-primary">
							@if($group->custom_import)
								<s>${{ number_format($quote_resource->total_import, 2) }}</s>
							@else
								${{ number_format($quote_resource->total_import, 2) }}
							@endif
						</td>
						<td width="5%" class="text-right cell-warning">
							@if($group->custom_import)
								<s>${{ number_format($quote_resource->exchange_cost, 2) }}</s>
							@else
								${{ number_format($quote_resource->exchange_cost, 2) }}
							@endif
						</td>
						<td width="5%" class="cell-warning"></td>
						<td width="5%" class="cell-warning text-right">
							@if($group->custom_import)
								<s>{{ $quote_resource->total_import }}</s>
							@else
								@if($quote_resource->custom_import)
									<s>${{ number_format($quote_resource->total_import, 2) }}</s> <br>
									<span>${{ number_format($quote_resource->custom_import, 2) }}</span>
								@else
									${{ number_format($quote_resource->total_import, 2) }}
								@endif
							@endif
						</td>
						<td width="5%" class="cell-success text-right">
							{{ number_format($quote_resource->margin_percent, 2) }}%
						</td>
						<td width="5%" class="cell-success text-right">
							${{ number_format($quote_resource->margin_total, 2) }}
						</td>
					</tr>
				@endforeach
			@endforeach
			<tr>
				<th width="45%" colspan="9" class="cell-gray"></th>
				<th width="5%" class="cell-primary text-right">${{ number_format($quote->provider_total_import, 2) }}</th>
				<td width="10%" colspan="2" class="cell-gray"></td>
				<th width="5%" class="cell-warning text-right">${{ number_format($quote->subtotal, 2) }}</th>
				<th width="5%" class="text-center cell-success text-right">{{ number_format($quote->margin_percent, 2) }}%</th>
				<th width="5%" class="text-center cell-success text-right">${{ number_format($quote->margin_cost, 2) }}</th>
			</tr>
		</tbody>
	</table>

</body>
</html>