<table cellpadding="3" cellspacing="0" style="margin-top: 10px">
    <tbody>
        <tr>
            <th style="border: 1px solid #ccc;" class="cell-gray text-center" width="8%">QTY</th>
            <th style="border: 1px solid #ccc;" class="cell-gray text-left" width="55%">DESCRIPTION</th>
            <th style="border: 1px solid #ccc;" class="cell-gray text-right">UNIT</th>
            @if($quote->display_customer_items_discount)
            <th style="border: 1px solid #ccc;" class="cell-gray text-right" nowrap>% DISC</th>
            <th style="border: 1px solid #ccc;" class="cell-gray text-right" nowrap>$ DISC</th>
            @endif
            <th style="border: 1px solid #ccc;" class="cell-gray text-right">SUBTOTAL</th>
        </tr>
    </tbody>
    <tbody>
        @if(!$quote->hide_description)
        <tr>
            <td style="border: 1px solid #ccc;" class="text-center">
                @if($quote->only_show_description)
                    1
                @endif
            </td>
            <th style="border: 1px solid #ccc;" class="text-left text-uppercase">
                {{ $quote->description }}
            </th>	
            <td style="border: 1px solid #ccc;" class="text-right">
                @if($quote->only_show_description)
                    ${{ number_format($quote->subtotal_on_quote, 2) }}
                @endif
            </td>
            @if($quote->display_customer_items_discount)
            <td style="border: 1px solid #ccc;"></td>
            <td style="border: 1px solid #ccc;"></td>
            @endif
            <td style="border: 1px solid #ccc;" class="text-right">
                @if($quote->only_show_description)
                    ${{ number_format($quote->subtotal_on_quote, 2) }}
                @endif
            </td>
        </tr>
        @endif
    </tbody>
    @foreach($quote->groups as $group)
        @if(!$quote->only_show_description)
            @if(!$group->hide_in_quote)
                <tbody>
                    <tr>
                        <td style="border: 1px solid #ccc;" class="text-center">
                            @if($group->hide_items)
                                1
                            @endif
                        </td>
                        <th style="border: 1px solid #ccc;" class="text-left text-uppercase">
                            @if(!$group->hide_name)
                                {{ $group->description }}
                            @endif
                        </th>
                        <td style="border: 1px solid #ccc;" class="text-right">
                            @if($group->hide_items)
                                ${{ number_format($group->sub_total_on_quote, 2) }}
                            @endif
                        </td>
                        @if($quote->display_customer_items_discount)
                        <td style="border: 1px solid #ccc;"></td>
                        <td style="border: 1px solid #ccc;"></td>
                        @endif
                        <td style="border: 1px solid #ccc;" class="text-right">
                            @if($group->hide_items)
                                ${{ number_format($group->sub_total_on_quote, 2) }}
                            @endif
                        </td>
                    </tr>
                    @if(!$group->hide_items)
                        @foreach($group->products as $quote_product)
                            @if(!$quote_product->hide_in_quote)
                            <tr>
                                <td style="border: 1px solid #ccc;" class="text-center">{{ $quote_product->qty }}</td>
                                <td style="border: 1px solid #ccc;">
                                    @if($quote_product->show_code)
                                        @if($quote_product->product->code)
                                            <b>#{{ $quote_product->product->code }} - </b>
                                        @endif
                                    @endif
                                    {{ $quote_product->product_description }} 
                                    @if($quote_product->detalles)
                                        <div style="text-transform: none !important" style="width: 300px;">
                                            {!! str_replace('&nbsp;', ' ', $quote_product->detalles) !!}
                                        </div>
                                    @endif
                                    <br>
                                    @if($quote_product->show_specifications_on_quote)
                                    <small class="d-block mt-3">
                                        {!! $quote_product->product->specifications !!}
                                    </small> <br>
                                    @endif
                                    @if($quote_product->show_image_on_quote)
                                    <img src="{{ public_path('uploads/products/' . $quote_product->product->thumb) }}" 
                                        class="quote_product_thumb ml-auto" alt="">
                                    @endif
                                </td>	
                                <td style="border: 1px solid #ccc;" class="text-right">${{ number_format($quote_product->customer_cost_by_unit, 2) }}</td>
                                @if($quote->display_customer_items_discount)
                                <td style="border: 1px solid #ccc;" class="text-right">{{ $quote_product->customer_discount }}%</td>
                                <td style="border: 1px solid #ccc;" class="text-right">${{ number_format($quote_product->customer_cost_by_unit_with_discount, 2) }}</td>
                                @endif
                                <td style="border: 1px solid #ccc;" class="text-right">${{ number_format($quote_product->customer_total_import_on_quote, 2) }}</td>
                            </tr>
                            @endif
                        @endforeach
                        @foreach($group->resources as $quote_resource)
                            <tr>
                                <td style="border: 1px solid #ccc;" class="text-center">{{ $quote_resource->qty }}</td>
                                <td style="border: 1px solid #ccc;">{{ $quote_resource->jobtitle_name }}</td>
                                <td style="border: 1px solid #ccc;" class="text-right">${{ number_format($quote_resource->total_import_on_quote, 2) }}</td>
                                <td style="border: 1px solid #ccc;"></td>
                                <td style="border: 1px solid #ccc;"></td>
                                <td style="border: 1px solid #ccc;" class="text-right">${{ number_format($quote_resource->total_import_on_quote, 2) }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            @endif
        @endif
    @endforeach
    <tbody>
        @if($quote->display_customer_items_discount)
            <tr>
                <th style="border: 1px solid #ccc;" colspan="3" class="text-uppercase text-danger cell-warning text-center">
                    @if(!$quote->display_iva)
                        NOTE: TAX NOT INCLUDED
                    @endif
                </th>
                <th style="border: 1px solid #ccc;" class="text-right" 
                    colspan="2">
                    SUBTOTAL (<span>{{ $quote->currency_short_name }}</span>):
                </th>
                <th style="border: 1px solid #ccc;" class="text-right">
                    ${{ number_format($quote->subtotal_on_quote, 2) }}
                </th>
            </tr>
            @if($quote->general_discount)
                <tr>
                    <th style="border: 1px solid #ccc;" colspan="3" class="cell-warning"></th>
                    <th style="border: 1px solid #ccc;" class="text-right"
                        colspan="2" nowrap>
                        <span>CUSTOMER SPECIAL DISCOUNT (<span class="text-danger">{{ $quote->general_discount }}%</span>):</span>
                    </th>
                    <th style="border: 1px solid #ccc;" class="text-right text-danger">
                        - ${{ number_format($quote->general_discount_ammount, 2) }}
                    </th>
                </tr>
            @endif
            @if($quote->display_iva)
                <tr>
                    <th style="border: 1px solid #ccc;" colspan="3" class="cell-warning"></th>
                    <th style="border: 1px solid #ccc;" class="text-right"
                        colspan="2" nowrap>
                        TAX ({{ $quote->iva }}%):
                    </th>
                    <th style="border: 1px solid #ccc;" class="text-right">
                        ${{ number_format($quote->iva_ammount, 2) }}
                    </th>
                </tr>
            @endif
            <tr>
                <th style="border: 1px solid #ccc;" colspan="3" class="cell-warning"></th>
                <th style="border: 1px solid #ccc;" class="text-right" colspan="2">
                    TOTAL  (<span>{{ $quote->currency_short_name }}</span>):
                </th>
                <th style="border: 1px solid #ccc;" class="text-right">
                    ${{ number_format($quote->total_on_quote, 2) }}
                </th>
            </tr>
        @else
            <tr>
                <th style="border: 1px solid #ccc;" colspan="2" class="text-uppercase text-danger cell-warning text-center">
                    @if(!$quote->display_iva)
                        NOTE: TAX NOT INCLUDED
                    @endif
                </th>
                <th style="border: 1px solid #ccc;" class="text-right" 
                    colspan="1">
                    SUBTOTAL (<span>{{ $quote->currency_short_name }}</span>):
                </th>
                <th style="border: 1px solid #ccc;" class="text-right">
                    ${{ number_format($quote->subtotal_on_quote, 2) }}
                </th>
            </tr>
            @if($quote->general_discount)
                <tr>
                    <th style="border: 1px solid #ccc;" colspan="2" class="cell-warning"></th>
                    <th style="border: 1px solid #ccc;" class="text-right"
                        colspan="1" nowrap>
                        <span>CUSTOMER SPECIAL DISCOUNT (<span class="text-danger">{{ $quote->general_discount }}%</span>):</span>
                    </th>
                    <th style="border: 1px solid #ccc;" class="text-right text-danger">
                        - ${{ number_format($quote->general_discount_ammount, 2) }}
                    </th>
                </tr>
            @endif
            @if($quote->display_iva)
                <tr>
                    <th style="border: 1px solid #ccc;" colspan="2" class="cell-warning"></th>
                    <th style="border: 1px solid #ccc;" class="text-right"
                        colspan="1" nowrap>
                        TAX ({{ $quote->iva }}%):
                    </th>
                    <th style="border: 1px solid #ccc;" class="text-right">
                        ${{ number_format($quote->iva_ammount, 2) }}
                    </th>
                </tr>
            @endif
            <tr>
                <th style="border: 1px solid #ccc;" colspan="2" class="cell-warning"></th>
                <th style="border: 1px solid #ccc;" class="text-right" colspan="1">
                    TOTAL  (<span>{{ $quote->currency_short_name }}</span>):
                </th>
                <th style="border: 1px solid #ccc;" class="text-right">
                    ${{ number_format($quote->total_on_quote, 2) }}
                </th>
            </tr>
        @endif
    </tbody>
</table>