<table cellpadding="0" cellspacing="0" style="margin: 0;">
    <tr>
        <td width="50%" style="border: 0 !important; padding-right: 9px">
            <table cellpadding="3" cellspacing="0">
                <tr>
                    <th class="cell-gray text-uppercase" 
                        width="50%" 
                        style="border: 1px solid #ccc">
                        Customer Information:
                    </th>
                </tr>
                <tr>
                    <td style="height: 55px; border: 1px solid #ccc">
                        @if($quote->account_logo)
                            <img src="{{ $quote->account_logo }}" style="width: auto; max-width: 100px; height: auto; max-height: 100px;" class="account_logo" alt="">
                        @endif
                        <p style="text-transform: uppercase; margin: 10px 0 10px 0; font-weight: bold">{{ $quote->account_name }}</p>
                        <p class="m-0" style="margin: 0">Contact: <span>{{ $quote->contact_name }}</span></p>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table cellpadding="3" cellspacing="0">
                <tr>
                    <th style="border: 1px solid #ccc;" class="cell-gray text-uppercase" width="25%">Date:</th>
                    <th style="border: 1px solid #ccc;" class="cell-gray text-uppercase" width="25%">Quote #</th>
                </tr>
                <tr>
                    <td style="border: 1px solid #ccc;">{{ $quote->formated_date }}</td>
                    <th style="border: 1px solid #ccc;" class="text-danger"><b>{{ '#' . $quote->number }}</b></th>
                </tr>
                <tr>
                    <th style="border: 1px solid #ccc;" class="cell-gray text-uppercase">Terms:</th>
                    <th style="border: 1px solid #ccc;" class="cell-gray text-uppercase">Delivery:</th>
                </tr>
                <tr>
                    <td style="border: 1px solid #ccc;">{{ $quote->terms }}</td>
                    <td style="border: 1px solid #ccc;">{{ $quote->delivery_time }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>