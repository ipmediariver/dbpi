<div class="nobreak" style="margin-top: 10px;">
    <table cellpadding="3" cellspacing="0">
        <tbody>
            <tr>
                <th style="border: 1px solid #ccc;" width="12%" class="cell-gray text-right">TERMS:</th>
                <td style="border: 1px solid #ccc;" class="text-left text-uppercase">{{ $quote->terms }}</td>
            </tr>
            <tr>
                <th style="border: 1px solid #ccc;" width="12%" class="cell-gray text-right">DELIVERY:</th>
                <td style="border: 1px solid #ccc;" class="text-left text-uppercase">{{ $quote->delivery_time }}</td>
            </tr>
            <tr>
                <th style="border: 1px solid #ccc;" width="12%" class="cell-gray text-right">F.O.B.:</th>
                <td style="border: 1px solid #ccc;" class="text-left text-uppercase">{{ $quote->fob }}</td>
            </tr>
            <tr>
                <th style="border: 1px solid #ccc;" width="12%" class="cell-gray text-right">PRICES:</th>
                <td style="border: 1px solid #ccc;" class="text-left text-uppercase">
                    ALL PRICES ARE IN
                    <span class="font-weight-bold">
                        <span>{{ $quote->currency_short_name }}</span>
                    </span>
                    AND VALID FOR
                    <span class="font-weight-bold">
                        <span>{{ $quote->vigency }}</span> DAYS
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
</div>