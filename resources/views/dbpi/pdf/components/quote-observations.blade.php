<!-- Observaciones -->
<div class="nobreak" style="margin-top: 10px;">
    <table cellpadding="3" cellspacing="0">
        <thead>
            <tr class="cell-gray">
                <th style="border: 1px solid #ccc;" class="text-uppercase">Observations</th>
            </tr>
        </thead>
        <tbody>
            <tr class="text-uppercase">
                <td style="border: 1px solid #ccc;">{!! str_replace('&nbsp;', ' ', $quote->observations) !!}</td>
            </tr>
        </tbody>
    </table>
</div>