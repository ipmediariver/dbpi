@extends('layouts.quote')
@section('content')
	<!-- Encabezado de cotización -->
	@component('dbpi.pdf.components.quote-header', ['quote' => $quote])
	@endcomponent
	<!-- Termina encabezado de cotización -->
	<!-- Elementos de cotización -->
	@component('dbpi.pdf.components.quote-items', ['quote' => $quote])
	@endcomponent
	<!-- Terminan elementos de cotizacion -->
	<!-- Observaciones -->
	@component('dbpi.pdf.components.quote-observations', ['quote' => $quote])
	@endcomponent
	<!-- Terminan las observaciones -->
	<!-- Terminos -->
	@component('dbpi.pdf.components.quote-terms', ['quote' => $quote])
	@endcomponent
	<!-- Terminan los terminos -->
	<script type="text/php">
	    if ( isset($pdf) ) {
	        $x = 498;
            $y = 80;
            $text = "Página {PAGE_NUM} de {PAGE_COUNT}";
            $font = null;
            $size = 10;
            $color = array(255,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
	    }
	</script>
@stop
