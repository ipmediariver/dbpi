<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>FUP #{{ $fup->number }}</title>
	<style>
		*{
			text-transform: uppercase !important;
		}
		body{
			font-size: 8px;
			padding: 90px 15px 100px 15px;
			margin: 0;
			font-family: DejaVu Sans, sans-serif;
			border: 1px solid #BA2025;
		}
		#header {
			position: fixed;
			top: 15px;
			left: 0px;
			right: 0px;
			padding: 15px;
		}
		#footer{
			position: fixed;
			bottom: 100px;
			left: 0px;
			right: 0px;
			padding: 15px;
		}
		#page{
			position: fixed;
			bottom: 35px;
			right: 0px;
			text-align: right;
			padding: 0 15px 0 0;
		}
		#fupNumber{
			position: fixed;
			top: 33px;
			right: 15px;
			text-align: right;
			padding: 0;
			font-size: 12px;
			font-weight: bold;
		}
		.lead{
			font-size: 12px;
		}
		.cell-gray{
			background-color: #f2f2f2
		}
		.cell-warning{
			background-color: #fffce3;
		}
		.text-uppercase{
			text-transform: uppercase;
		}
		table{
			width: 100%;
		}
		table tr th, table tr td{
			text-align: left;
			border: 1px solid #ccc;
			vertical-align: top;
			height: 10px;
		}
		table, table tr, table tr th, table tr td{
			border-collapse: collapse !important;
		}
		.text-center{
			text-align: center;
		}
		.text-left{
			text-align: left;
		}
		.text-right{
			text-align: right;
		}
		.text-danger {
			color: #BA2025;
		}
		.account_logo{
			margin: 10px 0;
		}
		.mb-3{
			margin-bottom: 5px;
		}
		.page-number:before {
		  content: "Página " counter(page);
		  font-size: 11px;
		  font-weight: bold;
		}
		.quote_product_thumb{
			width: 130px;
			height: auto;
		}
	</style>
</head>
<body>
	<!-- Logo y direccion -->
	<div id="header">
		<img src="{{ public_path('img/logo.png') }}" width="300">
	</div>

	<!-- Footer -->
	<div id="footer">
		<hr>
		<p class="small">DYNACOM- FPF-02 REV 2.3 NOV 1999 AAJR.</p>
		<p class="text-danger small m-0">
			<b>CONFIDENTIAL DYNAMIC COMMUNICATIONS PROPRIETARY</b>
		</p>
	</div>

	<div id="page" class="page-number"></div>

	<div id="fupNumber">
		FORMA UNICA DE PEDIDO<br>
		<span class="text-danger">#{{ $fup->number }}</span>
	</div>
	

	
	<table cellpadding="2" cellspacing="0" style="border-collapse: collapse;" class="mb-3">
		<tr>
			<th class="cell-gray text-uppercase">NO. DE COTIZACIóN:</th>
			<th class="cell-gray text-uppercase">NO. DE ORDEN DEL CLIENTE:</th>
			<th class="cell-gray text-uppercase">FECHA:</th>
		</tr>
		<tr>
			<th class="text-danger">#{{ $quote->number }}</th>
			<td>{{ $fup->po_number }}</td>
			<td>{{ $fup->date->format('d M, Y') }}</td>
		</tr>
	</table>
	
	<table cellpadding="2" cellspacing="0" style="border-collapse: collapse;" class="mb-3">
		<tr>
			<th class="cell-gray text-uppercase">NOMBRE COMERCIAL:</th>
		</tr>
		<tr>
			<td>{{ $fup->comercial_name }}</td>
		</tr>
		<tr>
			<th class="cell-gray text-uppercase">DESCRIPCIóN DEL PROYECTO:</th>
		</tr>
		<tr>
			<td>{{ $fup->fup_description }}</td>
		</tr>
	</table>

	<table cellpadding="2" cellspacing="0" style="border-collapse: collapse;" class="mb-3">
		<tr>
			<th class="cell-gray text-uppercase">CONTACTO RESPONSABLE TéCNICO:</th>
			<th class="cell-gray text-uppercase">E-MAIL:</th>
			<th class="cell-gray text-uppercase">TELéFONO:</th>
		</tr>
		<tr>
			<td>{{ $fup->contact_name }}</td>
			<td>{{ $fup->contact_email }}</td>
			<td>{{ $fup->contact_phone }}</td>
		</tr>
	</table>

	<table cellpadding="0" cellspacing="0" border="0" style="border: 0" class="mb-3">
		<tr>
			<td width="50%" style="border: 0; padding-right: 2px">
				<table cellspacing="0" cellpadding="2">
					<tr>
						<th class="cell-warning text-center">EMBARQUE</th>
					</tr>
					<tr>
						<th class="cell-gray">RAZON SOCIAL:</th>
					</tr>
					<tr>
						<td>{{ $fup->business_name_shipment }}</td>
					</tr>
					<tr>
						<th class="cell-gray">DOMICILIO (CALLE Y NUMERO, COLONIA / PARQUE INDUSTRIAL):</th>
					</tr>
					<tr>
						<td>{{ $fup->shipment_address }}</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="2">
					<thead>
						<tr>
							<th nowrap class="cell-gray">CIUDAD Y ESTADO:</th>
							<th nowrap class="cell-gray" width="30%">C.P:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ $fup->shipment_city_state }}</td>
							<td>{{ $fup->shipment_zip_code }}</td>
						</tr>
					</tbody>
				</table>
				<table cellspacing="0" cellpadding="2">
					<thead>
						<tr>
							<th class="cell-gray">ENTREGAS PARCIALES:</th>
							<th class="cell-gray">DIAS DE ENTREGA:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								@if($fup->partial_deliveries)
									(SI)
								@else
									(NO)
								@endif
							</td>
							<td>
								<div class="d-flex align-items-center">
									<label for="entregaLunes" class="m-0 mr-2">
										<b>L</b>
										@if($fup->delivery_monday)
											(SI)
										@else
											(NO)
										@endif
									</label>
									<label for="entregaMartes" class="m-0 mr-2">
										<b>M</b>
										@if($fup->delivery_tuesday)
											(SI)
										@else
											(NO)
										@endif
									</label>
									<label for="entregaMiercoles" class="m-0 mr-2">
										<b>M</b>
										@if($fup->delivery_wednesday)
											(SI)
										@else
											(NO)
										@endif
									</label>
									<label for="entregaJueves" class="m-0 mr-2">
										<b>J</b>
										@if($fup->delivery_thursday)
											(SI)
										@else
											(NO)
										@endif
									</label>
									<label for="entregaViernes" class="m-0">
										<b>V</b>
										@if($fup->delivery_friday)
											(SI)
										@else
											(NO)
										@endif
									</label>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<table cellspacing="0" cellpadding="2">
					<tr>
						<th class="cell-gray">HORARIO DE ENTREGAS:</th>
					</tr>
					<tr>
						<td>{{ $fup->delivery_schedules }}</td>
					</tr>
					<tr>
						<th class="cell-gray">REFERENCIAS:</th>
					</tr>
					<tr>
						<td>{{ $fup->references }}</td>
					</tr>
				</table>
			</td>
			<td width="50%" style="border: 0; padding-left: 2px">
				<table cellspacing="0" cellpadding="2">
					<tr>
						<th class="cell-warning text-center">FACTURACION</th>
					</tr>
					<tr>
						<th class="cell-gray">RAZON SOCIAL:</th>
					</tr>
					<tr>
						<td>{{ $fup->business_name_billing }}</td>
					</tr>
					<tr>
						<th class="cell-gray">DOMICILIO (CALLE Y NUMERO, COLONIA / PARQUE INDUSTRIAL):</th>
					</tr>
					<tr>
						<td>{{ $fup->billing_address }}</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="2">
					<thead>
						<tr>
							<th nowrap class="cell-gray">CIUDAD Y ESTADO:</th>
							<th nowrap class="cell-gray">R.F.C.:</th>
							<th nowrap class="cell-gray" width="30%">C.P:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ $fup->billing_city_state }}</td>
							<td>{{ $fup->billing_rfc }}</td>
							<td>{{ $fup->billing_zip_code }}</td>
						</tr>
					</tbody>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" style="border: 0">
					
					<tr>
						<td width="50%" style="border: 0">
							<table cellspacing="0" cellpadding="2">
								<tr>
									<th class="cell-gray">MUB:</th>
								</tr>
								<tr>
									<td>{{ number_format($fup->mub, 2) }}%</td>
								</tr>
								<tr>
									<th class="cell-gray">MONEDA:</th>
								</tr>
								<tr>
									<td>{{ $fup->quote->currency->short_name }}</td>
								</tr>
								<tr>
									<th class="cell-gray">TIPO DE CAMBIO:</th>
								</tr>
								<tr>
									<td>{{ $fup->quote->exchange_rate }}</td>
								</tr>
							</table>
						</td>
						<td width="50%" style="border: 0">
							<table cellspacing="0" cellpadding="2">
								<tr>
									<th colspan="3" class="cell-primary text-center">CONDICIONES DE PAGO</th>
								</tr>
								<tr>
									<th width="33%" class="cell-gray">CONCEPTO</th>
									<th width="33%" class="cell-gray text-center">%</th>
									<th width="33%" class="cell-gray text-center">DIAS</th>
								</tr>
								<tr>
									<th class="cell-gray">ANTICIPO</th>
									<th>{{ $fup->payment_conditions_advance_percent }}</th>
									<th>{{ $fup->payment_conditions_advance_days }}</th>
								</tr>
								<tr>
									<th class="cell-gray">ENTREGA</th>
									<th>{{ $fup->payment_conditions_delivery_percent }}</th>
									<th>{{ $fup->payment_conditions_delivery_days }}</th>
								</tr>
								<tr>
									<th class="cell-gray">INSTALACION</th>
									<th>{{ $fup->payment_conditions_instalation_percent }}</th>
									<th>{{ $fup->payment_conditions_instalation_days }}</th>
								</tr>
								<tr>
									<th colspan="3" class="cell-gray"></th>
								</tr>
							</table>
						</td>
					</tr>

				</table>
				</div>
			</td>
		</tr>
	</table>

	<table cellspacing="0" cellpadding="2" class="mb-3">
		<tr>
			<th colspan="4" class="cell-warning text-center">INFORMACION DEL PEDIDO</th>
		</tr>
		<tr>
			<th colspan="4" class="cell-gray text-center">FECHAS DE COMPROMISO</th>
		</tr>
		<tr>
			<th class="cell-gray">TIEMPO DE ENTREGA:</th>
			<th class="cell-gray">TIEMPO COMPROMISO ENTREGA:</th>
			<th class="cell-gray">FECHA DE INICIO DE INSTALACION:</th>
			<th class="cell-gray">FECHA DE PROYECTO:</th>
		</tr>
		<tr>
			<td>{{ $fup->delivery_time }}</td>
			<td>{{ $fup->delivery_commitment }}</td>
			<td>{{ $fup->instalation_date }}</td>
			<td>{{ $fup->project_date }}</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="2" class="mb-3">
		<tr>
			<th colspan="2" class="cell-primary text-center">CLIENTE FINAL</th>
		</tr>
		<tr>
			<th class="cell-gray">NOMBRE COMERCIAL Y/O RAZON SOCIAL</th>
			<th class="cell-gray">CALLE Y NUMERO</th>
		</tr>
		<tr>
			<td>{{ $fup->final_client_business_name }}</td>
			<td>{{ $fup->final_client_address }}</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="2" class="mb-3">
		<tr>
			<th class="cell-gray">EJECUTIVO DE CUENTA</th>
			<th class="cell-gray">CIUDAD Y ESTADO</th>
			<th class="cell-gray">C.P.</th>
		</tr>
		<tr>
			<td>{{ $fup->final_client_account_executive }}</td>
			<td>{{ $fup->final_client_city_state }}</td>
			<td>{{ $fup->final_client_zip_code }}</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="2">
		<tr>
			<th class="cell-warning">OBSERVACIONES Y RECOMENDACIONES</th>
		</tr>
		<tr>
			<td class="cell-gray">
				@if($fup->observations()->count())
				<ul style="margin: 0; padding: 0; list-style: none !important;">
					@foreach($fup->observations as $observation)
					<li style="margin-bottom: 6px">
						<span>{{ $observation->description }}</span>
					</li>
					@endforeach
				</ul>
				@else
				<p class="m-0">No se han agregado observaciones</p>
				@endif
			</td>
		</tr>
	</table>

</body>
</html>