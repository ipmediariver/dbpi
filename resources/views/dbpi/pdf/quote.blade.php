<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cotización #{{ $quote->number }}</title>
	<style>
		*{
			text-transform: uppercase;
		}
		.nobreak {
		  	page-break-inside: avoid;
		}
		body{
			font-size: 9px;
			padding: 150px 15px 100px 15px;
			margin: 0;
			font-family: DejaVu Sans, sans-serif;
			border: 1px solid #BA2025;
		}
		#header {
			position: fixed;
			top: 15px;
			left: 0px;
			right: 0px;
			padding: 15px;
		}
		#footer{
			position: fixed;
			bottom: 100px;
			left: 0px;
			right: 0px;
			padding: 15px;
		}
		#page{
			position: fixed;
			bottom: 35px;
			right: 0px;
			text-align: right;
			padding: 0 15px 0 0;
		}
		.lead{
			font-size: 12px;
		}
		.cell-gray{
			background-color: #f2f2f2
		}
		.cell-warning{
			background-color: #fffce3;
		}
		.text-uppercase{
			text-transform: uppercase;
		}
		table{
			width: 100%;
			border-collapse: collapse;
		}
		table tr th, table tr td{
			text-align: left;
			border: 1px solid #ccc !important;
			vertical-align: top;
			height: 10px;
		}
		.text-center{
			text-align: center;
		}
		.text-left{
			text-align: left;
		}
		.text-right{
			text-align: right;
		}
		.text-danger {
			color: #BA2025;
		}
		.account_logo{
			margin: 10px 0;
		}
		.mb-3{
			margin-bottom: 15px;
		}
		.page-number:before {
		  content: "Página " counter(page);
		  font-size: 11px;
		  font-weight: bold;
		}
		.quote_product_thumb{
			width: 130px;
			height: auto;
		}
	</style>
</head>
<body>
	<table cellspacing="0" cellpadding="0" border="0" style="border: 0 !important;">
		<tr style="border: 0 !important">
			<td style="border: 0 !important;">
				<!-- Logo y direccion -->
				<div id="header">
					<img src="{{ public_path('img/logo.png') }}" width="300">
					<p class="lead"><b>CALLE LAZARO CARDENAS 341-C <br> COL. JARDINES DEL LAGO <br> MEXICALI, BC.</b></p>
				</div>
				<!-- Footer -->
				<div id="footer">
					<hr>
					<p class="small">DYNACOM- FPF-02 REV 2.3 NOV 1999 AAJR.</p>
					<p class="text-danger small m-0">
						<b>CONFIDENTIAL DYNAMIC COMMUNICATIONS PROPRIETARY</b>
					</p>
				</div>
				<div id="page" class="page-number"></div>
				<!-- Encabezado -->
				<table cellpadding="0" cellspacing="0" border="0" style="margin: 0">
					<tr>
						<td width="50%"  style="border: 0">
							<table cellpadding="3" cellspacing="0" style="border-collapse: collapse;" class="mb-3">
								<tr>
									<th class="cell-gray text-uppercase" width="50%">Customer Information:</th>
								</tr>
								<tr>
									<td style="height: 55px;">
										@if($quote->account_logo)
											<img src="{{ $quote->account_logo }}" style="width: auto; max-width: 100px; height: auto; max-height: 100px;" class="account_logo" alt="">
										@endif
										<p style="text-transform: uppercase; margin: 10px 0 10px 0; font-weight: bold">{{ $quote->account_name }}</p>
										<p class="m-0" style="margin: 0">Contact: <span>{{ $quote->contact_name }}</span></p>
									</td>
								</tr>
							</table>
						</td>
						<td width="50%"  style="border: 0">
							<table cellpadding="3" cellspacing="0" style="border-collapse: collapse;" class="mb-3">
								<tr>
									<th class="cell-gray text-uppercase" width="25%">Date:</th>
									<th class="cell-gray text-uppercase" width="25%">Quote #</th>
								</tr>
								<tr>
									<td>{{ $quote->formated_date }}</td>
									<th class="text-danger"><b>{{ '#' . $quote->number }}</b></th>
								</tr>
								<tr>
									<th class="cell-gray text-uppercase">Terms:</th>
									<th class="cell-gray text-uppercase">Delivery:</th>
								</tr>
								<tr>
									<td>{{ $quote->terms }}</td>
									<td>{{ $quote->delivery_time }}</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- Lista de grupos, contratos, productos y recursos -->
				<table cellpadding="3" cellspacing="0" class="mb-3 nobreak">
					<tbody>
						<tr>
							<th class="cell-gray text-center" width="8%">QTY</th>
							<th class="cell-gray text-left" width="55%">DESCRIPTION</th>
							<th class="cell-gray text-right">UNIT</th>
							@if($quote->display_customer_items_discount)
							<th class="cell-gray text-right">% DISC</th>
							<th class="cell-gray text-right">$ DISC</th>
							@endif
							<th class="cell-gray text-right">SUBTOTAL</th>
						</tr>
					</tbody>
					<tbody>
						@if(!$quote->hide_description)
						<tr>
							<td class="text-center">
								@if($quote->only_show_description)
									1
								@endif
							</td>
							<th class="text-left text-uppercase">
								{{ $quote->description }}
							</th>	
							<td class="text-right">
								@if($quote->only_show_description)
									${{ number_format($quote->subtotal_on_quote, 2) }}
								@endif
							</td>
							@if($quote->display_customer_items_discount)
							<td></td>
							<td></td>
							@endif
							<td class="text-right">
								@if($quote->only_show_description)
									${{ number_format($quote->subtotal_on_quote, 2) }}
								@endif
							</td>
						</tr>
						@endif
					</tbody>
					@foreach($quote->groups as $group)
						@if(!$quote->only_show_description)
							@if(!$group->hide_in_quote)
								<tbody>
									<tr>
										<td class="text-center">
											@if($group->hide_items)
												1
											@endif
										</td>
										<th class="text-left text-uppercase">
											@if(!$group->hide_name)
												{{ $group->description }}
											@endif
										</th>
										<td class="text-right">
											@if($group->hide_items)
												${{ number_format($group->sub_total_on_quote, 2) }}
											@endif
										</td>
										@if($quote->display_customer_items_discount)
										<td></td>
										<td></td>
										@endif
										<td class="text-right">
											@if($group->hide_items)
												${{ number_format($group->sub_total_on_quote, 2) }}
											@endif
										</td>
									</tr>
									@if(!$group->hide_items)
										@foreach($group->products as $quote_product)
											@if(!$quote_product->hide_in_quote)
											<tr>
												<td class="text-center">{{ $quote_product->qty }}</td>
												<td>
													@if($quote_product->show_code)
														@if($quote_product->product->code)
															<b>#{{ $quote_product->product->code }} - </b>
														@endif
													@endif
													{{ $quote_product->product_description }} 
													@if($quote_product->detalles)
														<div style="text-transform: none !important" style="width: 300px; overflow-wrap: anywhere; word-wrap: break-word">
															{!! str_replace('&nbsp;', ' ', $quote_product->detalles) !!}
														</div>
													@endif
													<br>
													@if($quote_product->show_specifications_on_quote)
													<small class="d-block mt-3">
														{!! $quote_product->product->specifications !!}
													</small> <br>
													@endif
													@if($quote_product->show_image_on_quote)
													<img src="{{ public_path('uploads/products/' . $quote_product->product->thumb) }}" 
														class="quote_product_thumb ml-auto" alt="">
													@endif
												</td>	
												<td class="text-right">${{ number_format($quote_product->customer_cost_by_unit, 2) }}</td>
												@if($quote->display_customer_items_discount)
												<td class="text-right">{{ $quote_product->customer_discount }}%</td>
												<td class="text-right">${{ number_format($quote_product->customer_cost_by_unit_with_discount, 2) }}</td>
												@endif
												<td class="text-right">${{ number_format($quote_product->customer_total_import_on_quote, 2) }}</td>
											</tr>
											@endif
										@endforeach
										@foreach($group->resources as $quote_resource)
											<tr>
												<td class="text-center">{{ $quote_resource->qty }}</td>
												<td>{{ $quote_resource->jobtitle_name }}</td>
												<td class="text-right">${{ number_format($quote_resource->total_import_on_quote, 2) }}</td>
												<td></td>
												<td></td>
												<td class="text-right">${{ number_format($quote_resource->total_import_on_quote, 2) }}</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							@endif
						@endif
					@endforeach
					<tbody>
						@if($quote->display_customer_items_discount)
							<tr>
								<th colspan="3" class="text-uppercase text-danger cell-warning text-center">
									@if(!$quote->display_iva)
										NOTE: TAX NOT INCLUDED
									@endif
								</th>
								<th class="text-right" 
									colspan="2">
									SUBTOTAL (<span>{{ $quote->currency_short_name }}</span>):
								</th>
								<th class="text-right">
									${{ number_format($quote->subtotal_on_quote, 2) }}
								</th>
							</tr>
							@if($quote->general_discount)
								<tr>
									<th colspan="3" class="cell-warning"></th>
									<th class="text-right"
										colspan="2" nowrap>
										<span>CUSTOMER SPECIAL DISCOUNT (<span class="text-danger">{{ $quote->general_discount }}%</span>):</span>
									</th>
									<th class="text-right text-danger">
										- ${{ number_format($quote->general_discount_ammount, 2) }}
									</th>
								</tr>
							@endif
							@if($quote->display_iva)
								<tr>
									<th colspan="3" class="cell-warning"></th>
									<th class="text-right"
										colspan="2" nowrap>
										TAX ({{ $quote->iva }}%):
									</th>
									<th class="text-right">
										${{ number_format($quote->iva_ammount, 2) }}
									</th>
								</tr>
							@endif
							<tr>
								<th colspan="3" class="cell-warning"></th>
								<th class="text-right" colspan="2">
									TOTAL  (<span>{{ $quote->currency_short_name }}</span>):
								</th>
								<th class="text-right">
									${{ number_format($quote->total_on_quote, 2) }}
								</th>
							</tr>
						@else
							<tr>
								<th colspan="2" class="text-uppercase text-danger cell-warning text-center">
									@if(!$quote->display_iva)
										NOTE: TAX NOT INCLUDED
									@endif
								</th>
								<th class="text-right" 
									colspan="1">
									SUBTOTAL (<span>{{ $quote->currency_short_name }}</span>):
								</th>
								<th class="text-right">
									${{ number_format($quote->subtotal_on_quote, 2) }}
								</th>
							</tr>
							@if($quote->general_discount)
								<tr>
									<th colspan="2" class="cell-warning"></th>
									<th class="text-right"
										colspan="1" nowrap>
										<span>CUSTOMER SPECIAL DISCOUNT (<span class="text-danger">{{ $quote->general_discount }}%</span>):</span>
									</th>
									<th class="text-right text-danger">
										- ${{ number_format($quote->general_discount_ammount, 2) }}
									</th>
								</tr>
							@endif
							@if($quote->display_iva)
								<tr>
									<th colspan="2" class="cell-warning"></th>
									<th class="text-right"
										colspan="1" nowrap>
										TAX ({{ $quote->iva }}%):
									</th>
									<th class="text-right">
										${{ number_format($quote->iva_ammount, 2) }}
									</th>
								</tr>
							@endif
							<tr>
								<th colspan="2" class="cell-warning"></th>
								<th class="text-right" colspan="1">
									TOTAL  (<span>{{ $quote->currency_short_name }}</span>):
								</th>
								<th class="text-right">
									${{ number_format($quote->total_on_quote, 2) }}
								</th>
							</tr>
						@endif
					</tbody>
				</table>
				<!-- Observaciones -->
				<div class="nobreak">
					<table cellpadding="3" cellspacing="0" class="mb-3">
						<thead>
							<tr class="cell-gray">
								<th class="text-uppercase">Observations</th>
							</tr>
						</thead>
						<tbody>
							<tr class="text-uppercase">
								<td>{!! $quote->observations !!}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- Terminos -->
				<div class="nobreak">
					<table cellpadding="3" cellspacing="0" class="mb-3">
						<tbody>
							<tr>
								<th width="12%" class="cell-gray text-right">TERMS:</th>
								<td class="text-left text-uppercase">{{ $quote->terms }}</td>
							</tr>
							<tr>
								<th width="12%" class="cell-gray text-right">DELIVERY:</th>
								<td class="text-left text-uppercase">{{ $quote->delivery_time }}</td>
							</tr>
							<tr>
								<th width="12%" class="cell-gray text-right">F.O.B.:</th>
								<td class="text-left text-uppercase">{{ $quote->fob }}</td>
							</tr>
							<tr>
								<th width="12%" class="cell-gray text-right">PRICES:</th>
								<td class="text-left text-uppercase">
									ALL PRICES ARE IN 
									<span class="font-weight-bold">
										<span>{{ $quote->currency_short_name }}</span>
									</span>
									AND VALID FOR 
									<span class="font-weight-bold">
										<span>{{ $quote->vigency }}</span> DAYS
									</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>