@extends('dbpi.comercial.layout')
@section('module_title', 'Tablero Dynamic Communications')
@section('module_actions')
	
@stop
@section('module_content')

	<indicadores-tablero-dynacom 
		:fechas="{{json_encode($fechas)}}" 
		:periodo="{{$ano}}">
	</indicadores-tablero-dynacom>

	<div class="card shadow-sm">
		<div class="table-responsive">
			<tablero-dynacom :registros="{{ json_encode($registros) }}"></tablero-dynacom>
		</div>
		<div class="card-footer">
			{{ $fups->appends(request()->query())->links() }}
		</div>
	</div>

@stop