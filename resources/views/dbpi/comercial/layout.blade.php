@extends('layouts.module')
@section('module_sidebar')
	<ul class="list-group">
		@role('admin')
		<a href="{{ route('comercial') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('comercial')) ? 'active' : '' }}
			">
			<i class="fa fa-desktop fa-fw fa-sm mr-2"></i>
			Escritorio
		</a>
		@endrole
		<a href="{{ route('oportunidades.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('comercial/oportunidades')) ? 'active' : '' }}
				{{ (request()->is('comercial/oportunidades/*')) ? 'active' : '' }}
			">
			<i class="fa fa-dollar-sign fa-fw fa-sm mr-2"></i>
			Oportunidades
		</a>
		<a href="{{ route('cotizaciones.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('comercial/cotizaciones')) ? 'active' : '' }}
				{{ (request()->is('comercial/cotizaciones/*')) ? 'active' : '' }}
			">
			<i class="fa fa-file-invoice-dollar fa-fw fa-sm mr-2"></i>
			Cotizaciones
		</a>
		<a href="{{ route('fuprep.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('')) ? 'active' : '' }}
				{{ (request()->is('/*')) ? 'active' : '' }}
			">
			<i class="fa fa-file-export fa-fw fa-sm mr-2"></i>
			FUP's
		</a>
		<a href="{{ route('productos.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('comercial/productos')) ? 'active' : '' }}
				{{ (request()->is('comercial/productos/*')) ? 'active' : '' }}
			">
			<i class="fa fa-cubes fa-fw fa-sm mr-2"></i>
			Productos
		</a>
		<a href="{{ route('proveedores.index') }}" 
			class="list-group-item 
				list-group-item-action
				{{ (request()->is('comercial/proveedores')) ? 'active' : '' }}
				{{ (request()->is('comercial/proveedores/*')) ? 'active' : '' }}
			">
			<i class="fa fa-briefcase fa-fw fa-sm mr-2"></i>
			Proveedores
		</a>
		<a href="{{ route('tablero') }}" 
			class="d-flex align-items-center list-group-item 
				list-group-item-action
				{{ (request()->is('comercial/tablero')) ? 'active' : '' }}
				{{ (request()->is('comercial/tablero/*')) ? 'active' : '' }}
			">
			<span>
				<i class="fa fa-table fa-fw fa-sm mr-2"></i>
				Tablero
			</span>
		</a>
	</ul>
@stop