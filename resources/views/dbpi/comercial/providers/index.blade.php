@extends('dbpi.comercial.layout')
@section('module_title', 'Lista de proveedores')
@section('module_actions')
	<a href="{{ route('proveedores.create') }}" class="btn btn-primary">Nuevo proveedor</a>
@stop
@section('module_content')
	@if($providers->count())
	<table class="table">
		<thead>
			<tr>
				<th>Nombre</th>
			</tr>
		</thead>
		<tbody>
			@foreach($providers as $provider)
			<tr>
				<td>
					<a href="{{ route('proveedores.edit', $provider) }}">{{ $provider->name }}</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	No se han agregado proveedores
	@endif
@stop