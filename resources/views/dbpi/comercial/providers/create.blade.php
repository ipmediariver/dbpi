@extends('dbpi.comercial.layout')
@section('module_title', 'Nuevo proveedor')
@section('module_content')
	<div class="row">
		<div class="col-sm-6">
			<div class="card card-body shadow-sm">
				<form action="{{ route('proveedores.store') }}" 
					method="post"
					onsubmit="submit_btn.disabled = true; return true;">
					@csrf
					@component('components.form-header', [
						'description' => 'Crear nuevo proveedor'
					])
					@endcomponent
					<div class="form-group">
						<label for="">Nombre del proveedor</label>
						<input type="text" name="name" value="{{ old('name') }}" class="form-control">
					</div>

					<button name="submit_btn" class="btn btn-success mt-4">
						<i class="fa fa-check fa-sm mr-2"></i>
						Guardar
					</button>
				</form>
			</div>
		</div>
	</div>
@stop