<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cotización</title>
	<style>
		body {
			margin: 0;
			padding: 30px 18px;
			border: 1px solid #e3342f;
			font-family: 'Arial', sans-serif;
			font-size: 10px;
		}
		.table{
			width: 100%;
		}
		.table th, .table td{
			border: 1px solid #aaa;
			padding: 5px;
			vertical-align: top;
		}
		.text-center{
			text-align: center;
		}
		.text-left{
			text-align: left;
		}
		.text-right{
			text-align: right;
		}
		.text-danger{
			color: #e3342f;
		}
		.cell-secondary{
			background: #eee;
		}
		.cell-warning{
			background: #fffce3;
		}
		.text-uppercase{
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<div class="full_content">
		
		<img src="{{ public_path() . '/img/png/logo_dynacom.png' }}" style="width: 50%; height: auto;" alt="">
		
		<div style="margin-top: 20px">
			<b>CALLE LAZARO CARDENAS 341-C</b><br>
			<b>COL. JARDINES DEL LAGO</b><br>
			<b>MEXICALI, BC.</b><br>
		</div>

		<table class="table" cellpadding="0" cellspacing="0" style="margin-top: 20px;">
			<thead>
				<tr>
					<th class="text-left cell-secondary" width="60%">CUSTOMER INFORMATION:</th>
					<th class="text-left cell-secondary" width="20%">DATE:</th>
					<th class="text-left cell-secondary" width="20%">QUOTE #:</th>
				</tr>
				<tr>
					<th rowspan="3" class="text-left cell-warning text-uppercase">{{ $quote->account->name }}</th>
					<td class="text-left">{{ $quote->date->format('d/m/Y') }}</td>
					<th class="text-left text-danger">{{ $quote->quote_id }}</th>
				</tr>
				<tr>
					<th class="text-left cell-secondary">TERMS:</th>
					<th class="text-left cell-secondary">DELIVERY:</th>
				</tr>
				<tr>
					<td class="text-left">{{ $quote->terms }}</td>
					<td class="text-left">{{ $quote->delivery_time }}</td>
				</tr>
			</thead>
		</table>

		<table class="table" cellpadding="0" cellspacing="0" style="margin-top: 20px;">
			<thead>
				<tr>
					<th class="cell-secondary" width="12%">QTY</th>
					<th class="cell-secondary text-left">DESCRIPTION</th>
					<th class="cell-secondary text-right" width="20%">UNIT</th>
					<th class="cell-secondary text-right" width="20%">SUBTOTAL</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<th class="text-left cell-warning text-uppercase">{{ $quote->description }}</th>	
					<td></td>
					<td></td>
				</tr>
			</tbody>
			@foreach($quote->groups as $group)
			<tbody>
				<tr>
					<td></td>
					<th class="text-left cell-warning text-uppercase">{{ $group->description }}</th>	
					<td></td>
					<td></td>
				</tr>
				@foreach($group->resources as $quote_resource)
				<tr>
					<td class="text-center">{{ $quote_resource->qty }}</td>
					<td>{{ $quote_resource->jobtitle_name }}</td>
					<td class="text-right">${{ number_format($quote_resource->resource_cost, 2) }}</td>
					<td class="text-right">${{ number_format($quote_resource->total_import, 2) }}</td>
				</tr>
				@endforeach
				@foreach($group->products as $quote_product)
				<tr>
					<td class="text-center">{{ $quote_product->qty }}</td>
					<td>{{ $quote_product->product_description }}</td>
					<td class="text-right">${{ number_format($quote_product->customer_cost_with_discount, 2) }}</td>
					<td class="text-right">${{ number_format($quote_product->customer_total_import, 2) }}</td>
				</tr>
				@endforeach
			</tbody>
			@endforeach
			<tbody>
				<tr>
					<th colspan="2" class="cell-secondary"></th>
					<th class="text-right">SUBTOTAL:</th>
					<th class="text-right">{{ number_format($quote->subtotal, 2) }}</th>
				</tr>
				<tr>
					<th colspan="2" class="cell-secondary"></th>
					<th class="text-right">DESCUENTO:</th>
					<th class="text-right"></th>
				</tr>
				<tr>
					<th colspan="2" class="cell-secondary"></th>
					<th class="text-right">IVA:</th>
					<th class="text-right">{{ $quote->iva }}%</th>
				</tr>
				<tr>
					<th colspan="2" class="cell-secondary text-danger">
						<!-- NOTE: TAX NOT INCLUDED -->
					</th>
					<th class="text-right">TOTAL:</th>
					<th class="text-right">{{ number_format($quote->total, 2) }}</th>
				</tr>
			</tbody>
		</table>

		<table class="table" cellpadding="0" cellspacing="0" style="margin-top: 20px;">
			<tr>
				<th class="cell-secondary text-right text-uppercase" width="12%">Notes:</th>
				<td>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae magni, officiis maxime veritatis. Eveniet provident, aliquam aliquid odit pariatur! Voluptate, harum voluptas assumenda debitis illum blanditiis molestiae? Nisi, dicta, illum.
				</td>
			</tr>
		</table>

		<table class="table" cellpadding="0" cellspacing="0" style="margin-top: 20px;">
			<tbody>
				<tr>
					<th width="12%" class="cell-secondary text-right">TERMS:</th>
					<td class="text-left text-uppercase">{{ $quote->terms }}</td>
				</tr>
				<tr>
					<th width="12%" class="cell-secondary text-right">DELIVERY:</th>
					<td class="text-left text-uppercase">{{ $quote->delivery_time }}</td>
				</tr>
				<tr>
					<th width="12%" class="cell-secondary text-right">F.O.B.:</th>
					<td class="text-left text-uppercase">MXL</td>
				</tr>
				<tr>
					<th width="12%" class="cell-secondary text-right">PRICES:</th>
					<td class="text-left text-uppercase">ALL PRICES ARE IN US DOLLARS AND VALID FOR 15 DAYS</td>
				</tr>
			</tbody>
		</table>

		<p>DYNACOM- FPF-02 REV 2.3 NOV 1999 AAJR.</p>
		<p class="text-danger">
			<b>CONFIDENTIAL DYNAMIC COMMUNICATIONS PROPRIETARY</b>
		</p>

	</div>	
</body>
</html>