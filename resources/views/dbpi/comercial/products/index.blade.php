@extends('dbpi.comercial.layout')
@section('module_title', 'Lista de productos')
@section('module_actions')
	<upload-products-list></upload-products-list>
@stop
@section('module_content')
	
	<p><b>Productos</b></p>
	
	@if(count($products))
		<table class="table">
			<thead>
				<tr>
					<th>No. parte</th>
					<th>Nombre</th>
					<th>Tipo</th>
					<th>Marca</th>
					<th>Modelo</th>
					<th>Detalles</th>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
				<tr>
					<td>{{ $product->code }}</td>
					<td>{{ $product->description }}</td>
					<td>{{ $product->type->name }}</td>
					<td>{{ $product->brand->name }}</td>
					<td>{{ $product->model->name }}</td>
					<td nowrap>
						<a href="{{ route('productos.show', $product) }}">Ver detalles</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@if($products->total() > 15)
			{{ $products->links() }}
		@endif
	@else
		No se han agregado productos al sistema.
	@endif
@stop