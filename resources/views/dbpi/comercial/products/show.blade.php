@extends('dbpi.comercial.layout')
@section('module_title', 'Información de producto')
@section('module_content')
	
	<product-form :id="{{ $product->id }}"></product-form>
	
@stop