@extends('dbpi.comercial.layout')
@section('module_title', "Historial de versiones")
@section('module_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>Versiones</b></p>
	</div>
	<table class="table table-sm">
		<thead>
			<tr>
				<th>#FUP</th>
				<th>Fecha de creacion</th>
				<th width="15%">Comentarios</th>
				<th class="text-right">Ver</th>
			</tr>
		</thead>
		<tbody>
			@foreach($fuprep->fups as $fup)
			<tr>
				<th>{{ $fup->number }}</th>
				<td>{{ $fup->created_at->format('Y-m-d') }}</td>
				<td>
					<comment-fup-version :id="{{ $fup->id }}"></comment-fup-version>
				</td>
				<td class="text-right">
					<a href="{{ route('fuprep.fups.show', [$fuprep, $fup]) }}">Ver detalles</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop