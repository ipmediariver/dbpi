@extends('dbpi.comercial.layout')
@section('module_title')
	FUP #{{$fup->number}}
@stop
@section('module_actions')
	<div class="d-flex align-items-center">
		@if(!$fup->is_cancelled)
			@role('user')
				<cancel-fup :fup="{{$fup}}"></cancel-fup>
			@endrole
			@if($fup->confirmations->where('user_id', auth()->user()->id)->where('status_id', 0)->first())
				<auth-fup-btn  :rep="{{ $fup->repository->id }}" :id="{{ $fup->id }}" class="ml-2"></auth-fup-btn>
			@endif
			@role('user')
			<request-fup-auth class="ml-2"></request-fup-auth>
			@endrole
		@endif
		<div class="dropdown ml-2">
			<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-download"></i>
			</button>
			<div class="dropdown-menu dropdown-menu-right shadow" aria-labelledby="dropdownMenuButton">
				<h6 class="dropdown-header">¿Que deseas descargar?</h6>
				<div class="dropdown-divider"></div>
				<form action="{{ route('download-fup', $fup) }}" method="post" target="_blank">
					@csrf
					<button type="submit" class="dropdown-item text-capitalize" href="#">
						<i class="fa fa-file fa-fw mr-2 text-muted"></i>
						FUP
					</button>
				</form>
				<form action="{{ route('export-quote-to-pdf', $fup->quote) }}" method="post" target="_blank">
					@csrf
					<input type="hidden" name="new_format">
					<button type="submit" class="dropdown-item text-capitalize" href="#">
						<i class="fa fa-file fa-fw mr-2 text-muted"></i>
						Cotización
					</button>
				</form>
				<form action="{{ route('export-margin-to-pdf', $fup->quote) }}" method="post" target="_blank">
					@csrf
					<button type="submit" class="dropdown-item text-capitalize" href="#">
						<i class="fa fa-file fa-fw mr-2 text-muted"></i>
						Margen
					</button>
				</form>
				<form action="{{ route('download-fup-and-quote', $fup) }}" method="post">
					@csrf
					<button type="submit" class="dropdown-item" href="#">
						<i class="fa fa-file-archive fa-fw mr-2 text-muted"></i>
						Todo
					</button>
				</form>
			</div>
		</div>
	</div>
@stop
@section('module_content')
	@if($fup->is_cancelled)
		<div class="alert alert-danger text-center font-weight-bold shadow-sm">
			<i class="fa fa-lg fa-exclamation-triangle mr-2"></i> CANCELADA
		</div>
	@endif
	@role('admin')
		@if($fup->cancellation)
			<fup-cancellation-alert :fup="{{ $fup }}" :cancellation="{{ $fup->cancellation->load('user') }}"></fup-cancellation-alert>
		@endif
	@endrole
	<div class="d-flex align-items-center mb-3">
		<a href="{{ route('fuprep.fups.index', [$fuprep]) }}" class="btn btn-link p-0">Historial de versiones</a>
		<quote-participants class="ml-auto"></quote-participants>
	</div>
	<fup-billing-import></fup-billing-import>
	<div class="card shadow-sm">
		<div class="card-header quote">
			<ul class="nav nav-tabs nav-fill card-header-tabs font-weight-bold" id="myTab" role="tablist">
				<li class="nav-item" role="presentation">
					<a class="nav-link active text-center" id="fup-tab" data-toggle="tab" href="#fup" role="tab" aria-controls="fup" aria-selected="true">
						<i class="fa fa-lg fa-file-alt"></i>
						<b class="d-block small">FUP</b>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link text-center" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
						<i class="fa fa-lg fa-file-invoice-dollar"></i>
						<b class="d-block small">Cotización</b>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link text-center" id="margin-tab" data-toggle="tab" href="#margin" role="tab" aria-controls="margin" aria-selected="false">
						<i class="fa fa-lg fa-file-alt"></i>
						<b class="d-block small">Margen</b>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link text-center" id="po-tab" data-toggle="tab" href="#po" role="tab" aria-controls="po" aria-selected="false">
						<i class="fa fa-lg fa-file-invoice"></i>
						<b class="d-block small">Orden de compra</b>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link text-center" id="attachments-tab" data-toggle="tab" href="#attachments" role="tab" aria-controls="attachments" aria-selected="false">
						<i class="fa fa-lg fa-paperclip"></i>
						<b class="d-block small">Adjuntos</b>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link text-center" id="contratos-tab" data-toggle="tab" href="#contratos" role="tab" aria-controls="contratos" aria-selected="false">
						<i class="fa fa-lg fa-file-alt"></i>
						<b class="d-block small">
							({{ $fup->quote->contratos->count() }}) 
							Contratos
						</b>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link text-center" id="checkin-tab" data-toggle="tab" href="#checkin" role="tab" aria-controls="checkin" aria-selected="false">
						<i class="fa fa-lg fa-users"></i>
						<b class="d-block small">
							({{ $fup->auth_departments->where('status', true)->count() .'/'. $fup->auth_departments->count() }}) Autorizaciones:
						</b>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link text-center" id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="false">
						<i class="fa fa-lg fa-comments"></i>
						<b class="d-block small">
							({{ $fup->comments->count() }}) Comentarios
						</b>
					</a>
				</li>
			</ul>
		</div>
		<div class="card-body py-4 px-4">
			<div class="tab-content" id="myTabContent">
				<!-- FUP -->
				<div class="tab-pane fade show active" id="fup" role="tabpanel" aria-labelledby="fup-tab">
					<fup rep="{{ $fuprep->id }}" id="{{ $fup->id }}"></fup>
				</div>
				<!-- COTIZACION -->
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					@component('dbpi.comercial.quotes.components.quote-preview', [
						'quote' => $fup->quote
					])
					@endcomponent
				</div>
				<!-- MARGEN -->
				<div class="tab-pane fade" id="margin" role="tabpanel" aria-labelledby="margin-tab">
					@component('dbpi.comercial.quotes.components.margin-preview', [
						'quote' => $fup->quote
					])
					@endcomponent
				</div>
				<!-- CONTRATOS -->
				<div class="tab-pane fade" id="contratos" role="tabpanel" aria-labelledby="contratos-tab">
					<x-contratos-en-fup :fup="$fup" />
				</div>
				<!-- ORDEN DE COMPRA -->
				<div class="tab-pane fade" id="po" role="tabpanel" aria-labelledby="po-tab">
					@component('dbpi.comercial.quotes.components.payment-order', ['quote' => $fup->quote])
					@endcomponent
				</div>
				<div class="tab-pane fade" id="attachments" role="tabpanel" aria-labelledby="attachments-tab">
					@component('dbpi.comercial.quotes.components.attachments', ['quote' => $fup->quote])
					@endcomponent
				</div>
				<div class="tab-pane fade" id="checkin" role="tabpanel" aria-labelledby="checkin-tab">
					@component('dbpi.comercial.fups.components.checkin', [
						'fup' => $fup
					])
					@endcomponent
				</div>
				<div class="tab-pane fade" id="comments" role="tabpanel" aria-labelledby="comments-tab">
					@component('dbpi.comercial.fups.components.comments', [
						'fup' => $fup
					])
					@endcomponent
				</div>
			</div>
		</div>
	</div>
@stop