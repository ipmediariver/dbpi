@if($fup->auth_departments->count())
<table class="table m-0 table-sm m-0 table-striped table-bordered">
	<thead>
		<tr>
			<th width="25%">Departamento</th>
			<th width="50%">Responsables</th>
			<th class="text-center">Estatus</th>
		</tr>
	</thead>
	<tbody>
		@foreach($fup->auth_departments as $department)
		<tr>
			<th>{{ $department->name }}</th>
			<td>
				@foreach($department->supervisors as $supervisor)
					@if($supervisor->status_id == 1) 
						<span class="d-block">{{ $supervisor->full_name }} </span>
						<span class="small text-success font-weight-bold">Autorizado el: {{ $supervisor->updated_at->format('d/m/Y h:i a') }}</span>
					@else 
						<span class="d-block">{{ $supervisor->full_name }} </span>
						<span class="small text-muted font-weight-bold">Pendiente de autorización</span>
					@endif
					@if(!$loop->last)
						<hr class="my-1">
					@endif
				@endforeach
			</td>
			<td class="text-center">
				{!! $department->status_badge !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@else
	No se ha solicitado autorización aún.
@endif