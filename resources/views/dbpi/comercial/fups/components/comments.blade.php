<div class="mb-3">
	<comment-about-fup :rep="{{ $fup->repository->id }}" :id="{{ $fup->id }}"></comment-about-fup>
</div>
@if($fup->comments->count())

<ul class="list-group">
	@foreach($fup->comments()->orderBy('created_at', 'desc')->get() as $comment)
	<li class="list-group-item">
		<p class="small mb-1">{{ $comment->author_name }} dice:</p>
		<p class="small mb-3 text-muted">{{ $comment->created_at->format('d M, Y h:i a') }}</p>
		<p class="mb-0">{{ $comment->description }}</p>
	</li>
	@endforeach
</ul>

@else
No se han agregado comentarios.
@endif