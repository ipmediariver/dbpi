<div>
	<div class="row">
		<div class="col-sm-4">
			<img src="{{ asset('/img/logo.png') }}" class="img-fluid" alt="">
		</div>
		<div class="col-sm-4 ml-auto text-right">
			<h5 class="font-weight-bold mb-2">FORMA UNICA DE PEDIDO</h5>
			<h5 class="m-0 text-danger">#{{ $fup->number }}</h5>
		</div>
	</div>
	<!-- Fup -->
	<table class="table table-sm small table-bordered fup mt-4 mb-0">
		<thead>
			<tr>
				<th width="25%" class="cell-gray">NO DE COTIZACION:</th>
				<th width="25%" class="cell-gray">NO DE ORDEN DEL CLIENTE:</th>
				<th width="25%" class="cell-gray">FECHA:</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{ $fup->quote->number }}</td>
				<td>{{ $fup->po_number }}</td>
				<td>{{ $fup->date->format('Y-m-d') }}</td>
			</tr>
		</tbody>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<thead>
			<tr>
				<th class="cell-gray">NOMBRE COMERCIAL:</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{ $fup->comercial_name }}</td>
			</tr>
		</tbody>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<thead>
			<tr>
				<th class="cell-gray">DESCRIPCION DEL PROYECTO:</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$fup->quote->description}}</td>
			</tr>
		</tbody>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<thead>
			<tr>
				<th class="cell-gray">CONTACTO RESPONSABLE TECNICO:</th>
				<th class="cell-gray" width="25%">E-MAIL:</th>
				<th class="cell-gray" width="25%">TELEFONO:</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{ $fup->contact_name }}</td>
				<td>{{ $fup->contact_email }}</td>
				<td>{{ $fup->contact_phone }}</td>
			</tr>
		</tbody>
	</table>
	<div class="d-flex align-items-start">
		<div class="w-100">
			<table class="table table-sm small table-bordered fup mb-0">
				<tr>
					<th class="cell-primary text-center">EMBARQUE</th>
				</tr>
				<tr>
					<th class="cell-gray">RAZON SOCIAL:</th>
				</tr>
				<tr>
					<td>{{ $fup->business_name_shipment }}</td>
				</tr>
				<tr>
					<th class="cell-gray">DOMICILIO (CALLE Y NUMERO, COLONIA / PARQUE INDUSTRIAL):</th>
				</tr>
				<tr>
					<td>{{ $fup->shipment_address }}</td>
				</tr>
			</table>
			<table class="table table-sm small table-bordered fup mb-0">
				<thead>
					<tr>
						<th class="cell-gray">CIUDAD Y ESTADO:</th>
						<th class="cell-gray" width="30%">C.P:</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{{ $fup->shipment_city_state }}</td>
						<td>{{ $fup->shipment_zip_code }}</td>
					</tr>
				</tbody>
			</table>
			<table class="table table-sm small table-bordered fup mb-0">
				<thead>
					<tr>
						<th class="cell-gray">ENTREGAS PARCIALES:</th>
						<th class="cell-gray">DIAS DE ENTREGA:</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							@if($fup->partial_deliveries)
								<i class="fa fa-check text-success"></i>
							@else
								<i class="fa fa-times text-danger"></i>
							@endif
						</td>
						<td>
							<div class="d-flex align-items-center">
								<label for="entregaLunes" class="m-0 mr-2">
									L
									@if($fup->delivery_monday)
										<i class="fa fa-check text-success"></i>
									@else
										<i class="fa fa-times text-danger"></i>
									@endif
								</label>
								<label for="entregaMartes" class="m-0 mr-2">
									M
									@if($fup->delivery_tuesday)
										<i class="fa fa-check text-success"></i>
									@else
										<i class="fa fa-times text-danger"></i>
									@endif
								</label>
								<label for="entregaMiercoles" class="m-0 mr-2">
									M
									@if($fup->delivery_wednesday)
										<i class="fa fa-check text-success"></i>
									@else
										<i class="fa fa-times text-danger"></i>
									@endif
								</label>
								<label for="entregaJueves" class="m-0 mr-2">
									J
									@if($fup->delivery_thursday)
										<i class="fa fa-check text-success"></i>
									@else
										<i class="fa fa-times text-danger"></i>
									@endif
								</label>
								<label for="entregaViernes" class="m-0">
									V
									@if($fup->delivery_friday)
										<i class="fa fa-check text-success"></i>
									@else
										<i class="fa fa-times text-danger"></i>
									@endif
								</label>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table table-sm small table-bordered fup mb-0">
				<tr>
					<th class="cell-gray">HORARIO DE ENTREGAS:</th>
				</tr>
				<tr>
					<td>{{ $fup->delivery_schedules }}</td>
				</tr>
				<tr>
					<th class="cell-gray">REFERENCIAS:</th>
				</tr>
				<tr>
					<td>{{ $fup->references }}</td>
				</tr>
			</table>
		</div>
		<div class="w-100">
			<table class="table table-sm small table-bordered fup mb-0">
				<tr>
					<th class="cell-primary text-center">FACTURACION</th>
				</tr>
				<tr>
					<th class="cell-gray">RAZON SOCIAL:</th>
				</tr>
				<tr>
					<td>{{ $fup->business_name_billing }}</td>
				</tr>
				<tr>
					<th class="cell-gray">DOMICILIO (CALLE Y NUMERO, COLONIA / PARQUE INDUSTRIAL):</th>
				</tr>
				<tr>
					<td>{{ $fup->billing_address }}</td>
				</tr>
			</table>
			<table class="table table-sm small table-bordered fup mb-0">
				<thead>
					<tr>
						<th class="cell-gray">CIUDAD Y ESTADO:</th>
						<th class="cell-gray">R.F.C.:</th>
						<th class="cell-gray" width="30%">C.P:</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{{ $fup->billing_city_state }}</td>
						<td>{{ $fup->billing_rfc }}</td>
						<td>{{ $fup->billing_zip_code }}</td>
					</tr>
				</tbody>
			</table>
			<div class="d-flex">
				<div class="w-100">
					<table class="table table-sm small table-bordered fup mb-0">
						<tr>
							<th class="cell-gray">MUB:</th>
						</tr>
						<tr>
							<td>{{ $fup->mub }}%</td>
						</tr>
						<tr>
							<th class="cell-gray">MONEDA:</th>
						</tr>
						<tr>
							<td>{{ $fup->quote->currency->short_name }}</td>
						</tr>
						<tr>
							<th class="cell-gray">TIPO DE CAMBIO:</th>
						</tr>
						<tr>
							<td>{{ $fup->quote->exchange_rate }}</td>
						</tr>
					</table>
				</div>
				<div class="w-100">
					<table class="table table-sm small table-bordered fup mb-0">
						<tr>
							<th colspan="3" class="cell-primary text-center">CONDICIONES DE PAGO</th>
						</tr>
						<tr>
							<th width="33%" class="cell-gray">CONCEPTO</th>
							<th width="33%" class="cell-gray text-center">%</th>
							<th width="33%" class="cell-gray text-center">DIAS</th>
						</tr>
						<tr>
							<th class="cell-gray">ANTICIPO</th>
							<th>{{ $fup->payment_conditions_advance_percent }}</th>
							<th>{{ $fup->payment_conditions_advance_days }}</th>
						</tr>
						<tr>
							<th class="cell-gray">ENTREGA</th>
							<th>{{ $fup->payment_conditions_delivery_percent }}</th>
							<th>{{ $fup->payment_conditions_delivery_days }}</th>
						</tr>
						<tr>
							<th class="cell-gray">INSTALACION</th>
							<th>{{ $fup->payment_conditions_instalation_percent }}</th>
							<th>{{ $fup->payment_conditions_instalation_days }}</th>
						</tr>
						<tr>
							<th colspan="3" class="cell-gray"></th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<table class="table table-sm small table-bordered fup mb-0">
		<tr>
			<th class="cell-primary text-center">INFORMACION DEL PEDIDO</th>
		</tr>
		<tr>
			<th class="cell-gray text-center">FECHAS DE COMPROMISO</th>
		</tr>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<tr>
			<th class="cell-gray">TIEMPO DE ENTREGA:</th>
			<th class="cell-gray">TIEMPO COMPROMISO ENTREGA:</th>
			<th class="cell-gray">FECHA DE INICIO DE INSTALACION:</th>
			<th class="cell-gray">FECHA DE PROYECTO:</th>
		</tr>
		<tr>
			<td>{{ $fup->delivery_time }}</td>
			<td>{{ $fup->delivery_commitment }}</td>
			<td>{{ $fup->instalation_date }}</td>
			<td>{{ $fup->project_date }}</td>
		</tr>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<tr>
			<th class="cell-primary text-center">CLIENTE FINAL</th>
		</tr>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<tr>
			<th class="cell-gray">NOMBRE COMERCIAL Y/O RAZON SOCIAL</th>
			<th class="cell-gray">CALLE Y NUMERO</th>
		</tr>
		<tr>
			<td>{{ $fup->final_client_business_name }}</td>
			<td>{{ $fup->final_client_address }}</td>
		</tr>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<tr>
			<th class="cell-gray">EJECUTIVO DE CUENTA</th>
			<th class="cell-gray">CIUDAD Y ESTADO</th>
			<th class="cell-gray">C.P.</th>
		</tr>
		<tr>
			<td>{{ $fup->final_client_account_executive }}</td>
			<td>{{ $fup->final_client_city_state }}</td>
			<td>{{ $fup->final_client_zip_code }}</td>
		</tr>
	</table>
	<table class="table table-sm small table-bordered fup mb-0">
		<tr>
			<th class="cell-primary">OBSERVACIONES Y RECOMENDACIONES</th>
		</tr>
		<tr>
			<td class="cell-gray">
				@if($fup->observations()->count())
				<ul class="list-group m-0">
					@foreach($fup->observations as $observation)
					<li class="list-group-item d-flex align-items-center">
						<span>{{ $observation->description }}</span>
					</li>
					@endforeach
				</ul>
				@else
				<p class="m-0">No se han agregado observaciones</p>
				@endif
			</td>
		</tr>
	</table>
	<div class="row mt-4">
		@foreach($fup->confirmations as $confirmation)
		<div class="col-sm-4">
			@if($confirmation->signature)
				<img src="{{ $confirmation->signature }}" class="img-fluid mb-3" alt="">
			@endif
			<p class="text-center font-weight-bold">{{ $confirmation->user->full_name }}</p>
		</div>
		@endforeach
	</div>
</div>