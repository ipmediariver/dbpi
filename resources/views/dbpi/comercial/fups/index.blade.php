@extends('dbpi.comercial.layout')
@section('module_title', "Lista de FUP's")

@section('module_actions')

	<filtrador-de-fups />

@stop

@section('module_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>FUP's</b></p>
	</div>
	
	@if(count($fupreps))
	
		<table class="table">
			<thead>
				<tr>
					<th>#FUP</th>
					<th>Cuenta</th>
					<th>Cotización</th>
					<th>Autorización</th>
					<th>Facturación</th>
					<th>Fecha</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($fupreps as $rep)
				@php
					$fup = $rep->current_fup;
					$current_quote = $fup->quote->current_version;
				@endphp
					@if($current_quote)
					<tr class="@if($fup->is_cancelled) cell-danger @endif">
						<td nowrap>
							<a href="{{ route('fuprep.fups.show', [$fup->repository, $fup]) }}" class="@if($rep->previous_fups->count()) version_commit @endif">
								#{{ $fup->number }}
							</a>
							@if($rep->previous_fups->count())
								@foreach($rep->previous_fups as $version)
									<a href="{{ route('fuprep.fups.show', [$fup->repository, $version]) }}" class="version_commit prev text-muted">
										<del>
											#{{ $version->number }}
										</del>
									</a>
								@endforeach
							@endif

							@if($fup->is_cancelled)
								<span class="d-block small font-weight-bold text-danger">
									<i class="fa fa-exclamation-triangle fa-sm mr-1"></i>
									Cancelada
								</span>
							@endif
						</td>
						<td>{{ $fup->quote->account->name }}</td>
						<td>
							<a href="{{ route('cotizaciones.show', $fup->quote) }}">#{{ $fup->quote->number }}</a>
						</td>
						<td>
							{!! $fup->status !!}
						</td>
						<td>
							{!! $fup->quote->billing_total_import_badge !!}
						</td>
						<td nowrap>{{ $fup->created_at->format('Y-m-d') }}</td>
						<td class="text-center">
							<div class="d-flex align-items-center">
								<a href="{{ route('fuprep.fups.show', [$fup->repository,$fup]) }}" class="btn btn-primary btn-icon ml-auto" title="Ver detalles">
									<i class="fa fa-search"></i>
								</a>
								<form action="{{ route('download-fup', $fup) }}" class="ml-2" method="post" target="_blank">
									@csrf
									<button type="submit" class="btn btn-primary btn-icon" href="#" title="Descargar FUP">
										<i class="fa fa-download"></i>
									</button>
								</form>
							</div>
						</td>
					</tr>
					@endif
				@endforeach
			</tbody>
		</table>
		
		@if($fupreps->total() > 15)
			{{ $fupreps->appends(request()->input())->links() }}
		@endif

	@else
		
		No se han creado fups aún.

	@endif
@stop