@extends('layouts.guest')
@section('sidebar')
	
	<comment-about-fup :rep="{{ $fup->repository->id }}" :id="{{ $fup->id }}"></comment-about-fup>
	<fup-confirm-of-received  :rep="{{ $fup->repository->id }}" :id="{{ $fup->id }}"></fup-confirm-of-received>

@stop

@section('content')

	<div class="card">
		<div class="card-header">
			<ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
				<li class="nav-item" role="presentation">
					<a class="nav-link active" id="fup-tab" data-toggle="tab" href="#fup" role="tab" aria-controls="fup" aria-selected="true">FUP</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="quote-tab" data-toggle="tab" href="#quote" role="tab" aria-controls="quote" aria-selected="false">Cotización</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="margin-tab" data-toggle="tab" href="#margin" role="tab" aria-controls="margin" aria-selected="false">Margen</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="fup" role="tabpanel" aria-labelledby="fup-tab">
					@component('dbpi.comercial.fups.components.fup-preview', [
						'fup' => $fup
					])
					@endcomponent
				</div>
				<div class="tab-pane fade" id="quote" role="tabpanel" aria-labelledby="quote-tab">
					@component('dbpi.comercial.quotes.components.quote-preview', [
						'quote' => $fup->quote
					])
					@endcomponent
				</div>
				<div class="tab-pane fade" id="margin" role="tabpanel" aria-labelledby="margin-tab">
					@component('dbpi.comercial.quotes.components.margin-preview', [
						'quote' => $fup->quote
					])
					@endcomponent
				</div>
			</div>
		</div>
	</div>

@stop