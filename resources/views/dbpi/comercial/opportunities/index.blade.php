@extends('dbpi.comercial.layout')
@section('module_title', 'Lista de oportunidades')
@section('module_actions')
	<!-- <opportunity-form></opportunity-form> -->
@stop
@section('module_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>Oportunidades</b></p>
	</div>
	<opportunities-list></opportunities-list>
@stop