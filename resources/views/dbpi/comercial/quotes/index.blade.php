@extends('dbpi.comercial.layout')
@section('module_title', 'Lista de cotizaciones')
@section('module_actions')
	<div class="d-flex align-items-center">
		<div class="mr-3">
			<filtrador-de-cotizaciones />
		</div>
		<form action="{{ route('search-quote') }}" method="post">
			@csrf
			<div class="form-group m-0 d-flex align-items-center">
				<input type="text" name="q" placeholder="Buscar cotización..." class="form-control">
				<button type="submit" class="btn btn-primary ml-2" style="white-space: nowrap;">
					<i class="fa fa-search fa-sm"></i>
				</button>
			</div>
		</form>
	</div>
@stop
@section('module_content')
	<div class="d-flex align-items-center mb-4">
		<p class="m-0"><b>Cotizaciones</b></p>
	</div>
	@if(count($quotes))
		<table class="table">
			<thead>
				<tr>
					<th>#Cotización</th>
					<th>Fecha</th>
					<th>Descripción</th>
					<th>Cuenta</th>
					<th>Estatus</th>
					<th>Margen</th>
					<th>Responsable</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($quotes as $quote)
				<tr>
					<td>
						<a href="{{ route('cotizaciones.show', $quote) }}" class="@if($quote->copy_of) version_commit @endif">
							#{{ $quote->number }}
						</a>
						@foreach($quote->previous_versions as $version)
							<a href="{{ route('cotizaciones.show', $version) }}" class="version_commit prev text-muted">
								<del>#{{ $version->number }}</del>
							</a>
						@endforeach
					</td>
					<td>{{ optional($quote->date)->format('d/m/Y') }}</td>
					<td>{{ $quote->description }}</td>
					<td>{{ $quote->account->name }}</td>
					<td>{!! $quote->status !!}</td>
					<td>
						@if($quote->margin_percent > 30)
						<span class="text-success">{{ number_format($quote->margin_percent, 2) }}%</span>
						@else
						<span class="text-danger">{{ number_format($quote->margin_percent, 2) }}%</span>
						@endif
					</td>
					<th nowrap>
						{{ $quote->author->first_name }} 
					</th>
					<td class="text-right" nowrap>
						<div class="d-flex align-items-center">
							<a href="{{ route('cotizaciones.show', $quote) }}" class="btn btn-primary btn-icon ml-auto" title="Ver detalles">
								<i class="fa fa-search fa-sm"></i>
							</a>
							<form action="{{ route('export-quote-to-pdf', $quote) }}" 
								method="post"
								target="_blank"
								class="ml-2">
								@csrf
								<input type="hidden" name="new_format">
								<button type="submit" name="submit_btn" class="btn btn-primary btn-icon" title="Descargar Cotización">
									<i class="fa fa-download"></i> 
								</button>
							</form>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@if($quotes->total() > 15)
			{{ $quotes->appends(request()->input())->links() }}
		@endif
	@else
		No se han creado cotizaciones aún.
	@endif
@stop