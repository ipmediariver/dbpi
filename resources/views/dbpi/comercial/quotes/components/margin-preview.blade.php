<div class="row mb-4">
	<div class="col-sm-4">
		<img src="{{ asset('img/png/logo_dynacom.png') }}" alt="" class="img-fluid">
	</div>
</div>
<div class="table-responsive text-uppercase">
	<table class="table table-sm small table-bordered">
		<thead>
			<tr>
				<th rowspan="2" class="text-center" nowrap>Cantidad:</th>
				<th rowspan="2" nowrap>No. parte</th>
				<th rowspan="2" style="min-width: 400px;" nowrap>Producto</th>
				<th colspan="7" nowrap class="text-center cell-primary">Proveedor</th>
				<th colspan="3" nowrap class="text-center cell-warning">Cliente</th>
				<th colspan="2" nowrap class="text-center cell-success">Margen</th>
			</tr>
			</tr>
				<!-- Proveedor -->
				<th nowrap>Nombre</th>
				<th nowrap>Reseller price</th>
				<th nowrap>% Desc</th>
				<th nowrap>% Desc especial</th>
				<th nowrap>$ Desc especial</th>
				<th nowrap>Precio extendido</th>
				<th nowrap>Importe</th>
				<!-- Cliente -->
				<th nowrap>Precio de venta</th>
				<th nowrap>% Desc</th>
				<th nowrap>Importe</th>
				<!-- Margen -->
				<th nowrap class="text-center">%</th>
				<th nowrap class="text-center">$</th>
			</tr>
		</thead>
		@foreach($quote->groups as $group)
		<tbody>
			<tr>
				<td colspan="2" class="cell-warning"></td>
				<td class="cell-warning font-weight-bold text-uppercase">{{ $group->description }}</td>
				<th colspan="9" class="cell-gray text-center">
					@if($group->custom_import)
						<span class="text-danger">Importe total modificado en cotización.</span>
					@endif
				</th>
				<td class="cell-gray text-right">
					@if($group->custom_import)
						${{ number_format($group->custom_import, 2) }}
					@endif
				</td>
				<td class="cell-gray text-center">
					@if($group->custom_import)
						{{ number_format($group->margin_percent, 2) }}%
					@endif
				</td>
				<td class="cell-gray text-right">
					@if($group->custom_import)
						${{ number_format($group->margin_cost, 2) }}
					@endif
				</td>
			</tr>

			<!-- Productos en grupo de cotización -->
			@foreach($group->products as $quote_product)
			<tr>
				<td class="text-center">{{ $quote_product->qty }}</td>
				<td style="background-color: {{ $quote_product->category['color']  }}">
					{{ optional($quote_product->product)->code }}
				</td>
				<td style="background-color: {{ $quote_product->category['color'] }}">
					{{ $quote_product->product->description }}
				</td>
				<td nowrap class="cell-primary">
					{{ $quote_product->provider_name }}
				</td>
				<td class="text-right cell-primary">${{ number_format($quote_product->provider_cost, 2) }}</td>
				<td class="text-right cell-primary">{{ number_format($quote_product->provider_discount, 2) }}%</td>
				<td class="text-right cell-primary">{{ number_format($quote_product->provider_special_discount, 2) }}%</td>
				<td class="text-right cell-primary">${{ number_format($quote_product->provider_special_discount_cost, 2) }}</td>
				<td class="text-right cell-primary" nowrap>
					${{ number_format($quote_product->provider_cost_with_discount, 2) }}
					@if(count($quote_product->acc_list_items) || $quote_product->shipping_cost)
						<p class="small mb-0 no_wrap" style="margin-top: 7px; margin-bottom: 0; white-space: nowrap"><b>Costos Adicionales</b></p>
						@if($quote_product->shipping_cost)
							<p class="small mb-0 no_wrap" style="margin: 0; white-space: nowrap">
								<span>Envío:</span>
								<b>${{ number_format($quote_product->shipping_cost, 2) }}</b>
							</p>
						@endif
						@foreach($quote_product->acc_list_items as $item)
							<p class="small mb-0 no_wrap" style="margin: 0; white-space: nowrap">
								<span>{{ $item['concept'] }}:</span>
								<b>${{ number_format($item['cost'], 2) }}</b>
							</p>
						@endforeach
					@endif
				</td>
				<td class="text-right cell-primary" nowrap>
					${{ number_format($quote_product->provider_total_import_without_acc_costs, 2) }}
					@if(count($quote_product->acc_list_items) || $quote_product->shipping_cost)
						<p class="small mb-0 no_wrap" style="margin-top: 7px; margin-bottom: 0; white-space: nowrap"><b>Costos Adicionales</b></p>
						@if($quote_product->shipping_cost)
							<p class="small mb-0 no_wrap" style="margin: 0; white-space: nowrap">
								<span>Envío:</span>
								<b>${{ number_format($quote_product->shipping_cost_total, 2) }}</b>
							</p>
						@endif
						@foreach($quote_product->acc_list_items_multiplied_by_qty as $item)
							<p class="small mb-0 no_wrap" style="margin: 0; white-space: nowrap">
								<span>{{ $item['concept'] }}:</span>
								<b>${{ number_format($item['cost'], 2) }}</b>
							</p>
						@endforeach
					@endif
				</td>
				<td class="text-right cell-warning">
					@if($group->custom_import)
						<s>${{ number_format($quote_product->customer_cost, 2) }}</s>
					@else
						@if($quote_product->custom_import)
							<s>${{ number_format($quote_product->customer_cost, 2) }}</s> <br>
							<span>${{ number_format($quote_product->custom_customer_cost, 2) }}</span>
						@else
							<span>${{ number_format($quote_product->customer_cost, 2) }}</span>
						@endif
					@endif
				</td>
				<td class="text-right cell-warning">{{ number_format($quote_product->customer_discount, 2) }}%</td>
				<td class="text-right cell-warning">
					@if($group->custom_import)
						<s>${{ number_format($quote_product->customer_total_import, 2) }}</s>
					@else
						@if($quote_product->custom_import)
							<s>${{ number_format($quote_product->customer_total_import, 2) }}</s> <br>
							<span>${{ number_format($quote_product->custom_import, 2) }}</span>
						@else
							<span>${{ number_format($quote_product->customer_total_import, 2) }}</span>
						@endif
					@endif
				</td>
				<td class="text-right cell-success">
					@if($group->custom_import)
						<s>{{ number_format($quote_product->margin_percent, 2) }}%</s>
					@else
						{{ number_format($quote_product->margin_percent, 2) }}%
					@endif
				</td>
				<td class="text-right cell-success">
					@if($group->custom_import)
						<s>${{ number_format($quote_product->margin_total, 2) }}</s>
					@else
						${{ number_format($quote_product->margin_total, 2) }}
					@endif
				</td>
			</tr>
			@endforeach

			<!-- Recursos en grupo de cotización -->
			@foreach($group->resources as $quote_resource)
			<tr>
				<td class="text-center">{{ $quote_resource->qty }}</td>
				<td></td>
				<td>{{ $quote_resource->jobtitle_name }}</td>
				<td colspan="6" class="cell-primary">
					({{ $quote_resource->days }}) Días
				</td>
				<td class="text-right cell-primary">
					@if($group->custom_import)
						<s>${{ number_format($quote_resource->total_import, 2) }}</s>
					@else
						${{ number_format($quote_resource->total_import, 2) }}
					@endif
				</td>
				<td class="text-right cell-warning">
					@if($group->custom_import)
						<s>${{ number_format($quote_resource->exchange_cost, 2) }}</s>
					@else
						${{ number_format($quote_resource->exchange_cost, 2) }}
					@endif
				</td>
				<td class="cell-warning"></td>
				<td class="cell-warning text-right">
					@if($group->custom_import)
						<s>{{ $quote_resource->total_import }}</s>
					@else
						@if($quote_resource->custom_import)
							<s>${{ number_format($quote_resource->total_import, 2) }}</s> <br>
							<span>${{ number_format($quote_resource->custom_import, 2) }}</span>
						@else
							${{ number_format($quote_resource->total_import, 2) }}
						@endif
					@endif
				</td>
				<td class="cell-success text-right">
					{{ number_format($quote_resource->margin_percent, 2) }}%
				</td>
				<td class="cell-success text-right">
					${{ number_format($quote_resource->margin_total, 2) }}
				</td>
			</tr>
			@endforeach
		</tbody>
		@endforeach
		<!-- Totales de margen -->
		<thead>
			<tr>
				<th colspan="9" class="cell-gray"></th>
				<th class="cell-primary">${{ number_format($quote->provider_total_import, 2) }}</th>
				<td colspan="2" class="cell-gray"></td>
				<th class="cell-warning">${{ number_format($quote->subtotal, 2) }}</th>
				<th class="text-center cell-success">{{ number_format($quote->margin_percent, 2) }}%</th>
				<th class="text-center cell-success">${{ number_format($quote->margin_cost, 2) }}</th>
			</tr>
		</thead>
	</table>
</div>