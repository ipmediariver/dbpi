@if($quote->payment_order)
	@if($quote->payment_order->ext == 'pdf')
		<embed src="{{ asset('/uploads/attachments/' . $quote->payment_order->filename) }}" 
			width="100%" 
			height="768" 
			type="application/pdf">
	@elseif($quote->payment_order->ext == 'jpg' || $quote->payment_order->ext == 'jpeg')
		<img src="{{ asset('/uploads/attachments/' . $quote->payment_order->filename) }}" class="img-fluid" alt="">
	@endif
@else
	No tiene orden de compra.
@endif