<div class="text-uppercase">
	<div class="row">
		<div class="col-sm-4">
			<img src="{{ asset('img/png/logo_dynacom.png') }}" alt="" class="img-fluid">
		</div>
	</div>
	<div class="my-4">
		<b>CALLE LAZARO CARDENAS 341-C</b><br>
		<b>COL. JARDINES DEL LAGO</b><br>
		<b>MEXICALI, BC.</b><br>
	</div>
	<table class="table table-bordered small table-sm">
		<tr>
			<th class="cell-gray text-uppercase" width="60%">Customer Information:</th>
			<th class="cell-gray text-uppercase" width="20%">Date:</th>
			<th class="cell-gray text-uppercase" width="20%">Quote #</th>
		</tr>
		<tr>
			<td rowspan="3">
				<img src="{{ $quote->account_logo }}" style="width: 200px" class="my-3" alt="">
				<p class="text-uppercase font-weight-bold mb-2">{{ $quote->account_name }}</p>
				<p class="m-0">Contact: <span>{{ $quote->contact_name }}</span></p>
			</td>
			<td>{{ $quote->formated_date }}</td>
			<th>
				<a href="{{ route('cotizaciones.show', $quote) }}" class="text-danger" target="_blank">{{ '#' . $quote->number }}</a>
			</th>
		</tr>
		<tr>
			<th class="cell-gray text-uppercase">Terms:</th>
			<th class="cell-gray text-uppercase">Delivery:</th>
		</tr>
		<tr>
			<td>{{ $quote->terms }}</td>
			<td>{{ $quote->delivery_time }}</td>
		</tr>
	</table>
	<table class="table table-bordered small table-sm quote-table">
		<thead>
			<tr>
				<th class="cell-gray text-center" width="12%">QTY</th>
				<th class="cell-gray text-left">DESCRIPTION</th>
				<th class="cell-gray text-right">UNIT</th>
				@if($quote->display_customer_items_discount)
				<th class="cell-gray text-right">% DISC</th>
				<th class="cell-gray text-right">$ DISC</th>
				@endif
				<th class="cell-gray text-right">SUBTOTAL</th>
			</tr>
		</thead>
		<tbody>
			@if(!$quote->hide_description)
			<tr>
				<td class="text-center">
					@if($quote->only_show_description)
						1
					@endif
				</td>
				<th class="text-left text-uppercase">
					{{ $quote->description }}
				</th>	
				<td class="text-right">
					@if($quote->only_show_description)
						${{ number_format($quote->subtotal_on_quote, 2) }}
					@endif
				</td>
				@if($quote->display_customer_items_discount)
				<td></td>
				<td></td>
				@endif
				<td class="text-right">
					@if($quote->only_show_description)
						${{ number_format($quote->subtotal_on_quote, 2) }}
					@endif
				</td>
			</tr>
			@endif
		</tbody>
		@foreach($quote->groups as $group)
			@if(!$quote->only_show_description)
				@if(!$group->hide_in_quote)
					<tbody>
						<tr>
							<td class="text-center">
								@if($group->hide_items)
									1
								@endif
							</td>
							<th class="text-left cell-warning text-uppercase">
								@if(!$group->hide_name)
									{{ $group->description }}
								@endif
							</th>
							<td class="text-right">
								@if($group->hide_items)
									${{ number_format($group->sub_total_on_quote, 2) }}
								@endif
							</td>
							@if($quote->display_customer_items_discount)
							<td></td>
							<td></td>
							@endif
							<td class="text-right">
								@if($group->hide_items)
									${{ number_format($group->sub_total_on_quote , 2)}}
								@endif
							</td>
						</tr>
						@if(!$group->hide_items)

							@foreach($group->products as $quote_product)
								@if(!$quote_product->hide_in_quote)
								<tr>
									<td class="text-center">{{ $quote_product->qty }}</td>
									<td>
										<div class="d-flex align-items-start">
											<div>
												<p class="m-0">
													@if($quote->show_code)
														@if($quote_product->product->code)
															<span class="font-weight-bold">#{{ $quote_product->product->code }} - </span>
														@endif
													@endif
													{{ $quote_product->product_description }}
												</p>
												@if($quote_product->detalles)
													<div style="text-transform: none !important" class="mt-3">
														{!! $quote_product->detalles !!}
													</div>
												@endif
												@if($quote_product->show_specifications_on_quote)
												<small class="d-block mt-3">
													{!! $quote_product->product->specifications !!}
												</small>
												@endif
											</div>
											@if($quote_product->show_image_on_quote)
											<div class="text-center ml-3">
												<img src="{{ asset('/uploads/products/' . $quote_product->product->thumb) }}" 
													class="quote_product_thumb ml-auto" alt="">
											</div>
											@endif
										</div>
									</td>	
									<td class="text-right">${{ number_format($quote_product->customer_cost_by_unit, 2) }}</td>
									@if($quote->display_customer_items_discount)
									<td class="text-right">{{ $quote_product->customer_discount }}%</td>
									<td class="text-right">${{ number_format($quote_product->customer_cost_by_unit_with_discount, 2) }}</td>
									@endif
									<td class="text-right">${{ number_format($quote_product->customer_total_import_on_quote, 2) }}</td>
								</tr>
								@endif
							@endforeach

							@foreach($group->resources as $quote_resource)
								<tr>
									<td class="text-center">{{ $quote_resource->qty }}</td>
									<td>{{ $quote_resource->jobtitle_name }}</td>
									<td class="text-right">${{ number_format($quote_resource->total_import_on_quote, 2) }}</td>
									<td></td>
									<td></td>
									<td class="text-right">${{ number_format($quote_resource->total_import_on_quote, 2) }}</td>
								</tr>
							@endforeach
							
						@endif
					</tbody>
				@endif
			@endif
		@endforeach
		<tbody>
			<tr>
				<th colspan="@if($quote->display_customer_items_discount) 3 @else 2 @endif" class="text-uppercase text-danger cell-warning text-center">
					@if(!$quote->display_iva)
						NOTE: TAX NOT INCLUDED
					@endif
				</th>
				<th class="text-right" 
					colspan="@if($quote->display_customer_items_discount) 2 @else 1 @endif">
					SUBTOTAL (<span>{{ $quote->currency_short_name }}</span>):
				</th>
				<th class="text-right">
					${{ number_format($quote->subtotal_on_quote, 2) }}
				</th>
			</tr>
			@if($quote->general_discount)
				<tr>
					<th colspan="@if($quote->display_customer_items_discount) 3 @else 2 @endif" class="cell-warning"></th>
					<th class="text-right"
						colspan="@if($quote->display_customer_items_discount) 2 @else 1 @endif" nowrap>
						<span>CUSTOMER SPECIAL DISCOUNT (<span class="text-danger">{{ $quote->general_discount }}%</span>):</span>
					</th>
					<th class="text-right text-danger">
						- ${{ number_format($quote->general_discount_ammount, 2) }}
					</th>
				</tr>
			@endif
			@if($quote->display_iva)
				<tr>
					<th colspan="@if($quote->display_customer_items_discount) 3 @else 2 @endif" class="cell-warning"></th>
					<th class="text-right"
						colspan="@if($quote->display_customer_items_discount) 2 @else 1 @endif" nowrap>
						TAX ({{ $quote->iva }}%):
					</th>
					<th class="text-right">
						${{ number_format($quote->iva_ammount, 2) }}
					</th>
				</tr>
			@endif
			<tr>
				<th colspan="@if($quote->display_customer_items_discount) 3 @else 2 @endif" class="cell-warning"></th>
				<th class="text-right" colspan="@if($quote->display_customer_items_discount) 2 @else 1 @endif">
					TOTAL  (<span>{{ $quote->currency_short_name }}</span>):
				</th>
				<th class="text-right">
					${{ number_format($quote->total_on_quote, 2) }}
				</th>
			</tr>
		</tbody>
	</table>
	<table class="table table-bordered small table-sm">
		<thead>
			<tr class="cell-gray">
				<th class="text-uppercase">Observations</th>
			</tr>
		</thead>
		<tbody>
			<tr class="text-uppercase">
				<td>{!! $quote->observations !!}</td>
			</tr>
		</tbody>
	</table>
	<table class="table table-bordered small table-sm">
		<tbody>
			<tr>
				<th width="12%" class="cell-gray text-right">TERMS:</th>
				<td class="text-left text-uppercase">{{ $quote->terms }}</td>
			</tr>
			<tr>
				<th width="12%" class="cell-gray text-right">DELIVERY:</th>
				<td class="text-left text-uppercase">{{ $quote->delivery_time }}</td>
			</tr>
			<tr>
				<th width="12%" class="cell-gray text-right">F.O.B.:</th>
				<td class="text-left text-uppercase">{{ $quote->fob }}</td>
			</tr>
			<tr>
				<th width="12%" class="cell-gray text-right">PRICES:</th>
				<td class="text-left text-uppercase">
					ALL PRICES ARE IN 
					<span class="font-weight-bold">
						<span>{{ $quote->currency_short_name }}</span>
					</span>
					AND VALID FOR 
					<span class="font-weight-bold">
						<span>{{ $quote->vigency }}</span> DAYS
					</span>
				</td>
			</tr>
		</tbody>
	</table>
	<hr>
	<p class="small">DYNACOM- FPF-02 REV 2.3 NOV 1999 AAJR.</p>
	<p class="text-danger small m-0">
		<b>CONFIDENTIAL DYNAMIC COMMUNICATIONS PROPRIETARY</b>
	</p>
</div>