@extends('dbpi.comercial.layout')
@section('module_title')
	{{'Cotización #' . $quote->number}}
	@if($quote->copy)
	<span class="text-danger small ml-3"> (Copia de <a href="{{ route('cotizaciones.show', $quote->copy) }}" class="text-danger">#{{ $quote->copy->number }}</a>) </span>
	@endif
@stop
@section('module_actions')
	<div class="d-flex ml-auto">
		@role('admin')
			<delete-quote-btn class="mr-2" :quoteid="{{ $quote->id }}">
			</delete-quote-btn>
		@endrole
		<quote-status-select class="mr-2"></quote-status-select>
		<refresh-quote-crm-data class="mr-2"></refresh-quote-crm-data>
		<request-internal-validation class="mr-2"></request-internal-validation>
		<clone-quote-btn class="mr-2"></clone-quote-btn>
		<generate-fup class="mr-2"></generate-fup>
		<div class="dropdown">
			<button class="btn btn-secondary dropdown-toggle" type="button" id="exportQuoteAndMarginDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-download"></i>
			</button>
			<div class="dropdown-menu dropdown-menu-right shadow" aria-labelledby="exportQuoteAndMarginDropdown">
				<h6 class="dropdown-header">¿Que deseas descargar?</h6>
				<div class="dropdown-divider"></div>
				<form action="{{ route('export-quote-to-pdf', $quote) }}" method="post" target="_blank">
					@csrf
					<input type="hidden" name="new_format">
					<button type="submit" class="dropdown-item text-capitalize" href="#">
						<i class="fa fa-file fa-fw mr-2 text-muted"></i>
						Cotización
					</button>
				</form>
				<form action="{{ route('export-margin-to-pdf', $quote) }}" method="post" target="_blank">
					@csrf
					<button type="submit" class="dropdown-item text-capitalize" href="#">
						<i class="fa fa-file fa-fw mr-2 text-muted"></i>
						Margen
					</button>
				</form>
			</div>
		</div>
	</div>
@stop
@section('module_content')
	<quote 
		quote_id="{{ $quote->id }}" 
		day="'{{ \Carbon\Carbon::now()->format('d') }}'"
		month="'{{ \Carbon\Carbon::now()->format('m') }}'"
		year="'{{ \Carbon\Carbon::now()->format('Y') }}'"
		responsable="{{ $quote->author->full_name }}">
	</quote>
@stop