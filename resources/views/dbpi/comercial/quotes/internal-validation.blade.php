@extends('layouts.guest')

@section('sidebar')
	@if($validation->status_id < 2)
		<p class="lead text-white mb-5">
			<b>{{ $user->full_name }}</b>, se ha solicitado tu autorización para envío de cotización al cliente.
		</p>

		@if($quote->status_id <= 2)
			<internal-validation-btns :quote="{{ $quote }}" :user="{{ $user }}" :validation="{{ $validation }}">
				
			</internal-validation-btns>
		@else
			<b class="text-white">Nota:</b> 
			<i><b class="text-white">Esta cotización ya ha sido enviada al cliente</b></i>
		@endif
	@else
		<div class="text-center">
			<i class="fa fa-check fa-2x text-success mb-3"></i>
			<p class="text-white lead">Gracias por tu autorización!</p>
		</div>
	@endif
@stop

@section('content')
	<h1 class="h4 mb-4">Cotización <span class="text-danger">#{{ $quote->number }}</span></h1>

	<div class="card-group shadow-sm mb-4 text-center">
		<div class="card card-body">
			<h3 class="m-0">
				Margen: <span class="@if($quote->margin_percent > 30) text-success @else text-danger @endif">{{ $quote->margin_percent }}%</span>
			</h3>
		</div>
		<div class="card card-body">
			<h3 class="m-0">Margen: ${{ number_format($quote->margin_cost, 2) }} USD</h3>
		</div>
	</div>

	<div class="card card-body mb-3">
		@component('dbpi.comercial.quotes.components.quote-preview', [
			'quote' => $quote
		])
		@endcomponent
	</div>

	<div class="card card-body">
		@component('dbpi.comercial.quotes.components.margin-preview', [
			'quote' => $quote
		])
		@endcomponent
	</div>
@stop