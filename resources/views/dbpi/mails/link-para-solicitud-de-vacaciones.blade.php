@component('mail::message')
# HOLA {{ $empleado->full_name }}

¿Deseas solicitar tus vacaciones?, por favor haz click en el siguiente enlace para llenar tu solicitud.

@component('mail::button', ['url' => $link])
Solicitar vacaciones
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
