@component('mail::message')
# Recordatorio!

@if(count($mantenimientos) == 1)

Te recordamos que tienes {{ count($mantenimientos) }} servicio agendado para hoy.

@else

Te recordamos que tienes {{ count($mantenimientos) }} servicios agendados para hoy.

@endif

Lista de servicios para hoy: <br />

@foreach($mantenimientos as $mantenimiento)
	
<b>Cliente:</b> {{ $mantenimiento->nombre_de_cliente  }} <br />
<b>Servicio:</b> {{ $mantenimiento->tipo_de_servicio  }} <br />
<b>Fecha:</b> {{ $mantenimiento->fecha->format('d/m/Y h:i a') }}

@if(!$loop->last)
<hr>
@endif

@endforeach

@component('mail::button', ['url' => route('login')])
Ver calendario de servicios
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
