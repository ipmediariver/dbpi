@component('mail::message')
# Hola {{ $author->full_name }}

La cotización #{{ $quote->number }} fue autorizada por {{ $validator->full_name }}.

@component('mail::button', ['url' => route('cotizaciones.show', $quote)])
Ver cotización
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
