@component('mail::message')
# Hola {{ $director->full_name }}

{{ $user->full_name }} ha solicitado la cancelación de la FUP #{{ $fup->number }}

El motivo es el siguiente:

"{{ $reasson }}"

@component('mail::button', ['url' => $link])
	Ver FUP
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
