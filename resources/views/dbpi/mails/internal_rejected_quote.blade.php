@component('mail::message')
# Hola, {{ $author->full_name }}

<p>La cotización ha sido rechazada internamente por {{ $validator->full_name }}.</p>

@if($comment)
<p><b>Comentarios adicionales:</b></p>
<p>{{ $comment }}</p>
@endif

@component('mail::button', ['url' => route('cotizaciones.show', $quote)])
Ver cotización
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
