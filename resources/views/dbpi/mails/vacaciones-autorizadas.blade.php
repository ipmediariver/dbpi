@component('mail::message')
# Vacaciones autorizadas

Las vacaciones de {{ $empleado->full_name }} fueron autorizadas por {{ $autorizacion->director->full_name }}

@component('mail::button', ['url' => $link])
Ver perfil del empleado
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
