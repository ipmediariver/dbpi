@component('mail::message')
# Hola, {{ $user->first_name }}

Se ha solicitado tu autorización en la FUP {{ '#' . $fup->number }}.

Ingresa a DBPI para más detalles.

@component('mail::button', ['url' => $link])
	Ver FUP
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
