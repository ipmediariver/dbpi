@component('mail::message')
# Solicitud de vacaciones

{{ $empleado->full_name }} ha solicitado sus vacaciones y esta en espera de autorización.

@component('mail::button', ['url' => $link])
Ver solicitud de vacaciones
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
