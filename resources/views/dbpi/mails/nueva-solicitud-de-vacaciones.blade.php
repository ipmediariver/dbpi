@component('mail::message')
# Solicitud de vacaciones

{{ $empleado->full_name }} ha solicitado sus vacaciones

@component('mail::button', ['url' => $link])
Ver solicitud
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
