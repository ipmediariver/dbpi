@component('mail::message')
# Hola, {{ $user->full_name }}

Se solicita tu autorización en la cotización #{{$quote->number}}.

@component('mail::button', ['url' => $link])
Ver cotización
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
