@component('mail::message')
# Recordatorio!

@if(count($mantenimientos) == 1)

Te recordamos que tienes {{ count($mantenimientos) }} servicio agendado dentro de los próximos 30 días.

@else

Te recordamos que tienes {{ count($mantenimientos) }} servicios agendados dentro de los próximos 30 días.

@endif

Lista de próximos servicios: <br />

@foreach($mantenimientos as $mantenimiento)
	
<b>Cliente:</b> {{ $mantenimiento->nombre_de_cliente  }} <br />
<b>Servicio:</b> {{ $mantenimiento->tipo_de_servicio  }} <br />
<b>Fecha:</b> {{ $mantenimiento->fecha->format('d/m/Y h:i a') }}

@if(!$loop->last)
<hr>
@endif

@endforeach

@component('mail::button', ['url' => route('login')])
Ver calendario de servicios
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
