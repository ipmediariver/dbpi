@component('mail::message')
# Hola, {{ $user->first_name }}

Tu solicitud de cancelacion de la FUP #{{ $fup->number }} ha sido autorizada y se canceló automaticamente.

@component('mail::button', ['url' => $link])
	Ver FUP
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
