<table border="1">
    <thead>
        <tr>
            <th style="vertical-align: center; height: 45px; background-color: #ffffff;">
                <img src="{{ public_path('img/logo.png') }}" width="270">
            </th>
            <th style="background-color: #ffffff;"></th>
            <th colspan="10" style="background-color: #ffffff; height: 45px; font-size: 14px; vertical-align: center;">
                {{ $payroll->account->name }} | Nómina del {{ $payroll->date->from->format('d-m-Y') .' a '. $payroll->date->to->format('d-m-Y') }}
            </th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd;"><b>Ciudad</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd;"><b>Nombre de Empleado</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: center;"><b>Banco</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>No. de Cuenta</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>No. de Tarjeta</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>Clabe Interbancaria</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>Importe Bruto</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>Otros Ingresos</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>Fonacot</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>Infonavit</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>Otros Descuentos</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>COVID-19</b></th>
            <th style="height: 30px; vertical-align: center; background-color: #dddddd; text-align: right;"><b>Total a Depositar</b></th>
        </tr>
    </thead>
    <!-- EMPLEADOS -->
    <tbody>
        @php
            $groups = $payroll->employee_payrolls->groupBy('employee.department.name');
        @endphp
        @foreach($groups as $key => $group)
            <tr>
                <th colspan="13" style="background-color: #777777; color: #ffffff"><b>{{ $key }}</b></th>
            </tr>
            @php
                $locations = $group->groupBy('employee.location.name');
            @endphp
            @foreach($locations as $location_key => $location)
                @foreach($location->sortBy('employee.last_name_1') as $employee_payroll)
                    <tr>
                        <td>{{ $location_key }}</td>
                        <td style="@if($employee_payroll->employee->assimilated) background-color: #e8e9f6 @endif">
                            {{ $employee_payroll->employee->full_name_reverse }}
                            @if($employee_payroll->employee->assimilated)
                                <br>
                                (Asimilado)
                            @endif           
                        </td>
                        @if($employee_payroll->bank)
                        <td style="text-align: center; color: #ffffff; background-color: {{ $employee_payroll->bank->color  }}">
                            {{ $employee_payroll->bank->name }}
                        </td>
                        @else
                        <td></td>
                        @endif
                        <td style="text-align: right">
                            @if($employee_payroll->bank_account_num)
                                {{ '="' . decrypt($employee_payroll->bank_account_num) . '"' }}
                            @endif
                        </td>
                        <td style="text-align: right">
                            @if($employee_payroll->bank_card_num)
                                {{ '="'. decrypt($employee_payroll->bank_card_num) .'"' }}
                            @endif
                        </td>
                        <td style="text-align: right">
                            @if($employee_payroll->interbank_clabe)
                                {{ '="'. decrypt($employee_payroll->interbank_clabe) .'"' }}
                            @endif
                        </td>
                        <td style="text-align: right; background-color: #fffce3">${{ number_format($employee_payroll->fortnight_salary, 2) }}</td>
                        <td style="text-align: right; background-color: #e7f8ee">${{ number_format($employee_payroll->entries_ammount, 2) }}</td>
                        <td style="text-align: right; background-color: #e8e9f6">${{ number_format($employee_payroll->fonacot, 2) }}</td>
                        <td style="text-align: right; background-color: #e8e9f6">${{ number_format($employee_payroll->infonavit, 2) }}</td>
                        <td style="text-align: right; background-color: #fbe3e3">${{ number_format($employee_payroll->simple_discounts, 2) }}</td>
                        <td style="text-align: right; background-color: #fbe3e3">${{ number_format($employee_payroll->discounts_by_days, 2) }}</td>
                        <td style="text-align: right; background-color: #fffce3">${{ number_format($employee_payroll->total_ammount, 2) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <th style="background-color: #e8e9f6;" colspan="6">
                        Subtotales de {{ $location_key }}
                    </th>
                    <th style="background-color: #e8e9f6; text-align: right;"><b>{{ number_format($location->sum('fortnight_salary'), 2) }}</b></th>
                    <th style="background-color: #e8e9f6; text-align: right;"><b>{{ number_format($location->sum('entries_ammount'), 2) }}</b></th>
                    <th style="background-color: #e8e9f6; text-align: right;"><b>{{ number_format($location->sum('fonacot_discount'), 2) }}</b></th>
                    <th style="background-color: #e8e9f6; text-align: right;"><b>{{ number_format($location->sum('infonavit_discount'), 2) }}</b></th>
                    <th style="background-color: #e8e9f6; text-align: right;"><b>{{ number_format($location->sum('simple_discounts'), 2) }}</b></th>
                    <th style="background-color: #e8e9f6; text-align: right;"><b>{{ number_format($location->sum('discounts_by_days'), 2) }}</b></th>
                    <th style="background-color: #e8e9f6; text-align: right;"><b>{{ number_format($location->sum('total_ammount'), 2) }}</b></th>
                </tr>
            @endforeach
            <tr>
                <th style="background-color: #444444; color: #ffffff;" colspan="6">
                    Subtotales de {{ $key }}
                </th>
                <th style="background-color: #444444; color: #ffffff; text-align: right;"><b>{{ number_format($group->sum('fortnight_salary'), 2) }}</b></th>
                <th style="background-color: #444444; color: #ffffff; text-align: right;"><b>{{ number_format($group->sum('entries_ammount'), 2) }}</b></th>
                <th style="background-color: #444444; color: #ffffff; text-align: right;"><b>{{ number_format($group->sum('fonacot_discount'), 2) }}</b></th>
                <th style="background-color: #444444; color: #ffffff; text-align: right;"><b>{{ number_format($group->sum('infonavit_discount'), 2) }}</b></th>
                <th style="background-color: #444444; color: #ffffff; text-align: right;"><b>{{ number_format($group->sum('simple_discounts'), 2) }}</b></th>
                <th style="background-color: #444444; color: #ffffff; text-align: right;"><b>{{ number_format($group->sum('discounts_by_days'), 2) }}</b></th>
                <th style="background-color: #444444; color: #ffffff; text-align: right;"><b>{{ number_format($group->sum('total_ammount'), 2) }}</b></th>
            </tr>
        @endforeach
    </tbody>
    <!-- TOTALES -->
    <tbody>
        <tr>
            <td></td>
            <td style="height: 30px; vertical-align: center; text-align: right;"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <th style="height: 30px; vertical-align: center; text-align: right; background-color: #fffce3">
                <b>${{ number_format($payroll->fortnight_total, 2) }}</b>
            </th>
            <th style="height: 30px; vertical-align: center; text-align: right; background-color: #e7f8ee">
                <b>${{ number_format($payroll->entries_total, 2) }}</b>
            </th>
            <th style="height: 30px; vertical-align: center; text-align: right; background-color: #e8e9f6">
                <b>${{ number_format($payroll->fonacot_total, 2) }}</b>
            </th>
            <th style="height: 30px; vertical-align: center; text-align: right; background-color: #e8e9f6">
                <b>${{ number_format($payroll->infonavit_total, 2) }}</b>
            </th>
            <th style="height: 30px; vertical-align: center; text-align: right; background-color: #fbe3e3">
                <b>${{ number_format($payroll->simple_discounts_total, 2) }}</b>
            </th>
            <th style="height: 30px; vertical-align: center; text-align: right; background-color: #fbe3e3">
                <b>${{ number_format($payroll->discounts_by_days_total, 2) }}</b>
            </th>
            <th style="height: 30px; vertical-align: center; text-align: right; background-color: #fffce3">
                <b>${{ number_format($payroll->total, 2) }}</b>
            </th>
        </tr>
    </tbody>
</table>