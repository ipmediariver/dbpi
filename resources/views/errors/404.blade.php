@extends('layouts.error')
@section('content')
	<h1>ERROR 404</h1>
	<p><b>La página que intentas buscar no existe, por favor regresa al inicio haciendo <a href="{{ route('apps') }}">click aquí</a></b></p>
@stop