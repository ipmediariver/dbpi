@extends('layouts.error')
@section('content')
	<h1>Aplicación en mantenimiento</h1>
	<p><b>Estamos trabajando en el mantenimiento de DBPI, por favor regresa más tarde.</b></p>
@stop