<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Cotización #{{ $quote->number }}</title>
		<style>
			html {
			    padding: 0;
			    margin: 0;
			}
			* {
			    text-transform: uppercase;
			}
			.nobreak {
			    page-break-inside: avoid;
			}
			body {
			    font-size: 9px;
			    padding: 135px 60px 100px 60px;
			    margin: 0;
			    font-family: DejaVu Sans, sans-serif;
			    border-collapse: collapse !important;
			}
			#header {
			    position: fixed;
			    top: 38px;
			    left: 60px;
			    right: 60px;
			    padding: 0px;
			    z-index: 1;
			}
			#footer {
			    position: fixed;
			    bottom: 100px;
			    left: 60px;
			    right: 60px;
			    padding: 0;
			    z-index: 1;
			}
			#watermark {
			    position: fixed;
			    top: 350px;
			    left: 210px;
			    z-index: 0;
			}
			#sidebarLeft {
			    position: fixed;
			    top: 0px;
			    left: -25px;
			    z-index: 0;
			}
			#sidebarRight {
			    position: fixed;
			    top: 0px;
			    left: 720px;
			    z-index: 0;
			}
			#quote_content{
				position: relative;
				z-index: 2;
			}
			#page {
			    position: fixed;
			    bottom: 60px;
			    right: 60px;
			    text-align: right;
			    padding: 0 0 0 0;
			}
			.lead {
			    font-size: 12px;
			}
			.cell-gray {
			    background-color: #f2f2f2;
			}
			.cell-warning {
			    background-color: #ffffff;
			}
			.text-uppercase {
			    text-transform: uppercase;
			}
			table {
			    width: 100%;
			    border-collapse: collapse;
			}
			table tr th, table tr td {
			    text-align: left;
			    border-collapse: collapse;
			    vertical-align: top;
			    height: 10px;
			}
			.text-center {
			    text-align: center;
			}
			.text-left {
			    text-align: left;
			}
			.text-right {
			    text-align: right;
			}
			.text-danger {
			    color: #BA2025;
			}
			.account_logo {
			    margin: 10px 0;
			}
			.mb-3 {
			    margin-bottom: 15px;
			}
			.page-number:before {
			    content: counter(page);
			    font-size: 11px;
			    font-weight: bold;
			}
			.quote_product_thumb {
			    width: 130px;
			    height: auto;
			}
		</style>
	</head>
	<body>
		<!-- Logo y direccion -->
		<div id="header">
			<img src="{{ public_path('/img/quote/header.png') }}" width="696" alt="">
		</div>
		<!-- Footer -->
		<div id="footer">
			<img src="{{ public_path('/img/quote/footer.png') }}" width="696" alt="">
		</div>
		<!-- Watermark -->
		<div id="watermark">
			<img src="{{ public_path('/img/quote/watermark.png') }}" width="400" alt="">
		</div>
		<!-- Sidebar left -->
		<div id="sidebarLeft">
			<img src="{{ public_path('/img/quote/sidebar_left.jpg') }}" height="1100" alt="">
		</div>
		<!-- Sidebar right -->
		<div id="sidebarRight">
			<img src="{{ public_path('/img/quote/sidebar_right.jpg') }}" height="1100" alt="">
		</div>
		<div id="quote_content">
			@yield('content')
		</div>
	</body>
</html>