@include('components.header')
<div class="d-flex align-items-center" style="height: 100vh;">
	<div class="w-50 bg-dark d-flex align-items-center justify-content-center h-100">
		<div class="row">
			<div class="col-sm-8 mx-auto d-flex justify-content-center align-items-center">
				<img src="{{ asset('img/svg/logo_dynacom_white.svg') }}" 
					alt=""
					style="width: 500px; height: auto;">
			</div>
		</div>
	</div>
	<div class="w-50 h-100 d-flex align-items-center justify-content-center">
		<div class="w-100 p-5">
		    <div class="row">
		        <div class="col-sm-8 mx-auto">
		        	@component('components.flashes')

		        	@endcomponent
		        	
		        	@yield('content')
		        </div>
		    </div>
		</div>
	</div>
</div>
@include('components.footer')