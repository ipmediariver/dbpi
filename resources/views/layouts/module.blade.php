@extends('layouts.app')
@section('content')
<aside v-cloak class="module_sidebar shadow-sm" :class="module_sidebar ? '' : 'hidden'">
	<a href="#" class="toggle_module_sidebar" @click.prevent="toggle_module_sidebar">
		<i class="fa fa-arrows-alt-h"></i>
	</a>
	<div class="text-center mb-4">
		<img src="{{ asset('img/svg/logo_dynacom_white.svg') }}" class="logo" alt="">
	</div>
	@yield('module_sidebar')
</aside>
<main v-cloak class="module_content" :class="module_sidebar ? '' : 'hidden'">
	<div class="d-flex align-items-center mb-4">
		<div>
			@component('components.breadcrumbs')
			@endcomponent
		</div>
		<div class="ml-auto d-flex align-items-center">
			@empty($remove_notifications)
			<div class="dropdown">
				<button class="btn btn-link p-0 dropdown-toggle font-weight-bold" 
					style="text-decoration: none" 
					type="button" 
					id="notificationsDropdown" 
					data-toggle="dropdown" 
					aria-haspopup="true" 
					aria-expanded="false">
					@if(auth()->user()->unreadNotifications->count())
					<span class="badge badge-pill badge-danger mr-2" style="font-weight: bold !important;">{{ auth()->user()->unreadNotifications->count() }}</span>
					@endif
					Notificaciones
				</button>
				<div class="dropdown-menu dropdown-menu-right shadow" aria-labelledby="notificationsDropdown">
					@if(auth()->user()->unreadNotifications->count())
						<h6 class="dropdown-header">Últimas notificaciones</h6>
						@foreach(auth()->user()->unreadNotifications->take(5) as $notification)
							<a class="dropdown-item" href="{{route('mark_notification_as_read', $notification->id)}}">
								{{ \Str::limit($notification->data['msg'], 75) }}
							</a>
						@endforeach
					@else
						<h6 class="dropdown-header">No tienes notificaciones.</h6>
					@endif
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="{{ route('notifications') }}">Ver todas mis notificaciones</a>
				</div>
			</div>
			@endempty
			<p class="m-0 font-weight-bold ml-4">Hola {{ auth()->user()->first_name }}!</p>
		</div>
	</div>
	<div class="module_title border-bottom pb-3 mb-3 d-flex align-items-center">
		<p class="lead m-0 d-flex align-items-center">@yield('module_title', 'DBPI')</p>
		<div class="ml-auto">
			@yield('module_actions')
		</div>
	</div>
	@include('components.flashes')
	@yield('module_content')
	<footer class="border-top py-5 mt-4">
		©{{ date('Y') }} {{ config('app.name') }}
	</footer>
</main>
@stop