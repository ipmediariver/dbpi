@include('components.header')
<div class="guest-full-content">
	<div class="guest-content">
		@yield('content')
	</div>
	<div class="guest-sidebar">
		@yield('sidebar')
	</div>
</div>
@include('components.footer')