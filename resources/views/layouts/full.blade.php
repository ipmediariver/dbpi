@extends('layouts.app')
@section('content')
<div class="full_content">
	@include('components.flashes')
	@yield('full_content')
</div>
@stop