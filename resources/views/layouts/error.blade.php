@include('components.header')
<div class="guest-full-content d-flex align-items-center" style="width: 100vw; height: 100vh">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 mx-auto text-center">
				<img src="{{ asset('img/png/logo_dynacom.png') }}" style="height: 45px" alt="">
				<div class="mt-5">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</div>
@include('components.footer')