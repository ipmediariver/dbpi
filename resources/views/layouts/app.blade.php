@include('components.header')
@include('components.sidebar') 
<section class="main_content">
	@yield('content')
</section>
@include('components.footer')