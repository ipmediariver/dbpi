@extends('layouts.auth')
@section('content')
<form method="POST" action="{{ route('login') }}"
    onsubmit="submit_btn.disabled = true; return true;">
    @csrf
    @component('components.form-header', [
        'description' => 'Por favor introduce tus credenciales para ingresar'
    ])
    @endcomponent
    <div class="form-group">
        <label for="username" 
            class="text-md-right"
            required>
            Usuario:
        </label>
        <input id="username" 
            type="text" 
            class="form-control @error('username') is-invalid @enderror" 
            name="username" 
            value="{{ old('username') }}" 
            required 
            autocomplete="username" 
            autofocus>
        @error('username')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="password" 
            class="text-md-right"
            required>
            Contraseña:
        </label>
        <input id="password" 
            type="password" 
            class="form-control @error('password') is-invalid @enderror" 
            name="password" 
            required 
            autocomplete="current-password">
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label" for="remember">
                {{ __('Recordar cuenta') }}
            </label>
        </div>
    </div>
    <div class="form-group mb-0">
        <button type="submit" class="btn btn-primary" name="submit_btn">
            <i class="fa fa-check fa-sm mr-2"></i>
            Ingresar
        </button>
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('¿Olivdaste tu contraseña?') }}
            </a>
        @endif
    </div>
</form>
@endsection