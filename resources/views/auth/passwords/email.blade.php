@extends('layouts.auth')
@section('content')
<form method="POST" action="{{ route('password.email') }}">
    @csrf
    @component('components.form-header', [
        'description' => 'Ingresa tu correo electrónico'
    ])
    @endcomponent
    <div class="form-group">
        <label for="email" 
            class="text-md-right"
            required>
            Correo electrónico:
        </label>
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group mb-0 mb-3">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-envelope fa-sm mr-2"></i>
            Enviar link de restauración
        </button>
    </div>
    <a href="{{ route('login') }}">
        <i class="fa fa-angle-left fa-sm mr-1"></i>
        Regresar a inicio de sesión
    </a>
</form>
@endsection