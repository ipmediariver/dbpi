@extends('layouts.auth')
@section('content')
<form method="POST" action="{{ route('password.confirm') }}">
    @csrf
    @component('components.form-header', [
        'description' => 'Por favor confirma tu contraseña para continuar.'
    ])
    @endcomponent
    <div class="form-group">
        <label for="password">{{ __('Contraseña') }}</label>
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="form-group mb-0">
        <button type="submit" class="btn btn-primary">
            {{ __('Confirmar contraseña') }}
        </button>

        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('¿Olvidaste tu contraseña?') }}
            </a>
        @endif
    </div>
</form>
@endsection
